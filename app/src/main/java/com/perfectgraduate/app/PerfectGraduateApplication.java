package com.perfectgraduate.app;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Application;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.FacebookSdk;
import com.perfectgraduate.R;
import com.perfectgraduate.listener.ICallBack;
import com.perfectgraduate.vo.DisciplineVo;
import com.perfectgraduate.vo.DisciplinesVo;
import com.perfectgraduate.vo.MessagesVo;
import com.perfectgraduate.vo.UserCompanyVo;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by user on 07-Dec-16.
 */

public class PerfectGraduateApplication extends Application {

    public static PerfectGraduateApplication application;
    private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})";

    private UserCompanyVo userCompanyVo;
    private MessagesVo messagesVo;
    private DisciplineVo disciplineVo;
    @Override
    public void onCreate() {
        super.onCreate();
        application = this;
        FacebookSdk.sdkInitialize(getApplicationContext());
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.perfectgraduate", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.e("KeyHash:",  Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

    }

    public static synchronized PerfectGraduateApplication getInstance() {
        return application;
    }

    public ProgressDialog getProgressDialog(Activity activity) {
        ProgressDialog progressDialog = ProgressDialog.show(activity, "",
                "Please wait...");
        progressDialog.setCancelable(false);
        return progressDialog;
    }

    public boolean isEmailValid(CharSequence input) {
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(input);
        return (!matcher.matches());
    }

    public boolean isEmailIdValid(String email) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);

        if (matcher.matches())
            return true;
        else
            return false;
    }

    public boolean isConnectInternet() {
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }

    public void setHideSoftKeyboard(View view) {
        InputMethodManager mInputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        mInputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public void setShowSoftKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }

    public void displayToast(String message) {
        Toast toast = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT);
       // toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    public String parseDateToddMMyyyy(String time) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "dd/MM/yy, HH:mm a";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern, Locale.US);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public AlertDialog setShowDialog(Activity activity, String title,
                                     String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        if (!title.equalsIgnoreCase("")) {
            builder.setTitle(title);


        }
        builder.setMessage(message);

        builder.setCancelable(false);
        builder.setPositiveButton(android.R.string.ok,
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();

       /* TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
        msgTxt.setTextSize(16);
        msgTxt.setGravity(Gravity.CENTER);*/

        return alertDialog;
    }

    public AlertDialog setShowDialogOKCallBack(Context activity, String title,
                                               String message, final ICallBack iCallBack, String btnPos) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton(btnPos, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                iCallBack.onCallBackResult(null);
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        return alertDialog;
    }

    public AlertDialog setShowDialogPosBntCallbackNegBtn(Activity activity,
                                                         String title, String message, final ICallBack iCallBackPosBtn,
                                                         String btnPos, String btnNeg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton(btnPos, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                iCallBackPosBtn.onCallBackResult(null);
            }
        });
        builder.setNegativeButton(btnNeg, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        return alertDialog;
    }

    public Dialog displayCustomDialog(Activity activity,String title,String message){
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.viewname);
        dialog.setCancelable(false);
        Window window = dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        window.setGravity(Gravity.CENTER);

        TextView textView_title = (TextView) dialog.findViewById(R.id.textView_title);
        textView_title.setText(title);

        TextView text = (TextView) dialog.findViewById(R.id.textView_msg);
        text.setText(message);


        TextView dialogButton = (TextView) dialog.findViewById(R.id.dialogButtonOK);
        // if button is clicked, close the custom dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();

        return dialog;
    }

    public Dialog displayCustomDialogPostNeg(Activity activity,String title,String message,final ICallBack iCallBackPosBtn,
                                             String postBtn,String negBtn){
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_twobtns);
        dialog.setCancelable(false);
        Window window = dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        window.setGravity(Gravity.CENTER);

        TextView textView_title = (TextView) dialog.findViewById(R.id.textView_title);
        textView_title.setText(title);

        TextView text = (TextView) dialog.findViewById(R.id.textView_msg);
        text.setText(message);


        TextView dialogNegativeButton = (TextView) dialog.findViewById(R.id.dialogNegativeButton);

        if(!negBtn.equals("")){

            dialogNegativeButton.setText(negBtn);
        }


        // if button is clicked, close the custom dialog
        dialogNegativeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        TextView dialogPositiveButton = (TextView) dialog.findViewById(R.id.dialogPositiveButton);

        if(!postBtn.equals("")){

            dialogPositiveButton.setText(postBtn);
        }

        // if button is clicked, close the custom dialog
        dialogPositiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                iCallBackPosBtn.onCallBackResult(null);
            }
        });

        dialog.show();

        return dialog;
    }

    public Dialog displayDialogOKCallBack(Context activity, String title,
                                               String message, final ICallBack iCallBack) {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_okbtn);
        dialog.setCancelable(false);
        Window window = dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        window.setGravity(Gravity.CENTER);

        TextView text = (TextView) dialog.findViewById(R.id.textView_msg);
        text.setText(message);


        TextView dialogPositiveButton = (TextView) dialog.findViewById(R.id.dialogButtonOK);
        // if button is clicked, close the custom dialog
        dialogPositiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                iCallBack.onCallBackResult(null);
            }
        });

        dialog.show();

        return dialog;
    }


    public String getDeviceId() {
        return Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
    }


    public UserCompanyVo getUserCompanyVo() {
        return userCompanyVo;
    }

    public void setUserCompanyVo(UserCompanyVo userCompanyVo) {
        this.userCompanyVo = userCompanyVo;
    }

    public MessagesVo getMessagesVo() {
        return messagesVo;
    }

    public void setMessagesVo(MessagesVo messagesVo) {
        this.messagesVo = messagesVo;
    }

    public DisciplineVo getDisciplineVo() {
        return disciplineVo;
    }

    public void setDisciplineVo(DisciplineVo disciplineVo) {
        this.disciplineVo = disciplineVo;
    }
}
