package com.perfectgraduate.api;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.perfectgraduate.R;
import com.perfectgraduate.app.PerfectGraduateApplication;
import com.perfectgraduate.listener.ICallBack;

import org.json.JSONObject;

import java.util.Map;

/**
 * Created by user on 12-Dec-16.
 */

public class PerfectGraduateRequest {

    Context context;

    public PerfectGraduateRequest(Context context) {
        this.context = context;
    }

    public void getResponse(final Activity context, Map<String, Object> parms, String Url,boolean isProgress, final ICallBack iCallBack){


        AQuery aQuery = new AQuery(context);

        System.out.println("Url : "+Url);
        System.out.println("parameter : "+parms);

        if(isProgress) {
            aQuery.progress(PerfectGraduateApplication.getInstance().getProgressDialog(context));
        }

        aQuery.ajax(Url,parms, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                System.out.println("response : "+json);
                int responseCode = status.getCode();

                if (responseCode == 200) {
                    iCallBack.onCallBackResult(json.toString());
                }else{
                    PerfectGraduateApplication.getInstance().setShowDialog(context,"",context.getString(R.string.dialog_error_message));
                }
            }
        });
    }


}
