package com.perfectgraduate.api;

/**
 * Created by user on 6/20/2016.
 */

public interface WebServiceAPI
{
    String BASE_URL = "http://www.perfectgraduate.com/perfect/webservices/web_service/";

    String API_GOOGLE = "https://maps.googleapis.com/maps/api/geocode/json?";

    String API_register_fb = BASE_URL + "register_fb";
    String API_getAllCountries = BASE_URL + "getAllCountries";
    String API_getAllsearchCountries = BASE_URL + "getAllsearchCountries";
    String API_getAllStates = BASE_URL + "getAllStates";
    String API_getAllUniversities = BASE_URL + "getAllUniversities";
    String API_getposition = BASE_URL + "getposition";
    String API_getAlldisciplines = BASE_URL + "getAlldisciplines";
    String API_DesciplineFilter = BASE_URL + "DesciplineFilter";
    String API_getAllSubdisciplines = BASE_URL + "getAllSubdisciplines";
    String API_getAllStudentSocieties = BASE_URL + "getAllStudentSocieties";
    String API_UpdateStudent = BASE_URL + "UpdateStudent";
    String API_UpStudent = BASE_URL + "UpStudent";
    String API_addAllSociety = BASE_URL + "addAllSociety";
    String API_logout = BASE_URL + "logout";
    String API_getProfile = BASE_URL + "getProfile";
    String API_getAllSalaries = BASE_URL + "getAllSalaries";
    String API_societyDetail = BASE_URL + "societyDetail";
    String API_user_society = BASE_URL + "user_society";
    String API_discpline_company_search = BASE_URL + "discpline_company_search";
    String API_addportfolio = BASE_URL + "addportfolio";
    String API_removeCompany = BASE_URL + "removeCompany";
    String API_read_message = BASE_URL + "read_message";
    String API_user_company = BASE_URL + "user_company";
    String API_getAllCompanies = BASE_URL + "getAllCompanies";
    String API_getmessages = BASE_URL + "getmessages";
    String API_getcountrystates = BASE_URL + "getcountrystates";
    String API_tempStudent = BASE_URL + "tempStudent";
    String API_login = BASE_URL + "login";
    String API_forgotpass = BASE_URL + "forgotpass";
    String API_getAllS = BASE_URL + "getAllS";
    String API_getMessageCountForUser = BASE_URL + "getMessageCountForUser";
    String API_addstudentsociety = BASE_URL + "addstudentsociety";
    String API_MessageLikeDislike = BASE_URL + "MessageLikeDislike";
    String API_MessageStatussociety = BASE_URL + "MessageStatussociety";
    String API_MessageStatus = BASE_URL + "MessageStatus";
    String API_CheckSocial = BASE_URL + "CheckSocial";
    String API_requestEvent = BASE_URL + "requestEvent";
    String API_studentLinkedIn = BASE_URL + "studentLinkedIn";
    String API_companyFilter = BASE_URL + "companyFilter";


    String status = "status";
    String data = "data";

    //Table Name

    String Companies = "Companies";
    String CompanyApplicationDueDateAll  = "CompanyApplicationDueDateAll";
    String CompanyAwards = "CompanyAwards";
    String CompanyDisciplineSubdiscipline = "CompanyDisciplineSubdiscipline";
    String CompanyDisciplines = "CompanyDisciplines";
    String CompanyLinks = "CompanyLinks";
    String CompanyLocations  = "CompanyLocations";
    String CompanySubDisciplines  = "CompanySubDisciplines";
    String CompanyQuesAns  = "CompanyQuesAns";
    String CompanyFilter  = "CompanyFilter";

    String Portfolio = "Portfolio";
    String PortfolioApplicationDueDateAll  = "PortfolioApplicationDueDateAll ";
    String PortfolioAwards  = "PortfolioAwards ";
    String PortfolioDisciplines = "PortfolioDisciplines";
    String PortfolioLinks = "PortfolioLinks";
    String PortfolioLocations  = "PortfolioLocations";
    String PortfolioQuesAns  = "PortfolioQuesAns";
    String PortfolioSubDisciplines  = "PortfolioSubDisciplines";

    String States = "States";
    String Countries = "Countries";
    String Disciplines = "Disciplines";
    String SubDisciplines  = "SubDisciplines";
    String AdderDetail  = "AdderDetail";
    String Messages  = "Messages";
    String AllCompanys  = "AllCompanys";
    String DisciplinesFilter = "DisciplinesFilter";
    String SubDisciplinesFilter  = "SubDisciplinesFilter";
    String SocietyDetail  = "SocietyDetail";
    String Salary  = "Salary";


    //Companies table column

    String id = "id";
    String company_code = "company_code";
    String parent_id = "parent_id";
    String parent_type = "parent_type";
    String bussiness_no = "bussiness_no";
    String company_name = "company_name";
    String company_intro = "company_intro";
    String company_type = "company_type";
    String website = "website";
    String logo = "logo";
    String rating = "rating";
    String awards = "awards";
    String email = "email";
    String password = "password";
    String pass = "pass";
    String description = "description";
    String contact_name = "contact_name";
    String contact_email = "contact_email";
    String phone = "phone";
    String phonecode = "phonecode";
    String mobilecode = "mobilecode";
    String mobile = "mobile";
    String general_no = "general_no";
    String company_address = "company_address";
    String city = "city";
    String country_id = "country_id";
    String state_id = "state_id";
    String notes_CRM = "notes_CRM";
    String first_login_status = "first_login_status";
    String change_pass = "change_pass";
    String lock = "lock";
    String user_type = "user_type";
    String package_id = "package_id";
    String messaging_country = "messaging_country";
    String application_due_date = "application_due_date";
    String key = "key";
    String message_limit = "message_limit";
    String send_message = "send_message";
    String date = "date";
    String modified_date = "modified_date";
    String added_by = "added_by";
    String modified_by = "modified_by";
    String package_start = "package_start";
    String package_end = "package_end";
    String country = "country";
    String abr = "abr";
    String is_deleted = "is_deleted";
    String international = "international";
    String message_events = "message_events";
    String regstates = "regstates";
    String message_state = "message_state";
    String message_status = "message_status";
    String unread_message = "unread_message";
    String portfolio_status = "portfolio_status";
    String perfect_logo = "perfect_logo";
    String isRemoved = "isRemoved";

    String company_id = "company_id";
    String year = "year";

    String discipline_id = "discipline_id";
    String sub_discipline_id = "sub_discipline_id";
    String type = "type";
    String discipline = "discipline";
    String sub_discipline = "sub_discipline";

    String name = "name";
    String media_type = "media_type";
    String media_link = "media_link";
    String media_logo = "media_logo";
    String default_id = "default_id";

    String branch_name = "branch_name";
    String location = "location";
    String lat = "lat";
    String latitude = "latitude";
    String longitude = "long";
    String state_longitude = "longitude";
    String map_reference = "map_reference";

    String program_id = "program_id";
    String start_date = "start_date";
    String close_date = "close_date";
    String created = "created";
    String state = "state";
    String program = "program";

    String question = "question";
    String answer = "answer";

    String companys = "companys";


    String adder_id = "adder_id";
    String sub_adder_id = "sub_adder_id";
    String message_type = "message_type";
    String event_title = "event_title";
    String event_date = "event_date";
    String eventtimefrom = "eventtimefrom";
    String eventtimeto = "eventtimeto";
    String event_venue = "event_venue";
    String message = "message";
    String gender = "gender";
    String send_by = "send_by";
    String recall = "recall";
    String like_status = "like_status";
    String report = "report";
    String addedStatus = "addedStatus";
    String eventstatus = "eventstatus";
    String eventconfirmation = "eventconfirmation";
    String default_timezone = "default_timezone";
    String society_id = "society_id";
    String staff_id = "staff_id";
    String creator_name = "creator_name";
    String society_email = "society_email";
    String uni_id = "uni_id";
    String societylogo = "societylogo";
    String creator_mobile = "creator_mobile";
    String society_name = "society_name";
    String society_president = "society_president";
    String president_email = "president_email";
    String president_mobile = "president_mobile";
    String Treasurer_name = "Treasurer_name";
    String Treasurer_email = "Treasurer_email";
    String Treasure_mobile = "Treasure_mobile";
    String Secetery_name = "Secetery_name";
    String Secetery_email = "Secetery_email";
    String Secetery_mobile = "Secetery_mobile";
    String facebookemail = "facebookemail";
    String locks = "locks";
    String created_by = "created_by";
    String society_code = "society_code";


    String sector_id = "sector_id";
    String occupation_id = "occupation_id";
    String title = "title";
    String salary0to3 = "salary0to3";
    String salary3to5 = "salary3to5";
    String salary5to7 = "salary5to7";
    String smallMedium = "smallMedium";
    String large = "large";
    String occupation = "occupation";
    String sector = "sector";
}
