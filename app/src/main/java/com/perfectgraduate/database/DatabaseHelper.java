package com.perfectgraduate.database;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


import com.perfectgraduate.activity.HomeActivity;
import com.perfectgraduate.api.WebServiceAPI;
import com.perfectgraduate.utility.Constants;
import com.perfectgraduate.utility.SharedPrefrenceUtil;
import com.perfectgraduate.vo.AdderDetailVo;
import com.perfectgraduate.vo.AllAwardsVo;
import com.perfectgraduate.vo.AllCompanysVo;
import com.perfectgraduate.vo.ApplicationDueDateAllVo;
import com.perfectgraduate.vo.CountryVo;
import com.perfectgraduate.vo.DisciplineVo;
import com.perfectgraduate.vo.DisciplinesVo;
import com.perfectgraduate.vo.LinksVo;
import com.perfectgraduate.vo.LocationVo;
import com.perfectgraduate.vo.MessagesVo;
import com.perfectgraduate.vo.QuesAnsVo;
import com.perfectgraduate.vo.SalaryVo;
import com.perfectgraduate.vo.SocietyVo;
import com.perfectgraduate.vo.StateVo;
import com.perfectgraduate.vo.SubDisciplineVo;
import com.perfectgraduate.vo.Sub_disciplineVo;
import com.perfectgraduate.vo.UserCompanyVo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.WeakHashMap;

public class DatabaseHelper extends SQLiteOpenHelper {

	private static final int DATABASE_VERSION = 1;
	private static final String DATABASE_NAME = "perfectgraduate.sqlite";
	private final Context context;
	private SQLiteDatabase dataBase;
	private SQLiteDatabase databaseQuery;
	
	// Code to copy database from assets start
	@SuppressLint("SdCardPath")
	private static String DB_PATH = "/data/data/com.perfectgraduate/databases/";

	public DatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		 this.context = context;
	}



	public void createDataBase(int flag, String path) throws IOException {
		boolean dbExist = checkDataBase();
		if (!dbExist) {
			this.getWritableDatabase();
			try {
				if (flag == 0)
					copyDataBase();
				else
					replaceDataBase(path);
			} catch (IOException e) {
				Log.e("#DataBaseCopy#", "Not Copy Database");
				throw new Error("Error copying database");
			}
			try {
				openDataBase();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} else {
			if (flag == 1) {
				replaceDataBase(path);
			}
		}
		Log.d("#D3ataBaseCreated#", "Success");
	}

	public boolean checkDataBase() {
		SQLiteDatabase checkDB = null;
		try {
			String myPath = DB_PATH + DATABASE_NAME;
			if (databaseExist()) {
				checkDB = SQLiteDatabase.openDatabase(myPath, null,
						SQLiteDatabase.OPEN_READWRITE);
				Log.d("#CheckDataBaseExists#",
						" Database exists in application");
				Log.d("#CheckDBGetVersion#", checkDB.getVersion() + "");
			} else
				Log.d("#CheckDataBaseExists#",
						" Database not exists in application");

		} catch (SQLiteException e) {
			Log.e("#CheckDataBaseExists#",
					" Database not exists in application");
		}
		if (checkDB != null) {
			checkDB.close();
		}
		return checkDB != null ? true : false;
	}

	public boolean databaseExist() {
		File dbFile = new File(DB_PATH + DATABASE_NAME);
		return dbFile.exists();
	}

	
	private void copyDataBase() throws IOException {
		InputStream suninputstream = context.getAssets().open(DATABASE_NAME);
		String sunFileName = DB_PATH + DATABASE_NAME;
		OutputStream sunoutputstream = new FileOutputStream(sunFileName);
		byte[] buffer = new byte[1024];
		int length;
		while ((length = suninputstream.read(buffer)) > 0) {
			sunoutputstream.write(buffer, 0, length);
		}
		sunoutputstream.flush();
		sunoutputstream.close();
		suninputstream.close();
		Log.d("#DataBaseCopy#", "Copy Database Done");
	}

	public void replaceDataBase(String Path) {

		// context.deleteDatabase(DATABASE_NAME);
		FileInputStream suninputstream = null;
		try {
			suninputstream = new FileInputStream(new File(Path));
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}

		// InputStream suninputstream =new ByteArrayInputStream(new
		// FileReader(file));

		String sunFileName = DB_PATH + DATABASE_NAME;

		OutputStream sunoutputstream;
		try {
			sunoutputstream = new FileOutputStream(sunFileName);
			byte[] buffer = new byte[3000];

			int length;

			while ((length = suninputstream.read(buffer)) > 0) {
				sunoutputstream.write(buffer, 0, length);
			}
			sunoutputstream.flush();
			sunoutputstream.close();
			suninputstream.close();
			Log.d("#DataBaseCopy#", "Copy Database Done");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private boolean openDataBaseQuery() {
		try {
			if (databaseQuery != null && databaseQuery.isOpen()){
				
				return true;
			}else {
				String myPath = DB_PATH + DATABASE_NAME;
				
				databaseQuery = SQLiteDatabase.openDatabase(myPath, null,
						SQLiteDatabase.OPEN_READWRITE);
				if (databaseQuery != null && databaseQuery.isOpen()){
					Log.e("open","");
					
				}else{
					Log.e("open","failed");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.e("Exception",""+e.getLocalizedMessage());
		}
		// Open the database
		return true;
	}

	public void openDataBase() throws SQLException {
		String path = DB_PATH + DATABASE_NAME;
		dataBase = SQLiteDatabase.openDatabase(path, null,
				SQLiteDatabase.OPEN_READWRITE);
		Log.d("#DataBaseOpen#", "open Database Done");
	}

	@Override
	public synchronized void close() {
		if (dataBase != null)
			dataBase.close();
		super.close();
		Log.d("#DataBaseClose#", "close Database Done");
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		Log.d("#DataBaseCreate#", "Create Database Done");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	}

	/** Query for Insert from Database **/
	public void onInsertSet(String table, ContentValues values) {
		try {
			// SQLiteDatabase database = this.getWritableDatabase();
			Log.e("Hello","....");
			openDataBaseQuery();
			databaseQuery.insertOrThrow(table, null, values);
			Log.e("Hello3","....");
			
			// database.close();
		} catch (Exception e) {
			Log.e("Hello4","....");
			e.printStackTrace();
			
			Log.e("",""+e.getLocalizedMessage());
		}
	}

	/** Query for Update from Database **/
	public void onUpdateSet(String table, ContentValues values,
			String whereColumnName, String[] whereArgs) {
		try {
			// SQLiteDatabase database = this.getWritableDatabase();
			openDataBaseQuery();
			databaseQuery.update(table, values, whereColumnName, whereArgs);
			// database.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	/** Query for Delete from Database **/
	public void onDeleteSet(String table, String whereColumnName,
			String[] whereArgs) {
		Log.e("Deleted","databse deleted1");
		try {
			// SQLiteDatabase database = this.getWritableDatabase();
			openDataBaseQuery();
			Log.e("Deleted1","databse deleted1");
			databaseQuery.delete(table, whereColumnName, whereArgs);
			Log.e("Deleted2","databse deleted2");
			// database.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @param query
	 */
	public void onExecuteQuery(String query) {
		try {
			if (query != null) {
				// SQLiteDatabase database = this.getWritableDatabase();
				openDataBaseQuery();
				databaseQuery.execSQL(query);
				// database.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @param query
	 * @return
	 */
	public Cursor onSelectQuery(String query) {
		Cursor cursor = null;
		try {
			if (query != null) {
				// SQLiteDatabase database = this.getWritableDatabase();
				openDataBaseQuery();
				cursor = databaseQuery.rawQuery(query, null);
				// database.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cursor;
	}

	public void insertAllCompanies(Context context, JSONArray jArray)
			throws JSONException {
		// TODO Auto-generated method stub
		try {
			SQLiteDatabase database = this.getWritableDatabase();

			for (int i = 0; i < jArray.length(); i++) {

				JSONObject jsonObjectAdded = jArray.getJSONObject(i);
				ContentValues values = new ContentValues();

				values.put(WebServiceAPI.id, jsonObjectAdded.optString(WebServiceAPI.id));
				values.put(WebServiceAPI.company_code, jsonObjectAdded.optString(WebServiceAPI.company_code));
				values.put(WebServiceAPI.parent_id, jsonObjectAdded.optString(WebServiceAPI.parent_id));
				values.put(WebServiceAPI.parent_type, jsonObjectAdded.optString(WebServiceAPI.parent_type));
				values.put(WebServiceAPI.bussiness_no, jsonObjectAdded.optString(WebServiceAPI.bussiness_no));
				values.put(WebServiceAPI.company_name, jsonObjectAdded.optString(WebServiceAPI.company_name));
				values.put(WebServiceAPI.company_intro, jsonObjectAdded.optString(WebServiceAPI.company_intro));
				values.put(WebServiceAPI.company_type, jsonObjectAdded.optString(WebServiceAPI.company_type));
				values.put(WebServiceAPI.website, jsonObjectAdded.optString(WebServiceAPI.website));
				values.put(WebServiceAPI.logo, jsonObjectAdded.optString(WebServiceAPI.logo));
				values.put(WebServiceAPI.rating, jsonObjectAdded.optString(WebServiceAPI.rating));
				values.put(WebServiceAPI.awards, jsonObjectAdded.optString(WebServiceAPI.awards));
				values.put(WebServiceAPI.email, jsonObjectAdded.optString(WebServiceAPI.email));
				values.put(WebServiceAPI.password, jsonObjectAdded.optString(WebServiceAPI.password));
				values.put(WebServiceAPI.pass, jsonObjectAdded.optString(WebServiceAPI.pass));
				values.put(WebServiceAPI.description, jsonObjectAdded.optString(WebServiceAPI.description));
				values.put(WebServiceAPI.contact_name, jsonObjectAdded.optString(WebServiceAPI.contact_name));
				values.put(WebServiceAPI.contact_email, jsonObjectAdded.optString(WebServiceAPI.contact_email));
				values.put(WebServiceAPI.phone, jsonObjectAdded.optString(WebServiceAPI.phone));
				values.put(WebServiceAPI.phonecode, jsonObjectAdded.optString(WebServiceAPI.phonecode));
				values.put(WebServiceAPI.mobilecode, jsonObjectAdded.optString(WebServiceAPI.mobilecode));
				values.put(WebServiceAPI.mobile, jsonObjectAdded.optString(WebServiceAPI.mobile));
				values.put(WebServiceAPI.general_no, jsonObjectAdded.optString(WebServiceAPI.general_no));
				values.put(WebServiceAPI.company_address, jsonObjectAdded.optString(WebServiceAPI.company_address));
				values.put(WebServiceAPI.city, jsonObjectAdded.optString(WebServiceAPI.city));
				values.put(WebServiceAPI.country_id, jsonObjectAdded.optString(WebServiceAPI.country_id));
				values.put(WebServiceAPI.state_id, jsonObjectAdded.optString(WebServiceAPI.state_id));
				values.put(WebServiceAPI.notes_CRM, jsonObjectAdded.optString(WebServiceAPI.notes_CRM));
				values.put(WebServiceAPI.status, jsonObjectAdded.optString(WebServiceAPI.status));
				values.put(WebServiceAPI.first_login_status, jsonObjectAdded.optString(WebServiceAPI.first_login_status));
				values.put(WebServiceAPI.change_pass, jsonObjectAdded.optString(WebServiceAPI.change_pass));
				values.put(WebServiceAPI.lock, jsonObjectAdded.optString(WebServiceAPI.lock));
				values.put(WebServiceAPI.user_type, jsonObjectAdded.optString(WebServiceAPI.user_type));
				values.put(WebServiceAPI.package_id, jsonObjectAdded.optString(WebServiceAPI.package_id));
				values.put(WebServiceAPI.messaging_country, jsonObjectAdded.optString(WebServiceAPI.messaging_country));
				values.put(WebServiceAPI.application_due_date, jsonObjectAdded.optString(WebServiceAPI.application_due_date));
				values.put(WebServiceAPI.key, jsonObjectAdded.optString(WebServiceAPI.key));
				values.put(WebServiceAPI.message_limit, jsonObjectAdded.optString(WebServiceAPI.message_limit));
				values.put(WebServiceAPI.send_message, jsonObjectAdded.optString(WebServiceAPI.send_message));
				values.put(WebServiceAPI.date, jsonObjectAdded.optString(WebServiceAPI.date));
				values.put(WebServiceAPI.modified_date, jsonObjectAdded.optString(WebServiceAPI.modified_date));
				values.put(WebServiceAPI.added_by, jsonObjectAdded.optString(WebServiceAPI.added_by));
				values.put(WebServiceAPI.modified_by, jsonObjectAdded.optString(WebServiceAPI.modified_by));
				values.put(WebServiceAPI.package_start, jsonObjectAdded.optString(WebServiceAPI.package_start));
				values.put(WebServiceAPI.package_end, jsonObjectAdded.optString(WebServiceAPI.package_end));
				values.put(WebServiceAPI.country, jsonObjectAdded.optString(WebServiceAPI.country));
				values.put(WebServiceAPI.international, jsonObjectAdded.optString(WebServiceAPI.international));
				values.put(WebServiceAPI.message_events, jsonObjectAdded.optString(WebServiceAPI.message_events));
				values.put(WebServiceAPI.regstates, jsonObjectAdded.optString(WebServiceAPI.regstates));
				values.put(WebServiceAPI.message_state, jsonObjectAdded.optString(WebServiceAPI.message_state));
				values.put(WebServiceAPI.message_status, jsonObjectAdded.optString(WebServiceAPI.message_status));
				values.put(WebServiceAPI.unread_message, jsonObjectAdded.optString(WebServiceAPI.unread_message));
				values.put(WebServiceAPI.portfolio_status, jsonObjectAdded.optString(WebServiceAPI.portfolio_status));



				database.insert(WebServiceAPI.Companies, null, values);

				JSONArray jsonArrayAwards= jsonObjectAdded.optJSONArray("allAwards");

				if(jsonArrayAwards != null && jsonArrayAwards.length() > 0) {

					for (int j = 0; j < jsonArrayAwards.length(); j++) {

						JSONObject jsonObjectAward = jsonArrayAwards.getJSONObject(j);

						ContentValues valuesAward = new ContentValues();

						valuesAward.put(WebServiceAPI.id, jsonObjectAward.optString(WebServiceAPI.id));
						valuesAward.put(WebServiceAPI.company_id, jsonObjectAward.optString(WebServiceAPI.company_id));
						valuesAward.put(WebServiceAPI.description, jsonObjectAward.optString(WebServiceAPI.description));
						valuesAward.put(WebServiceAPI.awards, jsonObjectAward.optString(WebServiceAPI.awards));
						valuesAward.put(WebServiceAPI.year, jsonObjectAward.optString(WebServiceAPI.year));

						database.insert(WebServiceAPI.CompanyAwards, null, valuesAward);

					}

				}

				JSONArray jsonQuesAns = jsonObjectAdded.optJSONArray("quesAns");

				if(jsonQuesAns != null && jsonQuesAns.length() > 0) {

					for (int j = 0; j < jsonQuesAns.length(); j++) {

						JSONObject jsonObjectQuesAns = jsonQuesAns.getJSONObject(j);

						ContentValues valuesQuesAns = new ContentValues();

						valuesQuesAns.put(WebServiceAPI.id, jsonObjectQuesAns.optString(WebServiceAPI.id));
						valuesQuesAns.put(WebServiceAPI.company_id, jsonObjectQuesAns.optString(WebServiceAPI.company_id));
						valuesQuesAns.put(WebServiceAPI.question, jsonObjectQuesAns.optString(WebServiceAPI.question));
						valuesQuesAns.put(WebServiceAPI.answer, jsonObjectQuesAns.optString(WebServiceAPI.answer));

						database.insert(WebServiceAPI.CompanyQuesAns, null, valuesQuesAns);

					}

				}

				JSONArray jsonArrayDueDates = jsonObjectAdded.optJSONArray("application_due_date_all");

				if(jsonArrayDueDates != null && jsonArrayDueDates.length() > 0) {

					for (int j = 0; j < jsonArrayDueDates.length(); j++) {

						JSONObject jsonObjectDueDates = jsonArrayDueDates.getJSONObject(j);

						ContentValues valuesDueDates = new ContentValues();

						valuesDueDates.put(WebServiceAPI.id, jsonObjectDueDates.optString(WebServiceAPI.id));
						valuesDueDates.put(WebServiceAPI.company_id, jsonObjectDueDates.optString(WebServiceAPI.company_id));
						valuesDueDates.put(WebServiceAPI.state_id, jsonObjectDueDates.optString(WebServiceAPI.state_id));
						valuesDueDates.put(WebServiceAPI.program_id, jsonObjectDueDates.optString(WebServiceAPI.program_id));
						valuesDueDates.put(WebServiceAPI.start_date, jsonObjectDueDates.optString(WebServiceAPI.start_date));
						valuesDueDates.put(WebServiceAPI.close_date, jsonObjectDueDates.optString(WebServiceAPI.close_date));
						valuesDueDates.put(WebServiceAPI.created, jsonObjectDueDates.optString(WebServiceAPI.created));
						valuesDueDates.put(WebServiceAPI.state, jsonObjectDueDates.optString(WebServiceAPI.state));
						valuesDueDates.put(WebServiceAPI.program, jsonObjectDueDates.optString(WebServiceAPI.program));

						database.insert(WebServiceAPI.CompanyApplicationDueDateAll, null, valuesDueDates);

					}

				}

				JSONArray jsonArrayDisSub = jsonObjectAdded.optJSONArray("disciplineSubdiscipline");


				if(jsonArrayDisSub != null && jsonArrayDisSub.length() > 0) {

					for (int k = 0; k < jsonArrayDisSub.length(); k++) {

						JSONObject jsonObjectDisSub = jsonArrayDisSub.getJSONObject(k);

						ContentValues valuesDisSub = new ContentValues();


						valuesDisSub.put(WebServiceAPI.id, jsonObjectDisSub.optString(WebServiceAPI.id));
						valuesDisSub.put(WebServiceAPI.company_id, jsonObjectDisSub.optString(WebServiceAPI.company_id));
						valuesDisSub.put(WebServiceAPI.discipline_id, jsonObjectDisSub.optString(WebServiceAPI.discipline_id));
						valuesDisSub.put(WebServiceAPI.sub_discipline_id, jsonObjectDisSub.optString(WebServiceAPI.sub_discipline_id));
						valuesDisSub.put(WebServiceAPI.type, jsonObjectDisSub.optString(WebServiceAPI.type));
						valuesDisSub.put(WebServiceAPI.discipline, jsonObjectDisSub.optString(WebServiceAPI.discipline));
						valuesDisSub.put(WebServiceAPI.sub_discipline, jsonObjectDisSub.optString(WebServiceAPI.sub_discipline));

						database.insert(WebServiceAPI.CompanyDisciplineSubdiscipline, null, valuesDisSub);

					}

				}

				JSONArray jsonArrayDis = jsonObjectAdded.optJSONArray("disciplines");

				if(jsonArrayDis != null && jsonArrayDis.length() > 0) {

					for (int l = 0; l < jsonArrayDis.length(); l++) {

						JSONObject jsonObjectDis = jsonArrayDis.getJSONObject(l);

						ContentValues valuesDis= new ContentValues();

						valuesDis.put(WebServiceAPI.company_id, jsonObjectDis.optString(WebServiceAPI.company_id));
						valuesDis.put(WebServiceAPI.discipline_id, jsonObjectDis.optString(WebServiceAPI.discipline_id));
						valuesDis.put(WebServiceAPI.name, jsonObjectDis.optString(WebServiceAPI.name));
						valuesDis.put(WebServiceAPI.type, jsonObjectDis.optString(WebServiceAPI.type));

						database.insert(WebServiceAPI.CompanyDisciplines, null, valuesDis);

						JSONArray jsonArraySubDis = jsonObjectDis.optJSONArray("sub_discipline");

						if(jsonArraySubDis != null && jsonArraySubDis.length() > 0) {

							for (int n = 0; n < jsonArraySubDis.length(); n++) {

								JSONObject jsonObjectSubDis = jsonArraySubDis.getJSONObject(n);

								ContentValues valuesSubDis = new ContentValues();

								valuesSubDis.put(WebServiceAPI.sub_discipline_id, jsonObjectSubDis.optString(WebServiceAPI.sub_discipline_id));
								valuesSubDis.put(WebServiceAPI.discipline_id, jsonObjectSubDis.optString(WebServiceAPI.discipline_id));
								valuesSubDis.put(WebServiceAPI.name, jsonObjectSubDis.optString(WebServiceAPI.name));

								database.insert(WebServiceAPI.CompanySubDisciplines, null, valuesSubDis);

							}
						}
					}
				}

				JSONArray jsonArrayLinks = jsonObjectAdded.optJSONArray("links");

				if(jsonArrayLinks != null && jsonArrayLinks.length() > 0) {

					for (int l = 0; l < jsonArrayLinks.length(); l++) {

						JSONObject jsonObjectLinks = jsonArrayLinks.getJSONObject(l);

						ContentValues valuesLinks = new ContentValues();

						valuesLinks.put(WebServiceAPI.id, jsonObjectLinks.optString(WebServiceAPI.id));
						valuesLinks.put(WebServiceAPI.company_id, jsonObjectLinks.optString(WebServiceAPI.company_id));
						valuesLinks.put(WebServiceAPI.media_type, jsonObjectLinks.optString(WebServiceAPI.media_type));
						valuesLinks.put(WebServiceAPI.media_link, jsonObjectLinks.optString(WebServiceAPI.media_link));
						valuesLinks.put(WebServiceAPI.media_logo, jsonObjectLinks.optString(WebServiceAPI.media_logo));
						valuesLinks.put(WebServiceAPI.default_id, jsonObjectLinks.optString(WebServiceAPI.default_id));

						database.insert(WebServiceAPI.CompanyLinks, null, valuesLinks);

					}
				}

				JSONArray jsonArrayLocations = jsonObjectAdded.optJSONArray("locations");


				if(jsonArrayLocations != null && jsonArrayLocations.length() > 0) {

					for (int l = 0; l < jsonArrayLocations.length(); l++) {

						JSONObject jsonObjectLocations = jsonArrayLocations.getJSONObject(l);

						ContentValues valuesLocations = new ContentValues();


						valuesLocations.put(WebServiceAPI.id, jsonObjectLocations.optString(WebServiceAPI.id));
						valuesLocations.put(WebServiceAPI.company_id, jsonObjectLocations.optString(WebServiceAPI.company_id));
						valuesLocations.put(WebServiceAPI.branch_name, jsonObjectLocations.optString(WebServiceAPI.branch_name));
						valuesLocations.put(WebServiceAPI.location, jsonObjectLocations.optString(WebServiceAPI.location));
						valuesLocations.put(WebServiceAPI.lat, jsonObjectLocations.optString(WebServiceAPI.lat));
						valuesLocations.put(WebServiceAPI.longitude, jsonObjectLocations.optString(WebServiceAPI.longitude));

						database.insert(WebServiceAPI.CompanyLocations, null, valuesLocations);

					}
				}



				SharedPrefrenceUtil.setPrefrence(context, Constants.isAllCompany, "Database");
			}

			database.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void insertAllCompanyFilter(Context context, JSONArray jArray)
			throws JSONException {
		// TODO Auto-generated method stub
		try {
			SQLiteDatabase database = this.getWritableDatabase();

			for (int i = 0; i < jArray.length(); i++) {

				JSONObject jsonObjectAdded = jArray.getJSONObject(i);
				ContentValues values = new ContentValues();

				values.put(WebServiceAPI.id, jsonObjectAdded.optString(WebServiceAPI.id));
				values.put(WebServiceAPI.company_code, jsonObjectAdded.optString(WebServiceAPI.company_code));
				values.put(WebServiceAPI.parent_id, jsonObjectAdded.optString(WebServiceAPI.parent_id));
				values.put(WebServiceAPI.parent_type, jsonObjectAdded.optString(WebServiceAPI.parent_type));
				values.put(WebServiceAPI.bussiness_no, jsonObjectAdded.optString(WebServiceAPI.bussiness_no));
				values.put(WebServiceAPI.company_name, jsonObjectAdded.optString(WebServiceAPI.company_name));
				values.put(WebServiceAPI.company_intro, jsonObjectAdded.optString(WebServiceAPI.company_intro));
				values.put(WebServiceAPI.company_type, jsonObjectAdded.optString(WebServiceAPI.company_type));
				values.put(WebServiceAPI.website, jsonObjectAdded.optString(WebServiceAPI.website));
				values.put(WebServiceAPI.logo, jsonObjectAdded.optString(WebServiceAPI.logo));
				values.put(WebServiceAPI.rating, jsonObjectAdded.optString(WebServiceAPI.rating));
				values.put(WebServiceAPI.awards, jsonObjectAdded.optString(WebServiceAPI.awards));
				values.put(WebServiceAPI.email, jsonObjectAdded.optString(WebServiceAPI.email));
				values.put(WebServiceAPI.password, jsonObjectAdded.optString(WebServiceAPI.password));
				values.put(WebServiceAPI.pass, jsonObjectAdded.optString(WebServiceAPI.pass));
				values.put(WebServiceAPI.description, jsonObjectAdded.optString(WebServiceAPI.description));
				values.put(WebServiceAPI.contact_name, jsonObjectAdded.optString(WebServiceAPI.contact_name));
				values.put(WebServiceAPI.contact_email, jsonObjectAdded.optString(WebServiceAPI.contact_email));
				values.put(WebServiceAPI.phone, jsonObjectAdded.optString(WebServiceAPI.phone));
				values.put(WebServiceAPI.phonecode, jsonObjectAdded.optString(WebServiceAPI.phonecode));
				values.put(WebServiceAPI.mobilecode, jsonObjectAdded.optString(WebServiceAPI.mobilecode));
				values.put(WebServiceAPI.mobile, jsonObjectAdded.optString(WebServiceAPI.mobile));
				values.put(WebServiceAPI.general_no, jsonObjectAdded.optString(WebServiceAPI.general_no));
				values.put(WebServiceAPI.company_address, jsonObjectAdded.optString(WebServiceAPI.company_address));
				values.put(WebServiceAPI.city, jsonObjectAdded.optString(WebServiceAPI.city));
				values.put(WebServiceAPI.country_id, jsonObjectAdded.optString(WebServiceAPI.country_id));
				values.put(WebServiceAPI.state_id, jsonObjectAdded.optString(WebServiceAPI.state_id));
				values.put(WebServiceAPI.notes_CRM, jsonObjectAdded.optString(WebServiceAPI.notes_CRM));
				values.put(WebServiceAPI.status, jsonObjectAdded.optString(WebServiceAPI.status));
				values.put(WebServiceAPI.first_login_status, jsonObjectAdded.optString(WebServiceAPI.first_login_status));
				values.put(WebServiceAPI.change_pass, jsonObjectAdded.optString(WebServiceAPI.change_pass));
				values.put(WebServiceAPI.lock, jsonObjectAdded.optString(WebServiceAPI.lock));
				values.put(WebServiceAPI.user_type, jsonObjectAdded.optString(WebServiceAPI.user_type));
				values.put(WebServiceAPI.package_id, jsonObjectAdded.optString(WebServiceAPI.package_id));
				values.put(WebServiceAPI.messaging_country, jsonObjectAdded.optString(WebServiceAPI.messaging_country));
				values.put(WebServiceAPI.application_due_date, jsonObjectAdded.optString(WebServiceAPI.application_due_date));
				values.put(WebServiceAPI.key, jsonObjectAdded.optString(WebServiceAPI.key));
				values.put(WebServiceAPI.message_limit, jsonObjectAdded.optString(WebServiceAPI.message_limit));
				values.put(WebServiceAPI.send_message, jsonObjectAdded.optString(WebServiceAPI.send_message));
				values.put(WebServiceAPI.date, jsonObjectAdded.optString(WebServiceAPI.date));
				values.put(WebServiceAPI.modified_date, jsonObjectAdded.optString(WebServiceAPI.modified_date));
				values.put(WebServiceAPI.added_by, jsonObjectAdded.optString(WebServiceAPI.added_by));
				values.put(WebServiceAPI.modified_by, jsonObjectAdded.optString(WebServiceAPI.modified_by));
				values.put(WebServiceAPI.package_start, jsonObjectAdded.optString(WebServiceAPI.package_start));
				values.put(WebServiceAPI.package_end, jsonObjectAdded.optString(WebServiceAPI.package_end));
				values.put(WebServiceAPI.country, jsonObjectAdded.optString(WebServiceAPI.country));
				values.put(WebServiceAPI.international, jsonObjectAdded.optString(WebServiceAPI.international));
				values.put(WebServiceAPI.message_events, jsonObjectAdded.optString(WebServiceAPI.message_events));
				values.put(WebServiceAPI.regstates, jsonObjectAdded.optString(WebServiceAPI.regstates));
				values.put(WebServiceAPI.message_state, jsonObjectAdded.optString(WebServiceAPI.message_state));
				values.put(WebServiceAPI.message_status, jsonObjectAdded.optString(WebServiceAPI.message_status));
				values.put(WebServiceAPI.unread_message, jsonObjectAdded.optString(WebServiceAPI.unread_message));
				values.put(WebServiceAPI.portfolio_status, jsonObjectAdded.optString(WebServiceAPI.portfolio_status));

				database.insert(WebServiceAPI.CompanyFilter, null, values);

				SharedPrefrenceUtil.setPrefrence(context, Constants.isCompanyFilter, "Database_companyFilter");
			}

			database.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void insertSocietyDetail(Context context,JSONArray jsonArray)throws JSONException {
		// TODO Auto-generated method stub
		try {
			SQLiteDatabase database = this.getWritableDatabase();

			for (int i = 0; i < jsonArray.length(); i++) {

				JSONObject jsonObjectAdded = jsonArray.getJSONObject(i);
				ContentValues values = new ContentValues();

				values.put(WebServiceAPI.id, jsonObjectAdded.optString(WebServiceAPI.id));
				values.put(WebServiceAPI.society_id, jsonObjectAdded.optString(WebServiceAPI.society_id));
				values.put(WebServiceAPI.parent_id, jsonObjectAdded.optString(WebServiceAPI.parent_id));
				values.put(WebServiceAPI.staff_id, jsonObjectAdded.optString(WebServiceAPI.staff_id));
				values.put(WebServiceAPI.creator_name, jsonObjectAdded.optString(WebServiceAPI.creator_name));
				values.put(WebServiceAPI.website, jsonObjectAdded.optString(WebServiceAPI.website));
				values.put(WebServiceAPI.society_email, jsonObjectAdded.optString(WebServiceAPI.society_email));
				values.put(WebServiceAPI.password, jsonObjectAdded.optString(WebServiceAPI.password));
				values.put(WebServiceAPI.first_login_status, jsonObjectAdded.optString(WebServiceAPI.first_login_status));
				values.put(WebServiceAPI.change_pass, jsonObjectAdded.optString(WebServiceAPI.change_pass));
				values.put(WebServiceAPI.pass, jsonObjectAdded.optString(WebServiceAPI.pass));
				values.put(WebServiceAPI.uni_id, jsonObjectAdded.optString(WebServiceAPI.uni_id));
				values.put(WebServiceAPI.country_id, jsonObjectAdded.optString(WebServiceAPI.country_id));
				values.put(WebServiceAPI.state_id, jsonObjectAdded.optString(WebServiceAPI.state_id));
				values.put(WebServiceAPI.societylogo, jsonObjectAdded.optString(WebServiceAPI.societylogo));
				values.put(WebServiceAPI.creator_mobile, jsonObjectAdded.optString(WebServiceAPI.creator_mobile));
				values.put(WebServiceAPI.society_name, jsonObjectAdded.optString(WebServiceAPI.society_name));
				values.put(WebServiceAPI.society_president, jsonObjectAdded.optString(WebServiceAPI.society_president));
				values.put(WebServiceAPI.president_email, jsonObjectAdded.optString(WebServiceAPI.president_email));
				values.put(WebServiceAPI.president_mobile, jsonObjectAdded.optString(WebServiceAPI.president_mobile));
				values.put(WebServiceAPI.Treasurer_name, jsonObjectAdded.optString(WebServiceAPI.Treasurer_name));
				values.put(WebServiceAPI.Treasurer_email, jsonObjectAdded.optString(WebServiceAPI.Treasurer_email));
				values.put(WebServiceAPI.Treasure_mobile, jsonObjectAdded.optString(WebServiceAPI.Treasure_mobile));
				values.put(WebServiceAPI.Secetery_name, jsonObjectAdded.optString(WebServiceAPI.Secetery_name));
				values.put(WebServiceAPI.Secetery_email, jsonObjectAdded.optString(WebServiceAPI.Secetery_email));
				values.put(WebServiceAPI.Secetery_mobile, jsonObjectAdded.optString(WebServiceAPI.Secetery_mobile));
				values.put(WebServiceAPI.facebookemail, jsonObjectAdded.optString(WebServiceAPI.facebookemail));
				values.put(WebServiceAPI.user_type, jsonObjectAdded.optString(WebServiceAPI.user_type));
				values.put(WebServiceAPI.status, jsonObjectAdded.optString(WebServiceAPI.status));
				values.put(WebServiceAPI.locks, jsonObjectAdded.optString(WebServiceAPI.locks));
				values.put(WebServiceAPI.date, jsonObjectAdded.optString(WebServiceAPI.date));
				values.put(WebServiceAPI.key, jsonObjectAdded.optString(WebServiceAPI.key));
				values.put(WebServiceAPI.created_by, jsonObjectAdded.optString(WebServiceAPI.created_by));
				values.put(WebServiceAPI.modified_date, jsonObjectAdded.optString(WebServiceAPI.modified_date));
				values.put(WebServiceAPI.society_code, jsonObjectAdded.optString(WebServiceAPI.society_code));
				values.put(WebServiceAPI.logo, jsonObjectAdded.optString(WebServiceAPI.logo));
				values.put(WebServiceAPI.unread_message, jsonObjectAdded.optString(WebServiceAPI.unread_message));

				database.insert(WebServiceAPI.SocietyDetail, null, values);

				SharedPrefrenceUtil.setPrefrence(context, Constants.isSocietyDetail, "Database_societyDetail");
			}

			database.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}



	public void insertUserCompany(Context context, JSONArray jArray)
			throws JSONException {
		// TODO Auto-generated method stub
		try {
			SQLiteDatabase database = this.getWritableDatabase();

			for (int i = 0; i < jArray.length(); i++) {

				JSONObject jsonObjectAdded = jArray.getJSONObject(i);
				ContentValues values = new ContentValues();

				values.put(WebServiceAPI.id, jsonObjectAdded.optString(WebServiceAPI.id));
				values.put(WebServiceAPI.company_code, jsonObjectAdded.optString(WebServiceAPI.company_code));
				values.put(WebServiceAPI.parent_id, jsonObjectAdded.optString(WebServiceAPI.parent_id));
				values.put(WebServiceAPI.parent_type, jsonObjectAdded.optString(WebServiceAPI.parent_type));
				values.put(WebServiceAPI.bussiness_no, jsonObjectAdded.optString(WebServiceAPI.bussiness_no));
				values.put(WebServiceAPI.company_name, jsonObjectAdded.optString(WebServiceAPI.company_name));
				values.put(WebServiceAPI.company_intro, jsonObjectAdded.optString(WebServiceAPI.company_intro));
				values.put(WebServiceAPI.company_type, jsonObjectAdded.optString(WebServiceAPI.company_type));
				values.put(WebServiceAPI.website, jsonObjectAdded.optString(WebServiceAPI.website));
				values.put(WebServiceAPI.logo, jsonObjectAdded.optString(WebServiceAPI.logo));
				values.put(WebServiceAPI.rating, jsonObjectAdded.optString(WebServiceAPI.rating));
				values.put(WebServiceAPI.awards, jsonObjectAdded.optString(WebServiceAPI.awards));
				values.put(WebServiceAPI.email, jsonObjectAdded.optString(WebServiceAPI.email));
				values.put(WebServiceAPI.password, jsonObjectAdded.optString(WebServiceAPI.password));
				values.put(WebServiceAPI.pass, jsonObjectAdded.optString(WebServiceAPI.pass));
				values.put(WebServiceAPI.description, jsonObjectAdded.optString(WebServiceAPI.description));
				values.put(WebServiceAPI.contact_name, jsonObjectAdded.optString(WebServiceAPI.contact_name));
				values.put(WebServiceAPI.contact_email, jsonObjectAdded.optString(WebServiceAPI.contact_email));
				values.put(WebServiceAPI.phone, jsonObjectAdded.optString(WebServiceAPI.phone));
				values.put(WebServiceAPI.phonecode, jsonObjectAdded.optString(WebServiceAPI.phonecode));
				values.put(WebServiceAPI.mobilecode, jsonObjectAdded.optString(WebServiceAPI.mobilecode));
				values.put(WebServiceAPI.mobile, jsonObjectAdded.optString(WebServiceAPI.mobile));
				values.put(WebServiceAPI.general_no, jsonObjectAdded.optString(WebServiceAPI.general_no));
				values.put(WebServiceAPI.company_address, jsonObjectAdded.optString(WebServiceAPI.company_address));
				values.put(WebServiceAPI.city, jsonObjectAdded.optString(WebServiceAPI.city));
				values.put(WebServiceAPI.country_id, jsonObjectAdded.optString(WebServiceAPI.country_id));
				values.put(WebServiceAPI.state_id, jsonObjectAdded.optString(WebServiceAPI.state_id));
				values.put(WebServiceAPI.notes_CRM, jsonObjectAdded.optString(WebServiceAPI.notes_CRM));
				values.put(WebServiceAPI.status, jsonObjectAdded.optString(WebServiceAPI.status));
				values.put(WebServiceAPI.first_login_status, jsonObjectAdded.optString(WebServiceAPI.first_login_status));
				values.put(WebServiceAPI.change_pass, jsonObjectAdded.optString(WebServiceAPI.change_pass));
				values.put(WebServiceAPI.lock, jsonObjectAdded.optString(WebServiceAPI.lock));
				values.put(WebServiceAPI.user_type, jsonObjectAdded.optString(WebServiceAPI.user_type));
				values.put(WebServiceAPI.package_id, jsonObjectAdded.optString(WebServiceAPI.package_id));
				values.put(WebServiceAPI.messaging_country, jsonObjectAdded.optString(WebServiceAPI.messaging_country));
				values.put(WebServiceAPI.application_due_date, jsonObjectAdded.optString(WebServiceAPI.application_due_date));
				values.put(WebServiceAPI.key, jsonObjectAdded.optString(WebServiceAPI.key));
				values.put(WebServiceAPI.message_limit, jsonObjectAdded.optString(WebServiceAPI.message_limit));
				values.put(WebServiceAPI.send_message, jsonObjectAdded.optString(WebServiceAPI.send_message));
				values.put(WebServiceAPI.date, jsonObjectAdded.optString(WebServiceAPI.date));
				values.put(WebServiceAPI.modified_date, jsonObjectAdded.optString(WebServiceAPI.modified_date));
				values.put(WebServiceAPI.added_by, jsonObjectAdded.optString(WebServiceAPI.added_by));
				values.put(WebServiceAPI.modified_by, jsonObjectAdded.optString(WebServiceAPI.modified_by));
				values.put(WebServiceAPI.package_start, jsonObjectAdded.optString(WebServiceAPI.package_start));
				values.put(WebServiceAPI.package_end, jsonObjectAdded.optString(WebServiceAPI.package_end));
				values.put(WebServiceAPI.country, jsonObjectAdded.optString(WebServiceAPI.country));
				values.put(WebServiceAPI.state, jsonObjectAdded.optString(WebServiceAPI.state));
				values.put(WebServiceAPI.abr, jsonObjectAdded.optString(WebServiceAPI.abr));
				values.put(WebServiceAPI.is_deleted, jsonObjectAdded.optString(WebServiceAPI.is_deleted));
				values.put(WebServiceAPI.international, jsonObjectAdded.optString(WebServiceAPI.international));
				values.put(WebServiceAPI.message_status, jsonObjectAdded.optString(WebServiceAPI.message_status));
				values.put(WebServiceAPI.unread_message, jsonObjectAdded.optString(WebServiceAPI.unread_message));
				values.put(WebServiceAPI.regstates, jsonObjectAdded.optString(WebServiceAPI.regstates));
				values.put(WebServiceAPI.perfect_logo, jsonObjectAdded.optString(WebServiceAPI.perfect_logo));


				JSONArray jsonArrayAwards= jsonObjectAdded.optJSONArray("allAwards");

				if(jsonArrayAwards != null && jsonArrayAwards.length() > 0) {

					for (int j = 0; j < jsonArrayAwards.length(); j++) {

						JSONObject jsonObjectAward = jsonArrayAwards.getJSONObject(j);

						ContentValues valuesAward = new ContentValues();

						valuesAward.put(WebServiceAPI.id, jsonObjectAward.optString(WebServiceAPI.id));
						valuesAward.put(WebServiceAPI.company_id, jsonObjectAward.optString(WebServiceAPI.company_id));
						valuesAward.put(WebServiceAPI.description, jsonObjectAward.optString(WebServiceAPI.description));
						valuesAward.put(WebServiceAPI.awards, jsonObjectAward.optString(WebServiceAPI.awards));
						valuesAward.put(WebServiceAPI.year, jsonObjectAward.optString(WebServiceAPI.year));

						database.insert(WebServiceAPI.PortfolioAwards, null, valuesAward);

					}

				}

				JSONArray jsonArrayDueDates = jsonObjectAdded.optJSONArray("application_due_date_all");

				if(jsonArrayDueDates != null && jsonArrayDueDates.length() > 0) {

					for (int j = 0; j < jsonArrayDueDates.length(); j++) {

						JSONObject jsonObjectDueDates = jsonArrayDueDates.getJSONObject(j);

						ContentValues valuesDueDates = new ContentValues();

						valuesDueDates.put(WebServiceAPI.id, jsonObjectDueDates.optString(WebServiceAPI.id));
						valuesDueDates.put(WebServiceAPI.company_id, jsonObjectDueDates.optString(WebServiceAPI.company_id));
						valuesDueDates.put(WebServiceAPI.state_id, jsonObjectDueDates.optString(WebServiceAPI.state_id));
						valuesDueDates.put(WebServiceAPI.program_id, jsonObjectDueDates.optString(WebServiceAPI.program_id));
						valuesDueDates.put(WebServiceAPI.start_date, jsonObjectDueDates.optString(WebServiceAPI.start_date));
						valuesDueDates.put(WebServiceAPI.close_date, jsonObjectDueDates.optString(WebServiceAPI.close_date));
						valuesDueDates.put(WebServiceAPI.created, jsonObjectDueDates.optString(WebServiceAPI.created));
						valuesDueDates.put(WebServiceAPI.state, jsonObjectDueDates.optString(WebServiceAPI.state));
						valuesDueDates.put(WebServiceAPI.program, jsonObjectDueDates.optString(WebServiceAPI.program));

						database.insert(WebServiceAPI.PortfolioApplicationDueDateAll, null, valuesDueDates);

					}

				}

				JSONArray jsonArrayquesAns = jsonObjectAdded.optJSONArray("quesAns");


				if(jsonArrayquesAns != null && jsonArrayquesAns.length() > 0) {

					for (int k = 0; k < jsonArrayquesAns.length(); k++) {

						JSONObject jsonObjectquesAns = jsonArrayquesAns.getJSONObject(k);

						ContentValues valuesquesAns = new ContentValues();

						valuesquesAns.put(WebServiceAPI.id, jsonObjectquesAns.optString(WebServiceAPI.id));
						valuesquesAns.put(WebServiceAPI.company_id, jsonObjectquesAns.optString(WebServiceAPI.company_id));
						valuesquesAns.put(WebServiceAPI.question, jsonObjectquesAns.optString(WebServiceAPI.question));
						valuesquesAns.put(WebServiceAPI.answer, jsonObjectquesAns.optString(WebServiceAPI.answer));

						database.insert(WebServiceAPI.PortfolioQuesAns, null, valuesquesAns);

					}

				}

				JSONArray jsonArrayDis = jsonObjectAdded.optJSONArray("disciplines");

				if(jsonArrayDis != null && jsonArrayDis.length() > 0) {

					for (int l = 0; l < jsonArrayDis.length(); l++) {

						JSONObject jsonObjectDis = jsonArrayDis.getJSONObject(l);

						ContentValues valuesDis= new ContentValues();

						valuesDis.put(WebServiceAPI.company_id, jsonObjectDis.optString(WebServiceAPI.company_id));
						valuesDis.put(WebServiceAPI.discipline_id, jsonObjectDis.optString(WebServiceAPI.discipline_id));
						valuesDis.put(WebServiceAPI.name, jsonObjectDis.optString(WebServiceAPI.name));

						database.insert(WebServiceAPI.PortfolioDisciplines, null, valuesDis);

						JSONArray jsonArraySubDis = jsonObjectDis.optJSONArray("sub_discipline");

						if(jsonArraySubDis != null && jsonArraySubDis.length() > 0) {

							for (int n = 0; n < jsonArraySubDis.length(); n++) {

								JSONObject jsonObjectSubDis = jsonArraySubDis.getJSONObject(n);

								ContentValues valuesSubDis = new ContentValues();

								valuesSubDis.put(WebServiceAPI.name, jsonObjectSubDis.optString(WebServiceAPI.name));
								valuesSubDis.put(WebServiceAPI.discipline_id, jsonObjectSubDis.optString(WebServiceAPI.discipline_id));
								valuesSubDis.put(WebServiceAPI.sub_discipline_id, jsonObjectSubDis.optString(WebServiceAPI.sub_discipline_id));

								database.insert(WebServiceAPI.PortfolioSubDisciplines, null, valuesSubDis);

							}
						}
					}
				}

				JSONArray jsonArrayLinks = jsonObjectAdded.optJSONArray("links");

				if(jsonArrayLinks != null && jsonArrayLinks.length() > 0) {

					for (int l = 0; l < jsonArrayLinks.length(); l++) {

						JSONObject jsonObjectLinks = jsonArrayLinks.getJSONObject(l);

						ContentValues valuesLinks = new ContentValues();

						valuesLinks.put(WebServiceAPI.id, jsonObjectLinks.optString(WebServiceAPI.id));
						valuesLinks.put(WebServiceAPI.company_id, jsonObjectLinks.optString(WebServiceAPI.company_id));
						valuesLinks.put(WebServiceAPI.media_type, jsonObjectLinks.optString(WebServiceAPI.media_type));
						valuesLinks.put(WebServiceAPI.media_link, jsonObjectLinks.optString(WebServiceAPI.media_link));
						valuesLinks.put(WebServiceAPI.media_logo, jsonObjectLinks.optString(WebServiceAPI.media_logo));
						valuesLinks.put(WebServiceAPI.default_id, jsonObjectLinks.optString(WebServiceAPI.default_id));

						database.insert(WebServiceAPI.PortfolioLinks, null, valuesLinks);

					}
				}

				JSONArray jsonArrayLocations = jsonObjectAdded.optJSONArray("location");


				if(jsonArrayLocations != null && jsonArrayLocations.length() > 0) {

					for (int l = 0; l < jsonArrayLocations.length(); l++) {

						JSONObject jsonObjectLocations = jsonArrayLocations.getJSONObject(l);

						ContentValues valuesLocations = new ContentValues();


						valuesLocations.put(WebServiceAPI.id, jsonObjectLocations.optString(WebServiceAPI.id));
						valuesLocations.put(WebServiceAPI.company_id, jsonObjectLocations.optString(WebServiceAPI.company_id));
						valuesLocations.put(WebServiceAPI.branch_name, jsonObjectLocations.optString(WebServiceAPI.branch_name));
						valuesLocations.put(WebServiceAPI.location, jsonObjectLocations.optString(WebServiceAPI.location));
						valuesLocations.put(WebServiceAPI.lat, jsonObjectLocations.optString(WebServiceAPI.lat));
						valuesLocations.put(WebServiceAPI.longitude, jsonObjectLocations.optString(WebServiceAPI.longitude));

						database.insert(WebServiceAPI.PortfolioLocations, null, valuesLocations);

					}
				}

				database.insert(WebServiceAPI.Portfolio, null, values);

				SharedPrefrenceUtil.setPrefrence(context, Constants.isFirstTime, "Database_portfolio");
			}

			database.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}



	public void insertAllStates(Activity activity, JSONArray jsonArray)throws JSONException {
		// TODO Auto-generated method stub
		try {
			SQLiteDatabase database = this.getWritableDatabase();

			for (int i = 0; i < jsonArray.length(); i++) {

				JSONObject jsonObjectAdded = jsonArray.getJSONObject(i);
				ContentValues values = new ContentValues();

				values.put(WebServiceAPI.id, jsonObjectAdded.optString(WebServiceAPI.id));
				values.put(WebServiceAPI.country_id, jsonObjectAdded.optString(WebServiceAPI.country_id));
				values.put(WebServiceAPI.name, jsonObjectAdded.optString(WebServiceAPI.name));
				values.put(WebServiceAPI.abr, jsonObjectAdded.optString(WebServiceAPI.abr));
				values.put(WebServiceAPI.map_reference, jsonObjectAdded.optString(WebServiceAPI.map_reference));
				values.put(WebServiceAPI.latitude, jsonObjectAdded.optString(WebServiceAPI.latitude));
				values.put(WebServiceAPI.state_longitude, jsonObjectAdded.optString(WebServiceAPI.state_longitude));
				values.put(WebServiceAPI.status, jsonObjectAdded.optString(WebServiceAPI.status));
				values.put(WebServiceAPI.date, jsonObjectAdded.optString(WebServiceAPI.date));

				database.insert(WebServiceAPI.States, null, values);

				SharedPrefrenceUtil.setPrefrence(activity,Constants.isStates,"Database_states");

			}

			database.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void insertAllCountries(Activity activity, JSONArray jsonArray)throws JSONException {
		// TODO Auto-generated method stub
		try {
			SQLiteDatabase database = this.getWritableDatabase();

			for (int i = 0; i < jsonArray.length(); i++) {

				JSONObject jsonObjectAdded = jsonArray.getJSONObject(i);
				ContentValues values = new ContentValues();

				values.put(WebServiceAPI.id, jsonObjectAdded.optString(WebServiceAPI.id));
				values.put(WebServiceAPI.name, jsonObjectAdded.optString(WebServiceAPI.name));
				values.put(WebServiceAPI.abr, jsonObjectAdded.optString(WebServiceAPI.abr));

				database.insert(WebServiceAPI.Countries, null, values);

				SharedPrefrenceUtil.setPrefrence(activity,Constants.isCountries,"Database_countries");
			}

			database.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void insertDisciplines(Activity activity, JSONArray jsonArray)throws JSONException {
		// TODO Auto-generated method stub
		try {
			SQLiteDatabase database = this.getWritableDatabase();

			for (int i = 0; i < jsonArray.length(); i++) {

				JSONObject jsonObjectAdded = jsonArray.getJSONObject(i);
				ContentValues values = new ContentValues();

				values.put(WebServiceAPI.id, jsonObjectAdded.optString(WebServiceAPI.id));
				values.put(WebServiceAPI.name, jsonObjectAdded.optString(WebServiceAPI.name));

				JSONArray jsonArraySubDis = jsonObjectAdded.getJSONArray("subdiscipline");

				if(jsonArraySubDis != null && jsonArraySubDis.length() > 0){

					for(int j = 0; j < jsonArraySubDis.length(); j++){

						JSONObject jsonObjectSubDis = jsonArraySubDis.getJSONObject(j);

						ContentValues valuesSubDis = new ContentValues();

						valuesSubDis.put(WebServiceAPI.date, jsonObjectSubDis.optString(WebServiceAPI.date));
						valuesSubDis.put(WebServiceAPI.id, jsonObjectSubDis.optString(WebServiceAPI.id));
						valuesSubDis.put(WebServiceAPI.name, jsonObjectSubDis.optString(WebServiceAPI.name));
						valuesSubDis.put(WebServiceAPI.discipline_id, jsonObjectSubDis.optString(WebServiceAPI.discipline_id));

						JSONObject object = jsonObjectSubDis.getJSONObject(jsonObjectSubDis.optString(WebServiceAPI.name));

						valuesSubDis.put(WebServiceAPI.companys, object.optString(WebServiceAPI.companys));



						JSONArray jsonArrayAllCompanys = object.getJSONArray("allcompanys");

						if(jsonArrayAllCompanys != null && jsonArrayAllCompanys.length() > 0){

							for(int l = 0; l < jsonArrayAllCompanys.length(); l++) {

								JSONObject jsonObjectAllCompanys = jsonArrayAllCompanys.getJSONObject(l);

								ContentValues valuesAllCompanys = new ContentValues();

								valuesAllCompanys.put(WebServiceAPI.id, jsonObjectAllCompanys.optString(WebServiceAPI.id));
								valuesAllCompanys.put(WebServiceAPI.company_id, jsonObjectAllCompanys.optString(WebServiceAPI.company_id));
								valuesAllCompanys.put(WebServiceAPI.discipline_id, jsonObjectAllCompanys.optString(WebServiceAPI.discipline_id));
								valuesAllCompanys.put(WebServiceAPI.sub_discipline_id, jsonObjectAllCompanys.optString(WebServiceAPI.sub_discipline_id));
								valuesAllCompanys.put(WebServiceAPI.type, jsonObjectAllCompanys.optString(WebServiceAPI.type));
								valuesAllCompanys.put(WebServiceAPI.company_code, jsonObjectAllCompanys.optString(WebServiceAPI.company_code));
								valuesAllCompanys.put(WebServiceAPI.parent_id, jsonObjectAllCompanys.optString(WebServiceAPI.parent_id));
								valuesAllCompanys.put(WebServiceAPI.parent_type, jsonObjectAllCompanys.optString(WebServiceAPI.parent_type));
								valuesAllCompanys.put(WebServiceAPI.bussiness_no, jsonObjectAllCompanys.optString(WebServiceAPI.bussiness_no));
								valuesAllCompanys.put(WebServiceAPI.company_name, jsonObjectAllCompanys.optString(WebServiceAPI.company_name));
								valuesAllCompanys.put(WebServiceAPI.company_intro, jsonObjectAllCompanys.optString(WebServiceAPI.company_intro));
								valuesAllCompanys.put(WebServiceAPI.company_type, jsonObjectAllCompanys.optString(WebServiceAPI.company_type));
								valuesAllCompanys.put(WebServiceAPI.website, jsonObjectAllCompanys.optString(WebServiceAPI.website));
								valuesAllCompanys.put(WebServiceAPI.logo, jsonObjectAllCompanys.optString(WebServiceAPI.logo));
								valuesAllCompanys.put(WebServiceAPI.rating, jsonObjectAllCompanys.optString(WebServiceAPI.rating));
								valuesAllCompanys.put(WebServiceAPI.awards, jsonObjectAllCompanys.optString(WebServiceAPI.awards));
								valuesAllCompanys.put(WebServiceAPI.email, jsonObjectAllCompanys.optString(WebServiceAPI.email));
								valuesAllCompanys.put(WebServiceAPI.password, jsonObjectAllCompanys.optString(WebServiceAPI.password));
								valuesAllCompanys.put(WebServiceAPI.pass, jsonObjectAllCompanys.optString(WebServiceAPI.pass));
								valuesAllCompanys.put(WebServiceAPI.description, jsonObjectAllCompanys.optString(WebServiceAPI.description));
								valuesAllCompanys.put(WebServiceAPI.contact_name, jsonObjectAllCompanys.optString(WebServiceAPI.contact_name));
								valuesAllCompanys.put(WebServiceAPI.contact_email, jsonObjectAllCompanys.optString(WebServiceAPI.contact_email));
								valuesAllCompanys.put(WebServiceAPI.phone, jsonObjectAllCompanys.optString(WebServiceAPI.phone));
								valuesAllCompanys.put(WebServiceAPI.phonecode, jsonObjectAllCompanys.optString(WebServiceAPI.phonecode));
								valuesAllCompanys.put(WebServiceAPI.mobilecode, jsonObjectAllCompanys.optString(WebServiceAPI.mobilecode));
								valuesAllCompanys.put(WebServiceAPI.mobile, jsonObjectAllCompanys.optString(WebServiceAPI.mobile));
								valuesAllCompanys.put(WebServiceAPI.general_no, jsonObjectAllCompanys.optString(WebServiceAPI.general_no));
								valuesAllCompanys.put(WebServiceAPI.company_address, jsonObjectAllCompanys.optString(WebServiceAPI.company_address));
								valuesAllCompanys.put(WebServiceAPI.city, jsonObjectAllCompanys.optString(WebServiceAPI.city));
								valuesAllCompanys.put(WebServiceAPI.country_id, jsonObjectAllCompanys.optString(WebServiceAPI.country_id));
								valuesAllCompanys.put(WebServiceAPI.state_id, jsonObjectAllCompanys.optString(WebServiceAPI.state_id));
								valuesAllCompanys.put(WebServiceAPI.notes_CRM, jsonObjectAllCompanys.optString(WebServiceAPI.notes_CRM));
								valuesAllCompanys.put(WebServiceAPI.status, jsonObjectAllCompanys.optString(WebServiceAPI.status));
								valuesAllCompanys.put(WebServiceAPI.first_login_status, jsonObjectAllCompanys.optString(WebServiceAPI.first_login_status));
								valuesAllCompanys.put(WebServiceAPI.change_pass, jsonObjectAllCompanys.optString(WebServiceAPI.change_pass));
								valuesAllCompanys.put(WebServiceAPI.lock, jsonObjectAllCompanys.optString(WebServiceAPI.lock));
								valuesAllCompanys.put(WebServiceAPI.user_type, jsonObjectAllCompanys.optString(WebServiceAPI.user_type));
								valuesAllCompanys.put(WebServiceAPI.package_id, jsonObjectAllCompanys.optString(WebServiceAPI.package_id));
								valuesAllCompanys.put(WebServiceAPI.messaging_country, jsonObjectAllCompanys.optString(WebServiceAPI.messaging_country));
								valuesAllCompanys.put(WebServiceAPI.application_due_date, jsonObjectAllCompanys.optString(WebServiceAPI.application_due_date));
								valuesAllCompanys.put(WebServiceAPI.key, jsonObjectAllCompanys.optString(WebServiceAPI.key));
								valuesAllCompanys.put(WebServiceAPI.message_limit, jsonObjectAllCompanys.optString(WebServiceAPI.message_limit));
								valuesAllCompanys.put(WebServiceAPI.send_message, jsonObjectAllCompanys.optString(WebServiceAPI.send_message));
								valuesAllCompanys.put(WebServiceAPI.date, jsonObjectAllCompanys.optString(WebServiceAPI.date));
								valuesAllCompanys.put(WebServiceAPI.modified_date, jsonObjectAllCompanys.optString(WebServiceAPI.modified_date));
								valuesAllCompanys.put(WebServiceAPI.added_by, jsonObjectAllCompanys.optString(WebServiceAPI.added_by));
								valuesAllCompanys.put(WebServiceAPI.modified_by, jsonObjectAllCompanys.optString(WebServiceAPI.modified_by));
								valuesAllCompanys.put(WebServiceAPI.package_start, jsonObjectAllCompanys.optString(WebServiceAPI.package_start));
								valuesAllCompanys.put(WebServiceAPI.package_end, jsonObjectAllCompanys.optString(WebServiceAPI.package_end));

								database.insert(WebServiceAPI.AllCompanys, null, valuesAllCompanys);
							}
						}

						database.insert(WebServiceAPI.SubDisciplines, null, valuesSubDis);
					}
				}
				database.insert(WebServiceAPI.Disciplines, null, values);
				SharedPrefrenceUtil.setPrefrence(context, Constants.isDiscipline, "Database_disciplines");
			}

			database.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void insertDisciplineFilter(Activity activity, JSONArray jsonArray)throws JSONException {
		// TODO Auto-generated method stub
		try {
			SQLiteDatabase database = this.getWritableDatabase();

			for (int i = 0; i < jsonArray.length(); i++) {

				JSONObject jsonObjectAdded = jsonArray.getJSONObject(i);
				ContentValues values = new ContentValues();

				values.put(WebServiceAPI.id, jsonObjectAdded.optString(WebServiceAPI.id));
				values.put(WebServiceAPI.name, jsonObjectAdded.optString(WebServiceAPI.name));

				database.insert(WebServiceAPI.DisciplinesFilter, null, values);

				JSONArray jsonArraySubDis = jsonObjectAdded.getJSONArray("subdiscipline");

				if(jsonArraySubDis != null && jsonArraySubDis.length() > 0){

					for(int j = 0; j < jsonArraySubDis.length(); j++){

						JSONObject jsonObjectSubDis = jsonArraySubDis.getJSONObject(j);

						ContentValues valuesSubDis = new ContentValues();

						valuesSubDis.put(WebServiceAPI.date, jsonObjectSubDis.optString(WebServiceAPI.date));
						valuesSubDis.put(WebServiceAPI.id, jsonObjectSubDis.optString(WebServiceAPI.id));
						valuesSubDis.put(WebServiceAPI.name, jsonObjectSubDis.optString(WebServiceAPI.name));
						valuesSubDis.put(WebServiceAPI.discipline_id, jsonObjectSubDis.optString(WebServiceAPI.discipline_id));

						//JSONObject object = jsonObjectSubDis.getJSONObject(jsonObjectSubDis.optString(WebServiceAPI.name));

						valuesSubDis.put(WebServiceAPI.companys, jsonObjectSubDis.optString(WebServiceAPI.companys));

						database.insert(WebServiceAPI.SubDisciplinesFilter, null, valuesSubDis);
					}
				}

				SharedPrefrenceUtil.setPrefrence(context, Constants.isDisciplineFilter, "Database_disciplinesFilter");
			}

			database.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void insertMessages(Activity activity, JSONArray jsonArray) throws JSONException{
		try {
			SQLiteDatabase database = this.getWritableDatabase();

			for (int i = 0; i < jsonArray.length(); i++) {

				JSONObject jsonObjectAdded = jsonArray.getJSONObject(i);
				ContentValues values = new ContentValues();

				values.put(WebServiceAPI.id, jsonObjectAdded.optString(WebServiceAPI.id));
				values.put(WebServiceAPI.type, jsonObjectAdded.optString(WebServiceAPI.type));
				values.put(WebServiceAPI.adder_id, jsonObjectAdded.optString(WebServiceAPI.adder_id));
				values.put(WebServiceAPI.sub_adder_id, jsonObjectAdded.optString(WebServiceAPI.sub_adder_id));
				values.put(WebServiceAPI.message_type, jsonObjectAdded.optString(WebServiceAPI.message_type));
				values.put(WebServiceAPI.event_title, jsonObjectAdded.optString(WebServiceAPI.event_title));
				values.put(WebServiceAPI.event_date, jsonObjectAdded.optString(WebServiceAPI.event_date));
				values.put(WebServiceAPI.eventtimefrom, jsonObjectAdded.optString(WebServiceAPI.eventtimefrom));
				values.put(WebServiceAPI.eventtimeto, jsonObjectAdded.optString(WebServiceAPI.eventtimeto));
				values.put(WebServiceAPI.event_venue, jsonObjectAdded.optString(WebServiceAPI.event_venue));
				values.put(WebServiceAPI.message, jsonObjectAdded.optString(WebServiceAPI.message));
				values.put(WebServiceAPI.gender, jsonObjectAdded.optString(WebServiceAPI.gender));
				values.put(WebServiceAPI.send_by, jsonObjectAdded.optString(WebServiceAPI.send_by));
				values.put(WebServiceAPI.recall, jsonObjectAdded.optString(WebServiceAPI.recall));
				values.put(WebServiceAPI.status, jsonObjectAdded.optString(WebServiceAPI.status));
				values.put(WebServiceAPI.like_status, jsonObjectAdded.optString(WebServiceAPI.like_status));
				values.put(WebServiceAPI.report, jsonObjectAdded.optString(WebServiceAPI.report));
				values.put(WebServiceAPI.date, jsonObjectAdded.optString(WebServiceAPI.date));
				values.put(WebServiceAPI.website, jsonObjectAdded.optString(WebServiceAPI.website));
				values.put(WebServiceAPI.addedStatus, jsonObjectAdded.optString(WebServiceAPI.addedStatus));
				values.put(WebServiceAPI.eventstatus, jsonObjectAdded.optString(WebServiceAPI.eventstatus));
				values.put(WebServiceAPI.international, jsonObjectAdded.optString(WebServiceAPI.international));
				values.put(WebServiceAPI.eventconfirmation, jsonObjectAdded.optString(WebServiceAPI.eventconfirmation));
				values.put(WebServiceAPI.default_timezone, jsonObjectAdded.optString(WebServiceAPI.default_timezone));
				values.put(WebServiceAPI.perfect_logo, jsonObjectAdded.optString(WebServiceAPI.perfect_logo));

				JSONObject jsonObjectAdder = jsonObjectAdded.getJSONObject("adder_detail");

				if(jsonObjectAdder != null && jsonObjectAdder.length() > 0){

					ContentValues valuesAdder = new ContentValues();

					valuesAdder.put(WebServiceAPI.id, jsonObjectAdder.optString(WebServiceAPI.id));
					valuesAdder.put(WebServiceAPI.name, jsonObjectAdder.optString(WebServiceAPI.name));
					valuesAdder.put(WebServiceAPI.logo, jsonObjectAdder.optString(WebServiceAPI.logo));
					valuesAdder.put(WebServiceAPI.package_id, jsonObjectAdder.optString(WebServiceAPI.package_id));

					database.insert(WebServiceAPI.AdderDetail, null, valuesAdder);
				}

				database.insert(WebServiceAPI.Messages, null, values);

				SharedPrefrenceUtil.setPrefrence(context, Constants.isMessages, "Database_messages");
			}

			database.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void insertSalary(HomeActivity activity, JSONArray jsonArray)  throws JSONException{
		try {
			SQLiteDatabase database = this.getWritableDatabase();

			for (int i = 0; i < jsonArray.length(); i++) {

				JSONObject jsonObjectAdded = jsonArray.getJSONObject(i);
				ContentValues values = new ContentValues();

				values.put(WebServiceAPI.id, jsonObjectAdded.optString(WebServiceAPI.id));
				values.put(WebServiceAPI.type, jsonObjectAdded.optString(WebServiceAPI.type));
				values.put(WebServiceAPI.year, jsonObjectAdded.optString(WebServiceAPI.year));
				values.put(WebServiceAPI.country_id, jsonObjectAdded.optString(WebServiceAPI.country_id));
				values.put(WebServiceAPI.sector_id, jsonObjectAdded.optString(WebServiceAPI.sector_id));
				values.put(WebServiceAPI.occupation_id, jsonObjectAdded.optString(WebServiceAPI.occupation_id));
				values.put(WebServiceAPI.title, jsonObjectAdded.optString(WebServiceAPI.title));
				values.put(WebServiceAPI.salary0to3, jsonObjectAdded.optString(WebServiceAPI.salary0to3));
				values.put(WebServiceAPI.salary3to5, jsonObjectAdded.optString(WebServiceAPI.salary3to5));
				values.put(WebServiceAPI.salary5to7, jsonObjectAdded.optString(WebServiceAPI.salary5to7));
				values.put(WebServiceAPI.smallMedium, jsonObjectAdded.optString(WebServiceAPI.smallMedium));
				values.put(WebServiceAPI.large, jsonObjectAdded.optString(WebServiceAPI.large));
				values.put(WebServiceAPI.status, jsonObjectAdded.optString(WebServiceAPI.status));
				values.put(WebServiceAPI.date, jsonObjectAdded.optString(WebServiceAPI.date));
				values.put(WebServiceAPI.occupation, jsonObjectAdded.optString(WebServiceAPI.occupation));
				values.put(WebServiceAPI.sector, jsonObjectAdded.optString(WebServiceAPI.sector));


				database.insert(WebServiceAPI.Salary, null, values);

				SharedPrefrenceUtil.setPrefrence(context, Constants.isSalary, "Database_salary");
			}

			database.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public List<SocietyVo> getSocietyDetail() {

		List<SocietyVo> societyVoList = new ArrayList<>();

		String selectQuery = "Select * from SocietyDetail";

		SQLiteDatabase database = this.getWritableDatabase();
		Cursor cursor = database.rawQuery(selectQuery, null);
		if (cursor != null && cursor.getCount() > 0) {
			if (cursor.moveToFirst()) {
				do {

					SocietyVo societyVo = new SocietyVo();

					societyVo.setId(cursor.getString(0));
					societyVo.setSociety_id(cursor.getString(1));
					societyVo.setParent_id(cursor.getString(2));
					societyVo.setStaff_id(cursor.getString(3));
					societyVo.setCreator_name(cursor.getString(4));
					societyVo.setWebsite(cursor.getString(5));
					societyVo.setSociety_email(cursor.getString(6));
					societyVo.setPassword(cursor.getString(7));
					societyVo.setFirst_login_status(cursor.getString(8));
					societyVo.setChange_pass(cursor.getString(9));
					societyVo.setPass(cursor.getString(10));
					societyVo.setUni_id(cursor.getString(11));
					societyVo.setCountry_id(cursor.getString(12));
					societyVo.setState_id(cursor.getString(13));
					societyVo.setSocietylogo(cursor.getString(14));
					societyVo.setCreator_name(cursor.getString(15));
					societyVo.setSociety_name(cursor.getString(16));
					societyVo.setSociety_president(cursor.getString(17));
					societyVo.setPresident_email(cursor.getString(18));
					societyVo.setPresident_mobile(cursor.getString(19));
					societyVo.setTreasurer_name(cursor.getString(20));
					societyVo.setTreasurer_email(cursor.getString(21));
					societyVo.setTreasure_mobile(cursor.getString(22));
					societyVo.setSecetery_name(cursor.getString(23));
					societyVo.setSecetery_email(cursor.getString(24));
					societyVo.setSecetery_mobile(cursor.getString(25));
					societyVo.setFacebookemail(cursor.getString(26));
					societyVo.setUser_type(cursor.getString(27));
					societyVo.setStatus(cursor.getString(28));
					societyVo.setLocks(cursor.getString(29));
					societyVo.setDate(cursor.getString(30));
					societyVo.setKey(cursor.getString(31));
					societyVo.setCreated_by(cursor.getString(32));
					societyVo.setModified_date(cursor.getString(33));
					societyVo.setSociety_code(cursor.getString(34));
					societyVo.setLogo(cursor.getString(35));
					societyVo.setUnread_message(cursor.getString(36));
					societyVoList.add(societyVo);
				} while (cursor.moveToNext());
			}
		}

		cursor.close();
		database.close();

		return societyVoList;
	}

	public List<UserCompanyVo> getUserCompany() {

		List<UserCompanyVo> userCompanyVoList = new ArrayList<>();

		String selectQuery = "Select * from Portfolio WHERE is_deleted='0'";

		SQLiteDatabase database = this.getWritableDatabase();
		Cursor cursor = database.rawQuery(selectQuery, null);
		if (cursor != null && cursor.getCount() > 0) {
			if (cursor.moveToFirst()) {
				do {
					UserCompanyVo userCompanyVo = new UserCompanyVo();

					userCompanyVo.setId(cursor.getString(0));
					userCompanyVo.setCompany_code(cursor.getString(1));
					userCompanyVo.setParent_id(cursor.getString(2));
					userCompanyVo.setParent_type(cursor.getString(3));
					userCompanyVo.setBussiness_no(cursor.getString(4));
					userCompanyVo.setCompany_name(cursor.getString(5));
					userCompanyVo.setCompany_intro(cursor.getString(6));
					userCompanyVo.setCompany_type(cursor.getString(7));
					userCompanyVo.setWebsite(cursor.getString(8));
					userCompanyVo.setLogo(cursor.getString(9));
					userCompanyVo.setRating(cursor.getString(10));
					userCompanyVo.setAwards(cursor.getString(11));
					userCompanyVo.setEmail(cursor.getString(12));
					userCompanyVo.setPassword(cursor.getString(13));
					userCompanyVo.setPass(cursor.getString(14));
					userCompanyVo.setDescription(cursor.getString(15));
					userCompanyVo.setContact_name(cursor.getString(16));
					userCompanyVo.setContact_email(cursor.getString(17));
					userCompanyVo.setPhone(cursor.getString(18));
					userCompanyVo.setPhonecode(cursor.getString(19));
					userCompanyVo.setMobilecode(cursor.getString(20));
					userCompanyVo.setMobile(cursor.getString(21));
					userCompanyVo.setGeneral_no(cursor.getString(22));
					userCompanyVo.setGeneral_no(cursor.getString(23));
					userCompanyVo.setCity(cursor.getString(24));
					userCompanyVo.setCountry_id(cursor.getString(25));
					userCompanyVo.setState_id(cursor.getString(26));
					userCompanyVo.setNotes_CRM(cursor.getString(27));
					userCompanyVo.setStatus(cursor.getString(28));
					userCompanyVo.setFirst_login_status(cursor.getString(29));
					userCompanyVo.setChange_pass(cursor.getString(30));
					userCompanyVo.setLock(cursor.getString(31));
					userCompanyVo.setUser_type(cursor.getString(32));
					userCompanyVo.setPackage_id(cursor.getString(33));
					userCompanyVo.setMessaging_country(cursor.getString(34));
					userCompanyVo.setApplication_due_date(cursor.getString(35));
					userCompanyVo.setKey(cursor.getString(36));
					userCompanyVo.setMessage_limit(cursor.getString(37));
					userCompanyVo.setSend_message(cursor.getString(38));
					userCompanyVo.setDate(cursor.getString(39));
					userCompanyVo.setModified_date(cursor.getString(40));
					userCompanyVo.setAdded_by(cursor.getString(41));
					userCompanyVo.setModified_by(cursor.getString(42));
					userCompanyVo.setPackage_start(cursor.getString(43));
					userCompanyVo.setPackage_end(cursor.getString(44));
					userCompanyVo.setCountry(cursor.getString(45));
					userCompanyVo.setState(cursor.getString(46));
					userCompanyVo.setAbr(cursor.getString(47));
					userCompanyVo.setIs_deleted(cursor.getString(48));
					userCompanyVo.setInternational(cursor.getString(49));
					userCompanyVo.setMessage_status(cursor.getString(50));
					userCompanyVo.setUnread_message(cursor.getString(51));
					userCompanyVo.setRegstates(cursor.getString(52));
					userCompanyVo.setPerfect_logo(cursor.getString(53));

					userCompanyVoList.add(userCompanyVo);

				} while (cursor.moveToNext());
			}
		}

		cursor.close();
		database.close();

		return  userCompanyVoList;
	}

	public List<MessagesVo> getMessages(){

		List<MessagesVo> messagesVoList = new ArrayList<>();

		String selectQuery = "Select * from Messages";

		SQLiteDatabase database = this.getWritableDatabase();
		Cursor cursor = database.rawQuery(selectQuery, null);
		if (cursor != null && cursor.getCount() > 0) {
			if (cursor.moveToFirst()) {
				do {
					MessagesVo messagesVo = new MessagesVo();

					messagesVo.setId(cursor.getString(0));
					messagesVo.setType(cursor.getString(1));
					messagesVo.setAdder_id(cursor.getString(2));
					messagesVo.setSub_adder_id(cursor.getString(3));
					messagesVo.setMessage_type(cursor.getString(4));
					messagesVo.setEvent_title(cursor.getString(5));
					messagesVo.setEvent_date(cursor.getString(6));
					messagesVo.setEventtimefrom(cursor.getString(7));
					messagesVo.setEventtimeto(cursor.getString(8));
					messagesVo.setEvent_venue(cursor.getString(9));
					messagesVo.setMessage(cursor.getString(10));
					messagesVo.setGender(cursor.getString(11));
					messagesVo.setSend_by(cursor.getString(12));
					messagesVo.setRecall(cursor.getString(13));
					messagesVo.setStatus(cursor.getString(14));
					messagesVo.setLike_status(cursor.getString(15));
					messagesVo.setReport(cursor.getString(16));
					messagesVo.setDate(cursor.getString(17));
					messagesVo.setWebsite(cursor.getString(18));
					messagesVo.setAddedStatus(cursor.getString(19));
					messagesVo.setEventstatus(cursor.getString(20));
					messagesVo.setInternational(cursor.getString(21));
					messagesVo.setEventconfirmation(cursor.getString(22));
					messagesVo.setDefault_timezone(cursor.getString(23));
					messagesVo.setPerfect_logo(cursor.getString(24));

					messagesVoList.add(messagesVo);

				} while (cursor.moveToNext());
			}
		}

		cursor.close();
		database.close();

		return messagesVoList;
	}

	public List<AdderDetailVo> getAdder(String id){

		List<AdderDetailVo> adderDetailVoList = new ArrayList<>();

		String selectQuery = "Select * from AdderDetail WHERE id='" + id + "'";

		SQLiteDatabase database = this.getWritableDatabase();
		Cursor cursor = database.rawQuery(selectQuery, null);
		if (cursor != null && cursor.getCount() > 0) {
			if (cursor.moveToFirst()) {
				do {
					AdderDetailVo adderDetailVo = new AdderDetailVo();

					adderDetailVo.setId(cursor.getString(0));
					adderDetailVo.setName(cursor.getString(1));
					adderDetailVo.setLogo(cursor.getString(2));
					adderDetailVo.setPackage_id(cursor.getString(3));

					adderDetailVoList.add(adderDetailVo);

				} while (cursor.moveToNext());
			}
		}

		cursor.close();
		database.close();

		return adderDetailVoList;
	}


	public List<UserCompanyVo> getAllCompany() {

		List<UserCompanyVo> userCompanyVoList = new ArrayList<>();

		String selectQuery = "Select * from Companies";

		SQLiteDatabase database = this.getWritableDatabase();
		Cursor cursor = database.rawQuery(selectQuery, null);
		if (cursor != null && cursor.getCount() > 0) {
			if (cursor.moveToFirst()) {
				do {
					UserCompanyVo userCompanyVo = new UserCompanyVo();

					userCompanyVo.setId(cursor.getString(0));
					userCompanyVo.setCompany_code(cursor.getString(1));
					userCompanyVo.setParent_id(cursor.getString(2));
					userCompanyVo.setParent_type(cursor.getString(3));
					userCompanyVo.setBussiness_no(cursor.getString(4));
					userCompanyVo.setCompany_name(cursor.getString(5));
					userCompanyVo.setCompany_intro(cursor.getString(6));
					userCompanyVo.setCompany_type(cursor.getString(7));
					userCompanyVo.setWebsite(cursor.getString(8));
					userCompanyVo.setLogo(cursor.getString(9));
					userCompanyVo.setRating(cursor.getString(10));
					userCompanyVo.setAwards(cursor.getString(11));
					userCompanyVo.setEmail(cursor.getString(12));
					userCompanyVo.setPassword(cursor.getString(13));
					userCompanyVo.setPass(cursor.getString(14));
					userCompanyVo.setDescription(cursor.getString(15));
					userCompanyVo.setContact_name(cursor.getString(16));
					userCompanyVo.setContact_email(cursor.getString(17));
					userCompanyVo.setPhone(cursor.getString(18));
					userCompanyVo.setPhonecode(cursor.getString(19));
					userCompanyVo.setMobilecode(cursor.getString(20));
					userCompanyVo.setMobile(cursor.getString(21));
					userCompanyVo.setGeneral_no(cursor.getString(22));
					userCompanyVo.setCompany_address(cursor.getString(23));
					userCompanyVo.setCity(cursor.getString(24));
					userCompanyVo.setCountry_id(cursor.getString(25));
					userCompanyVo.setState_id(cursor.getString(26));
					userCompanyVo.setNotes_CRM(cursor.getString(27));
					userCompanyVo.setStatus(cursor.getString(28));
					userCompanyVo.setFirst_login_status(cursor.getString(29));
					userCompanyVo.setChange_pass(cursor.getString(30));
					userCompanyVo.setLock(cursor.getString(31));
					userCompanyVo.setUser_type(cursor.getString(32));
					userCompanyVo.setPackage_id(cursor.getString(33));
					userCompanyVo.setMessaging_country(cursor.getString(34));
					userCompanyVo.setApplication_due_date(cursor.getString(35));
					userCompanyVo.setKey(cursor.getString(36));
					userCompanyVo.setMessage_limit(cursor.getString(37));
					userCompanyVo.setSend_message(cursor.getString(38));
					userCompanyVo.setDate(cursor.getString(39));
					userCompanyVo.setModified_date(cursor.getString(40));
					userCompanyVo.setAdded_by(cursor.getString(41));
					userCompanyVo.setModified_by(cursor.getString(42));
					userCompanyVo.setPackage_start(cursor.getString(43));
					userCompanyVo.setPackage_end(cursor.getString(44));
					userCompanyVo.setCountry(cursor.getString(45));
					userCompanyVo.setInternational(cursor.getString(46));
					userCompanyVo.setMessage_events(cursor.getString(47));
					userCompanyVo.setRegstates(cursor.getString(48));
					userCompanyVo.setMessage_state(cursor.getString(49));
					userCompanyVo.setMessage_status(cursor.getString(50));
					userCompanyVo.setUnread_message(cursor.getString(51));
					userCompanyVo.setPortfolio_status(cursor.getString(52));

					userCompanyVoList.add(userCompanyVo);

				} while (cursor.moveToNext());
			}
		}

		cursor.close();
		database.close();

		return  userCompanyVoList;
	}

	public List<UserCompanyVo> getAllCompanyDetail(String id) {

		List<UserCompanyVo> userCompanyVoList = new ArrayList<UserCompanyVo>();

		String selectQuery = "Select * from Companies WHERE id='" + id + "'";

		SQLiteDatabase database = this.getWritableDatabase();
		Cursor cursor = database.rawQuery(selectQuery, null);
		if (cursor != null && cursor.getCount() > 0) {
			if (cursor.moveToFirst()) {
				do {
					UserCompanyVo userCompanyVo = new UserCompanyVo();

					userCompanyVo.setId(cursor.getString(0));
					userCompanyVo.setCompany_code(cursor.getString(1));
					userCompanyVo.setParent_id(cursor.getString(2));
					userCompanyVo.setParent_type(cursor.getString(3));
					userCompanyVo.setBussiness_no(cursor.getString(4));
					userCompanyVo.setCompany_name(cursor.getString(5));
					userCompanyVo.setCompany_intro(cursor.getString(6));
					userCompanyVo.setCompany_type(cursor.getString(7));
					userCompanyVo.setWebsite(cursor.getString(8));
					userCompanyVo.setLogo(cursor.getString(9));
					userCompanyVo.setRating(cursor.getString(10));
					userCompanyVo.setAwards(cursor.getString(11));
					userCompanyVo.setEmail(cursor.getString(12));
					userCompanyVo.setPassword(cursor.getString(13));
					userCompanyVo.setPass(cursor.getString(14));
					userCompanyVo.setDescription(cursor.getString(15));
					userCompanyVo.setContact_name(cursor.getString(16));
					userCompanyVo.setContact_email(cursor.getString(17));
					userCompanyVo.setPhone(cursor.getString(18));
					userCompanyVo.setPhonecode(cursor.getString(19));
					userCompanyVo.setMobilecode(cursor.getString(20));
					userCompanyVo.setMobile(cursor.getString(21));
					userCompanyVo.setGeneral_no(cursor.getString(22));
					userCompanyVo.setCompany_address(cursor.getString(23));
					userCompanyVo.setCity(cursor.getString(24));
					userCompanyVo.setCountry_id(cursor.getString(25));
					userCompanyVo.setState_id(cursor.getString(26));
					userCompanyVo.setNotes_CRM(cursor.getString(27));
					userCompanyVo.setStatus(cursor.getString(28));
					userCompanyVo.setFirst_login_status(cursor.getString(29));
					userCompanyVo.setChange_pass(cursor.getString(30));
					userCompanyVo.setLock(cursor.getString(31));
					userCompanyVo.setUser_type(cursor.getString(32));
					userCompanyVo.setPackage_id(cursor.getString(33));
					userCompanyVo.setMessaging_country(cursor.getString(34));
					userCompanyVo.setApplication_due_date(cursor.getString(35));
					userCompanyVo.setKey(cursor.getString(36));
					userCompanyVo.setMessage_limit(cursor.getString(37));
					userCompanyVo.setSend_message(cursor.getString(38));
					userCompanyVo.setDate(cursor.getString(39));
					userCompanyVo.setModified_date(cursor.getString(40));
					userCompanyVo.setAdded_by(cursor.getString(41));
					userCompanyVo.setModified_by(cursor.getString(42));
					userCompanyVo.setPackage_start(cursor.getString(43));
					userCompanyVo.setPackage_end(cursor.getString(44));
					userCompanyVo.setCountry(cursor.getString(45));
					userCompanyVo.setInternational(cursor.getString(46));
					userCompanyVo.setMessage_events(cursor.getString(47));
					userCompanyVo.setRegstates(cursor.getString(48));
					userCompanyVo.setMessage_state(cursor.getString(49));
					userCompanyVo.setMessage_status(cursor.getString(50));
					userCompanyVo.setUnread_message(cursor.getString(51));
					userCompanyVo.setPortfolio_status(cursor.getString(52));

					userCompanyVoList.add(userCompanyVo);

				} while (cursor.moveToNext());
			}
		}

		cursor.close();
		database.close();

		return  userCompanyVoList;
	}

	public List<UserCompanyVo> getAllCompanyFilter() {

		List<UserCompanyVo> userCompanyVoList = new ArrayList<>();

		String selectQuery = "Select * from CompanyFilter";

		SQLiteDatabase database = this.getWritableDatabase();
		Cursor cursor = database.rawQuery(selectQuery, null);
		if (cursor != null && cursor.getCount() > 0) {
			if (cursor.moveToFirst()) {
				do {
					UserCompanyVo userCompanyVo = new UserCompanyVo();

					userCompanyVo.setId(cursor.getString(0));
					userCompanyVo.setCompany_code(cursor.getString(1));
					userCompanyVo.setParent_id(cursor.getString(2));
					userCompanyVo.setParent_type(cursor.getString(3));
					userCompanyVo.setBussiness_no(cursor.getString(4));
					userCompanyVo.setCompany_name(cursor.getString(5));
					userCompanyVo.setCompany_intro(cursor.getString(6));
					userCompanyVo.setCompany_type(cursor.getString(7));
					userCompanyVo.setWebsite(cursor.getString(8));
					userCompanyVo.setLogo(cursor.getString(9));
					userCompanyVo.setRating(cursor.getString(10));
					userCompanyVo.setAwards(cursor.getString(11));
					userCompanyVo.setEmail(cursor.getString(12));
					userCompanyVo.setPassword(cursor.getString(13));
					userCompanyVo.setPass(cursor.getString(14));
					userCompanyVo.setDescription(cursor.getString(15));
					userCompanyVo.setContact_name(cursor.getString(16));
					userCompanyVo.setContact_email(cursor.getString(17));
					userCompanyVo.setPhone(cursor.getString(18));
					userCompanyVo.setPhonecode(cursor.getString(19));
					userCompanyVo.setMobilecode(cursor.getString(20));
					userCompanyVo.setMobile(cursor.getString(21));
					userCompanyVo.setGeneral_no(cursor.getString(22));
					userCompanyVo.setCompany_address(cursor.getString(23));
					userCompanyVo.setCity(cursor.getString(24));
					userCompanyVo.setCountry_id(cursor.getString(25));
					userCompanyVo.setState_id(cursor.getString(26));
					userCompanyVo.setNotes_CRM(cursor.getString(27));
					userCompanyVo.setStatus(cursor.getString(28));
					userCompanyVo.setFirst_login_status(cursor.getString(29));
					userCompanyVo.setChange_pass(cursor.getString(30));
					userCompanyVo.setLock(cursor.getString(31));
					userCompanyVo.setUser_type(cursor.getString(32));
					userCompanyVo.setPackage_id(cursor.getString(33));
					userCompanyVo.setMessaging_country(cursor.getString(34));
					userCompanyVo.setApplication_due_date(cursor.getString(35));
					userCompanyVo.setKey(cursor.getString(36));
					userCompanyVo.setMessage_limit(cursor.getString(37));
					userCompanyVo.setSend_message(cursor.getString(38));
					userCompanyVo.setDate(cursor.getString(39));
					userCompanyVo.setModified_date(cursor.getString(40));
					userCompanyVo.setAdded_by(cursor.getString(41));
					userCompanyVo.setModified_by(cursor.getString(42));
					userCompanyVo.setPackage_start(cursor.getString(43));
					userCompanyVo.setPackage_end(cursor.getString(44));
					userCompanyVo.setCountry(cursor.getString(45));
					userCompanyVo.setInternational(cursor.getString(46));
					userCompanyVo.setMessage_events(cursor.getString(47));
					userCompanyVo.setRegstates(cursor.getString(48));
					userCompanyVo.setMessage_state(cursor.getString(49));
					userCompanyVo.setMessage_status(cursor.getString(50));
					userCompanyVo.setUnread_message(cursor.getString(51));
					userCompanyVo.setPortfolio_status(cursor.getString(52));

					userCompanyVoList.add(userCompanyVo);

				} while (cursor.moveToNext());
			}
		}

		cursor.close();
		database.close();
		Log.e("userCompanyVoList"," -- "+userCompanyVoList.size());
		return  userCompanyVoList;
	}

	public List<LocationVo> getLocations(String id) {
		List<LocationVo> locationVoList = new ArrayList<>();

		String selectQuery = "Select * from PortfolioLocations WHERE company_id='" + id + "'";

		SQLiteDatabase database = this.getWritableDatabase();
		Cursor cursor = database.rawQuery(selectQuery, null);
		if (cursor != null && cursor.getCount() > 0) {
			if (cursor.moveToFirst()) {
				do {
					LocationVo locationVo = new LocationVo();

					locationVo.setId(cursor.getString(0));
					locationVo.setCompany_id(cursor.getString(1));
					locationVo.setBranch_name(cursor.getString(2));
					locationVo.setLocation(cursor.getString(3));
					locationVo.setLat(cursor.getString(4));
					locationVo.setLongitude(cursor.getString(5));

					locationVoList.add(locationVo);

				} while (cursor.moveToNext());
			}
		}

		cursor.close();
		database.close();

		return locationVoList;
	}


	public List<LinksVo> getLinks(String id) {
		List<LinksVo> linksVoList = new ArrayList<>();

		String selectQuery = "Select * from PortfolioLinks WHERE company_id='" + id + "'";

		SQLiteDatabase database = this.getWritableDatabase();
		Cursor cursor = database.rawQuery(selectQuery, null);
		if (cursor != null && cursor.getCount() > 0) {
			if (cursor.moveToFirst()) {
				do {
					LinksVo linksVo = new LinksVo();

					linksVo.setId(cursor.getString(0));
					linksVo.setCompany_id(cursor.getString(1));
					linksVo.setMedia_type(cursor.getString(2));
					linksVo.setMedia_link(cursor.getString(3));
					linksVo.setMedia_logo(cursor.getString(4));
					linksVo.setDefault_id(cursor.getString(5));

					linksVoList.add(linksVo);

				} while (cursor.moveToNext());
			}
		}

		cursor.close();
		database.close();

		return linksVoList;
	}

	public List<DisciplineVo> getDisciplines() {
		List<DisciplineVo> disciplinesVoList = new ArrayList<>();

		String selectQuery = "Select * from Disciplines";

		SQLiteDatabase database = this.getWritableDatabase();
		Cursor cursor = database.rawQuery(selectQuery, null);
		if (cursor != null && cursor.getCount() > 0) {
			if (cursor.moveToFirst()) {
				do {
					DisciplineVo disciplinesVo = new DisciplineVo();

					disciplinesVo.setId(cursor.getString(0));
					disciplinesVo.setName(cursor.getString(1));

					disciplinesVoList.add(disciplinesVo);

				} while (cursor.moveToNext());
			}
		}

		cursor.close();
		database.close();

		return disciplinesVoList;
	}

	public List<DisciplineVo> getDisciplinesFilter() {
		List<DisciplineVo> disciplinesVoList = new ArrayList<>();

		String selectQuery = "Select * from DisciplinesFilter";

		SQLiteDatabase database = this.getWritableDatabase();
		Cursor cursor = database.rawQuery(selectQuery, null);
		if (cursor != null && cursor.getCount() > 0) {
			if (cursor.moveToFirst()) {
				do {
					DisciplineVo disciplinesVo = new DisciplineVo();

					disciplinesVo.setId(cursor.getString(0));
					disciplinesVo.setName(cursor.getString(1));

					disciplinesVoList.add(disciplinesVo);

				} while (cursor.moveToNext());
			}
		}

		cursor.close();
		database.close();

		return disciplinesVoList;
	}

	public List<SubDisciplineVo> getSubDisciplines(String id) {
		List<SubDisciplineVo> subDisciplineVoList = new ArrayList<>();

		String selectQuery = "Select * from SubDisciplines WHERE discipline_id='" + id + "'";

		SQLiteDatabase database = this.getWritableDatabase();
		Cursor cursor = database.rawQuery(selectQuery, null);
		if (cursor != null && cursor.getCount() > 0) {
			if (cursor.moveToFirst()) {
				do {
					SubDisciplineVo subDisciplineVo = new SubDisciplineVo();

					subDisciplineVo.setDate(cursor.getString(0));
					subDisciplineVo.setId(cursor.getString(1));
					subDisciplineVo.setName(cursor.getString(2));
					subDisciplineVo.setDiscipline_id(cursor.getString(3));
					subDisciplineVo.setCompanys(cursor.getString(4));

					subDisciplineVoList.add(subDisciplineVo);

				} while (cursor.moveToNext());
			}
		}

		cursor.close();
		database.close();

		return subDisciplineVoList;
	}

	public List<SubDisciplineVo> getSubDisciplinesFilter(String id) {
		List<SubDisciplineVo> subDisciplineVoList = new ArrayList<>();

		String selectQuery = "Select * from SubDisciplinesFilter WHERE discipline_id='" + id + "'";

		SQLiteDatabase database = this.getWritableDatabase();
		Cursor cursor = database.rawQuery(selectQuery, null);
		if (cursor != null && cursor.getCount() > 0) {
			if (cursor.moveToFirst()) {
				do {
					SubDisciplineVo subDisciplineVo = new SubDisciplineVo();

					subDisciplineVo.setDate(cursor.getString(0));
					subDisciplineVo.setId(cursor.getString(1));
					subDisciplineVo.setName(cursor.getString(2));
					subDisciplineVo.setDiscipline_id(cursor.getString(3));
					subDisciplineVo.setCompanys(cursor.getString(4));

					subDisciplineVoList.add(subDisciplineVo);

				} while (cursor.moveToNext());
			}
		}

		cursor.close();
		database.close();

		return subDisciplineVoList;
	}

	public List<AllCompanysVo> getAllCompanys(String id) {
		List<AllCompanysVo> allCompanysVoList = new ArrayList<>();

		String selectQuery = "Select * from AllCompanys WHERE discipline_id='" + id + "' GROUP BY company_id";

		SQLiteDatabase database = this.getWritableDatabase();
		Cursor cursor = database.rawQuery(selectQuery, null);
		if (cursor != null && cursor.getCount() > 0) {
			if (cursor.moveToFirst()) {
				do {
					AllCompanysVo allCompanysVo = new AllCompanysVo();


					allCompanysVo.setId(cursor.getString(0));
					allCompanysVo.setCompany_id(cursor.getString(1));
					allCompanysVo.setDiscipline_id(cursor.getString(2));
					allCompanysVo.setSub_discipline_id(cursor.getString(3));
					allCompanysVo.setType(cursor.getString(4));
					allCompanysVo.setCompany_code(cursor.getString(5));
					allCompanysVo.setParent_id(cursor.getString(6));
					allCompanysVo.setParent_type(cursor.getString(7));
					allCompanysVo.setBussiness_no(cursor.getString(8));
					allCompanysVo.setCompany_name(cursor.getString(9));
					allCompanysVo.setCompany_intro(cursor.getString(10));
					allCompanysVo.setCompany_type(cursor.getString(11));
					allCompanysVo.setWebsite(cursor.getString(12));
					allCompanysVo.setLogo(cursor.getString(13));
					allCompanysVo.setRating(cursor.getString(14));
					allCompanysVo.setAwards(cursor.getString(15));
					allCompanysVo.setEmail(cursor.getString(16));
					allCompanysVo.setPassword(cursor.getString(17));
					allCompanysVo.setPass(cursor.getString(18));
					allCompanysVo.setDescription(cursor.getString(19));
					allCompanysVo.setContact_name(cursor.getString(20));
					allCompanysVo.setContact_email(cursor.getString(21));
					allCompanysVo.setPhone(cursor.getString(22));
					allCompanysVo.setPhonecode(cursor.getString(23));
					allCompanysVo.setMobilecode(cursor.getString(24));
					allCompanysVo.setMobile(cursor.getString(25));
					allCompanysVo.setGeneral_no(cursor.getString(26));
					allCompanysVo.setCompany_address(cursor.getString(27));
					allCompanysVo.setCity(cursor.getString(28));
					allCompanysVo.setCountry_id(cursor.getString(29));
					allCompanysVo.setState_id(cursor.getString(30));
					allCompanysVo.setNotes_CRM(cursor.getString(31));
					allCompanysVo.setStatus(cursor.getString(32));
					allCompanysVo.setFirst_login_status(cursor.getString(33));
					allCompanysVo.setChange_pass(cursor.getString(34));
					allCompanysVo.setLock(cursor.getString(35));
					allCompanysVo.setUser_type(cursor.getString(36));
					allCompanysVo.setPackage_id(cursor.getString(37));
					allCompanysVo.setMessaging_country(cursor.getString(38));
					allCompanysVo.setApplication_due_date(cursor.getString(39));
					allCompanysVo.setKey(cursor.getString(40));
					allCompanysVo.setMessage_limit(cursor.getString(41));
					allCompanysVo.setSend_message(cursor.getString(42));
					allCompanysVo.setDate(cursor.getString(43));
					allCompanysVo.setModified_date(cursor.getString(44));
					allCompanysVo.setAdded_by(cursor.getString(45));
					allCompanysVo.setModified_by(cursor.getString(46));
					allCompanysVo.setPackage_start(cursor.getString(47));
					allCompanysVo.setPackage_end(cursor.getString(48));

					allCompanysVoList.add(allCompanysVo);

				} while (cursor.moveToNext());
			}
		}

		cursor.close();
		database.close();

		return allCompanysVoList;
	}

	public List<SalaryVo> getSalary() {
		List<SalaryVo> salaryVoList = new ArrayList<>();

		//String selectQuery = "Select DISTINCT sector from Salary";
		String selectQuery = "Select * from Salary";

		SQLiteDatabase database = this.getWritableDatabase();
		Cursor cursor = database.rawQuery(selectQuery, null);
		if (cursor != null && cursor.getCount() > 0) {
			if (cursor.moveToFirst()) {
				do {
					SalaryVo salaryVo = new SalaryVo();

					salaryVo.setId(cursor.getString(0));
					salaryVo.setType(cursor.getString(1));
					salaryVo.setYear(cursor.getString(2));
					salaryVo.setCountry_id(cursor.getString(3));
					salaryVo.setSector_id(cursor.getString(4));
					salaryVo.setOccupation_id(cursor.getString(5));
					salaryVo.setTitle(cursor.getString(6));
					salaryVo.setSalary0to3(cursor.getString(7));
					salaryVo.setSalary3to5(cursor.getString(8));
					salaryVo.setSalary5to7(cursor.getString(9));
					salaryVo.setSmallMedium(cursor.getString(10));
					salaryVo.setLarge(cursor.getString(11));
					salaryVo.setStatus(cursor.getString(12));
					salaryVo.setDate(cursor.getString(13));
					salaryVo.setOccupation(cursor.getString(14));
					salaryVo.setSector(cursor.getString(15));

					salaryVoList.add(salaryVo);

				} while (cursor.moveToNext());
			}
		}

		cursor.close();
		database.close();

		return salaryVoList;
	}

	public SalaryVo getSalaryDistinct(String sector_id) {
		SalaryVo salaryVo= null;

		String selectQuery = "Select * from Salary WHERE sector='" + sector_id + "' GROUP BY sector";

		SQLiteDatabase database = this.getWritableDatabase();
		Cursor cursor = database.rawQuery(selectQuery, null);
		if (cursor != null && cursor.getCount() > 0) {
			if (cursor.moveToFirst()) {
				do {
					salaryVo = new SalaryVo();
					salaryVo.setId(cursor.getString(0));
					salaryVo.setType(cursor.getString(1));
					salaryVo.setYear(cursor.getString(2));
					salaryVo.setCountry_id(cursor.getString(3));
					salaryVo.setSector_id(cursor.getString(4));
					salaryVo.setOccupation_id(cursor.getString(5));
					salaryVo.setTitle(cursor.getString(6));
					salaryVo.setSalary0to3(cursor.getString(7));
					salaryVo.setSalary3to5(cursor.getString(8));
					salaryVo.setSalary5to7(cursor.getString(9));
					salaryVo.setSmallMedium(cursor.getString(10));
					salaryVo.setLarge(cursor.getString(11));
					salaryVo.setStatus(cursor.getString(12));
					salaryVo.setDate(cursor.getString(13));
					salaryVo.setOccupation(cursor.getString(14));
					salaryVo.setSector(cursor.getString(15));


				} while (cursor.moveToNext());
			}
		}
		Log.e("cursor count", " -- "+cursor.getCount());
		cursor.close();
		database.close();

		return salaryVo;
	}

	public List<StateVo> getStateList(){

		List<StateVo> stateVoList = new ArrayList<>();

		String selectQuery = "Select * from States";

		SQLiteDatabase database = this.getWritableDatabase();
		Cursor cursor = database.rawQuery(selectQuery, null);
		if (cursor != null && cursor.getCount() > 0) {
			if (cursor.moveToFirst()) {
				do {
					StateVo stateVo = new StateVo();

					stateVo.setId(cursor.getString(0));
					stateVo.setCountry_id(cursor.getString(1));
					stateVo.setName(cursor.getString(2));
					stateVo.setAbr(cursor.getString(3));
					stateVo.setMap_reference(cursor.getString(4));
					stateVo.setLatitude(cursor.getString(5));
					stateVo.setLongitude(cursor.getString(6));
					stateVo.setStatus(cursor.getString(7));
					stateVo.setDate(cursor.getString(8));
					stateVo.setChecked(false);

					stateVoList.add(stateVo);

				} while (cursor.moveToNext());
			}
		}

		cursor.close();
		database.close();

		return  stateVoList;
	}

	public List<CountryVo> getCountryList() {

		List<CountryVo> countryVoList = new ArrayList<>();

		String selectQuery = "Select * from Countries";

		SQLiteDatabase database = this.getWritableDatabase();
		Cursor cursor = database.rawQuery(selectQuery, null);
		if (cursor != null && cursor.getCount() > 0) {
			if (cursor.moveToFirst()) {
				do {
					CountryVo countryVo = new CountryVo();

					countryVo.setId(cursor.getString(0));
					countryVo.setName(cursor.getString(1));
					countryVo.setAbr(cursor.getString(2));

					countryVoList.add(countryVo);

				} while (cursor.moveToNext());
			}
		}

		cursor.close();
		database.close();

		return countryVoList;
	}

	public List<ApplicationDueDateAllVo> getApplicationDueDates(String id,boolean portfolio) {

		List<ApplicationDueDateAllVo> dueDateAllVoList = new ArrayList<>();

		String selectQuery = "";

		if(portfolio){
			selectQuery = "Select * from PortfolioApplicationDueDateAll WHERE company_id='" + id + "'";
		}else{
			selectQuery = "Select * from CompanyApplicationDueDateAll WHERE company_id='" + id + "'";
		}

		SQLiteDatabase database = this.getWritableDatabase();
		Cursor cursor = database.rawQuery(selectQuery, null);
		if (cursor != null && cursor.getCount() > 0) {
			if (cursor.moveToFirst()) {
				do {
					ApplicationDueDateAllVo dueDateAllVo = new ApplicationDueDateAllVo();

					dueDateAllVo.setId(cursor.getString(0));
					dueDateAllVo.setCompany_id(cursor.getString(1));
					dueDateAllVo.setState_id(cursor.getString(2));
					dueDateAllVo.setProgram_id(cursor.getString(3));
					dueDateAllVo.setStart_date(cursor.getString(4));
					dueDateAllVo.setClose_date(cursor.getString(5));
					dueDateAllVo.setCreated(cursor.getString(6));
					dueDateAllVo.setState(cursor.getString(7));
					dueDateAllVo.setProgram(cursor.getString(8));

					dueDateAllVoList.add(dueDateAllVo);

				} while (cursor.moveToNext());
			}
		}

		cursor.close();
		database.close();

		return dueDateAllVoList;
	}

	public List<AllAwardsVo> getAllAwards(String id, boolean portfolio) {

		List<AllAwardsVo> allAwardsVoList = new ArrayList<AllAwardsVo>();

		String selectQuery = "";

		if(portfolio){
			selectQuery = "Select * from PortfolioAwards WHERE company_id='" + id + "'";
		}else{
			selectQuery = "Select * from CompanyAwards WHERE company_id='" + id + "'";
		}

		SQLiteDatabase database = this.getWritableDatabase();
		Cursor cursor = database.rawQuery(selectQuery, null);
		if (cursor != null && cursor.getCount() > 0) {
			if (cursor.moveToFirst()) {
				do {
					AllAwardsVo allAwardsVo = new AllAwardsVo();

					allAwardsVo.setId(cursor.getString(0));
					allAwardsVo.setCompany_id(cursor.getString(1));
					allAwardsVo.setDescription(cursor.getString(2));
					allAwardsVo.setAwards(cursor.getString(3));
					allAwardsVo.setYear(cursor.getString(4));

					allAwardsVoList.add(allAwardsVo);

				} while (cursor.moveToNext());
			}
		}

		cursor.close();
		database.close();

		return allAwardsVoList;
	}

	public List<DisciplinesVo> getDisciplinesDetail(String id, boolean portfolio) {

		List<DisciplinesVo> disciplinesVoList = new ArrayList<DisciplinesVo>();

		String selectQuery = "";

		if(portfolio){
			selectQuery = "Select * from PortfolioDisciplines WHERE company_id='" + id + "'";
		}else{
			selectQuery = "Select * from CompanyDisciplines WHERE company_id='" + id + "'";
		}

		SQLiteDatabase database = this.getWritableDatabase();
		Cursor cursor = database.rawQuery(selectQuery, null);
		if (cursor != null && cursor.getCount() > 0) {
			if (cursor.moveToFirst()) {
				do {
					DisciplinesVo disciplinesVo = new DisciplinesVo();

					disciplinesVo.setCompany_id(cursor.getString(0));
					disciplinesVo.setDiscipline_id(cursor.getString(1));
					disciplinesVo.setName(cursor.getString(2));

					disciplinesVoList.add(disciplinesVo);

				} while (cursor.moveToNext());
			}
		}

		cursor.close();
		database.close();

		return disciplinesVoList;
	}

	public List<Sub_disciplineVo> getSubDisciplinesDetail(String id) {

		List<Sub_disciplineVo> voList = new ArrayList<Sub_disciplineVo>();

		String selectQuery = "Select * from PortfolioSubDisciplines WHERE discipline_id='" + id + "' GROUP BY sub_discipline_id";

		SQLiteDatabase database = this.getWritableDatabase();
		Cursor cursor = database.rawQuery(selectQuery, null);
		if (cursor != null && cursor.getCount() > 0) {
			if (cursor.moveToFirst()) {
				do {
					Sub_disciplineVo sub_disciplineVo = new Sub_disciplineVo();

					sub_disciplineVo.setSub_discipline_id(cursor.getString(0));
					sub_disciplineVo.setDiscipline_id(cursor.getString(1));
					sub_disciplineVo.setName(cursor.getString(2));

					voList.add(sub_disciplineVo);

				} while (cursor.moveToNext());
			}
		}

		cursor.close();
		database.close();

		return voList;
	}

	public List<QuesAnsVo> getQuesAns(String id, boolean port) {

		List<QuesAnsVo> quesAnsVoList = new ArrayList<>();

		String selectQuery = "";

		if(port){
			selectQuery = "Select * from PortfolioQuesAns WHERE company_id='" + id + "'";
		}else{
			selectQuery = "Select * from CompanyQuesAns WHERE company_id='" + id + "'";
		}

		SQLiteDatabase database = this.getWritableDatabase();
		Cursor cursor = database.rawQuery(selectQuery, null);
		if (cursor != null && cursor.getCount() > 0) {
			if (cursor.moveToFirst()) {
				do {
					QuesAnsVo quesAnsVo = new QuesAnsVo();

					quesAnsVo.setId(cursor.getString(0));
					quesAnsVo.setCompany_id(cursor.getString(1));
					quesAnsVo.setQuestion(cursor.getString(2));
					quesAnsVo.setAnswer(cursor.getString(3));

					quesAnsVoList.add(quesAnsVo);

				} while (cursor.moveToNext());
			}
		}

		cursor.close();
		database.close();


		return quesAnsVoList;
	}

	public List<LinksVo> getLinks(String id, boolean port) {

		List<LinksVo> linksVoList = new ArrayList<LinksVo>();

		String selectQuery = "";

		if(port){
			selectQuery = "Select * from CompanyLinks WHERE company_id='" + id + "'";
		}else{
			selectQuery = "Select * from PortfolioLinks WHERE company_id='" + id + "'";
		}

		SQLiteDatabase database = this.getWritableDatabase();
		Cursor cursor = database.rawQuery(selectQuery, null);
		if (cursor != null && cursor.getCount() > 0) {
			if (cursor.moveToFirst()) {
				do {
					LinksVo linksVo = new LinksVo();

					linksVo.setId(cursor.getString(0));
					linksVo.setCompany_id(cursor.getString(1));
					linksVo.setMedia_type(cursor.getString(2));
					linksVo.setMedia_link(cursor.getString(3));
					linksVo.setMedia_logo(cursor.getString(4));

					linksVoList.add(linksVo);

				} while (cursor.moveToNext());
			}
		}

		cursor.close();
		database.close();


		return linksVoList;
	}


	public void deleteRow(String id) {

		SQLiteDatabase database = this.getWritableDatabase();

		database.execSQL("delete from Portfolio where id='"+id+"'");
		database.execSQL("delete from PortfolioApplicationDueDateAll where company_id='"+id+"'");
		database.execSQL("delete from PortfolioAwards where company_id='"+id+"'");
		database.execSQL("delete from PortfolioDisciplines where company_id='"+id+"'");
		database.execSQL("delete from PortfolioLinks where company_id='"+id+"'");
		database.execSQL("delete from PortfolioLocations where company_id='"+id+"'");
		database.execSQL("delete from PortfolioQuesAns where company_id='"+id+"'");

		database.close();
	}

	public void deleteTable() {

		SQLiteDatabase database = this.getWritableDatabase();

		database.execSQL("delete from Portfolio");
		database.execSQL("delete from PortfolioApplicationDueDateAll");
		database.execSQL("delete from PortfolioAwards");
		database.execSQL("delete from PortfolioDisciplines");
		database.execSQL("delete from PortfolioSubDisciplines");
		database.execSQL("delete from PortfolioLinks");
		database.execSQL("delete from PortfolioLocations");
		database.execSQL("delete from PortfolioQuesAns");
		database.execSQL("delete from Disciplines");
		database.execSQL("delete from SubDisciplines");
		database.execSQL("delete from Messages");
		database.execSQL("delete from AdderDetail");
		database.execSQL("delete from Countries");
		database.execSQL("delete from States");
		database.execSQL("delete from AllCompanys");
		database.execSQL("delete from Companies");
		database.execSQL("delete from CompanyAwards");
		database.execSQL("delete from CompanyApplicationDueDateAll");
		database.execSQL("delete from CompanyDisciplineSubdiscipline");
		database.execSQL("delete from CompanyDisciplines");
		database.execSQL("delete from CompanySubDisciplines");
		database.execSQL("delete from CompanyLinks");
		database.execSQL("delete from CompanyLocations");
		database.execSQL("delete from CompanyQuesAns");
		database.execSQL("delete from CompanyFilter");
		database.execSQL("delete from DisciplinesFilter");
		database.execSQL("delete from SubDisciplinesFilter");
		database.execSQL("delete from SocietyDetail");
		database.execSQL("delete from Salary");

		database.close();
	}

	public void likeUnlike(String id, String flag) {

		SQLiteDatabase database = this.getWritableDatabase();

		database.execSQL("UPDATE Messages SET like_status='1' WHERE id='" + id + "'");

		database.close();
	}

	public void updateReport(String id, String flag) {

		SQLiteDatabase database = this.getWritableDatabase();

		if(flag.equals("0")) {
			database.execSQL("UPDATE Messages SET report='1' WHERE id='" + id + "'");
		}else{
			database.execSQL("UPDATE Messages SET report='0' WHERE id='" + id + "'");
		}

		database.close();
	}

	public void updatePortfolio(String id, String flag) {

		SQLiteDatabase database = this.getWritableDatabase();

		String query = "UPDATE Portfolio SET is_deleted='1' WHERE id='" + id + "'";
		Log.e("update"," -- "+query);
		if(flag.equals("0")) {
			database.execSQL("UPDATE Portfolio SET is_deleted='1' WHERE id='" + id + "'");
		}else{
			database.execSQL("UPDATE Portfolio SET is_deleted='0' WHERE id='" + id + "'");
		}

		database.close();
	}

	public void updateMessageStatus(String id,String status) {

		SQLiteDatabase database = this.getWritableDatabase();
		if (status.equals("unread")) {
			//String unfavQuery = "UPDATE Messages SET status='read' WHERE id='" + id + "'";
			database.execSQL("UPDATE Messages SET status='read' WHERE id='" + id + "'");
		}
		database.close();

	}

	public void updateMsgStatus(String id,String status) {

		SQLiteDatabase database = this.getWritableDatabase();
		if(status.equals("0")) {
			database.execSQL("UPDATE Companies SET message_status='1' WHERE id='" + id + "'");
		}else{
			database.execSQL("UPDATE Companies SET message_status='0' WHERE id='" + id + "'");
		}
		database.close();

	}

	public int getRowCount(String type) {
		String countQuery = "SELECT * FROM Messages WHERE status='unread' and message_type= '" + type + "'";
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery, null);
		int cnt = cursor.getCount();
		cursor.close();
		return cnt;
	}



}
