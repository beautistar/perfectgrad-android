package com.perfectgraduate.utility;

import com.perfectgraduate.vo.AllAwardsVo;
import com.perfectgraduate.vo.ApplicationDueDateAllVo;
import com.perfectgraduate.vo.ApplicationVo;
import com.perfectgraduate.vo.DisciplinesVo;
import com.perfectgraduate.vo.MessagesVo;
import com.perfectgraduate.vo.QuesAnsVo;
import com.perfectgraduate.vo.SocietyVo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 07-Dec-16.
 */

public class Constants {

    public static String isAllCompany = "isAllCompany";
    public static String isFirstTime = "isFirstTime";
    public static String isDiscipline = "isDiscipline";
    public static String isDisciplineFilter = "isDisciplineFilter";
    public static String isMessages = "isMessages";
    public static String isStates = "isStates";
    public static String isCountries = "isCountries";
    public static String isBack = "isBack";
    public static String isSearch = "isSearch";
    public static String isCompanyFilter = "isCompanyFilter";
    public static String isSocietyDetail = "isSocietyDetail";
    public static String isSalary = "isSalary";

    //for liked in
    public static String Access_token = "Access_token";
    public static String emailAddress = "emailAddress";
    public static String formattedName = "formattedName";
    public static String pictureUrl = "pictureUrl";
    public static String publicProfileUrl = "publicProfileUrl";

    //for facebook login----------------------
    public static String FB_ID = "FB_ID";
    public static String USER_ID = "USER_ID";
    public static String FIRST_NAME = "FIRST_NAME";
    public static String LAST_NAME = "LAST_NAME";
    public static String EMAIL_ID = "EMAIL_ID";

    public static String Country_id = "Country_id";
    public static String State_id = "State_id";
    public static String University_id = "University_id";
    public static String Position_id = "Position_id";
    public static String Discipline_id = "Discipline_id";
    public static String SubDiscipline_id = "SubDiscipline_id";
    public static String Discipline2_id = "Discipline2_id";
    public static String SubDiscipline2_id = "SubDiscipline2_id";

    public static String Country_name = "Country_name";
    public static String State_name = "State_name";
    public static String University_name = "University_name";
    public static String Position_name = "Position_name";
    public static String Discipline_name = "Discipline_name";
    public static String SubDiscipline_name = "SubDiscipline_name";
    public static String Discipline2_name = "Discipline2_name";
    public static String SubDiscipline2_name = "SubDiscipline2_name";

    public static String Society_name = "Society_name";
    public static String Society_id = "Society_id";
    public static String Gender = "";


    public static String id = "id";
    public static String fb_id = "fb_id";
    public static String email = "email";
    public static String password = "password";
    public static String image = "image";
    public static String linkedinUrl = "linkedinUrl";
    public static String name = "name";
    public static String gender = "gender";
    public static String mobile = "mobile";
    public static String country_id = "country_id";
    public static String state_id = "state_id";
    public static String uni_id = "uni_id";
    public static String graduateProg_id = "graduateProg_id";
    public static String disciplline_id = "disciplline_id";
    public static String subDiscipline_id = "subDiscipline_id";
    public static String status = "status";
    public static String device_id = "device_id";
    public static String device_type = "device_type";
    public static String score = "score";
    public static String date = "date";
    public static String modified_date = "modified_date";
    public static String unique_id = "unique_id";
    public static String country = "country";
    public static String state = "state";
    public static String abr = "abr";
    public static String university = "university";
    public static String program_name = "program_name";
    public static String program = "program";
    public static String badge_count = "badge_count";
    public static String society = "society";
    public static String admin = "admin";

    public static String subHeader = "subHeader";

    public static List<SocietyVo> societyVoList = new ArrayList<>();
    public static ArrayList<String> checkedState;
    public static ArrayList<String> checkedCountry;
    public static ArrayList<String> abrList = new ArrayList<>();
    public static ArrayList<String> searchabrList = new ArrayList<>();

    public static int size = 0;

    public static String message = "message";
    public static String locationFilter = "locationFilter";
    public static String headerText = "";
    public static String adder_id = "";

    public static boolean hideShow = false;

    public static List<MessagesVo> CompanyInvites = new ArrayList<MessagesVo>();
    public static List<MessagesVo> SocietyInvites = new ArrayList<MessagesVo>();

    public static boolean company_msg = false;
    public static String company_id = "";


    public static List<ApplicationDueDateAllVo> dateAllVoList = new ArrayList<ApplicationDueDateAllVo>();
    public static List<DisciplinesVo> disciplinesVoList = new ArrayList<DisciplinesVo>();
    public static List<AllAwardsVo> allAwardsVoList = new ArrayList<AllAwardsVo>();
    public static List<QuesAnsVo> quesAnsVoList = new ArrayList<QuesAnsVo>();


    public static boolean application = false;
    public static boolean disciplines = false;
    public static boolean awards = false;
    public static boolean faq = false;

    public static boolean isVisible = false;
    public static boolean isVisibleSocietyDelete = false;
    public static boolean isVisibleSociety = false;
    public static boolean isAllCompanies = false;

    public static int society_count = 0;
    public static int portfolio_count = 0;
    public static int port = 0;
    public static String cid = "";

}
