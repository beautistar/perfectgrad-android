package com.perfectgraduate.utility;

import android.content.Context;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;

/**
 * This class is used for storing and retrieving shared preference values.
 */
public class SharedPrefrenceUtil {

	/*
	 * Used to get a string value stored in SharedPreferences.
	 *
	 * @param context ,application context
	 * @param pref a string name with which the preference would be stored.
	 * @param def a default string value , nothing is stored in preference
	 *
	 * @return String
	 */
	public static String getPrefrence(Context context, String prefKey, String defValue) {
		return PreferenceManager.getDefaultSharedPreferences(context).getString(prefKey, defValue);
	}

	/*
	 * Used to get a string value stored in SharedPreferences.
	 *
	 * @param context ,application context
	 * @param pref a string name with which the preference would be stored
	 * @param def a string value to be stored in preference
	 */
	public static void setPrefrence(Context context, String prefKey, String value) {
		Editor e = PreferenceManager.getDefaultSharedPreferences(context).edit();
		e.putString(prefKey, value);
		e.commit();
	}

	/*
	 * Used to get a boolean value stored in SharedPreferences.
	 *
	 * @param context ,application context
	 * @param pref a boolean name with which the preference would be stored.
	 * @param def a default boolean value , nothing is stored in preference
	 *
	 * @return boolean
	 */
	public static boolean getPrefrence(Context context, String prefKey, boolean defValue) {
		return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(prefKey, defValue);
	}

	/*
	 * Used to get a boolean value stored in SharedPreferences.
	 *
	 * @param context ,application context
	 * @param pref a boolean name with which the preference would be stored
	 * @param def a boolean value to be stored in preference
	 */
	public static void setPrefrence(Context context, String prefKey, boolean value) {
		Editor e = PreferenceManager.getDefaultSharedPreferences(context).edit();
		e.putBoolean(prefKey, value);
		e.commit();
	}

	/*
	 * Used to get a integer value stored in SharedPreferences.
	 *
	 * @param context ,application context
	 * @param pref a integer name with which the preference would be stored
	 * @param def a integer value to be stored in preference
	 *
	 * @return integer
	 */
	public static int getPrefrence(Context context, String prefKey, int defValue) {
		int c = PreferenceManager.getDefaultSharedPreferences(context).getInt(prefKey, defValue);
		return c;
	}

	/*
	 * Used to get a integer value stored in SharedPreferences.
	 *
	 * @param context ,application context
	 * @param pref a integer name with which the preference would be stored
	 * @param def a integer value to be stored in preference
	 */
	public static void setPrefrence(Context context, String prefKey, int value) {
		Editor e = PreferenceManager.getDefaultSharedPreferences(context).edit();
		e.putInt(prefKey, value);
		e.commit();
	}

	/*
	 * Used to get a long value stored in SharedPreferences.
	 *
	 * @param context ,application context
	 * @param pref a long name with which the preference would be stored
	 * @param def a long value to be stored in preference
	 *
	 * @return long
	 */
	public static long getPrefrence(Context context, String prefKey, long defValue) {
		return PreferenceManager.getDefaultSharedPreferences(context).getLong(prefKey, defValue);
	}

	/*
	 * Used to get a long value stored in SharedPreferences.
	 *
	 * @param context ,application context
	 * @param pref a long name with which the preference would be stored
	 * @param def a long value to be stored in preference
	 */
	public static void setPrefrence(Context context, String prefKey, long value) {
		Editor e = PreferenceManager.getDefaultSharedPreferences(context).edit();
		e.putLong(prefKey, value);
		e.commit();
	}


}