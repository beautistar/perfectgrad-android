package com.perfectgraduate.utility.customtextview;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by user on 19-Dec-16.
 */

public class OpenSansRegularTextView extends TextView {

    public OpenSansRegularTextView(Context context) {
        super(context);

        applyCustomFont(context);
    }

    public OpenSansRegularTextView(Context context, AttributeSet attrs) {
        super(context, attrs);

        applyCustomFont(context);
    }

    public OpenSansRegularTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        applyCustomFont(context);
    }

    private void applyCustomFont(Context context) {
        Typeface customFont = FontCache.getTypeface("fonts/OpenSans-Regular.ttf", context);
        setTypeface(customFont);
    }

}
