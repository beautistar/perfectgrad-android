package com.perfectgraduate.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.perfectgraduate.R;
import com.perfectgraduate.vo.DisciplineVo;
import com.perfectgraduate.vo.DisciplinesVo;
import com.perfectgraduate.vo.SubDisciplineVo;
import com.perfectgraduate.vo.Sub_disciplineVo;

import java.util.List;

/**
 * Created by user on 17-Jan-17.
 */

public class TargetDisciplineAdapter  extends BaseExpandableListAdapter {

    private Context context;
    private List<DisciplinesVo> disciplineVoList;
    public TargetDisciplineAdapter(Context context, List<DisciplinesVo> VoList) {
        this.context = context;
        this.disciplineVoList = VoList;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        List<Sub_disciplineVo> subDisciplineVoList = disciplineVoList.get(groupPosition).getSub_disciplineVoList();
        return subDisciplineVoList.get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild,
                             View convertView, ViewGroup parent) {

        Sub_disciplineVo detailInfo = (Sub_disciplineVo) getChild(groupPosition, childPosition);
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.list_item, null);
        }
        TextView expandedListTextView = (TextView) convertView
                .findViewById(R.id.expandedListItem);
        expandedListTextView.setText(detailInfo.getName());
        return convertView;
    }

    @Override
    public int getChildrenCount(int listPosition) {
        List<Sub_disciplineVo> subDisciplineVoList = disciplineVoList.get(listPosition).getSub_disciplineVoList();
        return subDisciplineVoList.size();
    }

    @Override
    public Object getGroup(int listPosition) {
        return disciplineVoList.get(listPosition);
    }

    @Override
    public int getGroupCount() {
        return disciplineVoList.size();
    }

    @Override
    public long getGroupId(int listPosition) {
        return listPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isLastChild, View view,
                             ViewGroup parent) {
        DisciplinesVo headerInfo = (DisciplinesVo) getGroup(groupPosition);
        if (view == null) {
            LayoutInflater layoutInflater = (LayoutInflater) context.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.list_group, null);
        }
        TextView listTitleTextView = (TextView) view
                .findViewById(R.id.expandedTitle);

        listTitleTextView.setText(headerInfo.getName());

        ImageView imageView_expand = (ImageView) view.findViewById(R.id.imageView_expand);

        if(isLastChild){
            imageView_expand.setImageResource(R.drawable.negative_grey);
        }else{
            imageView_expand.setImageResource(R.drawable.add_grey);
        }

        return view;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public boolean isChildSelectable(int listPosition, int expandedListPosition) {
        return true;
    }
}