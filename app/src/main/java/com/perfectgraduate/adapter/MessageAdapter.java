package com.perfectgraduate.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.callback.BitmapAjaxCallback;
import com.perfectgraduate.R;
import com.perfectgraduate.activity.DetailActivity;
import com.perfectgraduate.activity.HomeActivity;
import com.perfectgraduate.activity.MessageDetailsActivity;
import com.perfectgraduate.app.PerfectGraduateApplication;
import com.perfectgraduate.database.DatabaseHelper;
import com.perfectgraduate.utility.Constants;
import com.perfectgraduate.utility.SharedPrefrenceUtil;
import com.perfectgraduate.vo.MessagesVo;

import java.util.List;

/**
 * Created by user on 09-Jan-17.
 */

public class MessageAdapter extends BaseAdapter{

    private List<MessagesVo> messagesVoList;
    private Context context;
    private HomeActivity activity;
    private DatabaseHelper databaseHelper;
    public MessageAdapter(Context context, List<MessagesVo> VoList){
        this.messagesVoList = VoList;
        this.context = context;
        activity = (HomeActivity) context;
        databaseHelper = new DatabaseHelper(activity);
    }

    @Override
    public int getCount() {
        return messagesVoList.size();
    }

    @Override
    public MessagesVo getItem(int pos) {
        return messagesVoList.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return pos;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;

        if (convertView == null) {

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.row_messages, null);

            holder = new ViewHolder();

            holder.relative_messages = (RelativeLayout) convertView.findViewById(R.id.relative_messages);

            holder.textView_left_header = (TextView) convertView.findViewById(R.id.textView_left_header);
            holder.textView_left_subheader = (TextView) convertView.findViewById(R.id.textView_left_subheader);
            holder.textView_right_header = (TextView) convertView.findViewById(R.id.textView_right_header);
            holder.textView_right_subheader = (TextView) convertView.findViewById(R.id.textView_right_subheader);
            holder.textView_dateTime = (TextView) convertView.findViewById(R.id.textView_dateTime);
            holder.textView_unread_left = (TextView) convertView.findViewById(R.id.textView_unread_left);
            holder.textView_unread_right = (TextView) convertView.findViewById(R.id.textView_unread_right);
            holder.textView_status_right = (TextView) convertView.findViewById(R.id.textView_status_right);

            holder.linearLayout_right_arrow = (LinearLayout) convertView.findViewById(R.id.linearLayout_right_arrow);
            holder.linearLayout_left_arrow = (LinearLayout) convertView.findViewById(R.id.linearLayout_left_arrow);
            holder.linearLayout_left = (LinearLayout) convertView.findViewById(R.id.linearLayout_left);
            holder.linearLayout_right = (LinearLayout) convertView.findViewById(R.id.linearLayout_right);
            holder.imageView_left = (ImageView) convertView.findViewById(R.id.imageView_left);
            holder.imageView_right = (ImageView) convertView.findViewById(R.id.imageView_right);

            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        AQuery aQuery = new AQuery(convertView);

        final MessagesVo messagesVo = getItem(position);

        holder.imageView_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(messagesVo.getType().equals("companybehalf")) {

                    String id = "";

                    //PerfectGraduateApplication.getInstance().setUserCompanyVo(userCompanyVoList.get(position));
                    for (int i = 0; i < messagesVo.getAdderDetailVoList().size(); i++) {
                        id = messagesVo.getAdderDetailVoList().get(i).getId();
                    }

                    Constants.company_id = id;
                    activity.displayDetail("");
                }
            }
        });

        holder.imageView_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(messagesVo.getType().equals("companybehalf")) {

                    String id = "";

                    for (int i = 0; i < messagesVo.getAdderDetailVoList().size(); i++) {
                        id = messagesVo.getAdderDetailVoList().get(i).getId();
                    }

                    Constants.company_id = id;
                    activity.displayDetail("");

                }
            }
        });


        //if(messagesVo.getStatus().equals("unread")){

        holder.textView_dateTime.setText(PerfectGraduateApplication.getInstance().parseDateToddMMyyyy(messagesVo.getDate()));

        holder.relative_messages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                PerfectGraduateApplication.getInstance().setMessagesVo(messagesVo);

                if (messagesVo.getType().equals("admin")) {
                    if (messagesVo.getStatus().equals("unread")) {
                        int admin = Integer.parseInt(SharedPrefrenceUtil.getPrefrence(context, Constants.admin, "")) - 1;
                        SharedPrefrenceUtil.setPrefrence(context, Constants.admin, String.valueOf(admin));
                    }
                }


                Intent intent = new Intent(activity, MessageDetailsActivity.class);
                activity.startActivity(intent);
                activity.overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_left);
            }
        });

        if (messagesVo.getType().equals("admin")) {

            if (messagesVo.getMessage_type().equals("message")) {

                if (messagesVo.getStatus().equals("unread")) {
                    holder.textView_unread_left.setVisibility(View.VISIBLE);
                } else {
                    holder.textView_unread_left.setVisibility(View.INVISIBLE);
                }

                holder.linearLayout_left.setVisibility(View.VISIBLE);
                holder.linearLayout_right.setVisibility(View.INVISIBLE);

                holder.linearLayout_left_arrow.setBackgroundResource(R.drawable.blueline_chat_left);

                for (int i = 0; i < messagesVo.getAdderDetailVoList().size(); i++) {
                    holder.textView_left_header.setText(messagesVo.getAdderDetailVoList().get(i).getName());

                    aQuery.id(holder.imageView_left).image("http://www.perfectgraduate.com/perfect/assets/uploads/" + messagesVo.getAdderDetailVoList().get(i).getLogo(),
                            true, true, 0, 0, new BitmapAjaxCallback() {
                                @Override
                                protected void callback(String url, ImageView iv, Bitmap bm, AjaxStatus status) {

                                    if (bm != null) {
                                        iv.setImageBitmap(bm);
                                    } else {
                                        iv.setImageResource(R.drawable.defaultbuilding);
                                    }
                                }
                            });
                }
                holder.textView_left_subheader.setText(messagesVo.getMessage());

            } else {
                if (messagesVo.getEventconfirmation().equals("Confirmed")) {

                    if (messagesVo.getStatus().equals("unread")) {
                        holder.textView_unread_right.setVisibility(View.VISIBLE);
                    } else {
                        holder.textView_unread_right.setVisibility(View.INVISIBLE);
                    }

                    holder.linearLayout_left.setVisibility(View.INVISIBLE);
                    holder.linearLayout_right.setVisibility(View.VISIBLE);

                    holder.linearLayout_right_arrow.setBackgroundResource(R.drawable.pink_right_arrow);

                    holder.textView_status_right.setVisibility(View.VISIBLE);

                    if (messagesVo.getEventstatus().equals("0")) {
                        holder.textView_status_right.setText("Event cancelled");
                        holder.textView_status_right.setTextColor(activity.getResources().getColor(R.color.colorPrimaryDark));
                    } else if (messagesVo.getEventstatus().equals("2")) {
                        holder.textView_status_right.setText("Event closed");
                        holder.textView_status_right.setTextColor(activity.getResources().getColor(R.color.colorPrimaryDark));
                    } else {
                        holder.textView_status_right.setText("Congratulations");
                        holder.textView_status_right.setTextColor(activity.getResources().getColor(R.color.white));
                    }

                    holder.textView_right_header.setGravity(Gravity.CENTER);

                    for (int i = 0; i < messagesVo.getAdderDetailVoList().size(); i++) {
                        holder.textView_right_header.setText(messagesVo.getAdderDetailVoList().get(i).getName());

                        aQuery.id(holder.imageView_right).image("http://www.perfectgraduate.com/perfect/assets/uploads/" + messagesVo.getAdderDetailVoList().get(i).getLogo(),
                                true, true, 0, 0, new BitmapAjaxCallback() {
                                    @Override
                                    protected void callback(String url, ImageView iv, Bitmap bm, AjaxStatus status) {

                                        if (bm != null) {
                                            iv.setImageBitmap(bm);
                                        } else {
                                            iv.setImageResource(R.drawable.defaultbuilding);
                                        }
                                    }
                                });
                    }
                    holder.textView_right_subheader.setSingleLine(false);
                    if (Build.VERSION.SDK_INT >= 24) {
                        holder.textView_right_subheader.setText(Html.fromHtml("<b>" + "Event: " + "</b>" + messagesVo.getMessage()
                                + "<b>" + "<br/>Event Date: " + "</b>" + messagesVo.getEvent_date(), Html.FROM_HTML_MODE_LEGACY));

                    } else {
                        holder.textView_right_subheader.setText(Html.fromHtml("<b>" + "Event: " + "</b>" + messagesVo.getMessage()
                                + "<b>" + "<br/>Event Date: " + "</b>" + messagesVo.getEvent_date()));
                    }

                } else if (messagesVo.getEventconfirmation().equals("Request Sent")) {

                    holder.linearLayout_left.setVisibility(View.INVISIBLE);
                    holder.linearLayout_right.setVisibility(View.VISIBLE);

                    holder.linearLayout_right_arrow.setBackgroundResource(R.drawable.blueline_chat_right);

                    if (messagesVo.getStatus().equals("unread")) {
                        holder.textView_unread_right.setVisibility(View.VISIBLE);
                    } else {
                        holder.textView_unread_right.setVisibility(View.INVISIBLE);
                    }

                    holder.textView_right_header.setGravity(Gravity.CENTER);

                    holder.textView_status_right.setVisibility(View.VISIBLE);

                    if (messagesVo.getEventstatus().equals("0")) {
                        holder.textView_status_right.setText("Event cancelled");
                        holder.textView_status_right.setTextColor(activity.getResources().getColor(R.color.colorPrimaryDark));
                    } else if (messagesVo.getEventstatus().equals("2")) {
                        holder.textView_status_right.setText("Event closed");
                        holder.textView_status_right.setTextColor(activity.getResources().getColor(R.color.colorPrimaryDark));
                    } else {
                        holder.textView_status_right.setTextColor(activity.getResources().getColor(R.color.colorPrimaryDark));
                        holder.textView_status_right.setText("Invitation requested");
                    }

                    //holder.imageView_right.setBackgroundResource(R.drawable.image_border);

                    holder.textView_right_header.setGravity(Gravity.CENTER);

                    for (int i = 0; i < messagesVo.getAdderDetailVoList().size(); i++) {
                        holder.textView_right_header.setText(messagesVo.getAdderDetailVoList().get(i).getName());

                        aQuery.id(holder.imageView_right).image("http://www.perfectgraduate.com/perfect/assets/uploads/" + messagesVo.getAdderDetailVoList().get(i).getLogo(),
                                true, true, 0, 0, new BitmapAjaxCallback() {
                                    @Override
                                    protected void callback(String url, ImageView iv, Bitmap bm, AjaxStatus status) {

                                        if (bm != null) {
                                            iv.setImageBitmap(bm);
                                        } else {
                                            iv.setImageResource(R.drawable.defaultbuilding);
                                        }
                                    }
                                });

                    }
                    holder.textView_right_subheader.setSingleLine(false);
                    if (Build.VERSION.SDK_INT >= 24) {
                        holder.textView_right_subheader.setText(Html.fromHtml("<b>" + "Event: " + "</b>" + messagesVo.getMessage()
                                + "<b>" + "<br/>Event Date: " + "</b>" + messagesVo.getEvent_date(), Html.FROM_HTML_MODE_LEGACY));

                    } else {
                        holder.textView_right_subheader.setText(Html.fromHtml("<b>" + "Event: " + "</b>" + messagesVo.getMessage()
                                + "<b>" + "<br/>Event Date: " + "</b>" + messagesVo.getEvent_date()));
                    }


                } else {

                    holder.linearLayout_left.setVisibility(View.INVISIBLE);
                    holder.linearLayout_right.setVisibility(View.VISIBLE);

                    holder.linearLayout_right_arrow.setBackgroundResource(R.drawable.blueline_chat_right);

                    if (messagesVo.getStatus().equals("unread")) {
                        holder.textView_unread_right.setVisibility(View.VISIBLE);
                    } else {
                        holder.textView_unread_right.setVisibility(View.INVISIBLE);
                    }

                    holder.textView_status_right.setVisibility(View.VISIBLE);

                    if (messagesVo.getEventstatus().equals("0")) {
                        holder.textView_status_right.setText("Event cancelled");
                        holder.textView_status_right.setTextColor(activity.getResources().getColor(R.color.colorPrimaryDark));
                    } else if (messagesVo.getEventstatus().equals("2")) {
                        holder.textView_status_right.setText("Event closed");
                        holder.textView_status_right.setTextColor(activity.getResources().getColor(R.color.colorPrimaryDark));
                    } else {
                        holder.textView_status_right.setTextColor(activity.getResources().getColor(R.color.colorPrimaryDark));
                        holder.textView_status_right.setText("Request an invitation today");
                    }

                    holder.textView_right_header.setGravity(Gravity.CENTER);

                    for (int i = 0; i < messagesVo.getAdderDetailVoList().size(); i++) {
                        holder.textView_right_header.setText(messagesVo.getAdderDetailVoList().get(i).getName());

                        aQuery.id(holder.imageView_right).image("http://www.perfectgraduate.com/perfect/assets/uploads/" + messagesVo.getAdderDetailVoList().get(i).getLogo(),
                                true, true, 0, 0, new BitmapAjaxCallback() {
                                    @Override
                                    protected void callback(String url, ImageView iv, Bitmap bm, AjaxStatus status) {

                                        if (bm != null) {
                                            iv.setImageBitmap(bm);
                                        } else {
                                            iv.setImageResource(R.drawable.defaultbuilding);
                                        }
                                    }
                                });

                    }
                    holder.textView_right_subheader.setSingleLine(false);
                    if (Build.VERSION.SDK_INT >= 24) {
                        holder.textView_right_subheader.setText(Html.fromHtml("<b>" + "Event: " + "</b>" + messagesVo.getMessage()
                                + "<b>" + "<br/>Event Date: " + "</b>" + messagesVo.getEvent_date(), Html.FROM_HTML_MODE_LEGACY));

                    } else {
                        holder.textView_right_subheader.setText(Html.fromHtml("<b>" + "Event: " + "</b>" + messagesVo.getMessage()
                                + "<b>" + "<br/>Event Date: " + "</b>" + messagesVo.getEvent_date()));
                    }
                }
            }

        } else if (messagesVo.getType().equals("companybehalf")) {

            if (messagesVo.getMessage_type().equals("message")) {

                holder.linearLayout_left_arrow.setBackgroundResource(R.drawable.skyblue_chat_left);

                if (messagesVo.getStatus().equals("unread")) {
                    holder.textView_unread_left.setVisibility(View.VISIBLE);
                } else {
                    holder.textView_unread_left.setVisibility(View.INVISIBLE);
                }

                holder.linearLayout_left.setVisibility(View.VISIBLE);
                holder.linearLayout_right.setVisibility(View.INVISIBLE);

                for (int i = 0; i < messagesVo.getAdderDetailVoList().size(); i++) {

                    holder.textView_left_header.setText(messagesVo.getAdderDetailVoList().get(i).getName());

                    aQuery.id(holder.imageView_left).image("http://www.perfectgraduate.com/perfect/assets/uploads/" + messagesVo.getAdderDetailVoList().get(i).getLogo(),
                            true, true, 0, 0, new BitmapAjaxCallback() {
                                @Override
                                protected void callback(String url, ImageView iv, Bitmap bm, AjaxStatus status) {

                                    if (bm != null) {
                                        iv.setImageBitmap(bm);
                                    } else {
                                        iv.setImageResource(R.drawable.defaultbuilding);
                                    }
                                }
                            });

                }
                holder.textView_left_subheader.setText(messagesVo.getMessage());


            } else {
                if (messagesVo.getEventconfirmation().equals("Request Sent")) {

                    holder.linearLayout_left.setVisibility(View.INVISIBLE);
                    holder.linearLayout_right.setVisibility(View.VISIBLE);

                    if (messagesVo.getStatus().equals("unread")) {
                        holder.textView_unread_right.setVisibility(View.VISIBLE);
                    } else {
                        holder.textView_unread_right.setVisibility(View.INVISIBLE);
                    }


                    holder.textView_status_right.setVisibility(View.VISIBLE);

                    if (messagesVo.getEventstatus().equals("0")) {
                        holder.textView_status_right.setTextColor(activity.getResources().getColor(R.color.black));
                        holder.textView_status_right.setText("Event cancelled");
                    } else if (messagesVo.getEventstatus().equals("2")) {
                        holder.textView_status_right.setTextColor(activity.getResources().getColor(R.color.black));
                        holder.textView_status_right.setText("Event closed");
                    } else {
                        holder.textView_status_right.setTextColor(activity.getResources().getColor(R.color.black));
                        holder.textView_status_right.setText("Invitation requested");
                    }

                    holder.linearLayout_right_arrow.setBackgroundResource(R.drawable.lightgreen_chat_right);

                    holder.textView_right_header.setGravity(Gravity.CENTER);

                    for (int i = 0; i < messagesVo.getAdderDetailVoList().size(); i++) {
                        holder.textView_right_header.setText(messagesVo.getAdderDetailVoList().get(i).getName());

                        aQuery.id(holder.imageView_right).image("http://www.perfectgraduate.com/perfect/assets/uploads/" + messagesVo.getAdderDetailVoList().get(i).getLogo(),
                                true, true, 0, 0, new BitmapAjaxCallback() {
                                    @Override
                                    protected void callback(String url, ImageView iv, Bitmap bm, AjaxStatus status) {

                                        if (bm != null) {
                                            iv.setImageBitmap(bm);
                                        } else {
                                            iv.setImageResource(R.drawable.defaultbuilding);
                                        }
                                    }
                                });

                    }
                    holder.textView_right_subheader.setSingleLine(false);
                    if (Build.VERSION.SDK_INT >= 24) {
                        holder.textView_right_subheader.setText(Html.fromHtml("<b>" + "Event: " + "</b>" + messagesVo.getMessage()
                                + "<b>" + "<br/>Event Date: " + "</b>" + messagesVo.getEvent_date(), Html.FROM_HTML_MODE_LEGACY));

                    } else {
                        holder.textView_right_subheader.setText(Html.fromHtml("<b>" + "Event: " + "</b>" + messagesVo.getMessage()
                                + "<b>" + "<br/>Event Date: " + "</b>" + messagesVo.getEvent_date()));
                    }

                } else if (messagesVo.getEventconfirmation().equals("Confirmed")) {

                    holder.linearLayout_left.setVisibility(View.INVISIBLE);
                    holder.linearLayout_right.setVisibility(View.VISIBLE);

                    holder.linearLayout_right_arrow.setBackgroundResource(R.drawable.pink_right_arrow);

                    if (messagesVo.getStatus().equals("unread")) {
                        holder.textView_unread_right.setVisibility(View.VISIBLE);
                    } else {
                        holder.textView_unread_right.setVisibility(View.INVISIBLE);
                    }

                    holder.textView_status_right.setVisibility(View.VISIBLE);

                    if (messagesVo.getEventstatus().equals("0")) {
                        holder.textView_status_right.setTextColor(activity.getResources().getColor(R.color.black));
                        holder.textView_status_right.setText("Event cancelled");
                    } else if (messagesVo.getEventstatus().equals("2")) {
                        holder.textView_status_right.setTextColor(activity.getResources().getColor(R.color.black));
                        holder.textView_status_right.setText("Event closed");
                    } else {
                        holder.textView_status_right.setTextColor(activity.getResources().getColor(R.color.white));
                        holder.textView_status_right.setText("Congratulations");
                    }

                    holder.textView_right_header.setGravity(Gravity.CENTER);

                    for (int i = 0; i < messagesVo.getAdderDetailVoList().size(); i++) {
                        holder.textView_right_header.setText(messagesVo.getAdderDetailVoList().get(i).getName());

                        aQuery.id(holder.imageView_right).image("http://www.perfectgraduate.com/perfect/assets/uploads/" + messagesVo.getAdderDetailVoList().get(i).getLogo(),
                                true, true, 0, 0, new BitmapAjaxCallback() {
                                    @Override
                                    protected void callback(String url, ImageView iv, Bitmap bm, AjaxStatus status) {

                                        if (bm != null) {
                                            iv.setImageBitmap(bm);
                                        } else {
                                            iv.setImageResource(R.drawable.defaultbuilding);
                                        }
                                    }
                                });

                    }
                    holder.textView_right_subheader.setSingleLine(false);
                    if (Build.VERSION.SDK_INT >= 24) {
                        holder.textView_right_subheader.setText(Html.fromHtml("<b>" + "Event: " + "</b>" + messagesVo.getMessage()
                                + "<b>" + "<br/>Event Date: " + "</b>" + messagesVo.getEvent_date(), Html.FROM_HTML_MODE_LEGACY));

                    } else {
                        holder.textView_right_subheader.setText(Html.fromHtml("<b>" + "Event: " + "</b>" + messagesVo.getMessage()
                                + "<b>" + "<br/>Event Date: " + "</b>" + messagesVo.getEvent_date()));
                    }

                } else {

                    holder.linearLayout_left.setVisibility(View.INVISIBLE);
                    holder.linearLayout_right.setVisibility(View.VISIBLE);

                    holder.linearLayout_right_arrow.setBackgroundResource(R.drawable.lightgreen_chat_right);

                    if (messagesVo.getStatus().equals("unread")) {
                        holder.textView_unread_right.setVisibility(View.VISIBLE);
                    } else {
                        holder.textView_unread_right.setVisibility(View.INVISIBLE);
                    }

                    holder.textView_status_right.setVisibility(View.VISIBLE);

                    if (messagesVo.getEventstatus().equals("0")) {
                        holder.textView_status_right.setTextColor(activity.getResources().getColor(R.color.black));
                        holder.textView_status_right.setText("Event cancelled");
                    } else if (messagesVo.getEventstatus().equals("2")) {
                        holder.textView_status_right.setTextColor(activity.getResources().getColor(R.color.black));
                        holder.textView_status_right.setText("Event closed");
                    } else {
                        holder.textView_status_right.setTextColor(activity.getResources().getColor(R.color.black));
                        holder.textView_status_right.setText("Request an invitation today");
                    }

                    holder.textView_right_header.setGravity(Gravity.CENTER);

                    for (int i = 0; i < messagesVo.getAdderDetailVoList().size(); i++) {
                        holder.textView_right_header.setText(messagesVo.getAdderDetailVoList().get(i).getName());

                        aQuery.id(holder.imageView_right).image("http://www.perfectgraduate.com/perfect/assets/uploads/" + messagesVo.getAdderDetailVoList().get(i).getLogo(),
                                true, true, 0, 0, new BitmapAjaxCallback() {
                                    @Override
                                    protected void callback(String url, ImageView iv, Bitmap bm, AjaxStatus status) {

                                        if (bm != null) {
                                            iv.setImageBitmap(bm);
                                        } else {
                                            iv.setImageResource(R.drawable.defaultbuilding);
                                        }
                                    }
                                });

                    }
                    holder.textView_right_subheader.setSingleLine(false);
                    if (Build.VERSION.SDK_INT >= 24) {
                        holder.textView_right_subheader.setText(Html.fromHtml("<b>" + "Event: " + "</b>" + messagesVo.getMessage()
                                + "<b>" + "<br/>Event Date: " + "</b>" + messagesVo.getEvent_date(), Html.FROM_HTML_MODE_LEGACY));

                    } else {
                        holder.textView_right_subheader.setText(Html.fromHtml("<b>" + "Event: " + "</b>" + messagesVo.getMessage()
                                + "<b>" + "<br/>Event Date: " + "</b>" + messagesVo.getEvent_date()));
                    }
                }
            }

        } else if (messagesVo.getType().equals("studentsociety")) {

            if (messagesVo.getMessage_type().equals("message")) {

                if (messagesVo.getStatus().equals("unread")) {
                    holder.textView_unread_left.setVisibility(View.VISIBLE);
                } else {
                    holder.textView_unread_left.setVisibility(View.INVISIBLE);
                }

                holder.linearLayout_left_arrow.setBackgroundResource(R.drawable.societymsg);

                holder.linearLayout_left.setVisibility(View.VISIBLE);
                holder.linearLayout_right.setVisibility(View.INVISIBLE);

                for (int i = 0; i < messagesVo.getAdderDetailVoList().size(); i++) {
                    holder.textView_left_header.setText(messagesVo.getAdderDetailVoList().get(i).getName());

                    aQuery.id(holder.imageView_left).image("http://www.perfectgraduate.com/perfect/assets/uploads/" + messagesVo.getAdderDetailVoList().get(i).getLogo(),
                            true, true, 0, 0, new BitmapAjaxCallback() {
                                @Override
                                protected void callback(String url, ImageView iv, Bitmap bm, AjaxStatus status) {

                                    if (bm != null) {
                                        iv.setImageBitmap(bm);
                                    } else {
                                        iv.setImageResource(R.drawable.defaultbuilding);
                                    }
                                }
                            });

                }
                holder.textView_left_subheader.setText(messagesVo.getMessage());

            } else {

                if (messagesVo.getEventconfirmation().equals("Request Sent")) {

                    holder.linearLayout_left.setVisibility(View.INVISIBLE);
                    holder.linearLayout_right.setVisibility(View.VISIBLE);

                    holder.linearLayout_right_arrow.setBackgroundResource(R.drawable.societyevent);

                    if (messagesVo.getStatus().equals("unread")) {
                        holder.textView_unread_right.setVisibility(View.VISIBLE);
                    } else {
                        holder.textView_unread_right.setVisibility(View.INVISIBLE);
                    }

                    holder.textView_status_right.setVisibility(View.VISIBLE);

                    if (messagesVo.getEventstatus().equals("0")) {
                        holder.textView_status_right.setTextColor(activity.getResources().getColor(R.color.white));
                        holder.textView_status_right.setText("Event cancelled");
                    } else if (messagesVo.getEventstatus().equals("2")) {
                        holder.textView_status_right.setTextColor(activity.getResources().getColor(R.color.white));
                        holder.textView_status_right.setText("Event closed");
                    } else {
                        holder.textView_status_right.setTextColor(activity.getResources().getColor(R.color.white));
                        holder.textView_status_right.setText("Invitation requested");
                    }

                    holder.textView_right_header.setGravity(Gravity.CENTER);

                    for (int i = 0; i < messagesVo.getAdderDetailVoList().size(); i++) {
                        holder.textView_right_header.setText(messagesVo.getAdderDetailVoList().get(i).getName());

                        aQuery.id(holder.imageView_right).image("http://www.perfectgraduate.com/perfect/assets/uploads/" + messagesVo.getAdderDetailVoList().get(i).getLogo(),
                                true, true, 0, 0, new BitmapAjaxCallback() {
                                    @Override
                                    protected void callback(String url, ImageView iv, Bitmap bm, AjaxStatus status) {

                                        if (bm != null) {
                                            iv.setImageBitmap(bm);
                                        } else {
                                            iv.setImageResource(R.drawable.defaultbuilding);
                                        }
                                    }
                                });

                    }
                    holder.textView_right_subheader.setSingleLine(false);
                    if (Build.VERSION.SDK_INT >= 24) {
                        holder.textView_right_subheader.setText(Html.fromHtml("<b>" + "Event: " + "</b>" + messagesVo.getMessage()
                                + "<b>" + "<br/>Event Date: " + "</b>" + messagesVo.getEvent_date(), Html.FROM_HTML_MODE_LEGACY));

                    } else {
                        holder.textView_right_subheader.setText(Html.fromHtml("<b>" + "Event: " + "</b>" + messagesVo.getMessage()
                                + "<b>" + "<br/>Event Date: " + "</b>" + messagesVo.getEvent_date()));
                    }


                } else if (messagesVo.getEventconfirmation().equals("Confirmed")) {

                    holder.linearLayout_left.setVisibility(View.INVISIBLE);
                    holder.linearLayout_right.setVisibility(View.VISIBLE);

                    holder.linearLayout_right_arrow.setBackgroundResource(R.drawable.pink_right_arrow);

                    if (messagesVo.getStatus().equals("unread")) {
                        holder.textView_unread_right.setVisibility(View.VISIBLE);
                    } else {
                        holder.textView_unread_right.setVisibility(View.INVISIBLE);
                    }

                    holder.textView_status_right.setVisibility(View.VISIBLE);

                    if (messagesVo.getEventstatus().equals("0")) {
                        holder.textView_status_right.setTextColor(activity.getResources().getColor(R.color.white));
                        holder.textView_status_right.setText("Event cancelled");
                    } else if (messagesVo.getEventstatus().equals("2")) {
                        holder.textView_status_right.setTextColor(activity.getResources().getColor(R.color.white));
                        holder.textView_status_right.setText("Event closed");
                    } else {
                        holder.textView_status_right.setTextColor(activity.getResources().getColor(R.color.white));
                        holder.textView_status_right.setText("Congratulations");
                    }

                    holder.textView_right_header.setGravity(Gravity.CENTER);

                    for (int i = 0; i < messagesVo.getAdderDetailVoList().size(); i++) {
                        holder.textView_right_header.setText(messagesVo.getAdderDetailVoList().get(i).getName());

                        aQuery.id(holder.imageView_right).image("http://www.perfectgraduate.com/perfect/assets/uploads/" + messagesVo.getAdderDetailVoList().get(i).getLogo(),
                                true, true, 0, 0, new BitmapAjaxCallback() {
                                    @Override
                                    protected void callback(String url, ImageView iv, Bitmap bm, AjaxStatus status) {

                                        if (bm != null) {
                                            iv.setImageBitmap(bm);
                                        } else {
                                            iv.setImageResource(R.drawable.defaultbuilding);
                                        }
                                    }
                                });

                    }
                    holder.textView_right_subheader.setSingleLine(false);
                    if (Build.VERSION.SDK_INT >= 24) {
                        holder.textView_right_subheader.setText(Html.fromHtml("<b>" + "Event: " + "</b>" + messagesVo.getMessage()
                                + "<b>" + "<br/>Event Date: " + "</b>" + messagesVo.getEvent_date(), Html.FROM_HTML_MODE_LEGACY));

                    } else {
                        holder.textView_right_subheader.setText(Html.fromHtml("<b>" + "Event: " + "</b>" + messagesVo.getMessage()
                                + "<b>" + "<br/>Event Date: " + "</b>" + messagesVo.getEvent_date()));
                    }

                } else {

                    holder.linearLayout_left.setVisibility(View.INVISIBLE);
                    holder.linearLayout_right.setVisibility(View.VISIBLE);

                    if (messagesVo.getStatus().equals("unread")) {
                        holder.textView_unread_right.setVisibility(View.VISIBLE);
                    } else {
                        holder.textView_unread_right.setVisibility(View.INVISIBLE);
                    }

                    holder.textView_status_right.setVisibility(View.VISIBLE);

                    if (messagesVo.getEventstatus().equals("0")) {
                        holder.textView_status_right.setTextColor(activity.getResources().getColor(R.color.white));
                        holder.textView_status_right.setText("Event cancelled");
                    } else if (messagesVo.getEventstatus().equals("2")) {
                        holder.textView_status_right.setTextColor(activity.getResources().getColor(R.color.white));
                        holder.textView_status_right.setText("Event closed");
                    } else {
                        holder.textView_status_right.setTextColor(activity.getResources().getColor(R.color.white));
                        holder.textView_status_right.setText("Request an invitation today");
                    }

                    holder.linearLayout_right_arrow.setBackgroundResource(R.drawable.societyevent);

                    holder.textView_right_header.setGravity(Gravity.CENTER);

                    for (int i = 0; i < messagesVo.getAdderDetailVoList().size(); i++) {
                        holder.textView_right_header.setText(messagesVo.getAdderDetailVoList().get(i).getName());

                        aQuery.id(holder.imageView_right).image("http://www.perfectgraduate.com/perfect/assets/uploads/" + messagesVo.getAdderDetailVoList().get(i).getLogo(),
                                true, true, 0, 0, new BitmapAjaxCallback() {
                                    @Override
                                    protected void callback(String url, ImageView iv, Bitmap bm, AjaxStatus status) {

                                        if (bm != null) {
                                            iv.setImageBitmap(bm);
                                        } else {
                                            iv.setImageResource(R.drawable.defaultbuilding);
                                        }
                                    }
                                });

                    }
                    holder.textView_right_subheader.setSingleLine(false);
                    if (Build.VERSION.SDK_INT >= 24) {
                        holder.textView_right_subheader.setText(Html.fromHtml("<b>" + "Event: " + "</b>" + messagesVo.getMessage()
                                + "<b>" + "<br/>Event Date: " + "</b>" + messagesVo.getEvent_date(), Html.FROM_HTML_MODE_LEGACY));

                    } else {
                        holder.textView_right_subheader.setText(Html.fromHtml("<b>" + "Event: " + "</b>" + messagesVo.getMessage()
                                + "<b>" + "<br/>Event Date: " + "</b>" + messagesVo.getEvent_date()));
                    }
                }
            }

        }

        return convertView;
    }

    static class ViewHolder{
        private TextView textView_left_header,textView_left_subheader,textView_right_header,textView_right_subheader,textView_dateTime;
        private LinearLayout linearLayout_left,linearLayout_right,linearLayout_left_arrow,linearLayout_right_arrow;
        private ImageView imageView_left,imageView_right;
        private TextView textView_unread_right,textView_unread_left,textView_status_right,textView_status_left;
        private RelativeLayout relative_messages;
    }

}
