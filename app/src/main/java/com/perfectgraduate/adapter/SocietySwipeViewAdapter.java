package com.perfectgraduate.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.callback.BitmapAjaxCallback;
import com.androidquery.callback.ImageOptions;
import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.perfectgraduate.R;
import com.perfectgraduate.activity.DetailActivity;
import com.perfectgraduate.activity.HomeActivity;
import com.perfectgraduate.app.PerfectGraduateApplication;
import com.perfectgraduate.database.DatabaseHelper;
import com.perfectgraduate.utility.Constants;
import com.perfectgraduate.vo.LinksVo;
import com.perfectgraduate.vo.SocietyVo;
import com.perfectgraduate.vo.UserCompanyVo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 03-Feb-17.
 */

public class SocietySwipeViewAdapter extends RecyclerSwipeAdapter<SocietySwipeViewAdapter.SimpleViewHolder> {

    private List<SocietyVo> societyVoList;
    private Context context;
    private HomeActivity activity;
    private DatabaseHelper databaseHelper;
    private List<LinksVo> linksVoList;
    AQuery aQuery;
    public SocietySwipeViewAdapter(Context context, List<SocietyVo> societyVoList) {
        this.context = context;
        this.societyVoList = societyVoList;
        activity = (HomeActivity) context;
        aQuery = new AQuery(activity);
        linksVoList = new ArrayList<>();
        databaseHelper = new DatabaseHelper(activity);
    }


    @Override
    public int getItemCount() {
        return societyVoList.size();
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_society, parent, false);
        return new SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final SimpleViewHolder viewHolder, final int position) {
        final SocietyVo item = societyVoList.get(position);

        viewHolder.textView_companyName.setText(item.getSociety_name());
        viewHolder.textView_due_date.setVisibility(View.GONE);


        viewHolder.swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);
        viewHolder.swipeLayout.setRightSwipeEnabled(false);
        viewHolder.swipeLayout.setLeftSwipeEnabled(false);

        if(Integer.parseInt(item.getUnread_message()) > 0) {
            viewHolder.message_badge.setVisibility(View.VISIBLE);
            viewHolder.message_badge.setText(item.getUnread_message());
        }else {
            viewHolder.message_badge.setVisibility(View.INVISIBLE);
        }

        viewHolder.imageView_company_messages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Constants.message = "society group";
                Constants.adder_id = item.getId();

                activity.particularSocietyMessage(item.getSociety_name());

            }
        });




        ImageOptions options = new ImageOptions();

        options.round = 10;

        options.memCache = true;

        options.fileCache = true;

        String imageUrl = "http://www.perfectgraduate.com/perfect/assets/uploads/"+item.getSocietylogo();

        aQuery.id(viewHolder.imageView_building).image(imageUrl, options);

        /*aQuery.id(viewHolder.imageView_building).image("http://www.perfectgraduate.com/perfect/assets/uploads/"+item.getLogo(), true, true, 0, 0, new BitmapAjaxCallback() {
            @Override
            protected void callback(String url, ImageView iv, Bitmap bm, AjaxStatus status) {

                if (bm != null) {
                    iv.setImageBitmap(bm);
                } else {
                    iv.setImageResource(R.drawable.defaultbuilding);
                }
            }
        });*/

        // Handling different events when swiping
        viewHolder.swipeLayout.addSwipeListener(new SwipeLayout.SwipeListener() {
            @Override
            public void onClose(SwipeLayout layout) {
                //when the SurfaceView totally cover the BottomView.
            }

            @Override
            public void onUpdate(SwipeLayout layout, int leftOffset, int topOffset) {
                //you are swiping.
            }

            @Override
            public void onStartOpen(SwipeLayout layout) {

            }

            @Override
            public void onOpen(SwipeLayout layout) {
                //when the BottomView totally show.
            }

            @Override
            public void onStartClose(SwipeLayout layout) {

                viewHolder.left_wrapper.setVisibility(View.VISIBLE);
            }

            @Override
            public void onHandRelease(SwipeLayout layout, float xvel, float yvel) {
                //when user's hand released.
            }
        });

        if (Constants.isVisibleSocietyDelete) {
            viewHolder.left_wrapper.setVisibility(View.VISIBLE);
        } else {
            viewHolder.left_wrapper.setVisibility(View.GONE);
        }

        viewHolder.left_wrapper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if( viewHolder.left_wrapper.getVisibility() == View.VISIBLE) {
                    viewHolder.swipeLayout.setRightSwipeEnabled(true);
                    viewHolder.swipeLayout.open();
                    viewHolder.left_wrapper.setVisibility(View.GONE);
                    viewHolder.swipeLayout.setRightSwipeEnabled(false);
                }
            }
        });

        viewHolder.linearLayout_company.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Uri uri;

                if(item.getWebsite().contains("http://")){
                    uri = Uri.parse(item.getWebsite());
                }else{
                    uri = Uri.parse("http://"+item.getWebsite());
                }

                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                activity.startActivity(intent);
            }
        });

        viewHolder.swipeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if ((((SwipeLayout) v).getOpenStatus() == SwipeLayout.Status.Close)) {
                    //Start your activity
                }

            }
        });

        viewHolder.swipeLayout.getSurfaceView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(context, " onClick : " + item.getNickName() , Toast.LENGTH_SHORT).show();
            }
        });


        viewHolder.textView_remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               /* viewHolder.swipeLayout.close();

                databaseHelper.deleteRow(item.getId());
                userCompanyVoList.remove(position);
                notifyDataSetChanged();
                notifyItemChanged(position);*/

            }
        });




        // mItemManger is member in RecyclerSwipeAdapter Class
        mItemManger.bindView(viewHolder.itemView, position);

    }



    //  ViewHolder Class

    public static class SimpleViewHolder extends RecyclerView.ViewHolder {
        SwipeLayout swipeLayout;
        TextView textView_companyName,textView_remove,textView_due_date,message_badge,textView_show;
        LinearLayout linearLayout_company;
        ImageView imageView_building,imageView_company_messages;
        RelativeLayout linearLayout_swipe,left_wrapper;
        public SimpleViewHolder(View itemView) {
            super(itemView);

            swipeLayout = (SwipeLayout) itemView.findViewById(R.id.swipe);
            textView_companyName = (TextView) itemView.findViewById(R.id.textView_companyName);
            textView_remove = (TextView) itemView.findViewById(R.id.textView_remove);
            textView_show = (TextView) itemView.findViewById(R.id.textView_show);
            textView_due_date = (TextView) itemView.findViewById(R.id.textView_due_date);
            message_badge = (TextView) itemView.findViewById(R.id.message_badge);
            imageView_building = (ImageView) itemView.findViewById(R.id.imageView_building);
            imageView_company_messages = (ImageView) itemView.findViewById(R.id.imageView_company_messages);

            linearLayout_company = (LinearLayout) itemView.findViewById(R.id.linearLayout_company);
            linearLayout_swipe = (RelativeLayout) itemView.findViewById(R.id.linearLayout_swipe);
            left_wrapper = (RelativeLayout) itemView.findViewById(R.id.left_wrapper);

        }
    }
}