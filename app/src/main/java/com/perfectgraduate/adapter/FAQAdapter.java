package com.perfectgraduate.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.perfectgraduate.R;
import com.perfectgraduate.vo.DisciplinesVo;
import com.perfectgraduate.vo.FAQVo;
import com.perfectgraduate.vo.QuesAnsVo;
import com.perfectgraduate.vo.Sub_disciplineVo;

import java.util.List;

/**
 * Created by user on 17-Jan-17.
 */

public class FAQAdapter extends BaseExpandableListAdapter {

    private Context context;
    private List<FAQVo> disciplineVoList;
    public FAQAdapter(Context context, List<FAQVo> VoList) {
        this.context = context;
        this.disciplineVoList = VoList;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        List<QuesAnsVo> subDisciplineVoList = disciplineVoList.get(groupPosition).getQuesAnsVoList();
        return subDisciplineVoList.get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild,
                             View convertView, ViewGroup parent) {

        QuesAnsVo detailInfo = (QuesAnsVo) getChild(groupPosition, childPosition);
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.list_item, null);
        }
        TextView expandedListTextView = (TextView) convertView
                .findViewById(R.id.expandedListItem);
        expandedListTextView.setText(detailInfo.getAnswer());
        return convertView;
    }

    @Override
    public int getChildrenCount(int listPosition) {
        List<QuesAnsVo> subDisciplineVoList = disciplineVoList.get(listPosition).getQuesAnsVoList();
        return subDisciplineVoList.size();
    }

    @Override
    public Object getGroup(int listPosition) {
        return disciplineVoList.get(listPosition);
    }

    @Override
    public int getGroupCount() {
        return disciplineVoList.size();
    }

    @Override
    public long getGroupId(int listPosition) {
        return listPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isLastChild, View view,
                             ViewGroup parent) {
        FAQVo headerInfo = (FAQVo) getGroup(groupPosition);
        if (view == null) {
            LayoutInflater layoutInflater = (LayoutInflater) context.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.list_group, null);
        }
        TextView listTitleTextView = (TextView) view
                .findViewById(R.id.expandedTitle);
        listTitleTextView.setTypeface(null, Typeface.BOLD);
        listTitleTextView.setText(headerInfo.getQuestion());

        return view;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public boolean isChildSelectable(int listPosition, int expandedListPosition) {
        return true;
    }
}