package com.perfectgraduate.adapter;

import android.app.Fragment;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.perfectgraduate.R;
import com.perfectgraduate.activity.HomeActivity;
import com.perfectgraduate.fragment.LocationFilterFragment;
import com.perfectgraduate.utility.Constants;
import com.perfectgraduate.vo.StateVo;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by user on 03-Jan-17.
 */

public class LocationFilterAdapter extends BaseAdapter{

    private List<StateVo> stateVoList;
    private List<StateVo> state;
    private ArrayList<String> stringList;
    private Context context;
    private LocationFilterFragment fragment;
    private HomeActivity activity;
    public LocationFilterAdapter(Context context, List<StateVo> VoList, ArrayList<String> list, LocationFilterFragment f){
        this.stateVoList = VoList;
        this.context = context;
        this.state = new ArrayList<StateVo>();
        this.stringList = new ArrayList<String>();
        this.state.addAll(stateVoList);
        this.stringList = list;
        Constants.checkedState = new ArrayList<>();
        Constants.checkedState.clear();
        if(stringList != null && stringList.size() > 0) {
            Constants.checkedState = stringList;
        }
        fragment = f;
        activity = (HomeActivity) context;
    }

    @Override
    public int getCount() {
        Constants.size = stateVoList.size();
        return stateVoList.size();
    }

    @Override
    public StateVo getItem(int position) {
        return stateVoList.get(position);
    }


    @Override
    public int getViewTypeCount() {
        return getCount();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {
        final ViewHolder holder;
        if (convertView == null) {

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.row_state, null);

            holder = new ViewHolder();
            holder.textView_state = (TextView) convertView.findViewById(R.id.textView_state);
            holder.linearLayout_states = (LinearLayout) convertView.findViewById(R.id.linearLayout_states);
            holder.checkBox_state = (CheckBox) convertView.findViewById(R.id.checkbox_state);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final StateVo stateVo = stateVoList.get(position);

        holder.textView_state.setText(stateVo.getName());

        if(stringList != null && stringList.size() > 0){
            if(stringList.contains(stateVo.getId())){
                holder.checkBox_state.setChecked(true);
            }else{
                holder.checkBox_state.setChecked(false);
            }
        }else{
            holder.checkBox_state.setChecked(false);
        }

        holder.linearLayout_states.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(holder.checkBox_state.isChecked()){
                    if(Constants.checkedState.contains(stateVo.getId())) {
                        Constants.checkedState.remove(stateVo.getId());
                        Constants.abrList.remove(stateVo.getAbr());
                        holder.checkBox_state.setChecked(false);
                    }
                }else {
                    if(Constants.checkedState != null && Constants.checkedState.size() > 0){
                        if (!Constants.checkedState.contains(stateVo.getId())) {
                            Constants.checkedState.add(stateVo.getId());
                            Constants.abrList.add(stateVo.getAbr());
                            holder.checkBox_state.setChecked(true);
                        }
                    }else {
                        Constants.checkedState.add(stateVo.getId());
                        Constants.abrList.add(stateVo.getAbr());
                        holder.checkBox_state.setChecked(true);
                    }
                }
                fragment.allLocation();

            }
        });

        return convertView;
    }

    public void filter(String charText) {
        // TODO Auto-generated method stub
        charText = charText.toLowerCase(Locale.getDefault());
        stateVoList.clear();
        if (charText.length() == 0) {
            stateVoList.addAll(state);
        } else {
            for (StateVo stateVo : state) {
                if (stateVo.getName().toLowerCase(Locale.getDefault()).contains(charText)) {
                    stateVoList.add(stateVo);
                }
            }
        }
        notifyDataSetChanged();
    }



    static class ViewHolder{
        private TextView textView_state;
        private CheckBox checkBox_state;
        private LinearLayout linearLayout_states;
    }
}
