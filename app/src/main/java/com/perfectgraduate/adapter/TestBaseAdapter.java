package com.perfectgraduate.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.SectionIndexer;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.callback.BitmapAjaxCallback;
import com.androidquery.callback.ImageOptions;
import com.perfectgraduate.R;
import com.perfectgraduate.activity.DetailActivity;
import com.perfectgraduate.app.PerfectGraduateApplication;
import com.perfectgraduate.utility.Constants;
import com.perfectgraduate.utility.stickylistheaders.StickyListHeadersAdapter;
import com.perfectgraduate.vo.UserCompanyVo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by user on 10-Dec-16.
 */

public class TestBaseAdapter extends BaseAdapter implements
        StickyListHeadersAdapter{

    private final Context mContext;
    private LayoutInflater mInflater;
    private List<UserCompanyVo> userCompanyVoList;
    private AQuery aQuery;
    private List<UserCompanyVo> searchList;
    private Activity activity;
    public TestBaseAdapter(Context context,List<UserCompanyVo> VoList) {
        mContext = context;
        mInflater = LayoutInflater.from(context);
        userCompanyVoList = VoList;
        aQuery = new AQuery(context);
        searchList = new ArrayList<>();
        searchList.addAll(VoList);
        activity = (Activity) mContext;
    }

    @Override
    public int getCount() {
        return userCompanyVoList.size();
    }

    @Override
    public UserCompanyVo getItem(int position) {
        return userCompanyVoList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.test_list_item_layout, parent, false);
            holder.text = (TextView) convertView.findViewById(R.id.text);
            holder.textView_message_text = (TextView) convertView.findViewById(R.id.textView_message_text);
            holder.view = (View) convertView.findViewById(R.id.view);
            holder.imageView_profile = (ImageView) convertView.findViewById(R.id.imageView_profile);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final UserCompanyVo userCompanyVo = userCompanyVoList.get(position);

        if(Constants.hideShow){
            if(position == 0){
                holder.view.setVisibility(View.GONE);
            }else {
                holder.view.setVisibility(View.VISIBLE);
            }
        }else{
            holder.view.setVisibility(View.GONE);
        }

        if(userCompanyVo.getApplication_due_date().equals("no")){

        }else if(userCompanyVo.getApplication_due_date().equals("Open Until Filled")){

        }else{
            holder.textView_message_text.setText(parseDateToddMMyyyy(userCompanyVo.getApplication_due_date()));
        }

        ImageOptions options = new ImageOptions();

        options.round = 20;

        options.memCache = true;

        options.fileCache = true;

        String imageUrl = "http://www.perfectgraduate.com/perfect/assets/uploads/"+userCompanyVo.getLogo();

        aQuery.id(holder.imageView_profile).image(imageUrl, options);

        /*aQuery.id(holder.imageView_profile).image("http://www.perfectgraduate.com/perfect/assets/uploads/"+userCompanyVo.getLogo(), true, true, 0, 0, new BitmapAjaxCallback() {
            @Override
            protected void callback(String url, ImageView iv, Bitmap bm, AjaxStatus status) {

                if (bm != null) {
                    iv.setImageBitmap(bm);
                } else {
                    iv.setImageResource(R.drawable.defaultbuilding);
                }
            }
        });*/

        holder.text.setText(userCompanyVo.getCompany_name());

        return convertView;
    }

    public String parseDateToddMMyyyy(String time) {
        String inputPattern = "yyyy-MM-dd";
        String outputPattern = "dd/MM/yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern, Locale.US);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    @Override
    public View getHeaderView(int position, View convertView, ViewGroup parent) {
        HeaderViewHolder holder;

        if (convertView == null) {
            holder = new HeaderViewHolder();
            convertView = mInflater.inflate(R.layout.row_header, parent, false);
            holder.text = (TextView) convertView.findViewById(R.id.text1);
            convertView.setTag(holder);
        } else {
            holder = (HeaderViewHolder) convertView.getTag();
        }

        if(Constants.hideShow){
            holder.text.setVisibility(View.GONE);
        }else{
            holder.text.setVisibility(View.VISIBLE);
        }

        // set header text as first char in name



        CharSequence headerChar = userCompanyVoList.get(position).getCompany_name().subSequence(0, 1);
        if (headerChar.toString().matches("[0-9]+") && headerChar.length() == 1) {
            holder.text.setText("#");
        }else{
            holder.text.setText(headerChar);
        }

        return convertView;
    }

    /**
     * Remember that these have to be static, postion=1 should always return
     * the same Id that is.
     */
    @Override
    public long getHeaderId(int position) {
        // return the first character of the country as ID because this is what
        // headers are based upon
        return userCompanyVoList.get(position).getCompany_name().subSequence(0, 1).charAt(0);
    }

    public void filter(String charText) {
        // TODO Auto-generated method stub
        charText = charText.toLowerCase(Locale.getDefault());
        userCompanyVoList.clear();
        if (charText.length() == 0) {
            userCompanyVoList.addAll(searchList);
        } else {
            for (UserCompanyVo userCompanyVo : searchList) {
                if (userCompanyVo.getCompany_name().toLowerCase(Locale.getDefault()).contains(charText)) {
                    userCompanyVoList.add(userCompanyVo);
                }
            }
        }
        notifyDataSetChanged();
    }

    class HeaderViewHolder {
        TextView text;
    }

    class ViewHolder {
        TextView text,textView_message_text;
        View view;
        ImageView imageView_profile;
    }

}
