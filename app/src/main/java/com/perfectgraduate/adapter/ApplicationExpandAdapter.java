package com.perfectgraduate.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.perfectgraduate.R;
import com.perfectgraduate.vo.ApplicationDueDateAllVo;
import com.perfectgraduate.vo.ApplicationVo;
import com.perfectgraduate.vo.DisciplineVo;
import com.perfectgraduate.vo.SubDisciplineVo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by user on 17-Jan-17.
 */

public class ApplicationExpandAdapter extends BaseExpandableListAdapter {

    private Context context;
    private List<ApplicationVo> applicationVoList;
    public ApplicationExpandAdapter(Context context, List<ApplicationVo> VoList) {
        this.context = context;
        this.applicationVoList = VoList;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        List<ApplicationDueDateAllVo> allVos = applicationVoList.get(groupPosition).getDateAllVoList();
        return allVos.get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild,
                             View convertView, ViewGroup parent) {

        ApplicationDueDateAllVo dateAllVo = (ApplicationDueDateAllVo) getChild(groupPosition, childPosition);
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.row_child_expand, null);
        }
        TextView location = (TextView) convertView
                .findViewById(R.id.textView_location);
        location.setText(dateAllVo.getState());

        TextView startDate = (TextView) convertView
                .findViewById(R.id.textView_start);
        startDate.setText(parseDateToddMMyyyy(dateAllVo.getStart_date()));

        TextView endDate = (TextView) convertView
                .findViewById(R.id.textView_end);
        endDate.setText(parseDateToddMMyyyy(dateAllVo.getClose_date()));
        return convertView;
    }

    public String parseDateToddMMyyyy(String time) {
        String inputPattern = "yyyy-MM-dd";
        String outputPattern = "dd-MM-yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern, Locale.US);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    @Override
    public int getChildrenCount(int listPosition) {
        List<ApplicationDueDateAllVo> allVos = applicationVoList.get(listPosition).getDateAllVoList();
        return allVos.size();
    }

    @Override
    public Object getGroup(int listPosition) {
        return applicationVoList.get(listPosition);
    }

    @Override
    public int getGroupCount() {
        return applicationVoList.size();
    }

    @Override
    public long getGroupId(int listPosition) {
        return listPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isLastChild, View view,
                             ViewGroup parent) {
        ApplicationVo applicationVo = (ApplicationVo) getGroup(groupPosition);
        if (view == null) {
            LayoutInflater layoutInflater = (LayoutInflater) context.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.list_group, null);
        }
        TextView listTitleTextView = (TextView) view
                .findViewById(R.id.expandedTitle);
        listTitleTextView.setText(applicationVo.getProgram());

        ImageView imageView_expand = (ImageView) view.findViewById(R.id.imageView_expand);

        if(isLastChild){
            imageView_expand.setImageResource(R.drawable.negative_grey);
        }else{
            imageView_expand.setImageResource(R.drawable.add_grey);
        }

        return view;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public boolean isChildSelectable(int listPosition, int expandedListPosition) {
        return true;
    }
}