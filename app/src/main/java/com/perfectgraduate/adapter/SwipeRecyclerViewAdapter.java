package com.perfectgraduate.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.callback.BitmapAjaxCallback;
import com.androidquery.callback.ImageOptions;
import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.perfectgraduate.R;
import com.perfectgraduate.activity.DetailActivity;
import com.perfectgraduate.activity.HomeActivity;
import com.perfectgraduate.app.PerfectGraduateApplication;
import com.perfectgraduate.database.DatabaseHelper;
import com.perfectgraduate.utility.Constants;
import com.perfectgraduate.vo.LinksVo;
import com.perfectgraduate.vo.UserCompanyVo;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by user on 7/22/2016.
 */

public class SwipeRecyclerViewAdapter extends RecyclerSwipeAdapter<SwipeRecyclerViewAdapter.SimpleViewHolder> {

    private List<UserCompanyVo> userCompanyVoList;
    private Context context;
    private HomeActivity activity;
    private DatabaseHelper databaseHelper;
    private List<LinksVo> linksVoList;
    AQuery aQuery;
    public SwipeRecyclerViewAdapter(Context context, List<UserCompanyVo> userCompanyVoList) {
        this.context = context;
        this.userCompanyVoList = userCompanyVoList;
        activity = (HomeActivity) context;
        aQuery = new AQuery(activity);
        linksVoList = new ArrayList<>();
        databaseHelper = new DatabaseHelper(activity);
    }


    @Override
    public int getItemCount() {
        return userCompanyVoList.size();
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.swipe_row_item, parent, false);
        return new SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final SimpleViewHolder viewHolder, final int position) {
        final UserCompanyVo item = userCompanyVoList.get(position);

        viewHolder.textView_companyName.setText(item.getCompany_name());
        viewHolder.textView_due_date.setText(item.getApplication_due_date());


        viewHolder.swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);
        viewHolder.swipeLayout.setRightSwipeEnabled(false);
        viewHolder.swipeLayout.setLeftSwipeEnabled(false);
//add drag edge.(If the BottomView has 'layout_gravity' attribute, this line is unnecessary)
        // Drag From Left

        if(Integer.parseInt(item.getUnread_message()) > 0) {
            viewHolder.message_badge.setVisibility(View.VISIBLE);
            viewHolder.message_badge.setText(item.getUnread_message());
        }else {
            viewHolder.message_badge.setVisibility(View.INVISIBLE);
        }

        viewHolder.imageView_company_messages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Constants.message = "particular message";

                activity.particularMessage(item.getCompany_name(),item,position);

            }
        });

        linksVoList = databaseHelper.getLinks(item.getId());

        if(linksVoList != null && linksVoList.size() > 0) {

            viewHolder.linearLayout_media.removeAllViews();

            for (int i = 0; i < linksVoList.size(); i++) {

                final ImageView imageView = new ImageView(activity);
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(45,45);
                if(i == 0) {
                    params.setMargins(0, 0, 0, 0);
                }else{
                    params.setMargins(8, 0, 0, 0);
                }
                imageView.setLayoutParams(params);
                imageView.setId(1000 + i);
                imageView.setTag(i);

                aQuery.id(imageView).image("http://www.perfectgraduate.com/perfect/assets/uploads/"+linksVoList.get(i).getMedia_logo(), true, true, 0, 0, new BitmapAjaxCallback() {
                    @Override
                    protected void callback(String url, ImageView iv, Bitmap bm, AjaxStatus status) {

                        if (bm != null) {
                            iv.setImageBitmap(bm);
                        }
                    }
                });

                viewHolder.linearLayout_media.addView(imageView);
            }
        }


        ImageOptions options = new ImageOptions();

        options.round = 10;

        options.memCache = true;

        options.fileCache = true;

        String imageUrl = "http://www.perfectgraduate.com/perfect/assets/uploads/"+item.getLogo();

        aQuery.id(viewHolder.imageView_building).image(imageUrl, options);

        /*aQuery.id(viewHolder.imageView_building).image("http://www.perfectgraduate.com/perfect/assets/uploads/"+item.getLogo(), true, true, 0, 0, new BitmapAjaxCallback() {
            @Override
            protected void callback(String url, ImageView iv, Bitmap bm, AjaxStatus status) {

                if (bm != null) {
                    iv.setImageBitmap(bm);
                } else {
                    iv.setImageResource(R.drawable.defaultbuilding);
                }
            }
        });*/

        // Handling different events when swiping
        viewHolder.swipeLayout.addSwipeListener(new SwipeLayout.SwipeListener() {
            @Override
            public void onClose(SwipeLayout layout) {
                //when the SurfaceView totally cover the BottomView.
            }

            @Override
            public void onUpdate(SwipeLayout layout, int leftOffset, int topOffset) {
                //you are swiping.
            }

            @Override
            public void onStartOpen(SwipeLayout layout) {

            }

            @Override
            public void onOpen(SwipeLayout layout) {
                //when the BottomView totally show.
            }

            @Override
            public void onStartClose(SwipeLayout layout) {

                viewHolder.left_wrapper.setVisibility(View.VISIBLE);
            }

            @Override
            public void onHandRelease(SwipeLayout layout, float xvel, float yvel) {
                //when user's hand released.
            }
        });

        if (Constants.isVisible) {
            viewHolder.left_wrapper.setVisibility(View.VISIBLE);
        } else {
            viewHolder.left_wrapper.setVisibility(View.GONE);
        }

        viewHolder.left_wrapper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if( viewHolder.left_wrapper.getVisibility() == View.VISIBLE) {
                    viewHolder.swipeLayout.setRightSwipeEnabled(true);
                    viewHolder.swipeLayout.open();
                    viewHolder.left_wrapper.setVisibility(View.GONE);
                    viewHolder.swipeLayout.setRightSwipeEnabled(false);
                }
            }
        });

        viewHolder.linearLayout_company.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                /*viewHolder.swipeLayout.setLeftSwipeEnabled(true);
                viewHolder.swipeLayout.open();
                viewHolder.swipeLayout.setLeftSwipeEnabled(false);*/
                PerfectGraduateApplication.getInstance().setUserCompanyVo(userCompanyVoList.get(position));

                /*Intent intent = new Intent(activity, DetailActivity.class);
                activity.startActivity(intent);*/
                activity.displayDetail(item.getCompany_name());
            }
        });

        viewHolder.swipeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if ((((SwipeLayout) v).getOpenStatus() == SwipeLayout.Status.Close)) {
                    //Start your activity
                }

            }
        });

        viewHolder.swipeLayout.getSurfaceView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(context, " onClick : " + item.getNickName() , Toast.LENGTH_SHORT).show();
            }
        });


        viewHolder.textView_remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                viewHolder.swipeLayout.close();

                databaseHelper.deleteRow(item.getId());
                userCompanyVoList.remove(position);
                notifyDataSetChanged();
                notifyItemChanged(position);

            }
        });




        // mItemManger is member in RecyclerSwipeAdapter Class
        mItemManger.bindView(viewHolder.itemView, position);

    }



    //  ViewHolder Class

    public static class SimpleViewHolder extends RecyclerView.ViewHolder {
        SwipeLayout swipeLayout;
        TextView textView_companyName,textView_remove,textView_due_date,message_badge,textView_show;
        LinearLayout linearLayout_company,linearLayout_media;
        ImageView imageView_building,imageView_company_messages;
        RelativeLayout linearLayout_swipe,left_wrapper;
        public SimpleViewHolder(View itemView) {
            super(itemView);

            swipeLayout = (SwipeLayout) itemView.findViewById(R.id.swipe);
            textView_companyName = (TextView) itemView.findViewById(R.id.textView_companyName);
            textView_remove = (TextView) itemView.findViewById(R.id.textView_remove);
            textView_show = (TextView) itemView.findViewById(R.id.textView_show);
            textView_due_date = (TextView) itemView.findViewById(R.id.textView_due_date);
            message_badge = (TextView) itemView.findViewById(R.id.message_badge);
            imageView_building = (ImageView) itemView.findViewById(R.id.imageView_building);
            imageView_company_messages = (ImageView) itemView.findViewById(R.id.imageView_company_messages);

            linearLayout_media = (LinearLayout) itemView.findViewById(R.id.linearLayout_media);
            linearLayout_company = (LinearLayout) itemView.findViewById(R.id.linearLayout_company);
            linearLayout_swipe = (RelativeLayout) itemView.findViewById(R.id.linearLayout_swipe);
            left_wrapper = (RelativeLayout) itemView.findViewById(R.id.left_wrapper);

        }
    }
}
