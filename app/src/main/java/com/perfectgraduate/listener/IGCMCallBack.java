package com.perfectgraduate.listener;

public interface IGCMCallBack {
	void onGCMCallBack(String response);
}
