package com.perfectgraduate.listener;

public interface ICallBack {
	void onCallBackResult(String response);
}
