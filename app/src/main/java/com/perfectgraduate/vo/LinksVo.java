package com.perfectgraduate.vo;

/**
 * Created by user on 19-Dec-16.
 */

public class LinksVo {

    private String id;
    private String company_id;
    private String media_type;
    private String media_link;
    private String media_logo;
    private String default_id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCompany_id() {
        return company_id;
    }

    public void setCompany_id(String company_id) {
        this.company_id = company_id;
    }

    public String getMedia_type() {
        return media_type;
    }

    public void setMedia_type(String media_type) {
        this.media_type = media_type;
    }

    public String getMedia_link() {
        return media_link;
    }

    public void setMedia_link(String media_link) {
        this.media_link = media_link;
    }

    public String getMedia_logo() {
        return media_logo;
    }

    public void setMedia_logo(String media_logo) {
        this.media_logo = media_logo;
    }

    public String getDefault_id() {
        return default_id;
    }

    public void setDefault_id(String default_id) {
        this.default_id = default_id;
    }

    @Override
    public String toString() {
        return "LinksVo{" +
                "id='" + id + '\'' +
                ", company_id='" + company_id + '\'' +
                ", media_type='" + media_type + '\'' +
                ", media_link='" + media_link + '\'' +
                ", media_logo='" + media_logo + '\'' +
                ", default_id='" + default_id + '\'' +
                '}';
    }
}
