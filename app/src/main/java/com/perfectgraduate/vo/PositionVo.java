package com.perfectgraduate.vo;

/**
 * Created by user on 13-Dec-16.
 */

public class PositionVo {

    private String id;
    private String name;
    private String abr;
    private String country_id;
    private String status;
    private String date;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAbr() {
        return abr;
    }

    public void setAbr(String abr) {
        this.abr = abr;
    }

    public String getCountry_id() {
        return country_id;
    }

    public void setCountry_id(String country_id) {
        this.country_id = country_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "PositionVo{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", abr='" + abr + '\'' +
                ", country_id='" + country_id + '\'' +
                ", status='" + status + '\'' +
                ", date='" + date + '\'' +
                '}';
    }
}
