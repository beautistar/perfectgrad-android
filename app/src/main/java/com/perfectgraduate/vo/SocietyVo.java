package com.perfectgraduate.vo;

/**
 * Created by user on 13-Dec-16.
 */

public class SocietyVo {

    private String id;
    private String society_id;
    private String parent_id;
    private String staff_id;
    private String creator_name;
    private String website;
    private String society_email;
    private String password;
    private String first_login_status;
    private String change_pass;
    private String pass;
    private String uni_id;
    private String country_id;
    private String state_id;
    private String societylogo;
    private String creator_mobile;
    private String society_name;
    private String society_president;
    private String president_email;
    private String president_mobile;
    private String Treasurer_name;
    private String Treasurer_email;
    private String Treasure_mobile;
    private String Secetery_name;
    private String Secetery_email;
    private String Secetery_mobile;
    private String facebookemail;
    private String user_type;
    private String status;
    private String locks;
    private String date;
    private String key;
    private String created_by;
    private String modified_date;
    private String society_code;
    private String logo;
    private String unread_message;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSociety_id() {
        return society_id;
    }

    public void setSociety_id(String society_id) {
        this.society_id = society_id;
    }

    public String getParent_id() {
        return parent_id;
    }

    public void setParent_id(String parent_id) {
        this.parent_id = parent_id;
    }

    public String getStaff_id() {
        return staff_id;
    }

    public void setStaff_id(String staff_id) {
        this.staff_id = staff_id;
    }

    public String getCreator_name() {
        return creator_name;
    }

    public void setCreator_name(String creator_name) {
        this.creator_name = creator_name;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getSociety_email() {
        return society_email;
    }

    public void setSociety_email(String society_email) {
        this.society_email = society_email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirst_login_status() {
        return first_login_status;
    }

    public void setFirst_login_status(String first_login_status) {
        this.first_login_status = first_login_status;
    }

    public String getChange_pass() {
        return change_pass;
    }

    public void setChange_pass(String change_pass) {
        this.change_pass = change_pass;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getUni_id() {
        return uni_id;
    }

    public void setUni_id(String uni_id) {
        this.uni_id = uni_id;
    }

    public String getCountry_id() {
        return country_id;
    }

    public void setCountry_id(String country_id) {
        this.country_id = country_id;
    }

    public String getState_id() {
        return state_id;
    }

    public void setState_id(String state_id) {
        this.state_id = state_id;
    }

    public String getSocietylogo() {
        return societylogo;
    }

    public void setSocietylogo(String societylogo) {
        this.societylogo = societylogo;
    }

    public String getCreator_mobile() {
        return creator_mobile;
    }

    public void setCreator_mobile(String creator_mobile) {
        this.creator_mobile = creator_mobile;
    }

    public String getSociety_name() {
        return society_name;
    }

    public void setSociety_name(String society_name) {
        this.society_name = society_name;
    }

    public String getSociety_president() {
        return society_president;
    }

    public void setSociety_president(String society_president) {
        this.society_president = society_president;
    }

    public String getPresident_email() {
        return president_email;
    }

    public void setPresident_email(String president_email) {
        this.president_email = president_email;
    }

    public String getPresident_mobile() {
        return president_mobile;
    }

    public void setPresident_mobile(String president_mobile) {
        this.president_mobile = president_mobile;
    }

    public String getTreasurer_name() {
        return Treasurer_name;
    }

    public void setTreasurer_name(String treasurer_name) {
        Treasurer_name = treasurer_name;
    }

    public String getTreasurer_email() {
        return Treasurer_email;
    }

    public void setTreasurer_email(String treasurer_email) {
        Treasurer_email = treasurer_email;
    }

    public String getTreasure_mobile() {
        return Treasure_mobile;
    }

    public void setTreasure_mobile(String treasure_mobile) {
        Treasure_mobile = treasure_mobile;
    }

    public String getSecetery_name() {
        return Secetery_name;
    }

    public void setSecetery_name(String secetery_name) {
        Secetery_name = secetery_name;
    }

    public String getSecetery_email() {
        return Secetery_email;
    }

    public void setSecetery_email(String secetery_email) {
        Secetery_email = secetery_email;
    }

    public String getSecetery_mobile() {
        return Secetery_mobile;
    }

    public void setSecetery_mobile(String secetery_mobile) {
        Secetery_mobile = secetery_mobile;
    }

    public String getFacebookemail() {
        return facebookemail;
    }

    public void setFacebookemail(String facebookemail) {
        this.facebookemail = facebookemail;
    }

    public String getUser_type() {
        return user_type;
    }

    public void setUser_type(String user_type) {
        this.user_type = user_type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLocks() {
        return locks;
    }

    public void setLocks(String locks) {
        this.locks = locks;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public String getModified_date() {
        return modified_date;
    }

    public void setModified_date(String modified_date) {
        this.modified_date = modified_date;
    }

    public String getSociety_code() {
        return society_code;
    }

    public void setSociety_code(String society_code) {
        this.society_code = society_code;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getUnread_message() {
        return unread_message;
    }

    public void setUnread_message(String unread_message) {
        this.unread_message = unread_message;
    }

    @Override
    public String toString() {
        return "SocietyVo{" +
                "id='" + id + '\'' +
                ", society_id='" + society_id + '\'' +
                ", parent_id='" + parent_id + '\'' +
                ", staff_id='" + staff_id + '\'' +
                ", creator_name='" + creator_name + '\'' +
                ", website='" + website + '\'' +
                ", society_email='" + society_email + '\'' +
                ", password='" + password + '\'' +
                ", first_login_status='" + first_login_status + '\'' +
                ", change_pass='" + change_pass + '\'' +
                ", pass='" + pass + '\'' +
                ", uni_id='" + uni_id + '\'' +
                ", country_id='" + country_id + '\'' +
                ", state_id='" + state_id + '\'' +
                ", societylogo='" + societylogo + '\'' +
                ", creator_mobile='" + creator_mobile + '\'' +
                ", society_name='" + society_name + '\'' +
                ", society_president='" + society_president + '\'' +
                ", president_email='" + president_email + '\'' +
                ", president_mobile='" + president_mobile + '\'' +
                ", Treasurer_name='" + Treasurer_name + '\'' +
                ", Treasurer_email='" + Treasurer_email + '\'' +
                ", Treasure_mobile='" + Treasure_mobile + '\'' +
                ", Secetery_name='" + Secetery_name + '\'' +
                ", Secetery_email='" + Secetery_email + '\'' +
                ", Secetery_mobile='" + Secetery_mobile + '\'' +
                ", facebookemail='" + facebookemail + '\'' +
                ", user_type='" + user_type + '\'' +
                ", status='" + status + '\'' +
                ", locks='" + locks + '\'' +
                ", date='" + date + '\'' +
                ", key='" + key + '\'' +
                ", created_by='" + created_by + '\'' +
                ", modified_date='" + modified_date + '\'' +
                ", society_code='" + society_code + '\'' +
                ", logo='" + logo + '\'' +
                ", unread_message='" + unread_message + '\'' +
                '}';
    }
}
