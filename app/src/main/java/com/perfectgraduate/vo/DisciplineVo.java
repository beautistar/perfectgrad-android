package com.perfectgraduate.vo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 13-Dec-16.
 */

public class DisciplineVo {

    private String id = "";
    private String name = "";
    private List<SubDisciplineVo> subDisciplineVoList = new ArrayList<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<SubDisciplineVo> getSubDisciplineVoList() {
        return subDisciplineVoList;
    }

    public void setSubDisciplineVoList(List<SubDisciplineVo> subDisciplineVoList) {
        this.subDisciplineVoList = subDisciplineVoList;
    }

    @Override
    public String toString() {
        return "DisciplineVo{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", subDisciplineVoList=" + subDisciplineVoList +
                '}';
    }
}
