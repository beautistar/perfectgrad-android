package com.perfectgraduate.vo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 13-Dec-16.
 */

public class SubDisciplineVo {

    private String discipline_id;
    private String id;
    private String country_id;
    private String name;
    private String status;
    private String date;
    private String companys;

    private List<AllCompanysVo> allCompanysVoList = new ArrayList<>();

    public String getDiscipline_id() {
        return discipline_id;
    }

    public void setDiscipline_id(String discipline_id) {
        this.discipline_id = discipline_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCountry_id() {
        return country_id;
    }

    public void setCountry_id(String country_id) {
        this.country_id = country_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCompanys() {
        return companys;
    }

    public void setCompanys(String companys) {
        this.companys = companys;
    }

    public List<AllCompanysVo> getAllCompanysVoList() {
        return allCompanysVoList;
    }

    public void setAllCompanysVoList(List<AllCompanysVo> allCompanysVoList) {
        this.allCompanysVoList = allCompanysVoList;
    }

    @Override
    public String toString() {
        return "SubDisciplineVo{" +
                "discipline_id='" + discipline_id + '\'' +
                ", id='" + id + '\'' +
                ", country_id='" + country_id + '\'' +
                ", name='" + name + '\'' +
                ", status='" + status + '\'' +
                ", date='" + date + '\'' +
                ", companys='" + companys + '\'' +
                ", allCompanysVoList=" + allCompanysVoList +
                '}';
    }
}
