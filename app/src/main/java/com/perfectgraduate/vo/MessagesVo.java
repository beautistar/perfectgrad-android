package com.perfectgraduate.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 30-Dec-16.
 */

public class MessagesVo implements Serializable {

    private String id = "";
    private String type = "";
    private String adder_id = "";
    private String sub_adder_id = "";
    private String message_type = "";
    private String event_title = "";
    private String event_date = "";
    private String eventtimefrom = "";
    private String eventtimeto = "";
    private String event_venue = "";
    private String message = "";
    private String gender = "";
    private String send_by = "";
    private String recall = "";
    private String status = "";
    private String like_status = "";
    private String report = "";
    private String date = "";
    private String website = "";
    private String addedStatus = "";
    private String eventstatus = "";
    private String international = "";
    private String eventconfirmation = "";
    private String default_timezone = "";
    private String perfect_logo = "";
    private List<AdderDetailVo> adderDetailVoList = new ArrayList<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAdder_id() {
        return adder_id;
    }

    public void setAdder_id(String adder_id) {
        this.adder_id = adder_id;
    }

    public String getSub_adder_id() {
        return sub_adder_id;
    }

    public void setSub_adder_id(String sub_adder_id) {
        this.sub_adder_id = sub_adder_id;
    }

    public String getMessage_type() {
        return message_type;
    }

    public void setMessage_type(String message_type) {
        this.message_type = message_type;
    }

    public String getEvent_title() {
        return event_title;
    }

    public void setEvent_title(String event_title) {
        this.event_title = event_title;
    }

    public String getEvent_date() {
        return event_date;
    }

    public void setEvent_date(String event_date) {
        this.event_date = event_date;
    }

    public String getEventtimefrom() {
        return eventtimefrom;
    }

    public void setEventtimefrom(String eventtimefrom) {
        this.eventtimefrom = eventtimefrom;
    }

    public String getEventtimeto() {
        return eventtimeto;
    }

    public void setEventtimeto(String eventtimeto) {
        this.eventtimeto = eventtimeto;
    }

    public String getEvent_venue() {
        return event_venue;
    }

    public void setEvent_venue(String event_venue) {
        this.event_venue = event_venue;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getSend_by() {
        return send_by;
    }

    public void setSend_by(String send_by) {
        this.send_by = send_by;
    }

    public String getRecall() {
        return recall;
    }

    public void setRecall(String recall) {
        this.recall = recall;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLike_status() {
        return like_status;
    }

    public void setLike_status(String like_status) {
        this.like_status = like_status;
    }

    public String getReport() {
        return report;
    }

    public void setReport(String report) {
        this.report = report;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getAddedStatus() {
        return addedStatus;
    }

    public void setAddedStatus(String addedStatus) {
        this.addedStatus = addedStatus;
    }

    public String getEventstatus() {
        return eventstatus;
    }

    public void setEventstatus(String eventstatus) {
        this.eventstatus = eventstatus;
    }

    public String getInternational() {
        return international;
    }

    public void setInternational(String international) {
        this.international = international;
    }

    public String getEventconfirmation() {
        return eventconfirmation;
    }

    public void setEventconfirmation(String eventconfirmation) {
        this.eventconfirmation = eventconfirmation;
    }

    public String getDefault_timezone() {
        return default_timezone;
    }

    public void setDefault_timezone(String default_timezone) {
        this.default_timezone = default_timezone;
    }

    public String getPerfect_logo() {
        return perfect_logo;
    }

    public void setPerfect_logo(String perfect_logo) {
        this.perfect_logo = perfect_logo;
    }

    public List<AdderDetailVo> getAdderDetailVoList() {
        return adderDetailVoList;
    }

    public void setAdderDetailVoList(List<AdderDetailVo> adderDetailVoList) {
        this.adderDetailVoList = adderDetailVoList;
    }

    @Override
    public String toString() {
        return "MessagesVo{" +
                "id='" + id + '\'' +
                ", type='" + type + '\'' +
                ", adder_id='" + adder_id + '\'' +
                ", sub_adder_id='" + sub_adder_id + '\'' +
                ", message_type='" + message_type + '\'' +
                ", event_title='" + event_title + '\'' +
                ", event_date='" + event_date + '\'' +
                ", eventtimefrom='" + eventtimefrom + '\'' +
                ", eventtimeto='" + eventtimeto + '\'' +
                ", event_venue='" + event_venue + '\'' +
                ", message='" + message + '\'' +
                ", gender='" + gender + '\'' +
                ", send_by='" + send_by + '\'' +
                ", recall='" + recall + '\'' +
                ", status='" + status + '\'' +
                ", like_status='" + like_status + '\'' +
                ", report='" + report + '\'' +
                ", date='" + date + '\'' +
                ", website='" + website + '\'' +
                ", addedStatus='" + addedStatus + '\'' +
                ", eventstatus='" + eventstatus + '\'' +
                ", international='" + international + '\'' +
                ", eventconfirmation='" + eventconfirmation + '\'' +
                ", default_timezone='" + default_timezone + '\'' +
                ", perfect_logo='" + perfect_logo + '\'' +
                ", adderDetailVoList=" + adderDetailVoList +
                '}';
    }
}
