package com.perfectgraduate.vo;

import java.io.Serializable;

/**
 * Created by user on 30-Dec-16.
 */

public class AdderDetailVo implements Serializable {

    private String id = "";
    private String name = "";
    private String logo = "";
    private String package_id = "";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getPackage_id() {
        return package_id;
    }

    public void setPackage_id(String package_id) {
        this.package_id = package_id;
    }

    @Override
    public String toString() {
        return "AdderDetailVo{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", logo='" + logo + '\'' +
                ", package_id='" + package_id + '\'' +
                '}';
    }
}
