package com.perfectgraduate.vo;

/**
 * Created by user on 08-Dec-16.
 */

public class ItemVo {

    private String title;
    private int index;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    @Override
    public String toString() {
        return "ItemVo{" +
                "title='" + title + '\'' +
                ", index=" + index +
                '}';
    }
}
