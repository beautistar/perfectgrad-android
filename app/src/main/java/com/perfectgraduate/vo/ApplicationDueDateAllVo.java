package com.perfectgraduate.vo;

/**
 * Created by user on 19-Dec-16.
 */

public class ApplicationDueDateAllVo {

    private String id;
    private String company_id;
    private String state_id;
    private String program_id;
    private String start_date;
    private String close_date;
    private String created;
    private String state;
    private String program;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCompany_id() {
        return company_id;
    }

    public void setCompany_id(String company_id) {
        this.company_id = company_id;
    }

    public String getState_id() {
        return state_id;
    }

    public void setState_id(String state_id) {
        this.state_id = state_id;
    }

    public String getProgram_id() {
        return program_id;
    }

    public void setProgram_id(String program_id) {
        this.program_id = program_id;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getClose_date() {
        return close_date;
    }

    public void setClose_date(String close_date) {
        this.close_date = close_date;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getProgram() {
        return program;
    }

    public void setProgram(String program) {
        this.program = program;
    }

    @Override
    public String toString() {
        return "ApplicationDueDateAllVo{" +
                "id='" + id + '\'' +
                ", company_id='" + company_id + '\'' +
                ", state_id='" + state_id + '\'' +
                ", program_id='" + program_id + '\'' +
                ", start_date='" + start_date + '\'' +
                ", close_date='" + close_date + '\'' +
                ", created='" + created + '\'' +
                ", state='" + state + '\'' +
                ", program='" + program + '\'' +
                '}';
    }
}
