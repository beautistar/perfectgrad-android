package com.perfectgraduate.vo;

/**
 * Created by user on 05-Jan-17.
 */

public class AllCompanysVo {

    private String id = "";
    private String company_id = "";
    private String discipline_id = "";
    private String sub_discipline_id = "";
    private String type = "";
    private String company_code = "";
    private String parent_id = "";
    private String parent_type = "";
    private String bussiness_no = "";
    private String company_name = "";
    private String company_intro = "";
    private String company_type = "";
    private String website = "";
    private String logo = "";
    private String rating = "";
    private String awards = "";
    private String email = "";
    private String password = "";
    private String pass = "";
    private String description = "";
    private String contact_name = "";
    private String contact_email = "";
    private String phone = "";
    private String phonecode = "";
    private String mobilecode = "";
    private String mobile = "";
    private String general_no = "";
    private String company_address = "";
    private String city = "";
    private String country_id = "";
    private String state_id = "";
    private String notes_CRM = "";
    private String status = "";
    private String first_login_status = "";
    private String change_pass = "";
    private String lock = "";
    private String user_type = "";
    private String package_id = "";
    private String messaging_country = "";
    private String application_due_date = "";
    private String key = "";
    private String message_limit = "";
    private String send_message = "";
    private String date = "";
    private String modified_date = "";
    private String added_by = "";
    private String modified_by = "";
    private String package_start = "";
    private String package_end = "";


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCompany_id() {
        return company_id;
    }

    public void setCompany_id(String company_id) {
        this.company_id = company_id;
    }

    public String getDiscipline_id() {
        return discipline_id;
    }

    public void setDiscipline_id(String discipline_id) {
        this.discipline_id = discipline_id;
    }

    public String getSub_discipline_id() {
        return sub_discipline_id;
    }

    public void setSub_discipline_id(String sub_discipline_id) {
        this.sub_discipline_id = sub_discipline_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCompany_code() {
        return company_code;
    }

    public void setCompany_code(String company_code) {
        this.company_code = company_code;
    }

    public String getParent_id() {
        return parent_id;
    }

    public void setParent_id(String parent_id) {
        this.parent_id = parent_id;
    }

    public String getParent_type() {
        return parent_type;
    }

    public void setParent_type(String parent_type) {
        this.parent_type = parent_type;
    }

    public String getBussiness_no() {
        return bussiness_no;
    }

    public void setBussiness_no(String bussiness_no) {
        this.bussiness_no = bussiness_no;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getCompany_intro() {
        return company_intro;
    }

    public void setCompany_intro(String company_intro) {
        this.company_intro = company_intro;
    }

    public String getCompany_type() {
        return company_type;
    }

    public void setCompany_type(String company_type) {
        this.company_type = company_type;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getAwards() {
        return awards;
    }

    public void setAwards(String awards) {
        this.awards = awards;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getContact_name() {
        return contact_name;
    }

    public void setContact_name(String contact_name) {
        this.contact_name = contact_name;
    }

    public String getContact_email() {
        return contact_email;
    }

    public void setContact_email(String contact_email) {
        this.contact_email = contact_email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhonecode() {
        return phonecode;
    }

    public void setPhonecode(String phonecode) {
        this.phonecode = phonecode;
    }

    public String getMobilecode() {
        return mobilecode;
    }

    public void setMobilecode(String mobilecode) {
        this.mobilecode = mobilecode;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getGeneral_no() {
        return general_no;
    }

    public void setGeneral_no(String general_no) {
        this.general_no = general_no;
    }

    public String getCompany_address() {
        return company_address;
    }

    public void setCompany_address(String company_address) {
        this.company_address = company_address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry_id() {
        return country_id;
    }

    public void setCountry_id(String country_id) {
        this.country_id = country_id;
    }

    public String getState_id() {
        return state_id;
    }

    public void setState_id(String state_id) {
        this.state_id = state_id;
    }

    public String getNotes_CRM() {
        return notes_CRM;
    }

    public void setNotes_CRM(String notes_CRM) {
        this.notes_CRM = notes_CRM;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFirst_login_status() {
        return first_login_status;
    }

    public void setFirst_login_status(String first_login_status) {
        this.first_login_status = first_login_status;
    }

    public String getChange_pass() {
        return change_pass;
    }

    public void setChange_pass(String change_pass) {
        this.change_pass = change_pass;
    }

    public String getLock() {
        return lock;
    }

    public void setLock(String lock) {
        this.lock = lock;
    }

    public String getUser_type() {
        return user_type;
    }

    public void setUser_type(String user_type) {
        this.user_type = user_type;
    }

    public String getPackage_id() {
        return package_id;
    }

    public void setPackage_id(String package_id) {
        this.package_id = package_id;
    }

    public String getMessaging_country() {
        return messaging_country;
    }

    public void setMessaging_country(String messaging_country) {
        this.messaging_country = messaging_country;
    }

    public String getApplication_due_date() {
        return application_due_date;
    }

    public void setApplication_due_date(String application_due_date) {
        this.application_due_date = application_due_date;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getMessage_limit() {
        return message_limit;
    }

    public void setMessage_limit(String message_limit) {
        this.message_limit = message_limit;
    }

    public String getSend_message() {
        return send_message;
    }

    public void setSend_message(String send_message) {
        this.send_message = send_message;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getModified_date() {
        return modified_date;
    }

    public void setModified_date(String modified_date) {
        this.modified_date = modified_date;
    }

    public String getAdded_by() {
        return added_by;
    }

    public void setAdded_by(String added_by) {
        this.added_by = added_by;
    }

    public String getModified_by() {
        return modified_by;
    }

    public void setModified_by(String modified_by) {
        this.modified_by = modified_by;
    }

    public String getPackage_start() {
        return package_start;
    }

    public void setPackage_start(String package_start) {
        this.package_start = package_start;
    }

    public String getPackage_end() {
        return package_end;
    }

    public void setPackage_end(String package_end) {
        this.package_end = package_end;
    }

    @Override
    public String toString() {
        return "AllCompanysVo{" +
                "id='" + id + '\'' +
                ", company_id='" + company_id + '\'' +
                ", discipline_id='" + discipline_id + '\'' +
                ", sub_discipline_id='" + sub_discipline_id + '\'' +
                ", type='" + type + '\'' +
                ", company_code='" + company_code + '\'' +
                ", parent_id='" + parent_id + '\'' +
                ", parent_type='" + parent_type + '\'' +
                ", bussiness_no='" + bussiness_no + '\'' +
                ", company_name='" + company_name + '\'' +
                ", company_intro='" + company_intro + '\'' +
                ", company_type='" + company_type + '\'' +
                ", website='" + website + '\'' +
                ", logo='" + logo + '\'' +
                ", rating='" + rating + '\'' +
                ", awards='" + awards + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", pass='" + pass + '\'' +
                ", description='" + description + '\'' +
                ", contact_name='" + contact_name + '\'' +
                ", contact_email='" + contact_email + '\'' +
                ", phone='" + phone + '\'' +
                ", phonecode='" + phonecode + '\'' +
                ", mobilecode='" + mobilecode + '\'' +
                ", mobile='" + mobile + '\'' +
                ", general_no='" + general_no + '\'' +
                ", company_address='" + company_address + '\'' +
                ", city='" + city + '\'' +
                ", country_id='" + country_id + '\'' +
                ", state_id='" + state_id + '\'' +
                ", notes_CRM='" + notes_CRM + '\'' +
                ", status='" + status + '\'' +
                ", first_login_status='" + first_login_status + '\'' +
                ", change_pass='" + change_pass + '\'' +
                ", lock='" + lock + '\'' +
                ", user_type='" + user_type + '\'' +
                ", package_id='" + package_id + '\'' +
                ", messaging_country='" + messaging_country + '\'' +
                ", application_due_date='" + application_due_date + '\'' +
                ", key='" + key + '\'' +
                ", message_limit='" + message_limit + '\'' +
                ", send_message='" + send_message + '\'' +
                ", date='" + date + '\'' +
                ", modified_date='" + modified_date + '\'' +
                ", added_by='" + added_by + '\'' +
                ", modified_by='" + modified_by + '\'' +
                ", package_start='" + package_start + '\'' +
                ", package_end='" + package_end + '\'' +
                '}';
    }
}
