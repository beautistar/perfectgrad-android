package com.perfectgraduate.vo;

import java.util.Comparator;

/**
 * Created by user on 13-Dec-16.
 */

public class StateVo {

    private String id;
    private String country_id;
    private String name;
    private String abr;
    private String map_reference;
    private String latitude;
    private String longitude;
    private String status;
    private String date;
    private String discipline_id;
    private boolean checked;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCountry_id() {
        return country_id;
    }

    public void setCountry_id(String country_id) {
        this.country_id = country_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAbr() {
        return abr;
    }

    public void setAbr(String abr) {
        this.abr = abr;
    }

    public String getMap_reference() {
        return map_reference;
    }

    public void setMap_reference(String map_reference) {
        this.map_reference = map_reference;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDiscipline_id() {
        return discipline_id;
    }

    public void setDiscipline_id(String discipline_id) {
        this.discipline_id = discipline_id;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    @Override
    public String toString() {
        return "StateVo{" +
                "id='" + id + '\'' +
                ", country_id='" + country_id + '\'' +
                ", name='" + name + '\'' +
                ", abr='" + abr + '\'' +
                ", map_reference='" + map_reference + '\'' +
                ", latitude='" + latitude + '\'' +
                ", longitude='" + longitude + '\'' +
                ", status='" + status + '\'' +
                ", date='" + date + '\'' +
                ", discipline_id='" + discipline_id + '\'' +
                ", checked=" + checked +
                '}';
    }

    public static final Comparator<StateVo> short_name = new Comparator<StateVo>(){

        @Override
        public int compare(StateVo o1, StateVo o2) {
            return o1.getName().compareTo(o2.getName());
        }

    };
}
