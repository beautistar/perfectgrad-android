package com.perfectgraduate.vo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 17-Jan-17.
 */

public class ApplicationVo {

    private String program;
    private List<ApplicationDueDateAllVo> dateAllVoList = new ArrayList<>();

    public String getProgram() {
        return program;
    }

    public void setProgram(String program) {
        this.program = program;
    }

    public List<ApplicationDueDateAllVo> getDateAllVoList() {
        return dateAllVoList;
    }

    public void setDateAllVoList(List<ApplicationDueDateAllVo> dateAllVoList) {
        this.dateAllVoList = dateAllVoList;
    }

    @Override
    public String toString() {
        return "ApplicationVo{" +
                "program='" + program + '\'' +
                ", dateAllVoList=" + dateAllVoList +
                '}';
    }
}
