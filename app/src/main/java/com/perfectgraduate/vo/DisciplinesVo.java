package com.perfectgraduate.vo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 19-Dec-16.
 */

public class DisciplinesVo {

    private String company_id;
    private String discipline_id;
    private String name;
    private List<Sub_disciplineVo> sub_disciplineVoList = new ArrayList<>();

    public String getCompany_id() {
        return company_id;
    }

    public void setCompany_id(String company_id) {
        this.company_id = company_id;
    }

    public String getDiscipline_id() {
        return discipline_id;
    }

    public void setDiscipline_id(String discipline_id) {
        this.discipline_id = discipline_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Sub_disciplineVo> getSub_disciplineVoList() {
        return sub_disciplineVoList;
    }

    public void setSub_disciplineVoList(List<Sub_disciplineVo> sub_disciplineVoList) {
        this.sub_disciplineVoList = sub_disciplineVoList;
    }

    @Override
    public String toString() {
        return "DisciplinesVo{" +
                "company_id='" + company_id + '\'' +
                ", discipline_id='" + discipline_id + '\'' +
                ", name='" + name + '\'' +
                ", sub_disciplineVoList=" + sub_disciplineVoList +
                '}';
    }
}
