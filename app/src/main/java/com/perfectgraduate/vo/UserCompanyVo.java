package com.perfectgraduate.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Created by user on 15-Dec-16.
 */

public class UserCompanyVo implements Serializable {

    private String id;
    private String company_code;
    private String parent_id;
    private String parent_type;
    private String bussiness_no;
    private String company_name;
    private String company_intro;
    private String company_type;
    private String website;
    private String logo;
    private String rating;
    private String awards;
    private String email;
    private String password;
    private String pass;
    private String description;
    private String contact_name;
    private String contact_email;
    private String phone;
    private String phonecode;
    private String mobilecode;
    private String mobile;
    private String general_no;
    private String company_address;
    private String city;
    private String country_id;
    private String state_id;
    private String notes_CRM;
    private String status;
    private String first_login_status;
    private String change_pass;
    private String lock;
    private String user_type;
    private String package_id;
    private String messaging_country;
    private String application_due_date;
    private String key;
    private String message_limit;
    private String send_message;
    private String date;
    private String modified_date;
    private String added_by;
    private String modified_by;
    private String package_start;
    private String package_end;
    private String country;
    private String state;
    private String abr;
    private String is_deleted;
    private String international;
    private String message_events;
    private String regstates;
    private String message_state;
    private String message_status;
    private String unread_message;
    private String portfolio_status;
    private String perfect_logo;
    private String isRemoved;
    private List<LinksVo> linksVoList = new ArrayList<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCompany_code() {
        return company_code;
    }

    public void setCompany_code(String company_code) {
        this.company_code = company_code;
    }

    public String getParent_id() {
        return parent_id;
    }

    public void setParent_id(String parent_id) {
        this.parent_id = parent_id;
    }

    public String getParent_type() {
        return parent_type;
    }

    public void setParent_type(String parent_type) {
        this.parent_type = parent_type;
    }

    public String getBussiness_no() {
        return bussiness_no;
    }

    public void setBussiness_no(String bussiness_no) {
        this.bussiness_no = bussiness_no;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getCompany_intro() {
        return company_intro;
    }

    public void setCompany_intro(String company_intro) {
        this.company_intro = company_intro;
    }

    public String getCompany_type() {
        return company_type;
    }

    public void setCompany_type(String company_type) {
        this.company_type = company_type;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getAwards() {
        return awards;
    }

    public void setAwards(String awards) {
        this.awards = awards;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getContact_name() {
        return contact_name;
    }

    public void setContact_name(String contact_name) {
        this.contact_name = contact_name;
    }

    public String getContact_email() {
        return contact_email;
    }

    public void setContact_email(String contact_email) {
        this.contact_email = contact_email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhonecode() {
        return phonecode;
    }

    public void setPhonecode(String phonecode) {
        this.phonecode = phonecode;
    }

    public String getMobilecode() {
        return mobilecode;
    }

    public void setMobilecode(String mobilecode) {
        this.mobilecode = mobilecode;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getGeneral_no() {
        return general_no;
    }

    public void setGeneral_no(String general_no) {
        this.general_no = general_no;
    }

    public String getCompany_address() {
        return company_address;
    }

    public void setCompany_address(String company_address) {
        this.company_address = company_address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry_id() {
        return country_id;
    }

    public void setCountry_id(String country_id) {
        this.country_id = country_id;
    }

    public String getState_id() {
        return state_id;
    }

    public void setState_id(String state_id) {
        this.state_id = state_id;
    }

    public String getNotes_CRM() {
        return notes_CRM;
    }

    public void setNotes_CRM(String notes_CRM) {
        this.notes_CRM = notes_CRM;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFirst_login_status() {
        return first_login_status;
    }

    public void setFirst_login_status(String first_login_status) {
        this.first_login_status = first_login_status;
    }

    public String getChange_pass() {
        return change_pass;
    }

    public void setChange_pass(String change_pass) {
        this.change_pass = change_pass;
    }

    public String getLock() {
        return lock;
    }

    public void setLock(String lock) {
        this.lock = lock;
    }

    public String getUser_type() {
        return user_type;
    }

    public void setUser_type(String user_type) {
        this.user_type = user_type;
    }

    public String getPackage_id() {
        return package_id;
    }

    public void setPackage_id(String package_id) {
        this.package_id = package_id;
    }

    public String getMessaging_country() {
        return messaging_country;
    }

    public void setMessaging_country(String messaging_country) {
        this.messaging_country = messaging_country;
    }

    public String getApplication_due_date() {
        return application_due_date;
    }

    public void setApplication_due_date(String application_due_date) {
        this.application_due_date = application_due_date;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getMessage_limit() {
        return message_limit;
    }

    public void setMessage_limit(String message_limit) {
        this.message_limit = message_limit;
    }

    public String getSend_message() {
        return send_message;
    }

    public void setSend_message(String send_message) {
        this.send_message = send_message;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getModified_date() {
        return modified_date;
    }

    public void setModified_date(String modified_date) {
        this.modified_date = modified_date;
    }

    public String getAdded_by() {
        return added_by;
    }

    public void setAdded_by(String added_by) {
        this.added_by = added_by;
    }

    public String getModified_by() {
        return modified_by;
    }

    public void setModified_by(String modified_by) {
        this.modified_by = modified_by;
    }

    public String getPackage_start() {
        return package_start;
    }

    public void setPackage_start(String package_start) {
        this.package_start = package_start;
    }

    public String getPackage_end() {
        return package_end;
    }

    public void setPackage_end(String package_end) {
        this.package_end = package_end;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getInternational() {
        return international;
    }

    public void setInternational(String international) {
        this.international = international;
    }

    public String getMessage_events() {
        return message_events;
    }

    public void setMessage_events(String message_events) {
        this.message_events = message_events;
    }

    public String getRegstates() {
        return regstates;
    }

    public void setRegstates(String regstates) {
        this.regstates = regstates;
    }

    public String getMessage_state() {
        return message_state;
    }

    public void setMessage_state(String message_state) {
        this.message_state = message_state;
    }

    public String getMessage_status() {
        return message_status;
    }

    public void setMessage_status(String message_status) {
        this.message_status = message_status;
    }

    public String getUnread_message() {
        return unread_message;
    }

    public void setUnread_message(String unread_message) {
        this.unread_message = unread_message;
    }

    public String getPortfolio_status() {
        return portfolio_status;
    }

    public void setPortfolio_status(String portfolio_status) {
        this.portfolio_status = portfolio_status;
    }

    public List<LinksVo> getLinksVoList() {
        return linksVoList;
    }

    public void setLinksVoList(List<LinksVo> linksVoList) {
        this.linksVoList = linksVoList;
    }

    public String getAbr() {
        return abr;
    }

    public void setAbr(String abr) {
        this.abr = abr;
    }

    public String getIs_deleted() {
        return is_deleted;
    }

    public void setIs_deleted(String is_deleted) {
        this.is_deleted = is_deleted;
    }

    public String getPerfect_logo() {
        return perfect_logo;
    }

    public void setPerfect_logo(String perfect_logo) {
        this.perfect_logo = perfect_logo;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getIsRemoved() {
        return isRemoved;
    }

    public void setIsRemoved(String isRemoved) {
        this.isRemoved = isRemoved;
    }

    @Override
    public String toString() {
        return "UserCompanyVo{" +
                "id='" + id + '\'' +
                ", company_code='" + company_code + '\'' +
                ", parent_id='" + parent_id + '\'' +
                ", parent_type='" + parent_type + '\'' +
                ", bussiness_no='" + bussiness_no + '\'' +
                ", company_name='" + company_name + '\'' +
                ", company_intro='" + company_intro + '\'' +
                ", company_type='" + company_type + '\'' +
                ", website='" + website + '\'' +
                ", logo='" + logo + '\'' +
                ", rating='" + rating + '\'' +
                ", awards='" + awards + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", pass='" + pass + '\'' +
                ", description='" + description + '\'' +
                ", contact_name='" + contact_name + '\'' +
                ", contact_email='" + contact_email + '\'' +
                ", phone='" + phone + '\'' +
                ", phonecode='" + phonecode + '\'' +
                ", mobilecode='" + mobilecode + '\'' +
                ", mobile='" + mobile + '\'' +
                ", general_no='" + general_no + '\'' +
                ", company_address='" + company_address + '\'' +
                ", city='" + city + '\'' +
                ", country_id='" + country_id + '\'' +
                ", state_id='" + state_id + '\'' +
                ", notes_CRM='" + notes_CRM + '\'' +
                ", status='" + status + '\'' +
                ", first_login_status='" + first_login_status + '\'' +
                ", change_pass='" + change_pass + '\'' +
                ", lock='" + lock + '\'' +
                ", user_type='" + user_type + '\'' +
                ", package_id='" + package_id + '\'' +
                ", messaging_country='" + messaging_country + '\'' +
                ", application_due_date='" + application_due_date + '\'' +
                ", key='" + key + '\'' +
                ", message_limit='" + message_limit + '\'' +
                ", send_message='" + send_message + '\'' +
                ", date='" + date + '\'' +
                ", modified_date='" + modified_date + '\'' +
                ", added_by='" + added_by + '\'' +
                ", modified_by='" + modified_by + '\'' +
                ", package_start='" + package_start + '\'' +
                ", package_end='" + package_end + '\'' +
                ", country='" + country + '\'' +
                ", state='" + state + '\'' +
                ", abr='" + abr + '\'' +
                ", is_deleted='" + is_deleted + '\'' +
                ", international='" + international + '\'' +
                ", message_events='" + message_events + '\'' +
                ", regstates='" + regstates + '\'' +
                ", message_state='" + message_state + '\'' +
                ", message_status='" + message_status + '\'' +
                ", unread_message='" + unread_message + '\'' +
                ", portfolio_status='" + portfolio_status + '\'' +
                ", perfect_logo='" + perfect_logo + '\'' +
                ", isRemoved='" + isRemoved + '\'' +
                ", linksVoList=" + linksVoList +
                '}';
    }

    public static final Comparator<UserCompanyVo> short_company_name= new Comparator<UserCompanyVo>(){

        @Override
        public int compare(UserCompanyVo o1, UserCompanyVo o2) {
            return o1.getCompany_name().compareTo(o2.getCompany_name());
        }

    };

    public static final Comparator<UserCompanyVo> short_due_date = new Comparator<UserCompanyVo>(){

        @Override
        public int compare(UserCompanyVo o1, UserCompanyVo o2) {
            return o1.getApplication_due_date().compareTo(o2.getApplication_due_date());
        }

    };
}
