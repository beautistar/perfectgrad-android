package com.perfectgraduate.vo;

import java.util.ArrayList;

/**
 * Created by user on 16-Dec-16.
 */

public class SalaryVo extends ArrayList<SalaryVo> {

    private String id;
    private String type;
    private String year;
    private String country_id;
    private String sector_id;
    private String occupation_id;
    private String title;
    private String salary0to3;
    private String salary3to5;
    private String salary5to7;
    private String smallMedium;
    private String large;
    private String status;
    private String date;
    private String occupation;
    private String sector;
    public ArrayList<SalaryVo> arrTempSalaryVo = new ArrayList<SalaryVo>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getCountry_id() {
        return country_id;
    }

    public void setCountry_id(String country_id) {
        this.country_id = country_id;
    }

    public String getSector_id() {
        return sector_id;
    }

    public void setSector_id(String sector_id) {
        this.sector_id = sector_id;
    }

    public String getOccupation_id() {
        return occupation_id;
    }

    public void setOccupation_id(String occupation_id) {
        this.occupation_id = occupation_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSalary0to3() {
        return salary0to3;
    }

    public void setSalary0to3(String salary0to3) {
        this.salary0to3 = salary0to3;
    }

    public String getSalary3to5() {
        return salary3to5;
    }

    public void setSalary3to5(String salary3to5) {
        this.salary3to5 = salary3to5;
    }

    public String getSalary5to7() {
        return salary5to7;
    }

    public void setSalary5to7(String salary5to7) {
        this.salary5to7 = salary5to7;
    }

    public String getSmallMedium() {
        return smallMedium;
    }

    public void setSmallMedium(String smallMedium) {
        this.smallMedium = smallMedium;
    }

    public String getLarge() {
        return large;
    }

    public void setLarge(String large) {
        this.large = large;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    @Override
    public String toString() {
        return "SalaryVo{" +
                "id='" + id + '\'' +
                ", type='" + type + '\'' +
                ", year='" + year + '\'' +
                ", country_id='" + country_id + '\'' +
                ", sector_id='" + sector_id + '\'' +
                ", occupation_id='" + occupation_id + '\'' +
                ", title='" + title + '\'' +
                ", salary0to3='" + salary0to3 + '\'' +
                ", salary3to5='" + salary3to5 + '\'' +
                ", salary5to7='" + salary5to7 + '\'' +
                ", smallMedium='" + smallMedium + '\'' +
                ", large='" + large + '\'' +
                ", status='" + status + '\'' +
                ", date='" + date + '\'' +
                ", occupation='" + occupation + '\'' +
                ", sector='" + sector + '\'' +
                '}';
    }
}
