package com.perfectgraduate.vo;

/**
 * Created by user on 19-Dec-16.
 */

public class UserDisciplineVo {

    private String id;
    private String student_id;
    private String discipline_id;
    private String sub_discipline_id;
    private String order;
    private String discipline;
    private String sub_discipline;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStudent_id() {
        return student_id;
    }

    public void setStudent_id(String student_id) {
        this.student_id = student_id;
    }

    public String getDiscipline_id() {
        return discipline_id;
    }

    public void setDiscipline_id(String discipline_id) {
        this.discipline_id = discipline_id;
    }

    public String getSub_discipline_id() {
        return sub_discipline_id;
    }

    public void setSub_discipline_id(String sub_discipline_id) {
        this.sub_discipline_id = sub_discipline_id;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getDiscipline() {
        return discipline;
    }

    public void setDiscipline(String discipline) {
        this.discipline = discipline;
    }

    public String getSub_discipline() {
        return sub_discipline;
    }

    public void setSub_discipline(String sub_discipline) {
        this.sub_discipline = sub_discipline;
    }

    @Override
    public String toString() {
        return "UserDisciplineVo{" +
                "id='" + id + '\'' +
                ", student_id='" + student_id + '\'' +
                ", discipline_id='" + discipline_id + '\'' +
                ", sub_discipline_id='" + sub_discipline_id + '\'' +
                ", order='" + order + '\'' +
                ", discipline='" + discipline + '\'' +
                ", sub_discipline='" + sub_discipline + '\'' +
                '}';
    }
}
