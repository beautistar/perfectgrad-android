package com.perfectgraduate.vo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 17-Jan-17.
 */

public class FAQVo {

    private String question;
    private List<QuesAnsVo> quesAnsVoList = new ArrayList<>();

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public List<QuesAnsVo> getQuesAnsVoList() {
        return quesAnsVoList;
    }

    public void setQuesAnsVoList(List<QuesAnsVo> quesAnsVoList) {
        this.quesAnsVoList = quesAnsVoList;
    }

    @Override
    public String toString() {
        return "FAQVo{" +
                "question='" + question + '\'' +
                ", quesAnsVoList=" + quesAnsVoList +
                '}';
    }
}
