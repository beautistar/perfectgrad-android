package com.perfectgraduate.vo;

/**
 * Created by user on 19-Dec-16.
 */

public class Sub_disciplineVo {

    private String name;
    private String discipline_id;
    private String sub_discipline_id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDiscipline_id() {
        return discipline_id;
    }

    public void setDiscipline_id(String discipline_id) {
        this.discipline_id = discipline_id;
    }

    public String getSub_discipline_id() {
        return sub_discipline_id;
    }

    public void setSub_discipline_id(String sub_discipline_id) {
        this.sub_discipline_id = sub_discipline_id;
    }

    @Override
    public String toString() {
        return "Sub_disciplineVo{" +
                "name='" + name + '\'' +
                ", discipline_id='" + discipline_id + '\'' +
                ", sub_discipline_id='" + sub_discipline_id + '\'' +
                '}';
    }
}
