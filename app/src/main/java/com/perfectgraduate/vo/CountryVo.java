package com.perfectgraduate.vo;

import com.perfectgraduate.utility.Constants;

import java.util.Comparator;

/**
 * Created by user on 12-Dec-16.
 */

public class CountryVo {

    private String id = "";
    private String name = "";
    private String abr = "";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAbr() {
        return abr;
    }

    public void setAbr(String abr) {
        this.abr = abr;
    }

    @Override
    public String toString() {
        return "CountryVo{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", abr='" + abr + '\'' +
                '}';
    }


    public static final Comparator<CountryVo> short_name = new Comparator<CountryVo>(){

        @Override
        public int compare(CountryVo o1, CountryVo o2) {
            return o1.getName().compareTo(o2.getName());
        }

    };

}
