package com.perfectgraduate.vo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by user on 08-Dec-16.
 */

public class ExpandableListDataPump {

    public static HashMap<String, List<String>> getData() {
        HashMap<String, List<String>> expandableListDetail = new HashMap<String, List<String>>();

        List<String> cricket = new ArrayList<String>();

        List<String> football = new ArrayList<String>();
        football.add("Helping students learn laws & rules-1(1)");
        football.add("Helping students learn laws & rules-2(1)");

        List<String> basketball = new ArrayList<String>();
        basketball.add("Better address mental health needs-1(8)");
        basketball.add("Better address mental health needs-2(8)");
        basketball.add("Finance(8)");

        expandableListDetail.put("specific", cricket);
        expandableListDetail.put("Helping students learn laws & rulz", football);
        expandableListDetail.put("Better address mental health needs", basketball);
        return expandableListDetail;
    }

}
