package com.perfectgraduate.vo;

/**
 * Created by user on 15-Dec-16.
 */

public class AllAwardsVo {

    private String id;
    private String company_id;
    private String description;
    private String awards;
    private String year;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCompany_id() {
        return company_id;
    }

    public void setCompany_id(String company_id) {
        this.company_id = company_id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAwards() {
        return awards;
    }

    public void setAwards(String awards) {
        this.awards = awards;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    @Override
    public String toString() {
        return "AllAwardsVo{" +
                "id='" + id + '\'' +
                ", company_id='" + company_id + '\'' +
                ", description='" + description + '\'' +
                ", awards='" + awards + '\'' +
                ", year='" + year + '\'' +
                '}';
    }
}
