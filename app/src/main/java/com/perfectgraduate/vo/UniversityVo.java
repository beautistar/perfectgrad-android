package com.perfectgraduate.vo;

/**
 * Created by user on 13-Dec-16.
 */

public class UniversityVo {

    private String id;
    private String country_id;
    private String name;
    private String status;
    private String date;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCountry_id() {
        return country_id;
    }

    public void setCountry_id(String country_id) {
        this.country_id = country_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "UniversityVo{" +
                "id='" + id + '\'' +
                ", country_id='" + country_id + '\'' +
                ", name='" + name + '\'' +
                ", status='" + status + '\'' +
                ", date='" + date + '\'' +
                '}';
    }
}
