package com.perfectgraduate.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.perfectgraduate.R;
import com.perfectgraduate.activity.HomeActivity;
import com.perfectgraduate.adapter.SocietySwipeViewAdapter;
import com.perfectgraduate.api.PerfectGraduateRequest;
import com.perfectgraduate.api.WebServiceAPI;
import com.perfectgraduate.app.PerfectGraduateApplication;
import com.perfectgraduate.database.DatabaseHelper;
import com.perfectgraduate.listener.ICallBack;
import com.perfectgraduate.utility.Constants;
import com.perfectgraduate.utility.SharedPrefrenceUtil;
import com.perfectgraduate.vo.SocietyVo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by user on 02-Feb-17.
 */

public class SocietyFragment extends Fragment{

    private DatabaseHelper databaseHelper;
    private RecyclerView recyclerView_Society;
    private PerfectGraduateApplication application;
    private PerfectGraduateRequest request;
    private List<SocietyVo> societyVoList;
    public static SocietySwipeViewAdapter adapter;
    private HomeActivity activity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_society, container, false);

        init(rootView);

        return rootView;
    }

    private void init(View rootView) {

        activity = (HomeActivity) getActivity();
        databaseHelper = new DatabaseHelper(getActivity());
        application = (PerfectGraduateApplication) getActivity().getApplicationContext();
        request = new PerfectGraduateRequest(getActivity());

        societyVoList = new ArrayList<>();

        recyclerView_Society = (RecyclerView) rootView.findViewById(R.id.recyclerView_Society);
        recyclerView_Society.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    @Override
    public void onResume() {
        super.onResume();
        getStudentSociety();
    }

    private void getStudentSociety() {

        if (SharedPrefrenceUtil.getPrefrence(getActivity(), Constants.isSocietyDetail, "").equalsIgnoreCase("Database_societyDetail")) {
            societyVoList = databaseHelper.getSocietyDetail();
            int society_count = 0;
            Constants.society_count = 0;
            for (int i = 0; i < societyVoList.size(); i++){

                if(society_count > 0) {
                    society_count = Integer.parseInt(societyVoList.get(i).getUnread_message()) + society_count;
                }else{
                    society_count = Integer.parseInt(societyVoList.get(i).getUnread_message());
                }
            }
            Constants.society_count = society_count;
            activity.changeSocietyImage(false);

            adapter = new SocietySwipeViewAdapter(activity,societyVoList);
            recyclerView_Society.setAdapter(adapter);
        }else{
            if(!application.isConnectInternet()){
                application.setShowDialog(getActivity(),"",getString(R.string.no_internet_connection));
            }else {
                getUserSociety();
            }
        }

    }

    private void getUserSociety() {

        Map<String, Object> map = new HashMap<String, Object>();

        map.put("uid", SharedPrefrenceUtil.getPrefrence(getActivity(), Constants.USER_ID, ""));

        request.getResponse(getActivity(), map, WebServiceAPI.API_user_society, true, new ICallBack() {
            @Override
            public void onCallBackResult(String response) {

                try {

                    JSONObject jsonObject = new JSONObject(response);

                    String status = jsonObject.getString(WebServiceAPI.status);
                    String admin = jsonObject.getString("admin");
                    String company = jsonObject.getString("company");
                    String badge_count = jsonObject.getString("badge_count");


                    if(Boolean.parseBoolean(status)) {

                        JSONArray jsonArray = jsonObject.getJSONArray(WebServiceAPI.data);

                        if (jsonArray != null && jsonArray.length() > 0) {

                            databaseHelper.insertSocietyDetail(getActivity(),jsonArray);

                        }

                        societyVoList = databaseHelper.getSocietyDetail();

                        Log.e("societyVoList", " --- "+societyVoList);

                        if(societyVoList != null && societyVoList.size() > 0){

                            adapter = new SocietySwipeViewAdapter(activity, societyVoList);
                            recyclerView_Society.setAdapter(adapter);

                            int society_count = 0;
                            Constants.society_count = 0;
                            for (int i = 0; i < societyVoList.size(); i++){

                                if(society_count > 0) {
                                    society_count = Integer.parseInt(societyVoList.get(i).getUnread_message()) + society_count;
                                }else{
                                    society_count = Integer.parseInt(societyVoList.get(i).getUnread_message());
                                }
                            }
                            Constants.society_count = society_count;
                            activity.changeSocietyImage(false);

                            SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
                            SharedPreferences.Editor editor = sharedPrefs.edit();
                            Gson gson = new Gson();
                            String json = gson.toJson(societyVoList);
                            editor.putString("societyDetail", json);
                            editor.apply();

                        }


                    }

                } catch (JSONException k) {
                    k.printStackTrace();
                }

            }
        });

    }

    public void displayRemove() {
        if(Constants.isVisibleSocietyDelete){
            Constants.isVisibleSocietyDelete = false;
            adapter.notifyDataSetChanged();
        }else {
            Constants.isVisibleSocietyDelete = true;
            adapter.notifyDataSetChanged();
        }
    }

}
