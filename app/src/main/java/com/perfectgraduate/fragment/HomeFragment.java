package com.perfectgraduate.fragment;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.perfectgraduate.R;
import com.perfectgraduate.activity.HomeActivity;
import com.perfectgraduate.utility.Constants;

import java.util.Collections;

/**
 * Created by user on 02-Feb-17.
 */

public class HomeFragment extends Fragment {

    private LinearLayout linearLayout_due_date;
    public static TextView textView_company, textView_abr_prg;
    public static PortfolioFragment portfolioFragment;
    HomeActivity activity;
    public static ViewPager viewPager;
    public static MyPagerAdapter myPagerAdapter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);

        init(rootView);

        return rootView;
    }

    private void init(View rootView) {

        activity = (HomeActivity) getActivity();

        portfolioFragment = new PortfolioFragment();

        textView_company = (TextView) rootView.findViewById(R.id.textView_company);
        textView_abr_prg = (TextView) rootView.findViewById(R.id.textView_abr_prg);
        linearLayout_due_date = (LinearLayout) rootView.findViewById(R.id.linearLayout_due_date);

        myPagerAdapter = new MyPagerAdapter(getActivity().getSupportFragmentManager());

        viewPager = (ViewPager) rootView.findViewById(R.id.viewPager_home);
        viewPager.setOffscreenPageLimit(2);


        linearLayout_due_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                portfolioFragment.shortByDueDates();
            }
        });

        textView_company.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                portfolioFragment.shortByCompany();
            }
        });

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                if(position == 0){
                    textView_company.setText(getString(R.string.portfolio_company));
                    linearLayout_due_date.setFocusableInTouchMode(true);
                    textView_company.setEnabled(true);
                    activity.changeSocietyImage(true);
                    Constants.isVisibleSociety = true;
                }else{
                    textView_company.setText(getString(R.string.student_society));
                    linearLayout_due_date.setFocusableInTouchMode(false);
                    textView_company.setEnabled(false);
                    activity.changeSocietyImage(false);
                    Constants.isVisibleSociety = false;
                }

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        if(Constants.message.equals("society group")) {
            Constants.message = "";
            viewPager.setAdapter(myPagerAdapter);
            viewPager.setCurrentItem(1);
            myPagerAdapter.notifyDataSetChanged();
        }else {
            viewPager.setAdapter(myPagerAdapter);
        }

    }

    private class MyPagerAdapter extends FragmentStatePagerAdapter
    {
        public MyPagerAdapter(FragmentManager fm)
        {
            super(fm);
        }
        @Override
        public int getCount()
        {
            return 2;
        }

        @Override
        public int getItemPosition(Object object) {
            // POSITION_NONE makes it possible to reload the PagerAdapter
            return POSITION_NONE;
        }

        @Override
        public Fragment getItem(int position)
        {

            switch(position)
            {
                case 0:
                    return portfolioFragment;
                case 1:
                    return new SocietyFragment();
                default :
                    return portfolioFragment;
            }
        }
    }

    public void changeItem(boolean society){
        if(society) {
            viewPager.setCurrentItem(0);
        }else{
            viewPager.setCurrentItem(1);
        }
        myPagerAdapter.notifyDataSetChanged();
    }



}
