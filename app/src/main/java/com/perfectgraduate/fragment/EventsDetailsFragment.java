package com.perfectgraduate.fragment;

import android.support.v4.app.Fragment;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.callback.BitmapAjaxCallback;
import com.perfectgraduate.R;
import com.perfectgraduate.activity.EventLocationActivity;
import com.perfectgraduate.database.DatabaseHelper;
import com.perfectgraduate.vo.MessagesVo;

/**
 * Created by user on 09-Jan-17.
 */

public class EventsDetailsFragment extends Fragment{

    public static final String ARG_OBJECT = "object";
    private TextView textView_eventTitle,textView_eventName,textView_eventDate,textView_eventTime,textView_eventLocation,textView_eventMessage;
    private ImageView imageView_eventLogo,imageView_like;
    private MessagesVo messagesVo;
    private DatabaseHelper databaseHelper;
    private LinearLayout linearLayout_event;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_eventdetails, container, false);

        init(rootView);

        return rootView;
    }

    private void init(View rootView) {

        databaseHelper = new DatabaseHelper(getActivity());

        textView_eventTitle = (TextView) rootView.findViewById(R.id.textView_eventTitle);
        textView_eventName = (TextView) rootView.findViewById(R.id.textView_eventName);
        textView_eventDate = (TextView) rootView.findViewById(R.id.textView_eventDate);
        textView_eventTime = (TextView) rootView.findViewById(R.id.textView_eventTime);
        textView_eventLocation = (TextView) rootView.findViewById(R.id.textView_eventLocation);
        textView_eventMessage = (TextView) rootView.findViewById(R.id.textView_eventMessage);

        imageView_eventLogo = (ImageView) rootView.findViewById(R.id.imageView_eventLogo);
        imageView_like = (ImageView) rootView.findViewById(R.id.imageView_like);

        linearLayout_event = (LinearLayout) rootView.findViewById(R.id.linearLayout_event);

        Bundle bundle = getArguments();
        messagesVo = (MessagesVo) bundle.getSerializable(ARG_OBJECT);



        for (int i = 0; i < messagesVo.getAdderDetailVoList().size(); i++) {
            textView_eventTitle.setText(messagesVo.getAdderDetailVoList().get(i).getName());
        }

        if(messagesVo.getStatus().equals("unread")){
            databaseHelper.updateMessageStatus(messagesVo.getId(),messagesVo.getStatus());
        }

        if(messagesVo.getMessage_type().equals("event")){
            linearLayout_event.setVisibility(View.VISIBLE);
        }else {
            linearLayout_event.setVisibility(View.GONE);
        }

        textView_eventName.setText(messagesVo.getEvent_title());
        textView_eventDate.setText(messagesVo.getEvent_date());
        textView_eventTime.setText(messagesVo.getEventtimefrom() +" to "+messagesVo.getEventtimeto());
        textView_eventLocation.setText(messagesVo.getEvent_venue());
        textView_eventMessage.setText(messagesVo.getMessage());

        textView_eventLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getActivity(), EventLocationActivity.class);
                intent.putExtra("location",messagesVo.getEvent_venue());
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_left);

            }
        });

        AQuery aQuery = new AQuery(getActivity());

        aQuery.id(imageView_eventLogo).image("http://www.perfectgraduate.com/perfect/assets/uploads/" + messagesVo.getPerfect_logo(),
                true, true, 0, 0, new BitmapAjaxCallback() {
                    @Override
                    protected void callback(String url, ImageView iv, Bitmap bm, AjaxStatus status) {

                        if (bm != null) {
                            iv.setImageBitmap(bm);
                        } else {
                            iv.setImageResource(R.drawable.defaultbuilding);
                        }
                    }
                });

        if(messagesVo.getLike_status().equals("0")){
            imageView_like.setImageResource(R.drawable.unliked);
        }else {
            imageView_like.setImageResource(R.drawable.liked);
        }

        imageView_like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(messagesVo.getLike_status().equals("0")){
                    imageView_like.setImageResource(R.drawable.liked);
                    databaseHelper.likeUnlike(messagesVo.getId(),messagesVo.getStatus());
                }else {
                    imageView_like.setImageResource(R.drawable.unliked);
                    databaseHelper.likeUnlike(messagesVo.getId(),messagesVo.getStatus());
                }
            }
        });

    }
}
