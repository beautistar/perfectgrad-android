package com.perfectgraduate.fragment;


import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.perfectgraduate.R;
import com.perfectgraduate.activity.HomeActivity;
import com.perfectgraduate.adapter.MessageAdapter;
import com.perfectgraduate.api.PerfectGraduateRequest;
import com.perfectgraduate.api.WebServiceAPI;
import com.perfectgraduate.app.PerfectGraduateApplication;
import com.perfectgraduate.database.DatabaseHelper;
import com.perfectgraduate.listener.ICallBack;
import com.perfectgraduate.utility.Constants;
import com.perfectgraduate.utility.SharedPrefrenceUtil;
import com.perfectgraduate.vo.AdderDetailVo;
import com.perfectgraduate.vo.MessagesVo;
import com.perfectgraduate.vo.SocietyVo;
import com.perfectgraduate.vo.UserCompanyVo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by user on 09-Dec-16.
 */

public class MessagesFragment extends Fragment {

    public static final String ARG_OBJECT = "object";
    private UserCompanyVo userCompanyVo;
    private ListView listView_messages;
    private PerfectGraduateApplication application;
    private PerfectGraduateRequest request;
    private DatabaseHelper databaseHelper;

    private List<MessagesVo> messagesVoList;
    private List<AdderDetailVo> adderDetailVoList;
    private List<MessagesVo> arrayCompanyInvites;
    private List<MessagesVo> arrayCompMsgsAndEvents;
    private List<MessagesVo> arrayCompanyEvents;
    private List<MessagesVo> arraySocietyInvite;
    private List<MessagesVo> arraySocietyEvent;
    private List<MessagesVo> arraySocietyMsgsAndEvents;

    private LinearLayout linearLayout_message_events,linearLayout_societies_companies;
    private TextView textView_societies,textView_message_events;
    private ImageView imageView_threestar;
    private HomeActivity activity;
    private boolean companySociety = false;
    private ProgressDialog progressDialog;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_messages, container, false);

        init(rootView);

        return rootView;
    }

    private void init(View rootView) {


        request = new PerfectGraduateRequest(getActivity());
        application = (PerfectGraduateApplication) getActivity().getApplicationContext();
        databaseHelper = new DatabaseHelper(getActivity());
        activity = (HomeActivity) getActivity();
        progressDialog = new ProgressDialog(activity);

        linearLayout_message_events = (LinearLayout) rootView.findViewById(R.id.linearLayout_message_events);
        linearLayout_societies_companies = (LinearLayout) rootView.findViewById(R.id.linearLayout_societies_companies);
        textView_societies = (TextView) rootView.findViewById(R.id.textView_societies);
        textView_message_events = (TextView) rootView.findViewById(R.id.textView_message_events);
        imageView_threestar = (ImageView) rootView.findViewById(R.id.imageView_threestar);

        listView_messages = (ListView) rootView.findViewById(R.id.listView_messages);

        if(Constants.message.equals("particular message")) {
            Bundle bundle = getArguments();
            userCompanyVo = (UserCompanyVo) bundle.getSerializable(ARG_OBJECT);
            textView_societies.setText(getString(R.string.student_societies));
            linearLayout_societies_companies.setEnabled(false);
            imageView_threestar.setVisibility(View.GONE);
        }else if(Constants.message.equals("society group")) {
            textView_societies.setText(getString(R.string.companies));
            textView_societies.setTextColor(getResources().getColor(R.color.default_btn));
            linearLayout_societies_companies.setEnabled(false);
            imageView_threestar.setVisibility(View.GONE);
        }else if(Constants.message.equals("admin messages")){
            textView_societies.setText(getString(R.string.companies));
            textView_societies.setTextColor(getResources().getColor(R.color.default_btn));
            linearLayout_societies_companies.setEnabled(false);
            imageView_threestar.setVisibility(View.GONE);
        }else if(Constants.message.equals("my events")){
            textView_societies.setText(getString(R.string.student_societies));
            textView_societies.setTextColor(getResources().getColor(R.color.default_btn));
            textView_message_events.setTextColor(getResources().getColor(R.color.default_btn));
            linearLayout_societies_companies.setEnabled(false);
            linearLayout_message_events.setEnabled(false);
            imageView_threestar.setVisibility(View.VISIBLE);
        }else if(Constants.message.equals("messages tab")){
            textView_societies.setText(getString(R.string.student_societies));
            textView_societies.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
            linearLayout_societies_companies.setEnabled(true);
            imageView_threestar.setVisibility(View.VISIBLE);
        }

        adderDetailVoList = new ArrayList<>();
        messagesVoList = new ArrayList<>();

        arrayCompanyInvites = new ArrayList<>();
        arrayCompMsgsAndEvents = new ArrayList<>();
        arrayCompanyEvents = new ArrayList<>();
        arraySocietyInvite = new ArrayList<>();
        arraySocietyEvent = new ArrayList<>();
        arraySocietyMsgsAndEvents = new ArrayList<>();

        // Get stored data from societyDetail

        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
        Gson gson = new Gson();
        String jsonData = sharedPrefs.getString("societyDetail", "");
        Type type = new TypeToken<ArrayList<SocietyVo>>() {
        }.getType();

        if(Constants.societyVoList != null){
            Constants.societyVoList.clear();
        }

        Constants.societyVoList = gson.fromJson(jsonData, type);

        linearLayout_message_events.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(companySociety) {
                    if(textView_message_events.getText().toString().equals(getString(R.string.my_events_message_events_only))){
                        textView_message_events.setText(getString(R.string.my_events_events_only));
                        displayList(arraySocietyEvent);
                    }else if (textView_message_events.getText().toString().equals(getString(R.string.my_events_events_only))) {
                        textView_message_events.setText(getString(R.string.my_events_message_events_only));
                        displayList(arraySocietyMsgsAndEvents);
                    }

                }else{
                    if (textView_message_events.getText().toString().equals(getString(R.string.my_events_message_events_only))) {
                        textView_message_events.setText(getString(R.string.my_events_events_only));
                        displayList(arrayCompMsgsAndEvents);
                    } else if (textView_message_events.getText().toString().equals(getString(R.string.my_events_events_only))) {
                        textView_message_events.setText(getString(R.string.my_events_message_events_only));
                        displayList(arrayCompanyEvents);
                    }
                }
            }
        });

        linearLayout_societies_companies.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Constants.message.equals("messages tab")){
                    if(textView_societies.getText().toString().equals(getString(R.string.student_societies))){
                        activity.displayHeader(getResources().getString(R.string.student_societies),SharedPrefrenceUtil.getPrefrence(getActivity(),Constants.university,""));
                        textView_societies.setText(getString(R.string.companies));
                        if(textView_message_events.getText().toString().equals(getString(R.string.my_events_message_events_only))){
                            displayList(arraySocietyEvent);
                        }else{
                            displayList(arraySocietyMsgsAndEvents);
                        }
                        Constants.company_msg = true;
                       companySociety = true;
                    }else if(textView_societies.getText().toString().equals(getString(R.string.companies))){
                        activity.displayHeader(getResources().getString(R.string.companies),SharedPrefrenceUtil.getPrefrence(getActivity(),Constants.subHeader,""));
                        textView_societies.setText(getString(R.string.student_societies));
                        if(textView_message_events.getText().toString().equals(getString(R.string.my_events_message_events_only))){
                            displayList(arrayCompanyEvents);
                        }else{
                            displayList(arrayCompMsgsAndEvents);
                        }
                        Constants.company_msg = false;
                        companySociety = false;
                    }
                }else{
                    displayList(arrayCompanyInvites);
                }
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();

        activity.hideBadges();
        arrayCompanyInvites.clear();
        arrayCompMsgsAndEvents.clear();
        arrayCompanyEvents.clear();
        arraySocietyInvite.clear();
        arraySocietyEvent.clear();
        arraySocietyMsgsAndEvents.clear();
        messagesVoList.clear();
        adderDetailVoList.clear();

        if(SharedPrefrenceUtil.getPrefrence(getActivity(),Constants.isMessages,"").equals("Database_messages")){
            switch (Constants.message) {
                case "particular message":

                    linearLayout_societies_companies.setEnabled(true);
                    linearLayout_message_events.setEnabled(true);
                    loadParticularMessage();

                    break;
                case "society group":

                    linearLayout_societies_companies.setEnabled(true);
                    linearLayout_message_events.setEnabled(true);
                    loadAllSocietyMessages();

                    break;
                case "admin messages":

                    linearLayout_societies_companies.setEnabled(true);
                    linearLayout_message_events.setEnabled(true);
                    loadAdminMessages();

                    break;
                case "my events": {

                    linearLayout_societies_companies.setEnabled(false);
                    linearLayout_message_events.setEnabled(false);

                    if(Constants.CompanyInvites != null){
                        Constants.CompanyInvites.clear();
                    }

                    messagesVoList = databaseHelper.getMessages();

                    List<MessagesVo> voList = new ArrayList<>();

                    voList = getAdderDetail();

                    for (int i = 0; i < voList.size(); i++) {

                        if(voList.get(i).getMessage_type().equals("event")) {

                            if (voList.get(i).getEventconfirmation().equals("Not Sent")) {
                                Constants.CompanyInvites.add(voList.get(i));
                            }
                        }
                    }

                    if (voList != null && voList.size() > 0) {
                        displayList(Constants.CompanyInvites);
                    }
                    break;
                }
                case "messages tab": {

                    if(Constants.CompanyInvites != null){
                        Constants.CompanyInvites.clear();
                    }

                    if(Constants.SocietyInvites != null){
                        Constants.SocietyInvites.clear();
                    }

                    messagesVoList = databaseHelper.getMessages();

                    List<MessagesVo> voList = new ArrayList<>();

                    voList = getAdderDetail();

                    for (int i = 0; i < voList.size(); i++) {

                        if (voList.get(i).getType().equals("companybehalf") || voList.get(i).getType().equals("admin")) {

                            if (voList.get(i).getMessage_type().equals("event")) {

                                arrayCompanyEvents.add(voList.get(i));

                                if (!voList.get(i).getEventconfirmation().equals("Not Sent")) {

                                    arrayCompanyInvites.add(voList.get(i));
                                    Constants.CompanyInvites.add(voList.get(i));
                                }
                            }
                            arrayCompMsgsAndEvents.add(voList.get(i));
                        }
                    }


                    for (int i = 0; i < voList.size(); i++) {

                        if (voList.get(i).getType().equals("studentsociety")) {

                            if (voList.get(i).getMessage_type().equals("event")) {

                                if (!voList.get(i).getEventconfirmation().equals("Not Sent")) {

                                    arraySocietyInvite.add(voList.get(i));
                                    Constants.SocietyInvites.add(voList.get(i));
                                }
                                arraySocietyEvent.add(voList.get(i));
                            }
                            arraySocietyMsgsAndEvents.add(voList.get(i));
                        }
                    }

                    displayList(arrayCompMsgsAndEvents);
                    break;
                }
            }
        }else{
            getMessages();
        }

    }

    private void displayList(List<MessagesVo> voList) {

        if(voList != null && voList.size() > 0) {

            listView_messages.setVisibility(View.VISIBLE);

            MessageAdapter adapter = new MessageAdapter(getActivity(),voList);

            listView_messages.setAdapter(adapter);
        }else{
            listView_messages.setVisibility(View.INVISIBLE);
        }

    }

    private void getMessages() {

        Map<String, Object> map = new HashMap<String, Object>();

        map.put("uid", SharedPrefrenceUtil.getPrefrence(getActivity(), Constants.USER_ID,""));

        progressDialog = application.getProgressDialog(activity);
        request.getResponse(getActivity(), map, WebServiceAPI.API_getmessages, false, new ICallBack() {
            @Override
            public void onCallBackResult(String response) {

                try {

                    JSONObject jsonObject = new JSONObject(response);

                    String status = jsonObject.getString(WebServiceAPI.status);

                    if(Boolean.parseBoolean(status)){

                        JSONArray jsonArray = jsonObject.getJSONArray(WebServiceAPI.data);

                        if(jsonArray != null && jsonArray.length() > 0){



                            databaseHelper.insertMessages(getActivity(),jsonArray);

                        }

                        switch (Constants.message) {
                            case "particular message":

                                linearLayout_societies_companies.setEnabled(true);
                                linearLayout_message_events.setEnabled(true);
                                loadParticularMessage();

                                break;
                            case "society group":

                                linearLayout_societies_companies.setEnabled(true);
                                linearLayout_message_events.setEnabled(true);
                                loadAllSocietyMessages();

                                break;
                            case "admin messages":

                                linearLayout_societies_companies.setEnabled(true);
                                linearLayout_message_events.setEnabled(true);
                                loadAdminMessages();

                                break;
                            case "my events": {

                                if(Constants.CompanyInvites != null){
                                    Constants.CompanyInvites.clear();
                                }

                                linearLayout_societies_companies.setEnabled(false);
                                linearLayout_message_events.setEnabled(false);

                                messagesVoList = databaseHelper.getMessages();

                                List<MessagesVo> voList = new ArrayList<>();

                                voList = getAdderDetail();


                                for (int i = 0; i < voList.size(); i++) {

                                    if(voList.get(i).getMessage_type().equals("event")) {

                                        if (voList.get(i).getEventconfirmation().equals("Not Sent")) {
                                            Constants.CompanyInvites.add(voList.get(i));
                                        }
                                    }
                                }

                                if (voList != null && voList.size() > 0) {

                                    displayList(Constants.CompanyInvites);
                                }
                                break;
                            }
                            case "messages tab": {

                                if(Constants.CompanyInvites != null){
                                    Constants.CompanyInvites.clear();
                                }

                                if(Constants.SocietyInvites != null){
                                    Constants.SocietyInvites.clear();
                                }

                                messagesVoList = databaseHelper.getMessages();

                                List<MessagesVo> voList = new ArrayList<>();

                                voList = getAdderDetail();

                                for (int i = 0; i < voList.size(); i++) {

                                    if (voList.get(i).getType().equals("companybehalf") || voList.get(i).getType().equals("admin")) {

                                        if (voList.get(i).getMessage_type().equals("event")) {

                                            arrayCompanyEvents.add(voList.get(i));

                                            if (!voList.get(i).getEventconfirmation().equals("Not Sent")) {

                                                arrayCompanyInvites.add(voList.get(i));
                                                Constants.CompanyInvites.add(voList.get(i));
                                            }
                                        }
                                        arrayCompMsgsAndEvents.add(voList.get(i));
                                    }
                                }

                                for (int i = 0; i < voList.size(); i++) {

                                    if (voList.get(i).getType().equals("studentsociety")) {

                                        if (voList.get(i).getMessage_type().equals("event")) {

                                            if (!voList.get(i).getEventconfirmation().equals("Not Sent")) {

                                                arraySocietyInvite.add(voList.get(i));
                                                Constants.SocietyInvites.add(voList.get(i));
                                            }
                                            arraySocietyEvent.add(voList.get(i));
                                        }
                                        arraySocietyMsgsAndEvents.add(voList.get(i));
                                    }
                                }

                                displayList(arrayCompMsgsAndEvents);
                                break;
                            }
                        }

                        if(progressDialog != null){
                            progressDialog.dismiss();

                        activity.starVisible();
                          PerfectGraduateApplication.getInstance().displayCustomDialog(getActivity(),"Messages","To help you connect with companies and stay active with student societies, you can follow their messages and sign up to their events." +
                                 "\nThe more events you sign up for, the more opportunities you'll have to connect with companies.");
                        }
                    }else{

                    }

                }catch(JSONException k){
                    k.printStackTrace();
                }

            }
        });

    }

    private List<MessagesVo> getAdderDetail() {
        List<MessagesVo> voList = new ArrayList<>();

        for (int i = 0; i < messagesVoList.size(); i++) {

            adderDetailVoList = databaseHelper.getAdder(messagesVoList.get(i).getAdder_id());

            messagesVoList.get(i).setAdderDetailVoList(adderDetailVoList);

            MessagesVo messagesVo = new MessagesVo();

            messagesVo.setId(messagesVoList.get(i).getId());
            messagesVo.setType(messagesVoList.get(i).getType());
            messagesVo.setAdder_id(messagesVoList.get(i).getAdder_id());
            messagesVo.setSub_adder_id(messagesVoList.get(i).getSub_adder_id());
            messagesVo.setMessage_type(messagesVoList.get(i).getMessage_type());
            messagesVo.setEvent_title(messagesVoList.get(i).getMessage_type());
            messagesVo.setEvent_date(messagesVoList.get(i).getEvent_date());
            messagesVo.setEventtimefrom(messagesVoList.get(i).getEventtimefrom());
            messagesVo.setEventtimeto(messagesVoList.get(i).getEventtimeto());
            messagesVo.setEvent_venue(messagesVoList.get(i).getEvent_venue());
            messagesVo.setMessage(messagesVoList.get(i).getMessage());
            messagesVo.setGender(messagesVoList.get(i).getGender());
            messagesVo.setSend_by(messagesVoList.get(i).getSend_by());
            messagesVo.setRecall(messagesVoList.get(i).getRecall());
            messagesVo.setStatus(messagesVoList.get(i).getStatus());
            messagesVo.setLike_status(messagesVoList.get(i).getLike_status());
            messagesVo.setReport(messagesVoList.get(i).getReport());
            messagesVo.setDate(messagesVoList.get(i).getDate());
            messagesVo.setWebsite(messagesVoList.get(i).getWebsite());
            messagesVo.setAddedStatus(messagesVoList.get(i).getAddedStatus());
            messagesVo.setEventstatus(messagesVoList.get(i).getEventstatus());
            messagesVo.setInternational(messagesVoList.get(i).getInternational());
            messagesVo.setEventconfirmation(messagesVoList.get(i).getEventconfirmation());
            messagesVo.setDefault_timezone(messagesVoList.get(i).getDefault_timezone());
            messagesVo.setPerfect_logo(messagesVoList.get(i).getPerfect_logo());
            messagesVo.setAdderDetailVoList(adderDetailVoList);

            voList.add(messagesVo);
        }

        return voList;
    }

    private void loadAdminMessages() {

        messagesVoList = databaseHelper.getMessages();

        List<MessagesVo> voList = new ArrayList<>();

        if(Constants.CompanyInvites != null){
            Constants.CompanyInvites.clear();
        }

        voList = getAdderDetail();

        for(int i = 0; i < voList.size();  i++){

            if(voList.get(i).getType().equals("admin")){

                if(voList.get(i).getMessage_type().equals("event")){

                    arrayCompanyEvents.add(voList.get(i));

                    if(!voList.get(i).getEventconfirmation().equals("Not Sent")){

                        arrayCompanyInvites.add(voList.get(i));

                    }
                }
                arrayCompMsgsAndEvents.add(voList.get(i));
            }
        }
        Constants.CompanyInvites.addAll(arrayCompanyInvites);
        displayList(arrayCompMsgsAndEvents);
    }

    private void loadParticularMessage() {

        messagesVoList = databaseHelper.getMessages();

        List<MessagesVo> voList = new ArrayList<>();

        if(voList != null){
            voList.clear();
        }

        if(Constants.CompanyInvites != null){
            Constants.CompanyInvites.clear();
        }

        voList = getAdderDetail();

        for(int i = 0; i < voList.size(); i++){

            if(voList.get(i).getMessage_type().equals("event")){

                for(int k = 0; k < voList.get(i).getAdderDetailVoList().size(); k++){

                    if(userCompanyVo.getId().equals(voList.get(i).getAdderDetailVoList().get(k).getId())){

                        if(!voList.get(i).getEventconfirmation().equals("Not Sent")){

                            arrayCompanyInvites.add(voList.get(i));
                        }

                        if(voList.get(i).getType().equals("companybehalf")){
                            arrayCompMsgsAndEvents.add(voList.get(i));

                        }
                    }
                }
                arrayCompanyEvents.add(voList.get(i));
            }
        }

        Constants.CompanyInvites.addAll(arrayCompanyInvites);

        displayList(arrayCompanyEvents);
    }

    public void loadAllSocietyMessages(){

        List<MessagesVo> voList = new ArrayList<>();

        if(Constants.CompanyInvites != null){
            Constants.CompanyInvites.clear();
        }

        messagesVoList = databaseHelper.getMessages();

        voList = getAdderDetail();

        for(int j = 0; j < voList.size(); j++){

            if(voList.get(j).getType().equals("studentsociety")){

                if(voList.get(j).getMessage_type().equals("event")){

                    for(int k = 0; k < voList.get(j).getAdderDetailVoList().size(); k++){

                        if(voList.get(j).getAdderDetailVoList().get(k).getId().equals(Constants.adder_id)){

                            if(!voList.get(j).getEventconfirmation().equals("Not Sent")){

                                arrayCompanyInvites.add(voList.get(j));
                            }
                            arrayCompMsgsAndEvents.add(voList.get(j));
                            arrayCompanyEvents.add(voList.get(j));
                        }
                    }
                }
            }
        }

        Constants.CompanyInvites.addAll(arrayCompanyInvites);
        displayList(arrayCompanyEvents);
    }

}
