package com.perfectgraduate.fragment;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.iid.FirebaseInstanceId;
import com.perfectgraduate.R;
import com.perfectgraduate.activity.HomeActivity;
import com.perfectgraduate.api.PerfectGraduateRequest;
import com.perfectgraduate.api.WebServiceAPI;
import com.perfectgraduate.app.PerfectGraduateApplication;
import com.perfectgraduate.listener.ICallBack;
import com.perfectgraduate.utility.Constants;
import com.perfectgraduate.utility.SharedPrefrenceUtil;
import com.perfectgraduate.vo.CountryVo;
import com.perfectgraduate.vo.DisciplineVo;
import com.perfectgraduate.vo.PositionVo;
import com.perfectgraduate.vo.StateVo;
import com.perfectgraduate.vo.SubDisciplineVo;
import com.perfectgraduate.vo.UniversityVo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by user on 08-Dec-16.
 */

public class StudentDetailsFragment extends Fragment{

    TextView tv_next;
    private TextView textView_gender,textView_country,textView_location,textView_university,
            textView_position,textView_discipline,textView_subdiscipline,textView_discipline2,
            textView_subdiscipline2;

    private LinearLayout linearLayout_gender,linearLayout_country,linearLayout_location,
    linearLayout_university,linearLayout_position,linearLayout_discipline,linearLayout_subdiscipline
    ,linearLayout_discipline2,linearLayout_subdiscipline2;

    private HomeActivity activity;
    private PerfectGraduateRequest request;

    private List<CountryVo> countryVoList;
    private List<StateVo> stateVoList;
    private List<UniversityVo> universityVoList;
    private List<DisciplineVo> disciplineVoList;
    private List<DisciplineVo> disciplineVoList2;
    private List<SubDisciplineVo> subDisciplineVoList;
    private List<SubDisciplineVo> subDisciplineVoList2;
    private List<PositionVo> positionVoList;
    private List<String> list_country;
    private List<String> list_state;
    private List<String> list_university;
    private List<String> list_position;
    private List<String> list_discipline;
    private List<String> list_subdiscipline;
    private List<String> list_discipline2;
    private List<String> list_subdiscipline2;

    private int genderSelect = 0;
    private int country = 0;
    private int state = 0;
    private int university = 0;
    private int position = 0;
    private int discipline = 0;
    private int subdiscipline = 0;
    private int discipline2 = 0;
    private int subdiscipline2 = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_student_details, container, false);

        init(rootView);

        return rootView;
    }

    private void init(View rootView) {

        activity = (HomeActivity) getActivity();

        /*PerfectGraduateApplication.getInstance().setShowDialog(getActivity(),"Warning","Changing the location or university will reset your student society selections." +
                "\nIf you do not want to reset your student society selections, use the menu button to exit the page.");
*/

        tv_next = (TextView) rootView.findViewById(R.id.tv_next);

        linearLayout_gender = (LinearLayout) rootView.findViewById(R.id.linearLayout_gender);
        linearLayout_country = (LinearLayout) rootView.findViewById(R.id.linearLayout_country);
        linearLayout_location = (LinearLayout) rootView.findViewById(R.id.linearLayout_location);
        linearLayout_university = (LinearLayout) rootView.findViewById(R.id.linearLayout_university);
        linearLayout_position = (LinearLayout) rootView.findViewById(R.id.linearLayout_position);
        linearLayout_discipline = (LinearLayout) rootView.findViewById(R.id.linearLayout_discipline);
        linearLayout_subdiscipline = (LinearLayout) rootView.findViewById(R.id.linearLayout_subdiscipline);
        linearLayout_discipline2 = (LinearLayout) rootView.findViewById(R.id.linearLayout_discipline2);
        linearLayout_subdiscipline2 = (LinearLayout) rootView.findViewById(R.id.linearLayout_subdiscipline2);

        textView_gender = (TextView) rootView.findViewById(R.id.textView_gender);
        textView_country = (TextView) rootView.findViewById(R.id.textView_country);
        textView_location = (TextView) rootView.findViewById(R.id.textView_location);
        textView_university = (TextView) rootView.findViewById(R.id.textView_university);
        textView_position = (TextView) rootView.findViewById(R.id.textView_position);
        textView_discipline = (TextView) rootView.findViewById(R.id.textView_discipline);
        textView_subdiscipline = (TextView) rootView.findViewById(R.id.textView_subdiscipline);
        textView_discipline2 = (TextView) rootView.findViewById(R.id.textView_discipline2);
        textView_subdiscipline2 = (TextView) rootView.findViewById(R.id.textView_subdiscipline2);

        request = new PerfectGraduateRequest(getActivity());

        countryVoList = new ArrayList<>();
        list_country = new ArrayList<>();

        stateVoList = new ArrayList<>();
        list_state = new ArrayList<>();

        universityVoList = new ArrayList<>();
        list_university = new ArrayList<>();

        disciplineVoList = new ArrayList<>();
        list_discipline = new ArrayList<>();

        subDisciplineVoList = new ArrayList<SubDisciplineVo>();
        list_subdiscipline = new ArrayList<>();

        positionVoList = new ArrayList<>();
        list_position = new ArrayList<>();

        disciplineVoList2 = new ArrayList<>();
        list_discipline2 = new ArrayList<>();

        subDisciplineVoList2 = new ArrayList<>();
        list_subdiscipline2 = new ArrayList<>();

        getProfile();

        linearLayout_gender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getGender();
            }
        });

        linearLayout_country.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                displayCountry();
            }
        });

        linearLayout_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                displayState();
            }
        });

        linearLayout_university.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                displayUniversity();
            }
        });

        linearLayout_position.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                displayPosition();
            }
        });

        linearLayout_discipline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                displayDiscipline();
            }
        });

        linearLayout_subdiscipline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                displaySubDiscipline();
            }
        });

        linearLayout_discipline2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                displayDiscipline2();
            }
        });

        linearLayout_subdiscipline2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                displaySubDiscipline2();
            }
        });

        tv_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UpdateStudent();
            }
        });
    }

    private void getProfile() {

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("uid", SharedPrefrenceUtil.getPrefrence(getActivity(),Constants.USER_ID,""));

        request.getResponse(getActivity(), map, WebServiceAPI.API_getProfile, true, new ICallBack() {
            @Override
            public void onCallBackResult(String response) {

                try {

                    JSONObject jsonObject = new JSONObject(response);

                    String status = jsonObject.getString(WebServiceAPI.status);

                    if(Boolean.parseBoolean(status)){

                        JSONObject object = jsonObject.getJSONObject(WebServiceAPI.data);

                        SharedPrefrenceUtil.setPrefrence(getActivity(),Constants.id,object.getString("id"));
                        SharedPrefrenceUtil.setPrefrence(getActivity(),Constants.fb_id,object.getString("fb_id"));
                        SharedPrefrenceUtil.setPrefrence(getActivity(),Constants.email,object.getString("email"));
                        SharedPrefrenceUtil.setPrefrence(getActivity(),Constants.password,object.getString("password"));
                        SharedPrefrenceUtil.setPrefrence(getActivity(),Constants.image,object.getString("image"));
                        SharedPrefrenceUtil.setPrefrence(getActivity(),Constants.linkedinUrl,object.getString("linkedinUrl"));
                        SharedPrefrenceUtil.setPrefrence(getActivity(),Constants.name,object.getString("name"));
                        SharedPrefrenceUtil.setPrefrence(getActivity(),Constants.gender,object.getString("gender"));

                        textView_gender.setText(object.getString("gender"));

                        SharedPrefrenceUtil.setPrefrence(getActivity(),Constants.mobile,object.getString("mobile"));
                        SharedPrefrenceUtil.setPrefrence(getActivity(),Constants.country_id,object.getString("country_id"));
                        Constants.Country_id = object.getString("country_id");
                        SharedPrefrenceUtil.setPrefrence(getActivity(),Constants.state_id,object.getString("state_id"));
                        Constants.State_id = object.getString("state_id");
                        SharedPrefrenceUtil.setPrefrence(getActivity(),Constants.uni_id,object.getString("uni_id"));
                        Constants.University_id = object.getString("uni_id");
                        SharedPrefrenceUtil.setPrefrence(getActivity(),Constants.graduateProg_id,object.getString("graduateProg_id"));
                        Constants.Position_id = object.getString("graduateProg_id");
                        SharedPrefrenceUtil.setPrefrence(getActivity(),Constants.disciplline_id,object.getString("disciplline_id"));
                        SharedPrefrenceUtil.setPrefrence(getActivity(),Constants.subDiscipline_id,object.getString("subDiscipline_id"));
                        SharedPrefrenceUtil.setPrefrence(getActivity(),Constants.status,object.getString("status"));
                        SharedPrefrenceUtil.setPrefrence(getActivity(),Constants.device_id,object.getString("device_id"));
                        SharedPrefrenceUtil.setPrefrence(getActivity(),Constants.device_type,object.getString("device_type"));
                        SharedPrefrenceUtil.setPrefrence(getActivity(),Constants.score,object.getString("score"));
                        SharedPrefrenceUtil.setPrefrence(getActivity(),Constants.date,object.getString("date"));
                        SharedPrefrenceUtil.setPrefrence(getActivity(),Constants.modified_date,object.getString("modified_date"));
                        SharedPrefrenceUtil.setPrefrence(getActivity(),Constants.unique_id,object.getString("unique_id"));
                        SharedPrefrenceUtil.setPrefrence(getActivity(),Constants.country,object.getString("country"));

                        textView_country.setText(object.getString("country"));

                        SharedPrefrenceUtil.setPrefrence(getActivity(),Constants.state,object.getString("state"));

                        textView_location.setText(object.getString("state"));

                        SharedPrefrenceUtil.setPrefrence(getActivity(),Constants.university,object.getString("university"));

                        textView_university.setText(object.getString("university"));

                        SharedPrefrenceUtil.setPrefrence(getActivity(),Constants.program,object.getString("program"));

                        textView_position.setText(object.getString("program"));

                        JSONArray jsonArray = object.getJSONArray("discipline");

                        if(jsonArray != null && jsonArray.length() > 0){

                            for(int i = 0; i < jsonArray.length(); i++){

                                JSONObject object1 = jsonArray.getJSONObject(i);

                                if(i == 0){
                                    textView_discipline.setText(object1.getString("discipline"));

                                    Constants.Discipline_id = object1.getString("discipline_id");


                                    if(!SharedPrefrenceUtil.getPrefrence(getActivity(),Constants.SubDiscipline_name,"").equals("")){
                                        textView_subdiscipline.setText(object1.getString("sub_discipline"));
                                    }

                                }else if(i == 1){

                                    Constants.Discipline2_id = object1.getString("discipline_id");
                                    Constants.SubDiscipline2_id = object1.getString("sub_discipline_id");

                                    if(!SharedPrefrenceUtil.getPrefrence(getActivity(),Constants.Discipline2_name,"").equals("")){
                                        textView_discipline2.setText(object1.getString("discipline"));
                                    }

                                    if(!SharedPrefrenceUtil.getPrefrence(getActivity(),Constants.SubDiscipline2_name,"").equals("")){
                                        textView_subdiscipline2.setText(object1.getString("sub_discipline"));
                                    }
                                }

                            }

                        }
                        getCountry();
                    }else{

                    }

                }catch(JSONException k){
                    k.printStackTrace();
                }

            }
        });

    }

    private void displaySubDiscipline2() {
        if(list_subdiscipline2.size() > 0) {
            final CharSequence[] items;
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

            builder.setTitle(getString(R.string.select_subdiscipline));

            items = list_subdiscipline2.toArray(new CharSequence[list_subdiscipline2.size()]);

            if(!textView_subdiscipline2.getText().toString().equals("")){
                for (int i = 0; i < list_subdiscipline2.size(); i++){
                    if (list_subdiscipline2.get(i).equals(textView_subdiscipline2.getText().toString())){
                        subdiscipline2 = i;
                    }
                }
            }else{
                subdiscipline2 = -1;
            }

            builder.setSingleChoiceItems(items,subdiscipline2, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item) {
                    // Do something with the selection
                    subdiscipline2 = item;
                    textView_subdiscipline2.setText(items[item]);
                    String cid = "";
                    for (int i = 0; i < subDisciplineVoList2.size(); i++){
                        if (subDisciplineVoList2.get(i).getName().equals(items[item])){
                            cid = subDisciplineVoList2.get(i).getCountry_id();
                            Constants.SubDiscipline2_id = subDisciplineVoList2.get(i).getId();
                            Constants.SubDiscipline2_name = subDisciplineVoList2.get(i).getName();
                        }
                    }

                }
            });
            AlertDialog alert = builder.create();
            alert.show();
        }

    }

    private void displayDiscipline2() {
        if(list_discipline2.size() > 0) {
            final CharSequence[] items;
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

            builder.setTitle(getString(R.string.select_discipline));

            items = list_discipline2.toArray(new CharSequence[list_discipline2.size()]);

            if(!textView_discipline2.getText().toString().equals("")){
                for (int i = 0; i < list_discipline2.size(); i++){
                    if (list_discipline2.get(i).equals(textView_discipline2.getText().toString())){
                        discipline2 = i;
                    }
                }
            }else{
                discipline2 = -1;
            }

            builder.setSingleChoiceItems(items,discipline2, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item) {
                    // Do something with the selection
                    discipline2 = item;
                    textView_discipline2.setText(items[item]);
                    String did = "";
                    for (int i = 0; i < disciplineVoList2.size(); i++) {
                        if (disciplineVoList2.get(i).getName().equals(items[item])) {
                            did = disciplineVoList2.get(i).getId();
                            Constants.Discipline2_id = disciplineVoList2.get(i).getId();
                            Constants.Discipline2_name = disciplineVoList2.get(i).getName();
                        }
                    }

                    getSubDiscipline2(did);
                }
            });
            AlertDialog alert = builder.create();
            alert.show();
        }

    }


    private void displaySubDiscipline() {

        if(list_subdiscipline.size() > 0) {
            final CharSequence[] items;
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

            builder.setTitle(getString(R.string.select_subdiscipline));

            items = list_subdiscipline.toArray(new CharSequence[list_subdiscipline.size()]);

            if(!textView_subdiscipline.getText().toString().equals("")){
                for (int i = 0; i < list_subdiscipline.size(); i++){
                    if (list_subdiscipline.get(i).equals(textView_subdiscipline.getText().toString())){
                        subdiscipline = i;
                    }
                }
            }else{
                subdiscipline = -1;
            }

            builder.setSingleChoiceItems(items,subdiscipline, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item) {
                    // Do something with the selection
                    subdiscipline = item;
                    textView_subdiscipline.setText(items[item]);
                    String cid = "";
                    for (int i = 0; i < subDisciplineVoList.size(); i++){
                        if (subDisciplineVoList.get(i).getName().equals(items[item])){
                            cid = subDisciplineVoList.get(i).getCountry_id();
                            Constants.SubDiscipline_id = subDisciplineVoList.get(i).getId();
                            Constants.SubDiscipline_name = subDisciplineVoList.get(i).getName();
                        }
                    }
                    getDiscipline2("", cid);
                }
            });
            AlertDialog alert = builder.create();
            alert.show();
        }

    }

    private void displayDiscipline() {

        if(list_discipline.size() > 0) {
            final CharSequence[] items;
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

            builder.setTitle(getString(R.string.select_discipline));

            items = list_discipline.toArray(new CharSequence[list_discipline.size()]);

            if(!textView_discipline.getText().toString().equals("")){
                for (int i = 0; i < list_discipline.size(); i++){
                    if (list_discipline.get(i).equals(textView_discipline.getText().toString())){
                        discipline = i;
                    }
                }
            }else{
                discipline = -1;
            }

            builder.setSingleChoiceItems(items,discipline, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item) {
                    // Do something with the selection
                    discipline = item;
                    textView_discipline.setText(items[item]);
                    String did = "";
                    for (int i = 0; i < disciplineVoList.size(); i++) {
                        if (disciplineVoList.get(i).getName().equals(items[item])) {
                            did = disciplineVoList.get(i).getId();
                            Constants.Discipline_id = disciplineVoList.get(i).getId();
                            Constants.Discipline2_name = disciplineVoList.get(i).getName();
                        }
                    }
                    tv_next.setEnabled(true);
                    tv_next.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    tv_next.setBackgroundDrawable(getResources().getDrawable(R.drawable.blue_border));
                    getSubDiscipline(did);
                }
            });
            AlertDialog alert = builder.create();
            alert.show();
        }

    }

    private void displayPosition() {
        if(list_position.size() > 0) {
            final CharSequence[] items;
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

            builder.setTitle(getString(R.string.select_position));

            items = list_position.toArray(new CharSequence[list_position.size()]);

            if(!textView_position.getText().toString().equals("")){
                for (int i = 0; i < list_position.size(); i++){
                    if (list_discipline.get(i).equals(textView_position.getText().toString())){
                        position = i;
                    }
                }
            }else{
                position = -1;
            }

            builder.setSingleChoiceItems(items,position, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item) {
                    // Do something with the selection
                    position = item;
                    textView_position.setText(items[item]);
                    String cid = "";
                    for (int i = 0; i < positionVoList.size(); i++) {
                        if (positionVoList.get(i).getName().equals(items[item])) {
                            cid = positionVoList.get(i).getCountry_id();
                            Constants.Position_id = positionVoList.get(i).getId();
                            Constants.Position_name = positionVoList.get(i).getName();
                        }
                    }

                    getDiscipline("", cid);
                }
            });
            AlertDialog alert = builder.create();
            alert.show();
        }

    }

    private void displayUniversity() {
        if(list_university.size() > 0) {
            final CharSequence[] items;
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

            builder.setTitle(getString(R.string.select_university));

            items = list_university.toArray(new CharSequence[list_university.size()]);

            if(!textView_university.getText().toString().equals("")){
                for (int i = 0; i < list_university.size(); i++){
                    if (list_university.get(i).equals(textView_university.getText().toString())){
                        university = i;
                    }
                }
            }else{
                university = -1;
            }

            builder.setSingleChoiceItems(items,university, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item) {
                    // Do something with the selection
                    university = item;
                    textView_university.setText(items[item]);
                    String cid = "";
                    for (int i = 0; i < universityVoList.size(); i++) {
                        if (universityVoList.get(i).getName().equals(items[item])) {
                            cid = universityVoList.get(i).getCountry_id();
                            Constants.University_id = universityVoList.get(i).getId();
                            Constants.University_name = universityVoList.get(i).getName();
                        }
                    }

                    getPosition(cid);
                }
            });
            AlertDialog alert = builder.create();
            alert.show();
        }

    }

    private void displayState() {
        if(list_state.size() > 0) {
            final CharSequence[] items;
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

            builder.setTitle(getString(R.string.select_state));

            items = list_state.toArray(new CharSequence[list_state.size()]);

            if(!textView_location.getText().toString().equals("")){
                for (int i = 0; i < list_state.size(); i++){
                    if (list_state.get(i).equals(textView_location.getText().toString())){
                        state = i;
                    }
                }
            }else{
                state = -1;
            }

            builder.setSingleChoiceItems(items,state, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item) {
                    // Do something with the selection
                    state = item;
                    textView_location.setText(items[item]);
                    String sid = "";
                    for (int i = 0; i < stateVoList.size(); i++) {
                        if (stateVoList.get(i).getName().equals(items[item])) {
                            sid = stateVoList.get(i).getId();
                            Constants.State_id = stateVoList.get(i).getId();
                            Constants.State_name = stateVoList.get(i).getName();
                        }
                    }

                    getUniversity(sid);
                }
            });
            AlertDialog alert = builder.create();
            alert.show();
        }

    }

    private void displayCountry() {
        if(list_country.size() > 0) {
            final CharSequence[] items;
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

            builder.setTitle(getString(R.string.select_country));

            items = list_country.toArray(new CharSequence[list_country.size()]);

            if(!textView_country.getText().toString().equals("")){
                for (int i = 0; i < list_country.size(); i++){
                    if (list_country.get(i).equals(textView_country.getText().toString())){
                        country = i;
                    }
                }
            }else{
                country = -1;
            }

            builder.setSingleChoiceItems(items,country, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item) {
                    // Do something with the selection
                    country = item;
                    textView_country.setText(items[item]);
                    String cid = "";
                    for (int i = 0; i < countryVoList.size(); i++) {
                        if (countryVoList.get(i).getName().equals(items[item])) {
                            cid = countryVoList.get(i).getId();
                            Constants.Country_id = countryVoList.get(i).getId();
                            Constants.Country_name = countryVoList.get(i).getName();
                        }
                    }

                    getLocation(cid);
                }
            });
            AlertDialog alert = builder.create();
            alert.show();
        }

    }


    private void getGender() {
        final CharSequence[] gender = {getString(R.string.male),getString(R.string.female)};
        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
        alert.setTitle(getString(R.string.select_gender));

        if(textView_gender.getText().toString().equals(getString(R.string.male))){
            genderSelect = 0;
        }else if(textView_gender.getText().toString().equals(getString(R.string.female))){
            genderSelect = 1;
        }else{
            genderSelect = -1;
        }

        alert.setSingleChoiceItems(gender,genderSelect, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                genderSelect = which;
                if(gender[which]== getString(R.string.male)) {
                    textView_gender.setText(getString(R.string.male));
                    Constants.Gender = getString(R.string.male);
                }
                else if (gender[which]== getString(R.string.female)) {
                    textView_gender.setText(getString(R.string.female));
                    Constants.Gender = getString(R.string.female);
                }
                dialog.dismiss();
            }
        });
        alert.show();
    }

    private void getCountry() {

        Map<String, Object> map = new HashMap<String, Object>();

        if(list_country.size() > 0 && list_country != null){
            list_country.clear();
        }
        if(countryVoList.size() > 0 && countryVoList != null){
            countryVoList.clear();
        }

        request.getResponse(getActivity(), map, WebServiceAPI.API_getAllCountries, false, new ICallBack() {
            @Override
            public void onCallBackResult(String response) {

                try {

                    JSONObject jsonObject = new JSONObject(response);

                    String status = jsonObject.getString(WebServiceAPI.status);

                    if(Boolean.parseBoolean(status)){

                        JSONArray jsonArray = jsonObject.getJSONArray(WebServiceAPI.data);

                        if(jsonArray != null && jsonArray.length() > 0){

                            for (int i = 0; i < jsonArray.length(); i++){

                                JSONObject object = jsonArray.getJSONObject(i);

                                CountryVo countryVo = new CountryVo();

                                countryVo.setId(object.getString("id"));
                                countryVo.setName(object.getString("name"));
                                countryVo.setAbr(object.getString("abr"));

                                countryVoList.add(countryVo);
                                list_country.add(object.getString("name"));
                            }

                        }

                        getLocation(Constants.Country_id);

                    }else{
                        if(list_country.size() > 0 && list_country != null){
                            list_country.clear();
                        }
                        if(countryVoList.size() > 0 && countryVoList != null){
                            countryVoList.clear();
                        }
                        textView_country.setText("");
                    }

                }catch(JSONException k){
                    k.printStackTrace();
                }

            }
        });

    }

    private void getLocation(String cid) {

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("cid",cid);

        if(list_state.size() > 0 && list_state != null){
            list_state.clear();
        }
        if(stateVoList.size() > 0 && stateVoList != null){
            stateVoList.clear();
        }

        request.getResponse(getActivity(), map, WebServiceAPI.API_getAllStates, false, new ICallBack() {
            @Override
            public void onCallBackResult(String response) {

                try {

                    JSONObject jsonObject = new JSONObject(response);

                    String status = jsonObject.getString(WebServiceAPI.status);

                    if(Boolean.parseBoolean(status)){

                        JSONArray jsonArray = jsonObject.getJSONArray(WebServiceAPI.data);

                        if(jsonArray != null && jsonArray.length() > 0){

                            for (int i = 0; i < jsonArray.length(); i++){

                                JSONObject object = jsonArray.getJSONObject(i);

                                StateVo stateVo = new StateVo();

                                stateVo.setId(object.getString("id"));
                                stateVo.setCountry_id(object.getString("country_id"));
                                stateVo.setName(object.getString("name"));
                                stateVo.setAbr(object.getString("abr"));
                                stateVo.setMap_reference(object.getString("map_reference"));
                                stateVo.setLatitude(object.getString("latitude"));
                                stateVo.setLongitude(object.getString("longitude"));
                                stateVo.setStatus(object.getString("status"));
                                stateVo.setDate(object.getString("date"));

                                stateVoList.add(stateVo);
                                list_state.add(object.getString("name"));
                            }

                        }

                        getUniversity(Constants.State_id);

                    }else{
                        if(list_state.size() > 0 && list_state != null){
                            list_state.clear();
                        }
                        if(stateVoList.size() > 0 && stateVoList != null){
                            stateVoList.clear();
                        }
                        textView_location.setText("");
                    }

                }catch(JSONException k){
                    k.printStackTrace();
                }

            }
        });

    }

    private void getUniversity(String sid) {

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("sid",sid);

        if(list_university.size() > 0 && list_university != null){
            list_university.clear();
        }
        if(universityVoList.size() > 0 && universityVoList != null){
            universityVoList.clear();
        }

        request.getResponse(getActivity(), map, WebServiceAPI.API_getAllUniversities, false, new ICallBack() {
            @Override
            public void onCallBackResult(String response) {

                try {

                    JSONObject jsonObject = new JSONObject(response);

                    String status = jsonObject.getString(WebServiceAPI.status);

                    if(Boolean.parseBoolean(status)){

                        JSONArray jsonArray = jsonObject.getJSONArray(WebServiceAPI.data);

                        if(jsonArray != null && jsonArray.length() > 0){

                            for (int i = 0; i < jsonArray.length(); i++){

                                JSONObject object = jsonArray.getJSONObject(i);

                                UniversityVo universityVo = new UniversityVo();

                                universityVo.setId(object.getString("id"));
                                universityVo.setCountry_id(object.getString("country_id"));
                                universityVo.setName(object.getString("name"));
                                universityVo.setStatus(object.getString("status"));
                                universityVo.setDate(object.getString("date"));

                                universityVoList.add(universityVo);
                                list_university.add(object.getString("name"));
                            }

                        }

                        getPosition(Constants.Country_id);

                    }else{
                        if(list_university.size() > 0 && list_university != null){
                            list_university.clear();
                        }
                        if(universityVoList.size() > 0 && universityVoList != null){
                            universityVoList.clear();
                        }
                        textView_university.setText("");
                    }

                }catch(JSONException k){
                    k.printStackTrace();
                }

            }
        });

    }

    private void getPosition(String cid) {

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("cid",cid);

        if(list_position.size() > 0 && list_position != null){
            list_position.clear();
        }
        if(positionVoList.size() > 0 && positionVoList != null){
            positionVoList.clear();
        }

        request.getResponse(getActivity(), map, WebServiceAPI.API_getposition, false, new ICallBack() {
            @Override
            public void onCallBackResult(String response) {

                try {

                    JSONObject jsonObject = new JSONObject(response);

                    String status = jsonObject.getString(WebServiceAPI.status);

                    if(Boolean.parseBoolean(status)){

                        JSONArray jsonArray = jsonObject.getJSONArray(WebServiceAPI.data);

                        if(jsonArray != null && jsonArray.length() > 0){

                            for (int i = 0; i < jsonArray.length(); i++){

                                JSONObject object = jsonArray.getJSONObject(i);

                                PositionVo positionVo = new PositionVo();

                                positionVo.setId(object.getString("id"));
                                positionVo.setAbr(object.getString("abr"));
                                positionVo.setCountry_id(object.getString("country_id"));
                                positionVo.setName(object.getString("name"));
                                positionVo.setStatus(object.getString("status"));
                                positionVo.setDate(object.getString("date"));

                                positionVoList.add(positionVo);
                                list_position.add(object.getString("name"));
                            }

                        }

                        getDiscipline("",Constants.Country_id);

                    }else{
                        if(list_position.size() > 0 && list_position != null){
                            list_position.clear();
                        }
                        if(positionVoList.size() > 0 && positionVoList != null){
                            positionVoList.clear();
                        }
                        textView_position.setText("");
                    }
                }catch(JSONException k){
                    k.printStackTrace();
                }

            }
        });

    }


    private void getDiscipline(String uid,String cid) {

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("uid","");
        map.put("cid",cid);

        if(list_discipline.size() > 0 && list_discipline != null){
            list_discipline.clear();
        }
        if(disciplineVoList.size() > 0 && disciplineVoList != null){
            disciplineVoList.clear();
        }

        request.getResponse(getActivity(), map, WebServiceAPI.API_getAlldisciplines, false, new ICallBack() {
            @Override
            public void onCallBackResult(String response) {

                try {

                    JSONObject jsonObject = new JSONObject(response);

                    String status = jsonObject.getString(WebServiceAPI.status);

                    if(Boolean.parseBoolean(status)){

                        JSONArray jsonArray = jsonObject.getJSONArray(WebServiceAPI.data);

                        if(jsonArray != null && jsonArray.length() > 0){

                            for (int i = 0; i < jsonArray.length(); i++) {

                                JSONObject object = jsonArray.getJSONObject(i);

                                DisciplineVo disciplineVo = new DisciplineVo();

                                disciplineVo.setId(object.getString("id"));
                                disciplineVo.setName(object.getString("name"));

                                disciplineVoList.add(disciplineVo);

                                list_discipline.add(object.getString("name"));
                            }
                        }

                        getSubDiscipline(Constants.Discipline_id);

                    }else{
                        if(list_discipline.size() > 0 && list_discipline != null){
                            list_discipline.clear();
                        }
                        if(disciplineVoList.size() > 0 && disciplineVoList != null){
                            disciplineVoList.clear();
                        }
                        textView_discipline.setText("");
                    }

                }catch(JSONException k){
                    k.printStackTrace();
                }

            }
        });

    }

    private void getSubDiscipline(String did) {

        Map<String, Object> map = new HashMap<String, Object>();

        map.put("did",did);

        if(list_subdiscipline.size() > 0 && list_subdiscipline != null){
            list_subdiscipline.clear();
        }
        if(subDisciplineVoList.size() > 0 && subDisciplineVoList != null){
            subDisciplineVoList.clear();
        }

        request.getResponse(getActivity(), map, WebServiceAPI.API_getAllSubdisciplines, false, new ICallBack() {
            @Override
            public void onCallBackResult(String response) {

                try {

                    JSONObject jsonObject = new JSONObject(response);

                    String status = jsonObject.getString(WebServiceAPI.status);

                    if(Boolean.parseBoolean(status)){

                        JSONArray jsonArray = jsonObject.getJSONArray(WebServiceAPI.data);

                        if(jsonArray != null && jsonArray.length() > 0){

                            for (int j = 0; j < jsonArray.length(); j++) {

                                JSONObject objectSub = jsonArray.getJSONObject(j);

                                SubDisciplineVo subDisciplineVo = new SubDisciplineVo();

                                subDisciplineVo.setId(objectSub.getString("id"));
                                subDisciplineVo.setName(objectSub.getString("name"));
                                subDisciplineVo.setCountry_id(objectSub.getString("country_id"));
                                subDisciplineVo.setDiscipline_id(objectSub.getString("discipline_id"));
                                subDisciplineVo.setDate(objectSub.getString("date"));
                                subDisciplineVo.setStatus(objectSub.getString("status"));

                                subDisciplineVoList.add(subDisciplineVo);

                                list_subdiscipline.add(objectSub.getString("name"));
                            }
                        }

                        getDiscipline2("",Constants.Country_id);

                    }else{
                        if(list_subdiscipline.size() > 0 && list_subdiscipline != null){
                            list_subdiscipline.clear();
                        }
                        if(subDisciplineVoList.size() > 0 && subDisciplineVoList != null){
                            subDisciplineVoList.clear();
                        }
                        textView_subdiscipline.setText("");
                    }

                }catch(JSONException k){
                    k.printStackTrace();
                }

            }
        });

    }


    private void getDiscipline2(String uid,String cid) {

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("uid","");
        map.put("cid",cid);

        if(list_discipline2.size() > 0 && list_discipline2 != null){
            list_discipline2.clear();
        }
        if(disciplineVoList2.size() > 0 && disciplineVoList2 != null){
            disciplineVoList2.clear();
        }

        request.getResponse(getActivity(), map, WebServiceAPI.API_getAlldisciplines, false, new ICallBack() {
            @Override
            public void onCallBackResult(String response) {

                try {

                    JSONObject jsonObject = new JSONObject(response);

                    String status = jsonObject.getString(WebServiceAPI.status);

                    if(Boolean.parseBoolean(status)){

                        JSONArray jsonArray = jsonObject.getJSONArray(WebServiceAPI.data);

                        if(jsonArray != null && jsonArray.length() > 0){

                            for (int i = 0; i < jsonArray.length(); i++) {

                                JSONObject object = jsonArray.getJSONObject(i);

                                DisciplineVo disciplineVo = new DisciplineVo();

                                disciplineVo.setId(object.getString("id"));
                                disciplineVo.setName(object.getString("name"));

                                disciplineVoList2.add(disciplineVo);

                                list_discipline2.add(object.getString("name"));
                            }
                        }

                        getSubDiscipline2(Constants.Discipline2_id);

                    }else{
                        if(list_discipline2.size() > 0 && list_discipline2 != null){
                            list_discipline2.clear();
                        }
                        if(disciplineVoList2.size() > 0 && disciplineVoList2 != null){
                            disciplineVoList2.clear();
                        }
                        textView_discipline2.setText("");
                    }

                }catch(JSONException k){
                    k.printStackTrace();
                }

            }
        });

    }

    private void getSubDiscipline2(String did) {

        Map<String, Object> map = new HashMap<String, Object>();

        map.put("did",did);

        if(list_subdiscipline2.size() > 0 && list_subdiscipline2 != null){
            list_subdiscipline2.clear();
        }
        if(subDisciplineVoList2.size() > 0 && subDisciplineVoList2 != null){
            subDisciplineVoList2.clear();
        }

        request.getResponse(getActivity(), map, WebServiceAPI.API_getAllSubdisciplines, false, new ICallBack() {
            @Override
            public void onCallBackResult(String response) {

                try {

                    JSONObject jsonObject = new JSONObject(response);

                    String status = jsonObject.getString(WebServiceAPI.status);

                    if(Boolean.parseBoolean(status)){

                        JSONArray jsonArray = jsonObject.getJSONArray(WebServiceAPI.data);

                        if(jsonArray != null && jsonArray.length() > 0){

                            for (int j = 0; j < jsonArray.length(); j++) {

                                JSONObject objectSub = jsonArray.getJSONObject(j);

                                SubDisciplineVo subDisciplineVo = new SubDisciplineVo();

                                subDisciplineVo.setId(objectSub.getString("id"));
                                subDisciplineVo.setName(objectSub.getString("name"));
                                subDisciplineVo.setCountry_id(objectSub.getString("country_id"));
                                subDisciplineVo.setDiscipline_id(objectSub.getString("discipline_id"));
                                subDisciplineVo.setDate(objectSub.getString("date"));
                                subDisciplineVo.setStatus(objectSub.getString("status"));

                                subDisciplineVoList2.add(subDisciplineVo);

                                list_subdiscipline2.add(objectSub.getString("name"));
                            }
                        }

                    }else{
                        if(list_subdiscipline2.size() > 0 && list_subdiscipline2 != null){
                            list_subdiscipline2.clear();
                        }
                        if(subDisciplineVoList2.size() > 0 && subDisciplineVoList2 != null){
                            subDisciplineVoList2.clear();
                        }
                        textView_subdiscipline2.setText("");
                    }

                }catch(JSONException k){
                    k.printStackTrace();
                }

            }
        });

    }

    private void UpdateStudent() {

        Map<String, Object> map = new HashMap<String, Object>();

        map.put("gender",textView_gender.getText().toString());
        map.put("id",SharedPrefrenceUtil.getPrefrence(getActivity(),Constants.USER_ID,""));
        map.put("cid",Constants.Country_id);
        map.put("sid",Constants.State_id);
        map.put("uid",Constants.University_id);
        map.put("gpid",Constants.Position_id);
        map.put("did",Constants.Discipline_id);
        map.put("sdid",Constants.SubDiscipline_id);
        map.put("did1",Constants.Discipline2_id);
        map.put("sdid1",Constants.SubDiscipline2_id);
        map.put("device", FirebaseInstanceId.getInstance().getToken());
        map.put("device type","android");


        request.getResponse(getActivity(), map, WebServiceAPI.API_UpStudent, true, new ICallBack() {
            @Override
            public void onCallBackResult(String response) {

                try {

                    JSONObject jsonObject = new JSONObject(response);

                    String status = jsonObject.getString(WebServiceAPI.status);

                    if(Boolean.parseBoolean(status)){

                        activity.displayView(0);

                    }else{
                        UpdateStudent();
                    }

                }catch(JSONException k){
                    k.printStackTrace();
                }

            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        activity.hideBadges();
    }
}
