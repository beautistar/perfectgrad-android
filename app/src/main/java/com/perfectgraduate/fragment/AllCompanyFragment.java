package com.perfectgraduate.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.ImageOptions;
import com.perfectgraduate.R;
import com.perfectgraduate.activity.HomeActivity;
import com.perfectgraduate.app.PerfectGraduateApplication;
import com.perfectgraduate.database.DatabaseHelper;
import com.perfectgraduate.utility.Constants;
import com.perfectgraduate.utility.SharedPrefrenceUtil;
import com.perfectgraduate.vo.AllCompanysVo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by user on 06-Jan-17.
 */

public class AllCompanyFragment extends Fragment{

    private TextView textView_abr_prg;
    private DatabaseHelper databaseHelper;
    private List<AllCompanysVo> allCompanysVoList;
    private ListView listView_company;
    private HomeActivity activity;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_allcompany, container, false);

        init(rootView);

        return rootView;
    }

    private void init(View rootView) {

        activity = (HomeActivity) getActivity();
        textView_abr_prg = (TextView) rootView.findViewById(R.id.textView_abr_prg);

        textView_abr_prg.setText(SharedPrefrenceUtil.getPrefrence(getActivity(), Constants.abr, "") + " " + SharedPrefrenceUtil.getPrefrence(getActivity(), Constants.program_name, ""));

        allCompanysVoList = new ArrayList<>();
        databaseHelper = new DatabaseHelper(getActivity());

        listView_company = (ListView) rootView.findViewById(R.id.listView_company);

        listView_company.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Constants.company_id = allCompanysVoList.get(i).getCompany_id();
                activity.displayDetail(allCompanysVoList.get(i).getCompany_name());
            }
        });


        allCompanysVoList = databaseHelper.getAllCompanys(PerfectGraduateApplication.getInstance().getDisciplineVo().getId());

        ArrayAdapter<AllCompanysVo> arrayAdapter = new ArrayAdapter<AllCompanysVo>(getActivity(), R.layout.test_list_item_layout, allCompanysVoList) {

            @NonNull
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                ViewHolder holder = null;
                if (convertView == null) {
                    convertView = getActivity().getLayoutInflater().inflate(R.layout.test_list_item_layout, null);
                    holder = new ViewHolder();
                    holder.text = (TextView) convertView.findViewById(R.id.text);
                    holder.textView_message_text = (TextView) convertView.findViewById(R.id.textView_message_text);
                    holder.imageView_profile = (ImageView) convertView.findViewById(R.id.imageView_profile);
                    convertView.setTag(holder);
                } else {
                    holder = (ViewHolder) convertView.getTag();
                }
                AllCompanysVo allCompanysVo = getItem(position);

                holder.text.setText(allCompanysVo.getCompany_name());

                AQuery aQuery = new AQuery(convertView);

                ImageOptions options = new ImageOptions();

                options.round = 10;

                options.memCache = true;

                options.fileCache = true;

                String imageUrl = "http://www.perfectgraduate.com/perfect/assets/uploads/"+allCompanysVo.getLogo();

                if(!allCompanysVo.getLogo().equals("")){
                    aQuery.id(holder.imageView_profile).image(imageUrl, options);
                }

                if(!allCompanysVo.getApplication_due_date().equals("no")){
                    holder.textView_message_text.setText(parseDateToddMMyyyy(allCompanysVo.getApplication_due_date()));
                }

                return convertView;
            }
        };
        listView_company.setAdapter(arrayAdapter);
    }
    static class ViewHolder{
        private TextView text,textView_message_text;
        private ImageView imageView_profile;
    }

    @Override
    public void onResume() {
        super.onResume();
        activity.hideBadges();
    }

    public String parseDateToddMMyyyy(String time) {
        String inputPattern = "yyyy-MM-dd";
        String outputPattern = "dd/MM/yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern, Locale.US);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }
}
