package com.perfectgraduate.fragment;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.perfectgraduate.R;

/**
 * Created by user on 10-Dec-16.
 */

public class MyStudentSocietiesFragment extends Fragment{

    private TextView textView_studentsocieties;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_events, container, false);

        init(rootView);

        return rootView;
    }

    private void init(View rootView) {

        textView_studentsocieties = (TextView) rootView.findViewById(R.id.textView_studentsocieties);

        textView_studentsocieties.setText("Companies");

    }

}
