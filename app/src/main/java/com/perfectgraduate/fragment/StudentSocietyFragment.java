package com.perfectgraduate.fragment;

import android.support.v4.app.Fragment;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.perfectgraduate.R;
import com.perfectgraduate.activity.HomeActivity;
import com.perfectgraduate.activity.StudentSocietyActivity;
import com.perfectgraduate.api.PerfectGraduateRequest;
import com.perfectgraduate.api.WebServiceAPI;
import com.perfectgraduate.app.PerfectGraduateApplication;
import com.perfectgraduate.listener.ICallBack;
import com.perfectgraduate.utility.Constants;
import com.perfectgraduate.utility.SharedPrefrenceUtil;
import com.perfectgraduate.vo.SocietyVo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by user on 08-Dec-16.
 */

public class StudentSocietyFragment extends Fragment{

    TextView tv_submit;
    int sequanceNumber=1;
    private TextView student_university,textView_addMore;
    private LinearLayout linearLayout_addmore_society;
    private List<SocietyVo> societyVoList;
    private List<SocietyVo> society=new ArrayList<>();
    private List<String> list_society;
    private List<String> list_society_result=new ArrayList<>();
    private PerfectGraduateRequest request;
    String result="";
    private HomeActivity activity;
    int count = 0;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_student_societies, container, false);

        init(rootView);

        return rootView;
    }

    private void init(View rootView) {

        tv_submit = (TextView) rootView.findViewById(R.id.tv_submit);

        activity = (HomeActivity) getActivity();
        request = new PerfectGraduateRequest(getActivity());
        societyVoList = new ArrayList<>();
        society = new ArrayList<>();
        list_society = new ArrayList<>();


        textView_addMore = (TextView) rootView.findViewById(R.id.textView_addMore);
        linearLayout_addmore_society = (LinearLayout) rootView.findViewById(R.id.linearLayout_addmore_society);


        textView_addMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                createNewTextView("");
                createNewTextView("");
            }
        });

        tv_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addAllSociety();
            }
        });
        getSocietyDetails();
    }

    private void getAllStudentSocieties() {

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("uid", SharedPrefrenceUtil.getPrefrence(getActivity(), Constants.uni_id, ""));


        request.getResponse(getActivity(), map, WebServiceAPI.API_getAllStudentSocieties, true, new ICallBack() {
            @Override
            public void onCallBackResult(String response) {

                try {

                    JSONObject jsonObject = new JSONObject(response);

                    String status = jsonObject.getString(WebServiceAPI.status);

                    if(Boolean.parseBoolean(status)){

                        JSONArray jsonArray = jsonObject.getJSONArray(WebServiceAPI.data);

                        if(jsonArray != null && jsonArray.length() > 0){

                            for (int i = 0; i < jsonArray.length(); i++) {

                                JSONObject object = jsonArray.getJSONObject(i);

                                SocietyVo societyVo = new SocietyVo();

                                societyVo.setId(object.getString("id"));
                                societyVo.setSociety_id(object.getString("society_id"));
                                societyVo.setParent_id(object.getString("parent_id"));
                                societyVo.setStaff_id(object.getString("staff_id"));
                                societyVo.setCreator_name(object.getString("creator_name"));
                                societyVo.setWebsite(object.getString("website"));
                                societyVo.setSociety_email(object.getString("society_email"));
                                societyVo.setPassword(object.getString("password"));
                                societyVo.setFirst_login_status(object.getString("first_login_status"));
                                societyVo.setChange_pass(object.getString("change_pass"));
                                societyVo.setPass(object.getString("pass"));
                                societyVo.setUni_id(object.getString("uni_id"));
                                societyVo.setCountry_id(object.getString("country_id"));
                                societyVo.setState_id(object.getString("state_id"));
                                societyVo.setSocietylogo(object.getString("societylogo"));
                                societyVo.setCreator_name(object.getString("creator_mobile"));
                                societyVo.setSociety_name(object.getString("society_name"));
                                societyVo.setSociety_president(object.getString("society_president"));
                                societyVo.setPresident_email(object.getString("president_email"));
                                societyVo.setPresident_mobile(object.getString("president_mobile"));
                                societyVo.setTreasurer_name(object.getString("Treasurer_name"));
                                societyVo.setTreasurer_email(object.getString("Treasurer_email"));
                                societyVo.setTreasure_mobile(object.getString("Treasure_mobile"));
                                societyVo.setSecetery_name(object.getString("Secetery_name"));
                                societyVo.setSecetery_email(object.getString("Secetery_email"));
                                societyVo.setSecetery_mobile(object.getString("Secetery_mobile"));
                                societyVo.setFacebookemail(object.getString("facebookemail"));
                                societyVo.setUser_type(object.getString("user_type"));
                                societyVo.setStatus(object.getString("status"));
                                societyVo.setLocks(object.getString("locks"));
                                societyVo.setDate(object.getString("date"));
                                societyVo.setKey(object.getString("key"));

                                societyVoList.add(societyVo);
                                list_society.add(object.getString("society_name"));
                                list_society_result.add("");
                            }
                        }

                    }else{

                    }
                    for (int i = 0; i < society.size(); i++) {
                        createNewTextView(society.get(i).getSociety_name());
                    }
                }catch(JSONException k){
                    k.printStackTrace();
                }

            }
        });

    }


    private void getSocietyDetails() {

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("uid", SharedPrefrenceUtil.getPrefrence(getActivity(), Constants.USER_ID, ""));

        request.getResponse(getActivity(), map, WebServiceAPI.API_societyDetail, true, new ICallBack() {
            @Override
            public void onCallBackResult(String response) {

                try {

                    JSONObject jsonObject = new JSONObject(response);

                    String status = jsonObject.getString(WebServiceAPI.status);

                    String Society = jsonObject.getString("society");

                    if (Boolean.parseBoolean(status)) {

                        JSONArray jsonArray = jsonObject.getJSONArray(WebServiceAPI.data);

                        if (jsonArray != null && jsonArray.length() > 0) {

                            for (int i = 0; i < jsonArray.length(); i++) {

                                JSONObject object = jsonArray.getJSONObject(i);

                                SocietyVo societyVo = new SocietyVo();

                                societyVo.setId(object.getString("id"));
                                societyVo.setSociety_id(object.getString("society_id"));
                                societyVo.setParent_id(object.getString("parent_id"));
                                societyVo.setStaff_id(object.getString("staff_id"));
                                societyVo.setCreator_name(object.getString("creator_name"));
                                societyVo.setWebsite(object.getString("website"));
                                societyVo.setSociety_email(object.getString("society_email"));
                                societyVo.setPassword(object.getString("password"));
                                societyVo.setFirst_login_status(object.getString("first_login_status"));
                                societyVo.setChange_pass(object.getString("change_pass"));
                                societyVo.setPass(object.getString("pass"));
                                societyVo.setUni_id(object.getString("uni_id"));
                                societyVo.setCountry_id(object.getString("country_id"));
                                societyVo.setState_id(object.getString("state_id"));
                                societyVo.setSocietylogo(object.getString("societylogo"));
                                societyVo.setCreator_name(object.getString("creator_mobile"));
                                societyVo.setSociety_name(object.getString("society_name"));
                                societyVo.setSociety_president(object.getString("society_president"));
                                societyVo.setPresident_email(object.getString("president_email"));
                                societyVo.setPresident_mobile(object.getString("president_mobile"));
                                societyVo.setTreasurer_name(object.getString("Treasurer_name"));
                                societyVo.setTreasurer_email(object.getString("Treasurer_email"));
                                societyVo.setTreasure_mobile(object.getString("Treasure_mobile"));
                                societyVo.setSecetery_name(object.getString("Secetery_name"));
                                societyVo.setSecetery_email(object.getString("Secetery_email"));
                                societyVo.setSecetery_mobile(object.getString("Secetery_mobile"));
                                societyVo.setFacebookemail(object.getString("facebookemail"));
                                societyVo.setUser_type(object.getString("user_type"));
                                societyVo.setStatus(object.getString("status"));
                                societyVo.setLocks(object.getString("locks"));
                                societyVo.setDate(object.getString("date"));
                                societyVo.setKey(object.getString("key"));
                                societyVo.setCreated_by(object.getString("created_by"));
                                societyVo.setModified_date(object.getString("modified_date"));
                                societyVo.setSociety_code(object.getString("society_code"));
                                societyVo.setLogo(object.getString("logo"));

                                society.add(societyVo);
                            }
                        }

                    } else {

                    }
                    getAllStudentSocieties();
                } catch (JSONException k) {
                    k.printStackTrace();
                }



            }
        });

    }

    private void createNewTextView(String name) {

        View viewToLoad = LayoutInflater.from(
                getActivity().getApplicationContext()).inflate(
                R.layout.activity_student_society_include, null);

        LinearLayout society_include_lin_addmore_society= (LinearLayout) viewToLoad.findViewById(R.id.society_include_lin_addmore_society);
        final TextView society_include_txt_addmore_society= (TextView) viewToLoad.findViewById(R.id.society_include_txt_addmore_society);
        TextView society_include_txt_addmore_society_name= (TextView) viewToLoad.findViewById(R.id.society_include_txt_addmore_society_name);

        society_include_txt_addmore_society_name.setText(sequanceNumber+") Society");
        society_include_txt_addmore_society.setTag(sequanceNumber);

        society_include_txt_addmore_society.setText(name);


        for (int i = 0; i < societyVoList.size(); i++) {
            if (societyVoList.get(i).getSociety_name().equals(name)) {
                if(result.equalsIgnoreCase(""))
                    result=societyVoList.get(i).getId()+"";
                else
                    result=result+","+societyVoList.get(i).getId()+"";
            }
        }



        society_include_lin_addmore_society.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displaySociety(society_include_txt_addmore_society);
            }
        });


        linearLayout_addmore_society.addView(viewToLoad);
        if(list_society.size()<sequanceNumber)
        {
            list_society_result.add("");
        }
        if(!name.equalsIgnoreCase(""))
        list_society_result.set(sequanceNumber-1,name+"");
        sequanceNumber++;
    }

    private void displaySociety(final TextView society_include_txt_addmore_society) {

        if(list_society.size() > 0) {
            final CharSequence[] items;
            int selectedItem = 0;
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

            builder.setTitle(getString(R.string.select_society));

            items = list_society.toArray(new CharSequence[list_society.size()]);

            /*if(!society_include_txt_addmore_society.getText().toString().equals("")){
                for (int i = 0; i < list_society.size(); i++){
                    if (list_society.get(i).equals(society_include_txt_addmore_society.getText().toString())){
                        selectedItem = i;
                    }
                }
            }else{
                selectedItem = -1;
            }*/

            builder.setItems(items, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item) {
                    // Do something with the selection
                    count = item;
                    int pos= (int) society_include_txt_addmore_society.getTag();
                    if(!list_society_result.contains(items[item]+"")) {
                        society_include_txt_addmore_society.setText(items[item]);
                        list_society_result.set(pos-1,items[item]+"");

                        for (int i = 0; i < societyVoList.size(); i++) {
                            if (societyVoList.get(i).getSociety_name().equals(items[item])) {
                                if(result.equalsIgnoreCase("")) {
                                    result = societyVoList.get(i).getId() + "";
                                }else {
                                    if(!result.contains(societyVoList.get(i).getId()))
                                        result = result + "," + societyVoList.get(i).getId() + "";
                                }
                            }
                        }

                    }
                    else
                    {

                        Log.e("result","Please select another Society");
                    }


                }
            });
            AlertDialog alert = builder.create();
            alert.show();
        }

    }

    private void addAllSociety() {

        Map<String, Object> map = new HashMap<String, Object>();


        map.put("sid",result);
        map.put("uid",SharedPrefrenceUtil.getPrefrence(getActivity(),Constants.USER_ID,""));


        request.getResponse(getActivity(), map, WebServiceAPI.API_addAllSociety, true, new ICallBack() {
            @Override
            public void onCallBackResult(String response) {

                try {

                    JSONObject jsonObject = new JSONObject(response);

                    String status = jsonObject.getString(WebServiceAPI.status);

                    if(Boolean.parseBoolean(status)){

                        JSONArray jsonArray = jsonObject.getJSONArray(WebServiceAPI.data);

                        if(jsonArray != null && jsonArray.length() > 0){

                            for (int i = 0; i < jsonArray.length(); i++){

                                JSONObject object = jsonArray.getJSONObject(i);

                                SocietyVo societyVo = new SocietyVo();

                                societyVo.setId(object.getString("id"));
                                societyVo.setSociety_id(object.getString("society_id"));
                                societyVo.setParent_id(object.getString("parent_id"));
                                societyVo.setStaff_id(object.getString("staff_id"));
                                societyVo.setCreator_name(object.getString("creator_name"));
                                societyVo.setWebsite(object.getString("website"));
                                societyVo.setSociety_email(object.getString("society_email"));
                                societyVo.setPassword(object.getString("password"));
                                societyVo.setFirst_login_status(object.getString("first_login_status"));
                                societyVo.setChange_pass(object.getString("change_pass"));
                                societyVo.setPass(object.getString("pass"));
                                societyVo.setUni_id(object.getString("uni_id"));
                                societyVo.setCountry_id(object.getString("country_id"));
                                societyVo.setState_id(object.getString("state_id"));
                                societyVo.setSocietylogo(object.getString("societylogo"));
                                societyVo.setCreator_mobile(object.getString("creator_mobile"));
                                societyVo.setSociety_name(object.getString("society_name"));
                                societyVo.setSociety_president(object.getString("society_president"));
                                societyVo.setPresident_email(object.getString("president_email"));
                                societyVo.setPresident_mobile(object.getString("president_mobile"));
                                societyVo.setTreasurer_name(object.getString("Treasurer_name"));
                                societyVo.setTreasurer_email(object.getString("Treasurer_email"));
                                societyVo.setTreasure_mobile(object.getString("Treasure_mobile"));
                                societyVo.setSecetery_name(object.getString("Secetery_name"));
                                societyVo.setSecetery_email(object.getString("Secetery_email"));
                                societyVo.setSecetery_mobile(object.getString("Secetery_mobile"));
                                societyVo.setFacebookemail(object.getString("facebookemail"));
                                societyVo.setUser_type(object.getString("user_type"));
                                societyVo.setStatus(object.getString("status"));
                                societyVo.setLocks(object.getString("locks"));
                                societyVo.setDate(object.getString("date"));
                                societyVo.setKey(object.getString("key"));
                                societyVo.setCreated_by(object.getString("created_by"));
                                societyVo.setModified_date(object.getString("modified_date"));
                                societyVo.setSociety_code(object.getString("society_code"));
                                societyVo.setLogo(object.getString("logo"));

                                societyVoList.add(societyVo);

                            }

                        }
                        activity.displayView(0);

                        PerfectGraduateApplication.getInstance().displayCustomDialog(activity,"Student Detail Updated","Your student details have been \nsuccessfully updated.");

                    }else{

                    }

                }catch(JSONException k){
                    k.printStackTrace();
                }

            }
        });

    }


    @Override
    public void onResume() {
        super.onResume();
        activity.hideBadges();
    }
}
