package com.perfectgraduate.fragment;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.google.android.gms.vision.text.Line;
import com.perfectgraduate.R;
import com.perfectgraduate.app.PerfectGraduateApplication;

/**
 * Created by user on 09-Dec-16.
 */

public class MyEventsFragment extends Fragment{

    private LinearLayout linearLayout_events,linearLayout_student_societies;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_events, container, false);

        init(rootView);

        return rootView;
    }

    private void init(View rootView) {

        linearLayout_student_societies = (LinearLayout) rootView.findViewById(R.id.linearLayout_student_societies);
        linearLayout_events = (LinearLayout) rootView.findViewById(R.id.linearLayout_events);

        linearLayout_events.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PerfectGraduateApplication.getInstance().displayToast("Still under development");
            }
        });

        linearLayout_student_societies.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PerfectGraduateApplication.getInstance().displayToast("Still under development");
            }
        });


    }

}
