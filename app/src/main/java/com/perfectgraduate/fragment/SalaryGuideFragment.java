package com.perfectgraduate.fragment;

import android.app.Dialog;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.perfectgraduate.R;
import com.perfectgraduate.activity.HomeActivity;
import com.perfectgraduate.api.PerfectGraduateRequest;
import com.perfectgraduate.api.WebServiceAPI;
import com.perfectgraduate.app.PerfectGraduateApplication;
import com.perfectgraduate.database.DatabaseHelper;
import com.perfectgraduate.listener.ICallBack;
import com.perfectgraduate.utility.Constants;
import com.perfectgraduate.utility.SharedPrefrenceUtil;
import com.perfectgraduate.vo.ItemVo;
import com.perfectgraduate.vo.SalaryVo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by user on 09-Dec-16.
 */

public class SalaryGuideFragment extends Fragment {

    private LinearLayout linearLayout_industry;
    private TextView textView_select_industry, textView_disclaimer;
    private HomeActivity activity;

    private PerfectGraduateRequest request;
    private List<SalaryVo> salaryVoList;
    private List<SalaryVo> salary;
    private TableLayout display_table;
    private DatabaseHelper databaseHelper;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_salaryguide, container, false);

        init(rootView);

        return rootView;
    }

    private void init(View rootView) {

        activity = (HomeActivity) getActivity();
        databaseHelper = new DatabaseHelper(activity);
        request = new PerfectGraduateRequest(getActivity());

        display_table = (TableLayout) rootView.findViewById(R.id.display_table);

        salaryVoList = new ArrayList<>();
        salary = new ArrayList<>();


        linearLayout_industry = (LinearLayout) rootView.findViewById(R.id.linearLayout_industry);
        textView_select_industry = (TextView) rootView.findViewById(R.id.textView_select_industry);
        textView_disclaimer = (TextView) rootView.findViewById(R.id.textView_disclaimer);


        linearLayout_industry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Dialog dialog = new Dialog(getActivity());
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
                dialog.setContentView(R.layout.dialog_industry);
                dialog.setCancelable(false);
                Window window = dialog.getWindow();
                window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                window.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.TOP);
                TextView dialogCancel = (TextView) dialog.findViewById(R.id.textView_cancel);
                ListView listView_dialog_industry = (ListView) dialog.findViewById(R.id.listView_dialog_industry);


                if (salaryVoList.size() > 0) {

                    ArrayAdapter<SalaryVo> arrayAdapter = new ArrayAdapter<SalaryVo>(getActivity(), R.layout.row_dialog_industry, salaryVoList) {

                        @NonNull
                        @Override
                        public View getView(int position, View convertView, ViewGroup parent) {

                            ViewHolder holder = null;
                            if (convertView == null) {
                                convertView = getActivity().getLayoutInflater().inflate(R.layout.row_dialog_industry, null);
                                holder = new ViewHolder();
                                holder.textView_industry = (TextView) convertView.findViewById(R.id.textView_industry);
                                convertView.setTag(holder);
                            } else {
                                holder = (ViewHolder) convertView.getTag();
                            }
                            SalaryVo salaryVo = getItem(position);

                            holder.textView_industry.setText(salaryVo.getSector());

                            return convertView;
                        }
                    };

                    listView_dialog_industry.setAdapter(arrayAdapter);

                    listView_dialog_industry.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                            textView_select_industry.setText(salaryVoList.get(i).getSector());
                            dialog.dismiss();
                            displayTable(salaryVoList.get(i).getSector());
                        }
                    });

                }

                dialogCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
        });

    }

    private void displayTable(String occupation_id) {

        display_table.removeAllViews();
        TableRow.LayoutParams layoutParams = new TableRow.LayoutParams(350, 180);
        layoutParams.rightMargin = 2;
        layoutParams.bottomMargin = 2;

        TableRow row1 = new TableRow(getActivity());
        row1.setLayoutParams(new TableRow.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT));


        LinearLayout linearLayout = new LinearLayout(getActivity());
        TextView tv1 = new TextView(getActivity());
        tv1.setGravity(Gravity.CENTER);
        tv1.setLayoutParams(layoutParams);
        tv1.setText("Occupation");
        tv1.setTextColor(Color.WHITE);
        linearLayout.setLayoutParams(layoutParams);
        linearLayout.addView(tv1);
        linearLayout.setGravity(Gravity.CENTER);
        linearLayout.setBackgroundResource(R.drawable.top_header);
        row1.addView(linearLayout);

        linearLayout = new LinearLayout(getActivity());
        TextView tv_title1 = new TextView(getActivity());
        tv_title1.setGravity(Gravity.CENTER);
        tv_title1.setLayoutParams(layoutParams);
        tv_title1.setText("Title");
        tv_title1.setTextColor(Color.WHITE);
        linearLayout.setLayoutParams(layoutParams);
        linearLayout.addView(tv_title1);
        linearLayout.setGravity(Gravity.CENTER);
        linearLayout.setBackgroundResource(R.drawable.top_header);
        row1.addView(linearLayout);

        linearLayout = new LinearLayout(getActivity());
        TextView tv_exp01 = new TextView(getActivity());
        tv_exp01.setGravity(Gravity.CENTER);
        tv_exp01.setLayoutParams(layoutParams);
        tv_exp01.setText("0 to 3 years Experience");
        tv_exp01.setTextColor(Color.WHITE);
        linearLayout.setLayoutParams(layoutParams);
        linearLayout.addView(tv_exp01);
        linearLayout.setGravity(Gravity.CENTER);
        linearLayout.setBackgroundResource(R.drawable.top_header);
        row1.addView(linearLayout);

        linearLayout = new LinearLayout(getActivity());
        TextView tv_exp31 = new TextView(getActivity());
        tv_exp31.setGravity(Gravity.CENTER);
        tv_exp31.setText("3 to 5 years Experience");
        tv_exp31.setTextColor(Color.WHITE);
        linearLayout.setLayoutParams(layoutParams);
        linearLayout.addView(tv_exp31);
        linearLayout.setGravity(Gravity.CENTER);
        linearLayout.setBackgroundResource(R.drawable.top_header);
        row1.addView(linearLayout);


        linearLayout = new LinearLayout(getActivity());
        TextView tv_exp51 = new TextView(getActivity());
        tv_exp51.setGravity(Gravity.CENTER);
        tv_exp51.setText("5 to 7 years Experience");
        tv_exp51.setTextColor(Color.WHITE);
        linearLayout.setLayoutParams(layoutParams);
        linearLayout.addView(tv_exp51);
        linearLayout.setGravity(Gravity.CENTER);
        linearLayout.setBackgroundResource(R.drawable.top_header);
        row1.addView(linearLayout);

        display_table.addView(row1, 0);
        int salaryNumber=0;
        for (int i = 0; i < salaryVoList.size(); i++) {
            if (salaryVoList.get(i).getSector().equalsIgnoreCase(occupation_id)) {
                salaryNumber=i;
                break;
            }
        }
        for (int i = 0; i < salaryVoList.get(salaryNumber).arrTempSalaryVo.size(); i++) {

SalaryVo salaryVo =  salaryVoList.get(salaryNumber).arrTempSalaryVo.get(i);

                TableRow row = new TableRow(getActivity());
                row.setLayoutParams(new TableRow.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT));


                TextView tv = new TextView(getActivity());
                tv.setGravity(Gravity.CENTER);
                tv.setLayoutParams(layoutParams);
                tv.setText(salaryVo.getOccupation());
                if (i % 2 == 0)
                    tv.setBackgroundResource(R.color.row_first);
                else
                    tv.setBackgroundResource(R.color.row_second);
                tv.setTextColor(Color.BLACK);

                row.addView(tv);

                TextView tv_title = new TextView(getActivity());
                tv_title.setGravity(Gravity.CENTER);
                tv_title.setLayoutParams(layoutParams);
                tv_title.setText(salaryVo.getTitle());
                if (i % 2 == 0)
                    tv_title.setBackgroundResource(R.color.row_first);
                else
                    tv_title.setBackgroundResource(R.color.row_second);
                tv_title.setTextColor(Color.BLACK);

                row.addView(tv_title);

                TextView tv_exp0 = new TextView(getActivity());
                tv_exp0.setGravity(Gravity.CENTER);
                tv_exp0.setLayoutParams(layoutParams);
                tv_exp0.setText(salaryVo.getSalary0to3());
                if (i % 2 == 0)
                    tv_exp0.setBackgroundResource(R.color.row_first);
                else
                    tv_exp0.setBackgroundResource(R.color.row_second);
                tv_exp0.setTextColor(Color.BLACK);

                row.addView(tv_exp0);

                TextView tv_exp3 = new TextView(getActivity());
                tv_exp3.setGravity(Gravity.CENTER);
                tv_exp3.setLayoutParams(layoutParams);
                tv_exp3.setText(salaryVo.getSalary3to5());
                if (i % 2 == 0)
                    tv_exp3.setBackgroundResource(R.color.row_first);
                else
                    tv_exp3.setBackgroundResource(R.color.row_second);
                tv_exp3.setTextColor(Color.BLACK);

                row.addView(tv_exp3);

                TextView tv_exp5 = new TextView(getActivity());
                tv_exp5.setGravity(Gravity.CENTER);
                tv_exp5.setLayoutParams(layoutParams);
                tv_exp5.setText(salaryVo.getSalary5to7());
                if (i % 2 == 0)
                    tv_exp5.setBackgroundResource(R.color.row_first);
                else
                    tv_exp5.setBackgroundResource(R.color.row_second);
                tv_exp5.setTextColor(Color.BLACK);


                row.addView(tv_exp5);

                display_table.addView(row, i + 1);
            }

    }

    private void getSalaries() {

        Map<String, Object> map = new HashMap<>();

        map.put("uid", SharedPrefrenceUtil.getPrefrence(getActivity(), Constants.USER_ID, ""));

        request.getResponse(getActivity(), map, WebServiceAPI.API_getAllSalaries, true, new ICallBack() {
            @Override
            public void onCallBackResult(String response) {

                try {

                    JSONObject jsonObject = new JSONObject(response);

                    if (Boolean.parseBoolean(jsonObject.getString(WebServiceAPI.status))) {

                        JSONObject jsonObjectData = jsonObject.getJSONObject(WebServiceAPI.data);

                        JSONArray jsonArray = jsonObjectData.getJSONArray("salary");

                        if (jsonArray != null && jsonArray.length() > 0) {

                            databaseHelper.insertSalary(activity, jsonArray);
                        }
                        salary = databaseHelper.getSalary();
                        ArrayList<String> arrTemp = new ArrayList<String>();
                        for (int i = 0; i < salary.size(); i++) {
                            boolean isChecked = false;
                            for (int j = 0; j < arrTemp.size(); j++) {
                                if (salary.get(i).getSector().equalsIgnoreCase(arrTemp.get(j))) {
                                    isChecked = true;
                                    SalaryVo salaryVo = salary.get(i);
                                    if (salaryVo != null) {
                                        SalaryVo salaryVo1 = salaryVoList.get(j);
                                        salaryVo1.arrTempSalaryVo.add(salaryVo);
                                        salaryVoList.set(j, salaryVo1);
                                    }
                                    break;
                                }

                            }
                            if (!isChecked) {
                                SalaryVo salaryVo = salary.get(i);
                                arrTemp.add(salary.get(i).getSector());
                                if (salaryVo != null) {
                                    salaryVo.arrTempSalaryVo.add(salaryVo);
                                    salaryVoList.add(salaryVo);
                                }
                            }
                        }

                    PerfectGraduateApplication.getInstance().displayCustomDialog(getActivity(), "Salary Guide", "To help you learn more about graduate and future salaries, you can search for occupations using the Salary Guide page." +
                            "\n\nNote: The salary indicators are based on averaged salaries recorded within each country and are to be used as a guide only.");


                }else{

                }

            }

            catch(
            JSONException e
            )

            {
                e.printStackTrace();
            }

        }
    }

    );

}

    public void isDisclaimerVisible() {

        if (textView_disclaimer.getVisibility() != View.VISIBLE) {
            textView_disclaimer.setVisibility(View.VISIBLE);

            new CountDownTimer(7000, 1000) {

                public void onTick(long millisUntilFinished) {
                    Log.e("seconds remaining ", " : " + millisUntilFinished / 1000);
                }

                public void onFinish() {
                    textView_disclaimer.setVisibility(View.GONE);
                    activity.isInfoVisible();
                }

            }.start();

        }

    }

static class ViewHolder {
    private TextView textView_industry;
}

    @Override
    public void onResume() {
        super.onResume();
        activity.hideBadges();
        if (SharedPrefrenceUtil.getPrefrence(activity, Constants.isSalary, "").equals("Database_salary")) {
            salary = databaseHelper.getSalary();
            salaryVoList.clear();
            ArrayList<String> arrTemp = new ArrayList<String>();
            for (int i = 0; i < salary.size(); i++) {
                boolean isChecked = false;
                for (int j = 0; j < arrTemp.size(); j++) {
                    if (salary.get(i).getSector().equalsIgnoreCase(arrTemp.get(j))) {
                        isChecked = true;
                        SalaryVo salaryVo = salary.get(i);
                        if (salaryVo != null) {
                            SalaryVo salaryVo1 = salaryVoList.get(j);
                            salaryVo1.arrTempSalaryVo.add(salaryVo);
                            salaryVoList.set(j, salaryVo1);
                            break;
                        }

                    }
                }
                if (!isChecked) {
                    SalaryVo salaryVo =salary.get(i);
                    arrTemp.add(salary.get(i).getSector());
                    if (salaryVo != null) {
                        salaryVo.arrTempSalaryVo.add(salaryVo);
                        salaryVoList.add(salaryVo);
                    }
                }
            }

        } else {
            getSalaries();
        }
    }
}
