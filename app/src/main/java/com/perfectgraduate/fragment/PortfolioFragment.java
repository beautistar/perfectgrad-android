package com.perfectgraduate.fragment;

import android.app.ProgressDialog;
import android.support.v4.app.Fragment;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.perfectgraduate.R;
import com.perfectgraduate.activity.HomeActivity;
import com.perfectgraduate.adapter.SwipeRecyclerViewAdapter;
import com.perfectgraduate.api.PerfectGraduateRequest;
import com.perfectgraduate.api.WebServiceAPI;
import com.perfectgraduate.app.PerfectGraduateApplication;
import com.perfectgraduate.database.DatabaseHelper;
import com.perfectgraduate.listener.ICallBack;
import com.perfectgraduate.utility.Constants;
import com.perfectgraduate.utility.SharedPrefrenceUtil;
import com.perfectgraduate.vo.AllAwardsVo;
import com.perfectgraduate.vo.ApplicationDueDateAllVo;
import com.perfectgraduate.vo.DisciplinesVo;
import com.perfectgraduate.vo.ItemVo;
import com.perfectgraduate.vo.LinksVo;
import com.perfectgraduate.vo.LocationVo;
import com.perfectgraduate.vo.QuesAnsVo;
import com.perfectgraduate.vo.SocietyVo;
import com.perfectgraduate.vo.Sub_disciplineVo;
import com.perfectgraduate.vo.UserCompanyVo;
import com.perfectgraduate.vo.UserDisciplineVo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by user on 07-Dec-16.
 */

public class PortfolioFragment extends Fragment {

    private ListView listView_portfolio;
    private List<ItemVo> itemVoList;
    private PerfectGraduateRequest request;
    private PerfectGraduateApplication application;
    public static SwipeRecyclerViewAdapter swipeRecyclerViewAdapter;
    private List<UserDisciplineVo> userDisciplineVoList;
    private List<UserCompanyVo> userCompanyVoList;
    private List<LocationVo> locationVoList;
    private List<AllAwardsVo> allAwardsVoList;
    private List<ApplicationDueDateAllVo> applicationDueDateAllVoList;
    private List<QuesAnsVo> quesAnsVoList;
    private List<DisciplinesVo> disciplinesVoList;
    private List<Sub_disciplineVo> sub_disciplineVoList;
    private List<LinksVo> linksVoList;
    private List<SocietyVo> societyVoList;
    private RecyclerView recyclerView_Portfolio;

    private DatabaseHelper databaseHelper;

   /* private LinearLayout linearLayout_due_date;
    private TextView textView_company, textView_abr_prg;*/

    public static HomeActivity activity;

    private StringBuilder result;
    public static HomeFragment homeFragment;

    private ProgressDialog progressDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_portfolio, container, false);

        init(rootView);

        return rootView;
    }

    private void init(View rootView) {

        activity = (HomeActivity) getActivity();
        homeFragment = new HomeFragment();

        progressDialog = new ProgressDialog(activity);

        recyclerView_Portfolio = (RecyclerView) rootView.findViewById(R.id.recyclerView_Portfolio);

        societyVoList = new ArrayList<>();
        userCompanyVoList = new ArrayList<>();
        userDisciplineVoList = new ArrayList<>();

       /* textView_company = (TextView) rootView.findViewById(R.id.textView_company);
        textView_abr_prg = (TextView) rootView.findViewById(R.id.textView_abr_prg);
        linearLayout_due_date = (LinearLayout) rootView.findViewById(R.id.linearLayout_due_date);*/

        application = (PerfectGraduateApplication) getActivity().getApplicationContext();

        // Layout Managers:
        recyclerView_Portfolio.setLayoutManager(new LinearLayoutManager(getActivity()));

        result = new StringBuilder();

        databaseHelper = new DatabaseHelper(getActivity());

        try {
            if (!databaseHelper.checkDataBase()) {
                databaseHelper.createDataBase(0, "");
                databaseHelper.close();
            }
        } catch (IOException e1) {
            e1.printStackTrace();
        }

        // Item Decorator:
        //recyclerView_Portfolio.addItemDecoration(new DividerItemDecoration(getResources().getDrawable(R.drawable.divider)));
        // mRecyclerView.setItemAnimator(new FadeInLeftAnimator());

        request = new PerfectGraduateRequest(getActivity());


        /*linearLayout_due_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Collections.sort(userCompanyVoList, UserCompanyVo.short_due_date);
                swipeRecyclerViewAdapter.notifyDataSetChanged();
            }
        });*/

        /*textView_company.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Collections.sort(userCompanyVoList, UserCompanyVo.short_company_name);
                swipeRecyclerViewAdapter.notifyDataSetChanged();
            }
        });*/
    }

    @Override
    public void onResume() {
        super.onResume();
        getCompanyData();
    }

    public void getCompanyData() {


        if (SharedPrefrenceUtil.getPrefrence(getActivity(), Constants.isFirstTime, "").equalsIgnoreCase("Database_portfolio")) {

            if(userCompanyVoList != null && userCompanyVoList.size() > 0){
                userCompanyVoList.clear();
            }

            userCompanyVoList = databaseHelper.getUserCompany();

            if (SharedPrefrenceUtil.getPrefrence(getActivity(), Constants.abr, "").equalsIgnoreCase("") && SharedPrefrenceUtil.getPrefrence(getActivity(), Constants.program_name, "").equalsIgnoreCase("")) {
                PerfectGraduateApplication.getInstance().setShowDialog(getActivity(), "", "Please update your profile again.");
            } else {
                homeFragment.textView_abr_prg.setText(SharedPrefrenceUtil.getPrefrence(getActivity(), Constants.abr, "") + " " + SharedPrefrenceUtil.getPrefrence(getActivity(), Constants.program_name, ""));
            }
            int portfolio_count = 0;
            Constants.portfolio_count = 0;
            for (int i = 0; i < userCompanyVoList.size(); i++){
                if(portfolio_count > 0) {
                    portfolio_count = Integer.parseInt(userCompanyVoList.get(i).getUnread_message()) + portfolio_count;
                }else{
                    portfolio_count = Integer.parseInt(userCompanyVoList.get(i).getUnread_message());
                }
            }
            Constants.portfolio_count = portfolio_count;
            activity.changeSocietyImage(true);

            activity.setScore(SharedPrefrenceUtil.getPrefrence(activity, Constants.score, ""), SharedPrefrenceUtil.getPrefrence(activity, Constants.subHeader, ""));

            if (userCompanyVoList != null && userCompanyVoList.size() > 0) {
                swipeRecyclerViewAdapter = new SwipeRecyclerViewAdapter(getActivity(), userCompanyVoList);
                recyclerView_Portfolio.setAdapter(swipeRecyclerViewAdapter);
            }
        } else {
            if (!application.isConnectInternet()) {
                application.setShowDialog(getActivity(), "", getString(R.string.no_internet_connection));
            } else {
                user_company();
            }
        }


    }

    private void user_company() {

        Map<String, Object> map = new HashMap<String, Object>();

        map.put("uid", SharedPrefrenceUtil.getPrefrence(getActivity(), Constants.USER_ID, ""));

        progressDialog = application.getProgressDialog(activity);
        request.getResponse(getActivity(), map, WebServiceAPI.API_user_company, false, new ICallBack() {
            @Override
            public void onCallBackResult(String response) {

                try {

                    JSONObject jsonObject = new JSONObject(response);

                    String status = jsonObject.getString(WebServiceAPI.status);
                    SharedPrefrenceUtil.setPrefrence(getActivity(), Constants.society, jsonObject.getString("society"));
                    SharedPrefrenceUtil.setPrefrence(getActivity(), Constants.admin, jsonObject.getString("admin"));

                    if (Boolean.parseBoolean(status)) {

                        JSONArray jsonArrayUserDiscipline = jsonObject.getJSONArray("user_discipline");

                        if (jsonArrayUserDiscipline != null && jsonArrayUserDiscipline.length() > 0) {

                            for (int j = 0; j < jsonArrayUserDiscipline.length(); j++) {

                                JSONObject objectUserDiscipline = jsonArrayUserDiscipline.getJSONObject(j);

                                UserDisciplineVo userDisciplineVo = new UserDisciplineVo();

                                userDisciplineVo.setId(objectUserDiscipline.getString("id"));
                                userDisciplineVo.setStudent_id(objectUserDiscipline.getString("student_id"));
                                userDisciplineVo.setDiscipline_id(objectUserDiscipline.getString("discipline_id"));
                                userDisciplineVo.setSub_discipline_id(objectUserDiscipline.getString("sub_discipline_id"));
                                userDisciplineVo.setOrder(objectUserDiscipline.getString("order"));
                                userDisciplineVo.setDiscipline(objectUserDiscipline.getString("discipline"));
                                userDisciplineVo.setSub_discipline(objectUserDiscipline.getString("sub_discipline"));


                                result.append(objectUserDiscipline.getString("discipline"));
                                result.append(",");


                                userDisciplineVoList.add(userDisciplineVo);
                            }

                        }
                        result.setLength(result.length() - 1);

                        SharedPrefrenceUtil.setPrefrence(getActivity(), Constants.subHeader, result.toString());

                        JSONArray jsonArray = jsonObject.getJSONArray(WebServiceAPI.data);

                        if (jsonArray != null && jsonArray.length() > 0) {

                            databaseHelper.insertUserCompany(getActivity(), jsonArray);
                        }

                        JSONObject jsonObjectUserProfile = jsonObject.getJSONObject("user_profile");

                        SharedPrefrenceUtil.setPrefrence(getActivity(), Constants.id, jsonObjectUserProfile.getString("id"));
                        SharedPrefrenceUtil.setPrefrence(getActivity(), Constants.fb_id, jsonObjectUserProfile.getString("fb_id"));
                        SharedPrefrenceUtil.setPrefrence(getActivity(), Constants.email, jsonObjectUserProfile.getString("email"));
                        SharedPrefrenceUtil.setPrefrence(getActivity(), Constants.password, jsonObjectUserProfile.getString("password"));
                        SharedPrefrenceUtil.setPrefrence(getActivity(), Constants.image, jsonObjectUserProfile.getString("image"));
                        SharedPrefrenceUtil.setPrefrence(getActivity(), Constants.linkedinUrl, jsonObjectUserProfile.getString("linkedinUrl"));
                        SharedPrefrenceUtil.setPrefrence(getActivity(), Constants.name, jsonObjectUserProfile.getString("name"));
                        SharedPrefrenceUtil.setPrefrence(getActivity(), Constants.gender, jsonObjectUserProfile.getString("gender"));
                        SharedPrefrenceUtil.setPrefrence(getActivity(), Constants.mobile, jsonObjectUserProfile.getString("mobile"));
                        SharedPrefrenceUtil.setPrefrence(getActivity(), Constants.country_id, jsonObjectUserProfile.getString("country_id"));
                        SharedPrefrenceUtil.setPrefrence(getActivity(), Constants.state_id, jsonObjectUserProfile.getString("state_id"));
                        SharedPrefrenceUtil.setPrefrence(getActivity(), Constants.uni_id, jsonObjectUserProfile.getString("uni_id"));
                        SharedPrefrenceUtil.setPrefrence(getActivity(), Constants.graduateProg_id, jsonObjectUserProfile.getString("graduateProg_id"));
                        SharedPrefrenceUtil.setPrefrence(getActivity(), Constants.disciplline_id, jsonObjectUserProfile.getString("disciplline_id"));
                        SharedPrefrenceUtil.setPrefrence(getActivity(), Constants.subDiscipline_id, jsonObjectUserProfile.getString("subDiscipline_id"));
                        SharedPrefrenceUtil.setPrefrence(getActivity(), Constants.status, jsonObjectUserProfile.getString("status"));
                        SharedPrefrenceUtil.setPrefrence(getActivity(), Constants.device_id, jsonObjectUserProfile.getString("device_id"));
                        SharedPrefrenceUtil.setPrefrence(getActivity(), Constants.device_type, jsonObjectUserProfile.getString("device_type"));
                        SharedPrefrenceUtil.setPrefrence(getActivity(), Constants.score, jsonObjectUserProfile.getString("score"));
                        SharedPrefrenceUtil.setPrefrence(getActivity(), Constants.date, jsonObjectUserProfile.getString("date"));
                        SharedPrefrenceUtil.setPrefrence(getActivity(), Constants.modified_date, jsonObjectUserProfile.getString("modified_date"));
                        SharedPrefrenceUtil.setPrefrence(getActivity(), Constants.unique_id, jsonObjectUserProfile.getString("unique_id"));
                        SharedPrefrenceUtil.setPrefrence(getActivity(), Constants.country, jsonObjectUserProfile.getString("country"));
                        SharedPrefrenceUtil.setPrefrence(getActivity(), Constants.state, jsonObjectUserProfile.getString("state"));
                        SharedPrefrenceUtil.setPrefrence(getActivity(), Constants.abr, jsonObjectUserProfile.getString("abr"));
                        SharedPrefrenceUtil.setPrefrence(getActivity(), Constants.university, jsonObjectUserProfile.getString("university"));
                        SharedPrefrenceUtil.setPrefrence(getActivity(), Constants.program_name, jsonObjectUserProfile.optString("program_name"));

                        homeFragment.textView_abr_prg.setText(jsonObjectUserProfile.getString("abr") + " " + jsonObjectUserProfile.optString("program_name"));

                        SharedPrefrenceUtil.setPrefrence(getActivity(), Constants.badge_count, jsonObject.optString("badge_count"));

                        activity.setScore(jsonObjectUserProfile.getString("score"), result.toString());
                        userCompanyVoList = databaseHelper.getUserCompany();

                        if (userCompanyVoList != null && userCompanyVoList.size() > 0) {
                            swipeRecyclerViewAdapter = new SwipeRecyclerViewAdapter(getActivity(), userCompanyVoList);
                            recyclerView_Portfolio.setAdapter(swipeRecyclerViewAdapter);

                            int portfolio_count = 0;
                            Constants.portfolio_count = 0;
                            for (int i = 0; i < userCompanyVoList.size(); i++){
                                if(portfolio_count > 0) {
                                    portfolio_count = Integer.parseInt(userCompanyVoList.get(i).getUnread_message()) + portfolio_count;
                                }else{
                                    portfolio_count = Integer.parseInt(userCompanyVoList.get(i).getUnread_message());
                                }
                            }
                            Constants.portfolio_count = portfolio_count;
                            activity.changeSocietyImage(true);
                            if(progressDialog != null) {
                                progressDialog.dismiss();

                                PerfectGraduateApplication.getInstance().displayCustomDialog(getActivity(), "Welcome to Perfect \nGraduate!", "To begin, we've included a few suggested companies for your portfolio." +
                                        "\n\nTo find more companies, use the 'Disciplines' and 'Search' buttons located at the bottom of the page." +
                                        "\n\nAdd companies to your portfolio to easily follow their application due dates, messages and events.");

                            }
                        }

                    } else {

                    }
                    // getProfile();
                    //getAllStates();
                   // getSocietyDetails();
                } catch (JSONException k) {
                    k.printStackTrace();
                }

            }
        });

    }

    private void getProfile() {

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("uid", SharedPrefrenceUtil.getPrefrence(getActivity(), Constants.USER_ID, ""));

        request.getResponse(getActivity(), map, WebServiceAPI.API_getProfile, true, new ICallBack() {
            @Override
            public void onCallBackResult(String response) {

                try {

                    JSONObject jsonObject = new JSONObject(response);

                    String status = jsonObject.getString(WebServiceAPI.status);

                    if (Boolean.parseBoolean(status)) {

                        JSONObject object = jsonObject.getJSONObject(WebServiceAPI.data);

                        SharedPrefrenceUtil.setPrefrence(getActivity(), Constants.id, object.getString("id"));
                        SharedPrefrenceUtil.setPrefrence(getActivity(), Constants.fb_id, object.getString("fb_id"));
                        SharedPrefrenceUtil.setPrefrence(getActivity(), Constants.email, object.getString("email"));
                        SharedPrefrenceUtil.setPrefrence(getActivity(), Constants.password, object.getString("password"));
                        SharedPrefrenceUtil.setPrefrence(getActivity(), Constants.image, object.getString("image"));
                        SharedPrefrenceUtil.setPrefrence(getActivity(), Constants.linkedinUrl, object.getString("linkedinUrl"));
                        SharedPrefrenceUtil.setPrefrence(getActivity(), Constants.name, object.getString("name"));
                        SharedPrefrenceUtil.setPrefrence(getActivity(), Constants.gender, object.getString("gender"));
                        SharedPrefrenceUtil.setPrefrence(getActivity(), Constants.mobile, object.getString("mobile"));
                        SharedPrefrenceUtil.setPrefrence(getActivity(), Constants.country_id, object.getString("country_id"));
                        SharedPrefrenceUtil.setPrefrence(getActivity(), Constants.state_id, object.getString("state_id"));
                        SharedPrefrenceUtil.setPrefrence(getActivity(), Constants.uni_id, object.getString("uni_id"));
                        SharedPrefrenceUtil.setPrefrence(getActivity(), Constants.graduateProg_id, object.getString("graduateProg_id"));
                        SharedPrefrenceUtil.setPrefrence(getActivity(), Constants.disciplline_id, object.getString("disciplline_id"));
                        SharedPrefrenceUtil.setPrefrence(getActivity(), Constants.subDiscipline_id, object.getString("subDiscipline_id"));
                        SharedPrefrenceUtil.setPrefrence(getActivity(), Constants.status, object.getString("status"));
                        SharedPrefrenceUtil.setPrefrence(getActivity(), Constants.device_id, object.getString("device_id"));
                        SharedPrefrenceUtil.setPrefrence(getActivity(), Constants.device_type, object.getString("device_type"));
                        SharedPrefrenceUtil.setPrefrence(getActivity(), Constants.score, object.getString("score"));
                        SharedPrefrenceUtil.setPrefrence(getActivity(), Constants.date, object.getString("date"));
                        SharedPrefrenceUtil.setPrefrence(getActivity(), Constants.modified_date, object.getString("modified_date"));
                        SharedPrefrenceUtil.setPrefrence(getActivity(), Constants.unique_id, object.getString("unique_id"));
                        SharedPrefrenceUtil.setPrefrence(getActivity(), Constants.country, object.getString("country"));
                        SharedPrefrenceUtil.setPrefrence(getActivity(), Constants.state, object.getString("state"));
                        SharedPrefrenceUtil.setPrefrence(getActivity(), Constants.university, object.getString("university"));
                        SharedPrefrenceUtil.setPrefrence(getActivity(), Constants.program, object.getString("program"));

                    } else {

                    }

                } catch (JSONException k) {
                    k.printStackTrace();
                }

            }
        });

    }


    private void getSocietyDetails() {

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("uid", SharedPrefrenceUtil.getPrefrence(getActivity(), Constants.USER_ID, ""));

        request.getResponse(getActivity(), map, WebServiceAPI.API_societyDetail, true, new ICallBack() {
            @Override
            public void onCallBackResult(String response) {

                try {

                    JSONObject jsonObject = new JSONObject(response);

                    String status = jsonObject.getString(WebServiceAPI.status);

                    String society = jsonObject.getString("society");

                    if (Boolean.parseBoolean(status)) {

                        JSONArray jsonArray = jsonObject.getJSONArray(WebServiceAPI.data);

                        if (jsonArray != null && jsonArray.length() > 0) {

                            for (int i = 0; i < jsonArray.length(); i++) {

                                JSONObject object = jsonArray.getJSONObject(i);

                                SocietyVo societyVo = new SocietyVo();

                                societyVo.setId(object.getString("id"));
                                societyVo.setSociety_id(object.getString("society_id"));
                                societyVo.setParent_id(object.getString("parent_id"));
                                societyVo.setStaff_id(object.getString("staff_id"));
                                societyVo.setCreator_name(object.getString("creator_name"));
                                societyVo.setWebsite(object.getString("website"));
                                societyVo.setSociety_email(object.getString("society_email"));
                                societyVo.setPassword(object.getString("password"));
                                societyVo.setFirst_login_status(object.getString("first_login_status"));
                                societyVo.setChange_pass(object.getString("change_pass"));
                                societyVo.setPass(object.getString("pass"));
                                societyVo.setUni_id(object.getString("uni_id"));
                                societyVo.setCountry_id(object.getString("country_id"));
                                societyVo.setState_id(object.getString("state_id"));
                                societyVo.setSocietylogo(object.getString("societylogo"));
                                societyVo.setCreator_name(object.getString("creator_mobile"));
                                societyVo.setSociety_name(object.getString("society_name"));
                                societyVo.setSociety_president(object.getString("society_president"));
                                societyVo.setPresident_email(object.getString("president_email"));
                                societyVo.setPresident_mobile(object.getString("president_mobile"));
                                societyVo.setTreasurer_name(object.getString("Treasurer_name"));
                                societyVo.setTreasurer_email(object.getString("Treasurer_email"));
                                societyVo.setTreasure_mobile(object.getString("Treasure_mobile"));
                                societyVo.setSecetery_name(object.getString("Secetery_name"));
                                societyVo.setSecetery_email(object.getString("Secetery_email"));
                                societyVo.setSecetery_mobile(object.getString("Secetery_mobile"));
                                societyVo.setFacebookemail(object.getString("facebookemail"));
                                societyVo.setUser_type(object.getString("user_type"));
                                societyVo.setStatus(object.getString("status"));
                                societyVo.setLocks(object.getString("locks"));
                                societyVo.setDate(object.getString("date"));
                                societyVo.setKey(object.getString("key"));
                                societyVo.setCreated_by(object.getString("created_by"));
                                societyVo.setModified_date(object.getString("modified_date"));
                                societyVo.setSociety_code(object.getString("society_code"));
                                societyVo.setLogo(object.getString("logo"));

                                societyVoList.add(societyVo);
                            }
                        }

                        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
                        SharedPreferences.Editor editor = sharedPrefs.edit();
                        Gson gson = new Gson();
                        String json = gson.toJson(societyVoList);
                        editor.putString("societyDetail", json);
                        editor.apply();



                    } else {

                    }

                } catch (JSONException k) {
                    k.printStackTrace();
                }

            }
        });

    }


    public void displayRemove() {
        if(Constants.isVisible){
            Constants.isVisible = false;
            swipeRecyclerViewAdapter.notifyDataSetChanged();
        }else {
            Constants.isVisible = true;
            swipeRecyclerViewAdapter.notifyDataSetChanged();
        }
    }

    public void shortByCompany(){
        Collections.sort(userCompanyVoList, UserCompanyVo.short_company_name);
        swipeRecyclerViewAdapter.notifyDataSetChanged();
    }

    public void shortByDueDates(){
        Collections.sort(userCompanyVoList, UserCompanyVo.short_due_date);
        swipeRecyclerViewAdapter.notifyDataSetChanged();
    }
}
