package com.perfectgraduate.fragment;

import android.support.v4.app.Fragment;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.perfectgraduate.R;
import com.perfectgraduate.activity.HomeActivity;
import com.perfectgraduate.adapter.CountryFilterAdapter;
import com.perfectgraduate.adapter.LocationFilterAdapter;
import com.perfectgraduate.api.PerfectGraduateRequest;
import com.perfectgraduate.api.WebServiceAPI;
import com.perfectgraduate.database.DatabaseHelper;
import com.perfectgraduate.listener.ICallBack;
import com.perfectgraduate.utility.Constants;
import com.perfectgraduate.utility.SharedPrefrenceUtil;
import com.perfectgraduate.utility.stickylistheaders.StickyListHeadersListView;
import com.perfectgraduate.vo.CountryVo;
import com.perfectgraduate.vo.StateVo;
import com.perfectgraduate.vo.UserCompanyVo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by user on 03-Jan-17.
 */

public class CountryFilterFragment extends Fragment{

    private PerfectGraduateRequest request;
    private DatabaseHelper databaseHelper;
    private StickyListHeadersListView stickyList;
    private CountryFilterAdapter adapter;
    private List<CountryVo> countryVoList;
    private HomeActivity activity;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_country, container, false);

        init(rootView);

        return rootView;
    }

    private void init(View rootView) {

        activity = (HomeActivity) getActivity();

        request = new PerfectGraduateRequest(getActivity());
        databaseHelper = new DatabaseHelper(getActivity());

        stickyList = (StickyListHeadersListView) rootView.findViewById(R.id.list_country);

        countryVoList = new ArrayList<>();

    }

    @Override
    public void onResume() {
        super.onResume();
        activity.hideBadges();
        if(SharedPrefrenceUtil.getPrefrence(getActivity(),Constants.isCountries,"").equals("Database_countries")){

            displayAdapter();

        }else{

            getAllCountries();

        }
    }

    private void displayAdapter() {

        countryVoList = databaseHelper.getCountryList();

        Collections.sort(countryVoList, CountryVo.short_name);

        String [] mCountries = new String[countryVoList.size()];
        String [] mIds = new String[countryVoList.size()];
        for (int i = 0; i < countryVoList.size(); i++) {
            mCountries[i] = countryVoList.get(i).getName();
            mIds[i] = countryVoList.get(i).getId();
        }

        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
        Gson gson = new Gson();
        String jsonData = sharedPrefs.getString("checkedCountry", "");
        Type type = new TypeToken<ArrayList<String>>() {
        }.getType();
        ArrayList<String> checkedCountry = new ArrayList<String>();
        checkedCountry = gson.fromJson(jsonData, type);

        if(Constants.checkedCountry != null && Constants.checkedCountry.size() > 0){
            if(Constants.checkedCountry.size() == countryVoList.size()) {
                adapter = new CountryFilterAdapter(getActivity(), countryVoList, mCountries, mIds, Constants.checkedCountry);
            }else{
                if(checkedCountry != null && checkedCountry.size() > 0){
                    adapter = new CountryFilterAdapter(getActivity(),countryVoList,mCountries,mIds,checkedCountry);
                }else{
                    adapter = new CountryFilterAdapter(getActivity(),countryVoList,mCountries,mIds,Constants.checkedCountry);
                }
            }
        }else{
            if(checkedCountry != null && checkedCountry.size() > 0){
                adapter = new CountryFilterAdapter(getActivity(),countryVoList,mCountries,mIds,checkedCountry);
            }else{
                adapter = new CountryFilterAdapter(getActivity(),countryVoList,mCountries,mIds,Constants.checkedCountry);
            }
        }

        stickyList.setDrawingListUnderStickyHeader(true);
        stickyList.setAreHeadersSticky(true);
        stickyList.setAdapter(adapter);
        stickyList.setStickyHeaderTopOffset(-20);

    }


    private void getAllCountries() {

        Map<String, Object> map = new HashMap<String, Object>();

        map.put("user_id", SharedPrefrenceUtil.getPrefrence(getActivity(), Constants.USER_ID,""));


        request.getResponse(getActivity(), map, WebServiceAPI.API_getAllsearchCountries, true, new ICallBack() {
            @Override
            public void onCallBackResult(String response) {

                try {

                    JSONObject jsonObject = new JSONObject(response);

                    String status = jsonObject.getString(WebServiceAPI.status);

                    if(Boolean.parseBoolean(status)){

                        JSONArray jsonArray = jsonObject.getJSONArray(WebServiceAPI.data);

                        if(jsonArray != null && jsonArray.length() > 0){

                            databaseHelper.insertAllCountries(getActivity(), jsonArray);

                        }

                        displayAdapter();

                    }else{

                    }

                }catch(JSONException k){
                    k.printStackTrace();
                }

            }
        });

    }

}
