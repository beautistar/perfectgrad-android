package com.perfectgraduate.fragment;

import android.support.v4.app.Fragment;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SearchView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.perfectgraduate.R;
import com.perfectgraduate.activity.HomeActivity;
import com.perfectgraduate.adapter.LocationFilterAdapter;
import com.perfectgraduate.adapter.SearchLocationFilterAdapter;
import com.perfectgraduate.api.PerfectGraduateRequest;
import com.perfectgraduate.api.WebServiceAPI;
import com.perfectgraduate.database.DatabaseHelper;
import com.perfectgraduate.listener.ICallBack;
import com.perfectgraduate.utility.Constants;
import com.perfectgraduate.utility.SharedPrefrenceUtil;
import com.perfectgraduate.vo.CountryVo;
import com.perfectgraduate.vo.StateVo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by user on 11-Jan-17.
 */

public class SearchLocationFilterFragment extends Fragment implements SearchView.OnQueryTextListener {

    private PerfectGraduateRequest request;
    private DatabaseHelper databaseHelper;
    private ListView listView_states;
    private List<StateVo> stateVoList;
    private List<CountryVo> countryVoList;
    private LinearLayout linearLayout_international,linearLayout_allLocations;
    private SearchLocationFilterAdapter adapter;
    private HomeActivity activity;
    private SearchView searchView_locationFilter;
    private CheckBox checkbox_international,checkbox_all;
    private int size = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_locationfilter, container, false);

        init(rootView);

        return rootView;
    }

    private void init(View rootView) {


        activity = (HomeActivity) getActivity();
        request = new PerfectGraduateRequest(getActivity());
        databaseHelper = new DatabaseHelper(getActivity());

        stateVoList = new ArrayList<StateVo>();
        countryVoList = new ArrayList<CountryVo>();

        Constants.checkedState = new ArrayList<String>();
        Constants.checkedCountry = new ArrayList<String>();

        listView_states = (ListView) rootView.findViewById(R.id.listView_states);
        linearLayout_international = (LinearLayout) rootView.findViewById(R.id.linearLayout_international);
        linearLayout_allLocations = (LinearLayout) rootView.findViewById(R.id.linearLayout_allLocations);
        searchView_locationFilter = (SearchView) rootView.findViewById(R.id.searchView_locationFilter);

        checkbox_all = (CheckBox) rootView.findViewById(R.id.checkbox_all);
        checkbox_international = (CheckBox) rootView.findViewById(R.id.checkbox_international);

        checkbox_all.setChecked(false);
        checkbox_international.setChecked(false);

        setupSearchView();

        linearLayout_allLocations.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(Constants.checkedState != null && Constants.checkedCountry != null) {
                    Constants.checkedCountry.clear();
                    Constants.checkedState.clear();
                }

                if(checkbox_all.isChecked()){
                    checkbox_all.setChecked(false);
                    checkbox_international.setChecked(false);

                    Constants.checkedCountry.clear();
                    Constants.checkedState.clear();
                    Constants.searchabrList.clear();
                    activity.setSubheader(Constants.searchabrList);

                }else{
                    checkbox_all.setChecked(true);
                    checkbox_international.setChecked(true);

                    countryVoList = databaseHelper.getCountryList();

                    for (int i = 0; i < stateVoList.size(); i++){
                        Constants.checkedState.add(stateVoList.get(i).getId());
                        Constants.searchabrList.add(stateVoList.get(i).getAbr());
                    }

                    for (int j = 0; j < countryVoList.size(); j++){
                        Constants.checkedCountry.add(countryVoList.get(j).getId());
                        Constants.searchabrList.add(stateVoList.get(j).getAbr());
                    }
                    activity.setSubheaderAll();
                }

                Collections.sort(stateVoList, StateVo.short_name);

                adapter = new SearchLocationFilterAdapter(getActivity(), stateVoList, Constants.checkedState,SearchLocationFilterFragment.this);
                listView_states.setTextFilterEnabled(true);
                listView_states.setAdapter(adapter);
                linearLayout_international.setVisibility(View.VISIBLE);
            }
        });
        linearLayout_international.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.SearchCountryFilter();
            }
        });
    }

    private void setupSearchView() {

        listView_states.setTextFilterEnabled(false);
        searchView_locationFilter.setIconifiedByDefault(false);
        searchView_locationFilter.setOnQueryTextListener(this);
        searchView_locationFilter.setSubmitButtonEnabled(true);
        searchView_locationFilter.setQueryHint("Search");

    }

    private void getAllStates() {

        Map<String, Object> map = new HashMap<String, Object>();

        map.put("cid", SharedPrefrenceUtil.getPrefrence(getActivity(), Constants.country_id,""));

        request.getResponse(getActivity(), map, WebServiceAPI.API_getcountrystates, true, new ICallBack() {
            @Override
            public void onCallBackResult(String response) {

                try {

                    JSONObject jsonObject = new JSONObject(response);

                    String status = jsonObject.getString(WebServiceAPI.status);

                    if(Boolean.parseBoolean(status)){

                        JSONArray jsonArray = jsonObject.getJSONArray(WebServiceAPI.data);


                        if(jsonArray != null && jsonArray.length() > 0){

                            databaseHelper.insertAllStates(getActivity(), jsonArray);

                        }

                        stateVoList = databaseHelper.getStateList();
                        displayStateList();

                    }else{

                    }

                    getAllCountries();

                }catch(JSONException k){
                    k.printStackTrace();
                }

            }
        });

    }

    private void getAllCountries() {

        Map<String, Object> map = new HashMap<String, Object>();

        map.put("user_id", SharedPrefrenceUtil.getPrefrence(getActivity(), Constants.USER_ID,""));


        request.getResponse(getActivity(), map, WebServiceAPI.API_getAllsearchCountries, true, new ICallBack() {
            @Override
            public void onCallBackResult(String response) {

                try {

                    JSONObject jsonObject = new JSONObject(response);

                    String status = jsonObject.getString(WebServiceAPI.status);

                    if(Boolean.parseBoolean(status)){

                        JSONArray jsonArray = jsonObject.getJSONArray(WebServiceAPI.data);

                        if(jsonArray != null && jsonArray.length() > 0){

                            databaseHelper.insertAllCountries(getActivity(), jsonArray);

                        }

                    }else{

                    }

                }catch(JSONException k){
                    k.printStackTrace();
                }

            }
        });

    }

    private void displayStateList() {

        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
        Gson gson = new Gson();
        String jsonData = sharedPrefs.getString("checkedState_search", "");
        String jsonDatacheckedCountry = sharedPrefs.getString("checkedCountry_search", "");
        Type type = new TypeToken<ArrayList<String>>() {
        }.getType();
        ArrayList<String> checkedState = new ArrayList<String>();
        ArrayList<String> checkedCountry = new ArrayList<String>();
        checkedState = gson.fromJson(jsonData, type);
        checkedCountry = gson.fromJson(jsonDatacheckedCountry, type);

        if(checkedCountry != null && checkedCountry.size() > 0) {
            Constants.checkedCountry.clear();
            Constants.checkedCountry.addAll(checkedCountry);
        }

        Log.e("checkedState "," -- "+checkedState);
        Log.e("stateVoList "," -- "+stateVoList);

        Collections.sort(stateVoList, StateVo.short_name);

        if(!checkbox_all.isChecked()) {

            Log.e("stateVoList 1"," -- ");

            if (checkedState != null && checkedState.size() > 0) {

                Log.e("stateVoList 2", " -- ");

                if (checkedState != null && checkedCountry != null) {

                    Log.e("stateVoList 3", " -- ");

                    if (checkedState.size() == stateVoList.size() && checkedCountry.size() == countryVoList.size()) {

                        Log.e("stateVoList 4", " -- ");
                        adapter = new SearchLocationFilterAdapter(getActivity(), stateVoList, checkedState, SearchLocationFilterFragment.this);
                        checkbox_all.setChecked(true);
                        checkbox_international.setChecked(true);
                    } else {

                        Log.e("stateVoList 5", " -- ");
                        checkbox_international.setChecked(true);
                        adapter = new SearchLocationFilterAdapter(getActivity(), stateVoList, checkedState, SearchLocationFilterFragment.this);
                    }

                } else if (checkedState != null && checkedState.size() > 0) {

                    Log.e("stateVoList 6", " -- ");

                    adapter = new SearchLocationFilterAdapter(getActivity(), stateVoList, checkedState, SearchLocationFilterFragment.this);
                }  else if (checkedCountry != null && checkedCountry.size() > 0)  {
                    Log.e("stateVoList 7", " -- ");

                    adapter = new SearchLocationFilterAdapter(getActivity(), stateVoList, Constants.checkedState, SearchLocationFilterFragment.this);
                    checkbox_international.setChecked(true);
                }
            }else {
                adapter = new SearchLocationFilterAdapter(getActivity(), stateVoList, Constants.checkedState, SearchLocationFilterFragment.this);
            }

        }else{
            Log.e("stateVoList 8"," -- ");

            if (checkedState != null && checkedState.size() > 0) {
                if ((checkedState.size() == stateVoList.size()) && (checkedCountry.size() == countryVoList.size())) {
                    Log.e("stateVoList 9"," -- ");
                    checkbox_all.setChecked(true);
                    checkbox_international.setChecked(true);
                } else {
                    Log.e("stateVoList 10"," -- ");
                    checkbox_all.setChecked(false);
                    checkbox_international.setChecked(false);
                }
                adapter = new SearchLocationFilterAdapter(getActivity(), stateVoList, checkedState,SearchLocationFilterFragment.this);
            }else {
                if (Constants.checkedState != null && Constants.checkedCountry != null) {
                    Log.e("stateVoList 11"," -- ");
                    if ((Constants.checkedState.size() == stateVoList.size()) && (Constants.checkedCountry.size() == countryVoList.size())) {
                        checkbox_all.setChecked(true);
                        checkbox_international.setChecked(true);
                    } else {
                        Log.e("stateVoList 12"," -- ");
                        checkbox_all.setChecked(false);
                        checkbox_international.setChecked(false);
                    }
                }
                adapter = new SearchLocationFilterAdapter(getActivity(), stateVoList, Constants.checkedState,SearchLocationFilterFragment.this);
            }
        }

        listView_states.setTextFilterEnabled(true);
        listView_states.setAdapter(adapter);
        linearLayout_international.setVisibility(View.VISIBLE);

        Constants.searchabrList.clear();

        if(!checkbox_all.isChecked()) {

            if (checkedCountry != null && checkedCountry.size() > 0) {
                Constants.checkedCountry.addAll(checkedCountry);

                for (int i = 0; i < countryVoList.size(); i++) {
                    if (checkedCountry.contains(countryVoList.get(i).getId())) {
                        Constants.searchabrList.add(countryVoList.get(i).getAbr());
                    }
                }
            }

            if (checkedState != null && checkedState.size() > 0) {

                for (int i = 0; i < stateVoList.size(); i++) {
                    if (checkedState.contains(stateVoList.get(i).getId())) {
                        Constants.searchabrList.add(stateVoList.get(i).getAbr());
                    }
                }
            }

            if (Constants.searchabrList.size() > 0) {

                activity.setSubheader(Constants.searchabrList);
            }

        }else if (checkedState.size() == stateVoList.size() && checkedCountry.size() == countryVoList.size()) {
            activity.setSubheaderAll();
        }else{
            activity.setSubheaderAll();
        }

    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        adapter.filter(s);
        return false;
    }



    @Override
    public void onResume() {
        super.onResume();
        activity.hideBadges();
        if(SharedPrefrenceUtil.getPrefrence(getActivity(),Constants.isStates,"").equals("Database_states")){
            stateVoList = databaseHelper.getStateList();
            countryVoList = databaseHelper.getCountryList();
            size = countryVoList.size();
            displayStateList();
        }else{
            getAllStates();
        }



    }

    public void allLocation(){

        int checked = Constants.checkedState.size();

        if(Constants.checkedCountry != null && Constants.checkedCountry.size() > 0) {

            if (size == Constants.checkedCountry.size()) {

                if (checked == Constants.size) {
                    checkbox_all.setChecked(true);
                } else {
                    checkbox_all.setChecked(false);
                }
            } else {
                checkbox_all.setChecked(false);
            }
        }
        if (checkbox_all.isChecked()) {
            activity.setSubheaderAll();
        }else{
            activity.setSubheader(Constants.searchabrList);
        }
    }
}
