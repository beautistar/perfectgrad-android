package com.perfectgraduate.fragment;

import android.app.ProgressDialog;
import android.support.v4.app.Fragment;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.perfectgraduate.R;
import com.perfectgraduate.activity.HomeActivity;
import com.perfectgraduate.adapter.ExpandableAdapter;
import com.perfectgraduate.adapter.SwipeRecyclerViewAdapter;
import com.perfectgraduate.api.PerfectGraduateRequest;
import com.perfectgraduate.api.WebServiceAPI;
import com.perfectgraduate.app.PerfectGraduateApplication;
import com.perfectgraduate.database.DatabaseHelper;
import com.perfectgraduate.listener.ICallBack;
import com.perfectgraduate.utility.Constants;
import com.perfectgraduate.utility.SharedPrefrenceUtil;
import com.perfectgraduate.vo.AllCompanysVo;
import com.perfectgraduate.vo.CountryVo;
import com.perfectgraduate.vo.DisciplineVo;
import com.perfectgraduate.vo.DisciplinesVo;
import com.perfectgraduate.vo.ExpandableListDataPump;
import com.perfectgraduate.vo.StateVo;
import com.perfectgraduate.vo.SubDisciplineVo;
import com.perfectgraduate.vo.Sub_disciplineVo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by user on 08-Dec-16.
 */

public class DisciplinesFragment extends Fragment{

    ExpandableListView expandableListView;
    ExpandableListAdapter expandableListAdapter;

    private PerfectGraduateRequest request;
    private PerfectGraduateApplication application;
    private DatabaseHelper databaseHelper;
    private List<DisciplineVo> disciplinesVoList;
    private HomeActivity activity;
    private List<StateVo> stateVoList;
    private List<CountryVo> countryVoList;
    private ProgressDialog progressDialog;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_disciplines, container, false);

        init(rootView);

        return rootView;
    }

    private void init(View rootView) {


        activity = (HomeActivity) getActivity();
        databaseHelper = new DatabaseHelper(getActivity());
        application = (PerfectGraduateApplication) getActivity().getApplicationContext();
        request = new PerfectGraduateRequest(getActivity());
        progressDialog = new ProgressDialog(activity);

        disciplinesVoList = new ArrayList<>();

        stateVoList = new ArrayList<StateVo>();
        countryVoList = new ArrayList<CountryVo>();

        expandableListView = (ExpandableListView) rootView.findViewById(R.id.expandableListView);


        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {

            }
        });

        expandableListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {

            }
        });



        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {

                Constants.isBack = "discipline";
                PerfectGraduateApplication.getInstance().setDisciplineVo(disciplinesVoList.get(groupPosition));
                activity.allCompany(disciplinesVoList.get(groupPosition).getSubDisciplineVoList().get(childPosition).getName());

                return false;
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        activity.hideBadges();

        if(SharedPrefrenceUtil.getPrefrence(getActivity(),Constants.locationFilter,"").equals("")){
            getDisciplineData();
        }else{
            getDisciplineFilterData();
        }
    }

    private void getDisciplineFilterData() {

        if (!application.isConnectInternet()) {
            application.setShowDialog(getActivity(), "", getString(R.string.no_internet_connection));
        } else {
            disciplineFilter();
        }
    }


    public void getDisciplineData() {

        if(disciplinesVoList != null && disciplinesVoList.size() > 0){
            disciplinesVoList.clear();
        }

        activity.setSubheaderAll();

        if(SharedPrefrenceUtil.getPrefrence(getActivity(), Constants.isDiscipline, "").equalsIgnoreCase("Database_disciplines")) {

            List<DisciplineVo> voList = databaseHelper.getDisciplines();

            if(voList != null && voList.size() > 0) {

                List<SubDisciplineVo> VoList;

                for (int i = 0; i < voList.size(); i++) {

                    VoList = databaseHelper.getSubDisciplines(voList.get(i).getId());

                    DisciplineVo disciplineVo = new DisciplineVo();

                    disciplineVo.setId(voList.get(i).getId());
                    disciplineVo.setName(voList.get(i).getName());
                    disciplineVo.setSubDisciplineVoList(VoList);

                    disciplinesVoList.add(disciplineVo);
                }

                ExpandableAdapter expandableListAdapter = new ExpandableAdapter(getActivity(), disciplinesVoList);
                expandableListView.setAdapter(expandableListAdapter);
                expandableListView.setIndicatorBounds(0, 20);
            }

        }else{
            if (!application.isConnectInternet()) {
                application.setShowDialog(getActivity(), "", getString(R.string.no_internet_connection));
            } else {
                discipline();
            }
        }

    }

    private void discipline() {

        Map<String, Object> map = new HashMap<String, Object>();

        map.put("uid",SharedPrefrenceUtil.getPrefrence(getActivity(),Constants.USER_ID,""));
        map.put("cid","");

        progressDialog = application.getProgressDialog(activity);
        request.getResponse(getActivity(), map, WebServiceAPI.API_getAlldisciplines, false, new ICallBack() {
            @Override
            public void onCallBackResult(String response) {

                try {

                    JSONObject jsonObject = new JSONObject(response);

                    String status = jsonObject.getString(WebServiceAPI.status);

                    if(Boolean.parseBoolean(status)){

                        JSONArray jsonArray = jsonObject.getJSONArray(WebServiceAPI.data);

                        if(jsonArray != null && jsonArray.length() > 0){

                            databaseHelper.insertDisciplines(getActivity(), jsonArray);

                        }

                        List<DisciplineVo> voList = databaseHelper.getDisciplines();

                        if(voList != null && voList.size() > 0) {

                            for (int i = 0; i < voList.size(); i++) {

                                List<SubDisciplineVo> subDisciplineVoList = databaseHelper.getSubDisciplines(voList.get(i).getId());

                                DisciplineVo disciplineVo = new DisciplineVo();

                                disciplineVo.setId(voList.get(i).getId());
                                disciplineVo.setName(voList.get(i).getName());
                                disciplineVo.setSubDisciplineVoList(subDisciplineVoList);

                                disciplinesVoList.add(disciplineVo);
                            }

                            ExpandableAdapter expandableListAdapter = new ExpandableAdapter(getActivity(), disciplinesVoList);
                            expandableListView.setAdapter(expandableListAdapter);
                            expandableListView.setIndicatorBounds(0, 20);
                        }


                    }else{

                    }

                    if(progressDialog != null) {
                        progressDialog.dismiss();
                        PerfectGraduateApplication.getInstance().displayCustomDialog(getActivity(), "University Disciplines", "To help you find companies that are interested in your field of study, you can search for companies using the Disciplines page." +
                                "\nSelect from a wide range of Disciplines and Sub Disciplines to learn more about the companies that are interested in your field of study.");

                    }
                }catch(JSONException k){
                    k.printStackTrace();
                }

            }
        });

    }

    private void disciplineFilter() {

        if(disciplinesVoList != null && disciplinesVoList.size() > 0){
            disciplinesVoList.clear();
        }

        Map<String, Object> map = new HashMap<String, Object>();

        StringBuilder builderCountry = new StringBuilder();

        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
        Gson gson = new Gson();
        String jsonData = sharedPrefs.getString("checkedState", "");
        String jsonDatacheckedCountry = sharedPrefs.getString("checkedCountry", "");
        Type type = new TypeToken<ArrayList<String>>() {
        }.getType();
        ArrayList<String> checkedState = new ArrayList<String>();
        ArrayList<String> checkedCountry = new ArrayList<String>();
        checkedState = gson.fromJson(jsonData, type);
        checkedCountry = gson.fromJson(jsonDatacheckedCountry, type);

        Log.e("checkedState"," --- "+checkedState);
        Log.e("checkedCountry"," --- "+checkedCountry);

        Constants.abrList.clear();

        stateVoList = databaseHelper.getStateList();
        countryVoList = databaseHelper.getCountryList();

        if (checkedState != null && checkedCountry != null) {

            if (checkedState.size() == stateVoList.size() && checkedCountry.size() == countryVoList.size()) {
                activity.setSubheaderAll();
            }  else {

                if (checkedCountry != null && checkedCountry.size() > 0) {
                    for (int i = 0; i < countryVoList.size(); i++) {
                        if (checkedCountry.contains(countryVoList.get(i).getId())) {
                            Constants.abrList.add(countryVoList.get(i).getAbr());
                        }
                    }
                }

                if (checkedState != null && checkedState.size() > 0) {

                    for (int i = 0; i < stateVoList.size(); i++) {
                        if (checkedState.contains(stateVoList.get(i).getId())) {
                            Constants.abrList.add(stateVoList.get(i).getAbr());
                        }
                    }
                }

                if (Constants.abrList.size() > 0) {

                    activity.setSubheader(Constants.abrList);
                }
            }
        } else {

            if (checkedCountry != null && checkedCountry.size() > 0) {
                for (int i = 0; i < countryVoList.size(); i++) {
                    if (checkedCountry.contains(countryVoList.get(i).getId())) {
                        Constants.abrList.add(countryVoList.get(i).getAbr());
                    }
                }
            }

            if (checkedState != null && checkedState.size() > 0) {

                for (int i = 0; i < stateVoList.size(); i++) {
                    if (checkedState.contains(stateVoList.get(i).getId())) {
                        Constants.abrList.add(stateVoList.get(i).getAbr());
                    }
                }
            }

            if (Constants.abrList.size() > 0) {

                activity.setSubheader(Constants.abrList);
            }
        }


        if(checkedCountry != null && checkedCountry.size() > 0) {

            for (int i = 0; i < checkedCountry.size(); i++) {
                builderCountry.append(checkedCountry.get(i));
                builderCountry.append(",");
            }
        }

        StringBuilder builderState = new StringBuilder();



        if(checkedState != null && checkedState.size() > 0) {

            for (int i = 0; i < checkedState.size(); i++) {
                builderState.append(checkedState.get(i));
                builderState.append(",");
            }
        }

        map.put("uid",SharedPrefrenceUtil.getPrefrence(getActivity(),Constants.USER_ID,""));

        if(builderCountry.toString().length() > 0 ) {
            map.put("cid", builderCountry.subSequence(0, builderCountry.length() - 1));
        }else{
            map.put("cid", "");
        }

        if(builderState.toString().length() > 0 ) {
            map.put("sid", builderState.subSequence(0, builderState.length() - 1));
        }else{
            map.put("sid", "");
        }


        request.getResponse(getActivity(), map, WebServiceAPI.API_DesciplineFilter, true, new ICallBack() {
            @Override
            public void onCallBackResult(String response) {

                try {

                    JSONObject jsonObject = new JSONObject(response);

                    String status = jsonObject.getString(WebServiceAPI.status);

                    if(Boolean.parseBoolean(status)){

                        JSONArray jsonArray = jsonObject.getJSONArray(WebServiceAPI.data);

                        List<DisciplineVo> voList = new ArrayList<DisciplineVo>();

                        //if (SharedPrefrenceUtil.getPrefrence(getActivity(), Constants.isDisciplineFilter, "").equalsIgnoreCase("Database_disciplinesFilter")) {

                            //voList = databaseHelper.getDisciplines();
                        //}else{
                            if (jsonArray != null && jsonArray.length() > 0) {

                                databaseHelper.insertDisciplineFilter(getActivity(), jsonArray);

                            }
                            voList = databaseHelper.getDisciplines();
                        //}

                        if(voList != null && voList.size() > 0) {

                            for (int i = 0; i < voList.size(); i++) {

                                List<SubDisciplineVo> subDisciplineVoList = databaseHelper.getSubDisciplinesFilter(voList.get(i).getId());

                                DisciplineVo disciplineVo = new DisciplineVo();

                                disciplineVo.setId(voList.get(i).getId());
                                disciplineVo.setName(voList.get(i).getName());
                                disciplineVo.setSubDisciplineVoList(subDisciplineVoList);

                                disciplinesVoList.add(disciplineVo);
                            }

                            ExpandableAdapter expandableListAdapter = new ExpandableAdapter(getActivity(), disciplinesVoList);
                            expandableListView.setAdapter(expandableListAdapter);
                            expandableListView.setIndicatorBounds(0, 20);
                        }

                    }else{

                    }

                }catch(JSONException k){
                    k.printStackTrace();
                }

            }
        });

    }

}
