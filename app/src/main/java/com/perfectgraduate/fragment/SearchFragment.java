package com.perfectgraduate.fragment;

import android.app.ProgressDialog;
import android.support.v4.app.Fragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.perfectgraduate.R;
import com.perfectgraduate.activity.DetailActivity;
import com.perfectgraduate.activity.HomeActivity;
import com.perfectgraduate.adapter.ExpandableAdapter;
import com.perfectgraduate.adapter.TestBaseAdapter;
import com.perfectgraduate.api.PerfectGraduateRequest;
import com.perfectgraduate.api.WebServiceAPI;
import com.perfectgraduate.app.PerfectGraduateApplication;
import com.perfectgraduate.database.DatabaseHelper;
import com.perfectgraduate.listener.ICallBack;
import com.perfectgraduate.utility.Constants;
import com.perfectgraduate.utility.SharedPrefrenceUtil;
import com.perfectgraduate.utility.stickylistheaders.StickyListHeadersListView;
import com.perfectgraduate.vo.CountryVo;
import com.perfectgraduate.vo.DisciplineVo;
import com.perfectgraduate.vo.StateVo;
import com.perfectgraduate.vo.SubDisciplineVo;
import com.perfectgraduate.vo.UserCompanyVo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Created by user on 09-Dec-16.
 */

public class SearchFragment extends Fragment implements SearchView.OnQueryTextListener {

    private StickyListHeadersListView stickyList;
    private TestBaseAdapter mAdapter;
    private List<UserCompanyVo> userCompanyVoList;
    private DatabaseHelper databaseHelper;
    private LinearLayout linearLayout_due_date;
    private TextView textView_company, textView_abr_prg;
    private SearchView searchView_search;
    private PerfectGraduateRequest request;
    private List<StateVo> stateVoList;
    private List<CountryVo> countryVoList;
    private HomeActivity activity;
    private ProgressDialog progressDialog;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_search, container, false);

        init(rootView);

        return rootView;
    }

    private void init(View rootView) {


        /*PerfectGraduateApplication.getInstance().displayCustomDialog(getActivity(),"Company Search","To help you learn more about companies and their hiring programs, you can scroll or search for companies of interest. " +
                "\n\nUse the filter in the top corner to display companies based on location (i.e., local as well as international).");*/

        activity = (HomeActivity) getActivity();

        progressDialog = new ProgressDialog(activity);

        databaseHelper = new DatabaseHelper(getActivity());

        stateVoList = new ArrayList<StateVo>();
        countryVoList = new ArrayList<CountryVo>();

        userCompanyVoList = new ArrayList<>();

        request =  new PerfectGraduateRequest(getActivity());

        stickyList = (StickyListHeadersListView) rootView.findViewById(R.id.list);

        searchView_search = (SearchView) rootView.findViewById(R.id.searchView_search);
        /*stickyList.setOnItemClickListener(this);
        stickyList.setOnHeaderClickListener(this);
        stickyList.setOnStickyHeaderChangedListener(this);
        stickyList.setOnStickyHeaderOffsetChangedListener(this);*/
        //stickyList.addHeaderView(getActivity().getLayoutInflater().inflate(R.layout.list_header, null));
        //stickyList.addFooterView(getActivity().getLayoutInflater().inflate(R.layout.list_footer, null));
        stickyList.setDrawingListUnderStickyHeader(true);
        stickyList.setAreHeadersSticky(true);
        stickyList.setStickyHeaderTopOffset(-20);

        textView_company = (TextView) rootView.findViewById(R.id.textView_company);
        textView_abr_prg = (TextView) rootView.findViewById(R.id.textView_abr_prg);
        linearLayout_due_date = (LinearLayout) rootView.findViewById(R.id.linearLayout_due_date);

        textView_abr_prg.setText(SharedPrefrenceUtil.getPrefrence(getActivity(), Constants.abr, "") + " " + SharedPrefrenceUtil.getPrefrence(getActivity(), Constants.program_name, ""));

        if(SharedPrefrenceUtil.getPrefrence(getActivity(),Constants.locationFilter,"").equals("countryStates_search")){

            getCompanyFilter();

        }else{
            userCompanyVoList = databaseHelper.getAllCompany();

            setupSearchView();

            Collections.sort(userCompanyVoList, UserCompanyVo.short_company_name);


            mAdapter = new TestBaseAdapter(getActivity(),userCompanyVoList);
            stickyList.setAdapter(mAdapter);
        }

        stickyList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                UserCompanyVo userCompanyVo = userCompanyVoList.get(i);

                PerfectGraduateApplication.getInstance().setUserCompanyVo(userCompanyVo);

                activity.displayDetail(userCompanyVo.getCompany_name());

                /*Intent intent = new Intent(getActivity(), DetailActivity.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_left);*/
            }
        });

        textView_company.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Collections.sort(userCompanyVoList, UserCompanyVo.short_company_name);

                Constants.hideShow = false;

                mAdapter = new TestBaseAdapter(getActivity(),userCompanyVoList);
                mAdapter.notifyDataSetChanged();
                stickyList.setAdapter(mAdapter);
            }
        });

        linearLayout_due_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Collections.sort(userCompanyVoList, UserCompanyVo.short_due_date);

                Constants.hideShow = true;

                mAdapter = new TestBaseAdapter(getActivity(),userCompanyVoList);
                mAdapter.notifyDataSetChanged();
                stickyList.setAdapter(mAdapter);
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        activity.hideBadges();
    }

    private void getCompanyFilter() {

        Map<String, Object> map = new HashMap<String, Object>();

        StringBuilder builderCountry = new StringBuilder();

        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
        Gson gson = new Gson();
        String jsonData = sharedPrefs.getString("checkedState_search", "");
        String jsonDatacheckedCountry = sharedPrefs.getString("checkedCountry_search", "");
        Type type = new TypeToken<ArrayList<String>>() {
        }.getType();
        ArrayList<String> checkedState = new ArrayList<String>();
        ArrayList<String> checkedCountry = new ArrayList<String>();
        checkedState = gson.fromJson(jsonData, type);
        checkedCountry = gson.fromJson(jsonDatacheckedCountry, type);


        Constants.searchabrList.clear();

        stateVoList = databaseHelper.getStateList();
        countryVoList = databaseHelper.getCountryList();


        if (checkedState != null && checkedCountry != null) {

            if (checkedState.size() == stateVoList.size() && checkedCountry.size() == countryVoList.size()) {
                activity.setSubheaderAll();
            } else {

                if (checkedCountry != null && checkedCountry.size() > 0) {
                    for (int i = 0; i < countryVoList.size(); i++) {
                        if (checkedCountry.contains(countryVoList.get(i).getId())) {
                            Constants.searchabrList.add(countryVoList.get(i).getAbr());
                        }
                    }
                }

                if (checkedState != null && checkedState.size() > 0) {

                    for (int i = 0; i < stateVoList.size(); i++) {
                        if (checkedState.contains(stateVoList.get(i).getId())) {
                            Constants.searchabrList.add(stateVoList.get(i).getAbr());
                        }
                    }
                }

                if (Constants.searchabrList.size() > 0) {

                    activity.setSubheader(Constants.searchabrList);
                }
            }

        } else {

            if (checkedCountry != null && checkedCountry.size() > 0) {
                for (int i = 0; i < countryVoList.size(); i++) {
                    if (checkedCountry.contains(countryVoList.get(i).getId())) {
                        Constants.searchabrList.add(countryVoList.get(i).getAbr());
                    }
                }
            }

            if (checkedState != null && checkedState.size() > 0) {

                for (int i = 0; i < stateVoList.size(); i++) {
                    if (checkedState.contains(stateVoList.get(i).getId())) {
                        Constants.searchabrList.add(stateVoList.get(i).getAbr());
                    }
                }
            }

            if (Constants.searchabrList.size() > 0) {

                activity.setSubheader(Constants.searchabrList);
            }
        }



        if(checkedCountry != null && checkedCountry.size() > 0) {

            for (int i = 0; i < checkedCountry.size(); i++) {
                builderCountry.append(checkedCountry.get(i));
                builderCountry.append(",");
            }
        }

        StringBuilder builderState = new StringBuilder();


        if(checkedState != null && checkedState.size() > 0) {

            for (int i = 0; i < checkedState.size(); i++) {
                builderState.append(checkedState.get(i));
                builderState.append(",");
            }
        }

        map.put("user_id",SharedPrefrenceUtil.getPrefrence(getActivity(),Constants.USER_ID,""));

        if(builderCountry.toString().length() > 0 ) {
            map.put("country_id", builderCountry.toString());
        }else{
            map.put("country_id", "");
        }

        if(builderState.toString().length() > 0 ) {
            map.put("state_id", builderState.toString());
        }else{
            map.put("state_id", "");
        }

        progressDialog = PerfectGraduateApplication.getInstance().getProgressDialog(activity);
        request.getResponse(getActivity(), map, WebServiceAPI.API_companyFilter, false, new ICallBack() {
            @Override
            public void onCallBackResult(String response) {

                try {

                    JSONObject jsonObject = new JSONObject(response);

                    String status = jsonObject.getString(WebServiceAPI.status);

                    if(Boolean.parseBoolean(status)){

                        JSONArray jsonArray = jsonObject.getJSONArray(WebServiceAPI.data);

                        List<DisciplineVo> voList = new ArrayList<DisciplineVo>();

                        if (SharedPrefrenceUtil.getPrefrence(getActivity(), Constants.isCompanyFilter, "").equalsIgnoreCase("Database_companyFilter")) {

                            userCompanyVoList = databaseHelper.getAllCompanyFilter();
                        }else{
                            if (jsonArray != null && jsonArray.length() > 0) {

                                databaseHelper.insertAllCompanyFilter(getActivity(), jsonArray);

                            }
                            userCompanyVoList = databaseHelper.getAllCompanyFilter();
                        }

                        userCompanyVoList = databaseHelper.getAllCompanyFilter();

                        //setupSearchView();

                        Collections.sort(userCompanyVoList, UserCompanyVo.short_company_name);

                        mAdapter = new TestBaseAdapter(getActivity(),userCompanyVoList);
                        stickyList.setAdapter(mAdapter);

                        if(progressDialog != null){
                            progressDialog.dismiss();
                        }

                    }else{

                    }

                }catch(JSONException k){
                    k.printStackTrace();
                }

            }
        });

    }
    private void setupSearchView() {
        searchView_search.setIconifiedByDefault(false);
        searchView_search.setOnQueryTextListener(this);
        searchView_search.setSubmitButtonEnabled(true);
        searchView_search.setQueryHint("Search");
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        mAdapter.filter(s);
        mAdapter.notifyDataSetChanged();
        return true;
    }


}
