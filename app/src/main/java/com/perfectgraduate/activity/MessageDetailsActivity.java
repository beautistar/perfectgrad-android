package com.perfectgraduate.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.callback.BitmapAjaxCallback;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.linkedin.platform.APIHelper;
import com.linkedin.platform.LISessionManager;
import com.linkedin.platform.errors.LIApiError;
import com.linkedin.platform.errors.LIAuthError;
import com.linkedin.platform.listeners.ApiListener;
import com.linkedin.platform.listeners.ApiResponse;
import com.linkedin.platform.listeners.AuthListener;
import com.linkedin.platform.utils.Scope;
import com.perfectgraduate.R;
import com.perfectgraduate.adapter.ExpandableAdapter;
import com.perfectgraduate.api.PerfectGraduateRequest;
import com.perfectgraduate.api.WebServiceAPI;
import com.perfectgraduate.app.PerfectGraduateApplication;
import com.perfectgraduate.database.DatabaseHelper;
import com.perfectgraduate.listener.ICallBack;
import com.perfectgraduate.utility.Constants;
import com.perfectgraduate.utility.SharedPrefrenceUtil;
import com.perfectgraduate.vo.DisciplineVo;
import com.perfectgraduate.vo.MessagesVo;
import com.perfectgraduate.vo.SocietyVo;
import com.perfectgraduate.vo.SubDisciplineVo;
import com.perfectgraduate.vo.UserCompanyVo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by user on 09-Jan-17.
 */

public class MessageDetailsActivity extends AppCompatActivity{

    private TextView textView_eventTitle,textView_eventName,textView_eventDate,textView_eventTime,textView_eventLocation,textView_eventMessage,textView_submit,
            textView_dateTime,textView_header,textView_confirm,textView_event;
    private ImageView imageView_eventLogo,imageView_like,imageView_back,imageView_Portfolio,imageView_msg,imageView_Societies;
    private MessagesVo messagesVo;
    private DatabaseHelper databaseHelper;
    private LinearLayout linearLayout_event;
    private MessageDetailsActivity activity;
    public static HomeActivity homeActivity;
    private RelativeLayout relative_details,relative_event;

    private int msgReport = 0;
    private PerfectGraduateRequest request;
    private List<UserCompanyVo> userCompanyVoList;
    private CallbackManager mCallbackManager;

    private String added = "";

    private static final String host = "api.linkedin.com";
    private static final String topCardUrl = "https://" + host + "/v1/people/~:" +
            "(email-address,formatted-name,phone-numbers,public-profile-url,picture-url,picture-urls::(original))";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_eventdetails);

        init();


    }

    private void init() {

        getSupportActionBar().hide();

        activity = this;
        databaseHelper = new DatabaseHelper(activity);
        request = new PerfectGraduateRequest(activity);
        userCompanyVoList = new ArrayList<>();

        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        Gson gson = new Gson();
        String jsonData = sharedPrefs.getString("societyDetail", "");
        Type type = new TypeToken<ArrayList<SocietyVo>>() {
        }.getType();

        if(Constants.societyVoList != null){
            Constants.societyVoList.clear();
        }

        Constants.societyVoList = gson.fromJson(jsonData, type);

        userCompanyVoList =  databaseHelper.getAllCompany();

        textView_eventTitle = (TextView) findViewById(R.id.textView_eventTitle);
        textView_eventName = (TextView) findViewById(R.id.textView_eventName);
        textView_eventDate = (TextView) findViewById(R.id.textView_eventDate);
        textView_eventTime = (TextView) findViewById(R.id.textView_eventTime);
        textView_eventLocation = (TextView) findViewById(R.id.textView_eventLocation);
        textView_eventMessage = (TextView) findViewById(R.id.textView_eventMessage);
        textView_submit = (TextView) findViewById(R.id.textView_submit);
        textView_dateTime = (TextView) findViewById(R.id.textView_dateTime);
        textView_confirm = (TextView) findViewById(R.id.textView_confirm);
        textView_header = (TextView) findViewById(R.id.textView_header);
        textView_event = (TextView) findViewById(R.id.textView_event);

        imageView_eventLogo = (ImageView) findViewById(R.id.imageView_eventLogo);
        imageView_like = (ImageView) findViewById(R.id.imageView_like);
        imageView_back = (ImageView) findViewById(R.id.imageView_back);

        imageView_msg = (ImageView) findViewById(R.id.imageView_msg);
        imageView_Portfolio = (ImageView) findViewById(R.id.imageView_Portfolio);
        imageView_Societies = (ImageView) findViewById(R.id.imageView_Societies);

        linearLayout_event = (LinearLayout) findViewById(R.id.linearLayout_event);
        relative_details = (RelativeLayout) findViewById(R.id.relative_details);
        relative_event = (RelativeLayout) findViewById(R.id.relative_event);

        messagesVo = PerfectGraduateApplication.getInstance().getMessagesVo();

        textView_dateTime.setText(PerfectGraduateApplication.getInstance().parseDateToddMMyyyy(messagesVo.getDate()));

        for (int i = 0; i < messagesVo.getAdderDetailVoList().size(); i++) {
            textView_eventTitle.setText(messagesVo.getAdderDetailVoList().get(i).getName());
        }

        if(messagesVo.getStatus().equals("unread")){
            databaseHelper.updateMessageStatus(messagesVo.getId(),messagesVo.getStatus());
        }

        imageView_eventLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Constants.company_id = messagesVo.getAdder_id();
               // homeActivity.displayDetail("");
                /*Intent intent = new Intent(MessageDetailsActivity.this, DetailActivity.class);
                startActivity(intent);*/
            }
        });

        FacebookSdk.sdkInitialize(activity.getApplicationContext());

        mCallbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().registerCallback(mCallbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        if (AccessToken.getCurrentAccessToken() != null) {
                            RequestData();
                        }
                    }

                    @Override
                    public void onCancel() {
                    }

                    @Override
                    public void onError(FacebookException exception) {
                    }
                });


        if(messagesVo.getType().equals("companybehalf")){
            imageView_like.setVisibility(View.VISIBLE);
        }else if(messagesVo.getType().equals("studentsociety")){
            imageView_like.setVisibility(View.VISIBLE);
        }else if(messagesVo.getType().equals("admin")){
            imageView_like.setVisibility(View.VISIBLE);
        }else{
            imageView_like.setVisibility(View.INVISIBLE);
        }

        if(Constants.message.equals("particular message") || Constants.message.equals("society group")
                || Constants.message.equals("admin messages")){

            if(messagesVo.getMessage_type().equals("event")) {

                textView_header.setText("Event Details");

                if (messagesVo.getType().equals("admin")) {
                    if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                        relative_details.setBackgroundDrawable(getResources().getDrawable(R.drawable.greenback));
                    }else{
                        relative_details.setBackground(getResources().getDrawable(R.drawable.greenback));
                    }
                    imageView_like.setImageResource(R.drawable.notlikedwhite);
                }


                if (messagesVo.getEventconfirmation().equals("Request Sent")) {

                    textView_submit.setText("Invitation requested");
                    textView_submit.setEnabled(false);
                    textView_confirm.setVisibility(View.VISIBLE);
                    textView_event.setVisibility(View.INVISIBLE);

                } else if (messagesVo.getEventconfirmation().equals("Confirmed")) {

                    textView_submit.setText("Congratulations");
                    textView_submit.setEnabled(false);
                    if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                        relative_details.setBackgroundDrawable(getResources().getDrawable(R.drawable.pinkback));
                    }else{
                        relative_details.setBackground(getResources().getDrawable(R.drawable.pinkback));
                    }
                    textView_confirm.setVisibility(View.INVISIBLE);
                    textView_event.setVisibility(View.VISIBLE);
                }

                if (messagesVo.getEventstatus().equals("0")) {
                    textView_submit.setText("Event cancelled");
                    textView_submit.setEnabled(false);
                    imageView_like.setVisibility(View.VISIBLE);
                    textView_submit.setVisibility(View.VISIBLE);
                    if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                        relative_details.setBackgroundDrawable(getResources().getDrawable(R.drawable.blue_border));
                    }else{
                        relative_details.setBackground(getResources().getDrawable(R.drawable.blue_border));
                    }
                } else if (messagesVo.getEventstatus().equals("2")) {
                    textView_submit.setText("Event closed");
                    imageView_like.setVisibility(View.VISIBLE);
                    textView_submit.setVisibility(View.VISIBLE);
                    if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                        relative_details.setBackgroundDrawable(getResources().getDrawable(R.drawable.blue_border));
                    }else{
                        relative_details.setBackground(getResources().getDrawable(R.drawable.blue_border));
                    }
                }


            }else if(messagesVo.getMessage_type().equals("studentsociety")){

                if(messagesVo.getMessage_type().equals("message")){
                    relative_event.setVisibility(View.INVISIBLE);
                }


            } else{

                if(!messagesVo.getType().equals("admin")){

                    if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                        relative_details.setBackgroundDrawable(getResources().getDrawable(R.drawable.blueback));
                    }else{
                        relative_details.setBackground(getResources().getDrawable(R.drawable.blueback));
                    }
                }


                relative_event.setVisibility(View.INVISIBLE);
                textView_header.setText("Message Details");

            }


        }else {

            if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                relative_details.setBackgroundDrawable(getResources().getDrawable(R.drawable.blue_border));
            }else{
                relative_details.setBackground(getResources().getDrawable(R.drawable.blue_border));
            }

            if (messagesVo.getMessage_type().equals("event")) {

                if (messagesVo.getType().equals("admin")) {
                    if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                        relative_details.setBackgroundDrawable(getResources().getDrawable(R.drawable.blue_border));
                    }else{
                        relative_details.setBackground(getResources().getDrawable(R.drawable.blue_border));
                    }
                    imageView_like.setImageResource(R.drawable.unliked);
                } else {

                    if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                        relative_details.setBackgroundDrawable(getResources().getDrawable(R.drawable.greenback));
                    }else{
                        relative_details.setBackground(getResources().getDrawable(R.drawable.greenback));
                    }
                    imageView_like.setImageResource(R.drawable.notlikedwhite);
                }
                if (messagesVo.getEventconfirmation().equals("Request Sent")) {

                    textView_submit.setText("Invitation requested");
                    textView_submit.setEnabled(false);
                    textView_confirm.setVisibility(View.VISIBLE);
                    textView_event.setVisibility(View.INVISIBLE);

                } else if (messagesVo.getEventconfirmation().equals("Confirmed")) {
                    textView_submit.setText("Congratulations");
                    textView_submit.setEnabled(false);
                    if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                        relative_details.setBackgroundDrawable(getResources().getDrawable(R.drawable.pinkback));
                    }else{
                        relative_details.setBackground(getResources().getDrawable(R.drawable.pinkback));
                    }
                    textView_confirm.setVisibility(View.INVISIBLE);
                    textView_event.setVisibility(View.VISIBLE);
                }

                if (messagesVo.getEventstatus().equals("0")) {
                    textView_submit.setText("Event cancelled");
                    textView_submit.setEnabled(false);
                    if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                        relative_details.setBackgroundDrawable(getResources().getDrawable(R.drawable.blue_border));
                    }else{
                        relative_details.setBackground(getResources().getDrawable(R.drawable.blue_border));
                    }
                    imageView_like.setVisibility(View.VISIBLE);
                    textView_submit.setVisibility(View.VISIBLE);
                } else if (messagesVo.getEventstatus().equals("2")) {
                    textView_submit.setText("Event closed");
                    if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                        relative_details.setBackgroundDrawable(getResources().getDrawable(R.drawable.blue_border));
                    }else{
                        relative_details.setBackground(getResources().getDrawable(R.drawable.blue_border));
                    }
                    imageView_like.setVisibility(View.VISIBLE);
                    textView_submit.setVisibility(View.VISIBLE);
                }


            } else {

                if (!messagesVo.getType().equals("admin")) {
                    if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                        relative_details.setBackgroundDrawable(getResources().getDrawable(R.drawable.blueback));
                    }else{
                        relative_details.setBackground(getResources().getDrawable(R.drawable.blueback));
                    }
                }
                relative_event.setVisibility(View.INVISIBLE);
                textView_header.setText("Message Details");
            }
        }

        if(messagesVo.getMessage_type().equals("admin")){


            imageView_like.setVisibility(View.INVISIBLE);


            if(!messagesVo.getEventconfirmation().equals("Confirmed")){
                if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    relative_details.setBackgroundDrawable(getResources().getDrawable(R.drawable.blue_border));
                }else{
                    relative_details.setBackground(getResources().getDrawable(R.drawable.blue_border));
                }
            }

        }else if(messagesVo.getEventconfirmation().equals("companybehalf")){

            //addRightButton create method

            addRightButton();

            if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                relative_details.setBackgroundDrawable(getResources().getDrawable(R.drawable.blue_border));
            }else{
                relative_details.setBackground(getResources().getDrawable(R.drawable.blue_border));
            }

        }else if(messagesVo.getEventconfirmation().equals("studentsociety")){

            //addRightButton create method

            addRightButton();

            if (messagesVo.getMessage_type().equals("message")){
                if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    relative_details.setBackgroundDrawable(getResources().getDrawable(R.drawable.societymsgdetail));
                }else{
                    relative_details.setBackground(getResources().getDrawable(R.drawable.societymsgdetail));
                }
            }else{
                if(messagesVo.getEventconfirmation().equals("Confirmed")){
                    textView_submit.setText("Congratulations");
                    textView_submit.setEnabled(false);
                    if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                        relative_details.setBackgroundDrawable(getResources().getDrawable(R.drawable.pinkback));
                    }else{
                        relative_details.setBackground(getResources().getDrawable(R.drawable.pinkback));
                    }
                    textView_confirm.setVisibility(View.INVISIBLE);
                    textView_event.setVisibility(View.VISIBLE);
                }else{
                    if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                        relative_details.setBackgroundDrawable(getResources().getDrawable(R.drawable.orangeback));
                    }else{
                        relative_details.setBackground(getResources().getDrawable(R.drawable.orangeback));
                    }
                }
            }
        }else{

            if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                relative_details.setBackgroundDrawable(getResources().getDrawable(R.drawable.blue_border));
            }else{
                relative_details.setBackground(getResources().getDrawable(R.drawable.blue_border));
            }

            if (messagesVo.getMessage_type().equals("event")){

                if(messagesVo.getEventconfirmation().equals("Confirmed")){
                    if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                        relative_details.setBackgroundDrawable(getResources().getDrawable(R.drawable.pinkback));
                    }else{
                        relative_details.setBackground(getResources().getDrawable(R.drawable.pinkback));
                    }
                }else{
                    if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                        relative_details.setBackgroundDrawable(getResources().getDrawable(R.drawable.greenback));
                    }else{
                        relative_details.setBackground(getResources().getDrawable(R.drawable.greenback));
                    }
                }

            }else{
                if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    relative_details.setBackgroundDrawable(getResources().getDrawable(R.drawable.blueback));
                }else{
                    relative_details.setBackground(getResources().getDrawable(R.drawable.blueback));
                }
            }
        }

        textView_eventName.setText(messagesVo.getEvent_title());
        textView_eventDate.setText(messagesVo.getEvent_date());
        textView_eventTime.setText(messagesVo.getEventtimefrom() +" to "+messagesVo.getEventtimeto());
        textView_eventLocation.setText(messagesVo.getEvent_venue());
        textView_eventMessage.setText(messagesVo.getMessage());

        textView_eventLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(activity, EventLocationActivity.class);
                intent.putExtra("location",messagesVo.getEvent_venue());
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_left);

            }
        });

        AQuery aQuery = new AQuery(activity);

        aQuery.id(imageView_eventLogo).image("http://www.perfectgraduate.com/perfect/assets/uploads/" + messagesVo.getPerfect_logo(),
                true, true, 0, 0, new BitmapAjaxCallback() {
                    @Override
                    protected void callback(String url, ImageView iv, Bitmap bm, AjaxStatus status) {

                        if (bm != null) {
                            iv.setImageBitmap(bm);
                        } else {
                            iv.setImageResource(R.drawable.defaultbuilding);
                        }
                    }
                });


        textView_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                requestButton();

            }
        });



        imageView_like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(messagesVo.getLike_status().equals("0")){
                    imageView_like.setImageResource(R.drawable.liked);
                    databaseHelper.likeUnlike(messagesVo.getId(),messagesVo.getStatus());
                    updateLikeDislike("1",messagesVo.getId());
                }
            }
        });

        imageView_Portfolio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (int i = 0; i < messagesVo.getAdderDetailVoList().size(); i++) {
                    addremovePort(messagesVo.getAdderDetailVoList().get(i).getId());
                }
            }
        });

        imageView_msg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String Status = "";

                if(messagesVo.getType().equals("companybehalf")){

                    if(msgReport == 0){
                        msgReport = 1;
                        Status = "1";
                        imageView_msg.setImageResource(R.drawable.msgstop);
                    }else{
                        msgReport = 0;
                        Status = "2";
                        imageView_msg.setImageResource(R.drawable.msgdetail);
                    }
                    databaseHelper.updateReport(messagesVo.getId(),String.valueOf(msgReport));
                    updateMsg(Status,WebServiceAPI.API_MessageStatus,messagesVo.getAdder_id(),"company_id");
                }else if(messagesVo.getType().equals("studentsociety")){
                    if(msgReport == 0){
                        msgReport = 1;
                        Status = "1";
                        imageView_msg.setImageResource(R.drawable.msgstop);
                    }else{
                        msgReport = 0;
                        Status = "2";
                        imageView_msg.setImageResource(R.drawable.msgdetail);
                    }
                    databaseHelper.updateReport(messagesVo.getId(),String.valueOf(msgReport));
                    updateMsg(Status,WebServiceAPI.API_MessageStatus,messagesVo.getAdder_id(),"Society_id");
                }
            }
        });

        imageView_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }

    private void requestButton() {

        if(messagesVo.getType().equals("companybehalf")){


            if(SharedPrefrenceUtil.getPrefrence(activity,Constants.FB_ID,"").equals("")){

                PerfectGraduateApplication.getInstance().displayDialogOKCallBack(activity,"","Please save your portfolio before continuing.", new ICallBack() {
                    @Override
                    public void onCallBackResult(String response) {
                        LoginManager.getInstance().logInWithReadPermissions(activity, Arrays.asList("public_profile", "email"));
                    }
                });

            }else if(SharedPrefrenceUtil.getPrefrence(activity,Constants.emailAddress,"").equals("")){
                PerfectGraduateApplication.getInstance().displayCustomDialogPostNeg(activity,
                        "LinkedIn Sign In", "Please Sign in to LinkedIn to request invitations to company events.", new ICallBack() {
                            @Override
                            public void onCallBackResult(String response) {
                                login_linkedin();

                            }
                        },"Sign In",getString(R.string.no));
            }else{

                PerfectGraduateApplication.getInstance().displayCustomDialogPostNeg(activity,"Confirmation","Please confirm if you would like to request an invitation?", new ICallBack() {
                    @Override
                    public void onCallBackResult(String response) {
                        apiCall();

                    }
                },getString(R.string.yes),getString(R.string.no));
            }

        }

    }

    private void RequestData() {

        GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {

                        JSONObject json = response.getJSONObject();

                        Log.e("response"," -- "+json.toString());

                        try {
                            if (json != null) {

                                SharedPrefrenceUtil.setPrefrence(activity,Constants.FB_ID,json.getString("id"));
                                SharedPrefrenceUtil.setPrefrence(activity,Constants.FIRST_NAME,json.getString("first_name"));
                                SharedPrefrenceUtil.setPrefrence(activity,Constants.LAST_NAME,json.getString("last_name"));

                                if (json.has("email")) {
                                    SharedPrefrenceUtil.setPrefrence(activity,Constants.EMAIL_ID,json.getString("email"));
                                }
                                registerFb();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,first_name,last_name,email");
        request.setParameters(parameters);
        request.executeAsync();

    }

    private void registerFb() {

        Map<String, Object> map = new HashMap<String, Object>();

        map.put("fb_id",SharedPrefrenceUtil.getPrefrence(activity,Constants.FB_ID,""));
        map.put("email",SharedPrefrenceUtil.getPrefrence(activity,Constants.EMAIL_ID,""));
        map.put("Name",SharedPrefrenceUtil.getPrefrence(activity,Constants.FIRST_NAME,"")+" "+SharedPrefrenceUtil.getPrefrence(this,Constants.LAST_NAME,""));
        map.put("device", FirebaseInstanceId.getInstance().getToken());
        map.put("device type", "android");

        request.getResponse(activity, map, WebServiceAPI.API_register_fb, true, new ICallBack() {
            @Override
            public void onCallBackResult(String response) {
                try{

                    JSONObject jsonObject = new JSONObject(response);

                    String status = jsonObject.getString(WebServiceAPI.status);

                    requestButton();

                }catch (JSONException e){
                    e.printStackTrace();
                }
            }
        });

    }

    private void apiCall() {

        Map<String, Object> map = new HashMap<String, Object>();

        map.put("uid",SharedPrefrenceUtil.getPrefrence(activity,Constants.USER_ID,""));
        map.put("evid", messagesVo.getAdder_id());

        request.getResponse(activity, map, WebServiceAPI.API_requestEvent, true, new ICallBack() {
            @Override
            public void onCallBackResult(String response) {
                try{

                    JSONObject jsonObject = new JSONObject(response);

                    if(Boolean.parseBoolean(jsonObject.getString("status"))){

                        textView_submit.setText("Invitation requested");
                        textView_submit.setEnabled(false);

                        if(messagesVo.getType().equals("studentsociety")){

                        }else if(messagesVo.getType().equals("admin")){

                        }else{

                            if(added.equals("added")){
                                int points = Integer.parseInt(SharedPrefrenceUtil.getPrefrence(activity, Constants.score, ""));

                                points = points + 150;

                                SharedPrefrenceUtil.setPrefrence(activity, Constants.score, String.valueOf(points));

                            }else{

                                for (int i = 0; i < messagesVo.getAdderDetailVoList().size(); i++) {
                                    addremovePort(messagesVo.getAdderDetailVoList().get(i).getId());
                                }

                                int points = Integer.parseInt(SharedPrefrenceUtil.getPrefrence(activity, Constants.score, ""));

                                points = points + 150;

                                SharedPrefrenceUtil.setPrefrence(activity, Constants.score, String.valueOf(points));

                            }

                        }

                        PerfectGraduateApplication.getInstance().displayCustomDialog(activity,"Confirmation","You have successfully signed in to LinkedIn.");

                    }else{

                    }

                }catch (JSONException e){
                    e.printStackTrace();
                }
            }
        });

    }

    private void login_linkedin() {
        LISessionManager.getInstance(getApplicationContext()).init(activity, buildScope(), new AuthListener() {
            @Override
            public void onAuthSuccess() {

                SharedPrefrenceUtil.setPrefrence(activity,Constants.Access_token,LISessionManager.getInstance(getApplicationContext()).getSession().getAccessToken().toString());

            }

            @Override
            public void onAuthError(LIAuthError error) {

                Log.e("onAuthError"," -- "+error.toString());

            }
        }, true);
    }

    private static Scope buildScope() {
        return Scope.build(Scope.R_BASICPROFILE, Scope.R_EMAILADDRESS);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        LISessionManager.getInstance(getApplicationContext()).onActivityResult(this,
                requestCode, resultCode, data);
        if(mCallbackManager.onActivityResult(requestCode, resultCode, data)) {
            return;
        }else {
            getUserData();
        }
    }

    private void getUserData() {
        APIHelper apiHelper = APIHelper.getInstance(getApplicationContext());
        apiHelper.getRequest(activity, topCardUrl, new ApiListener() {
            @Override
            public void onApiSuccess(ApiResponse result) {
                try {

                    JSONObject jsonObject = new JSONObject(result.getResponseDataAsJson().toString());

                    SharedPrefrenceUtil.setPrefrence(activity,Constants.emailAddress,jsonObject.getString("emailAddress"));
                    SharedPrefrenceUtil.setPrefrence(activity,Constants.formattedName,jsonObject.getString("formattedName"));
                    SharedPrefrenceUtil.setPrefrence(activity,Constants.pictureUrl,jsonObject.getString("pictureUrl"));
                    SharedPrefrenceUtil.setPrefrence(activity,Constants.publicProfileUrl,jsonObject.getString("publicProfileUrl"));

                    studentLinkedIn();


                } catch (Exception e) {

                    e.printStackTrace();
                }

            }

            @Override
            public void onApiError(LIApiError error) {
            }
        });
    }

    private void studentLinkedIn() {

        Map<String, Object> map = new HashMap<String, Object>();

        map.put("uid",SharedPrefrenceUtil.getPrefrence(activity,Constants.USER_ID,""));
        map.put("image", SharedPrefrenceUtil.getPrefrence(activity,Constants.pictureUrl,""));
        map.put("url", SharedPrefrenceUtil.getPrefrence(activity,Constants.publicProfileUrl,""));

        request.getResponse(activity, map, WebServiceAPI.API_studentLinkedIn, true, new ICallBack() {
            @Override
            public void onCallBackResult(String response) {
                try{

                    JSONObject jsonObject = new JSONObject(response);
                    requestButton();
                    PerfectGraduateApplication.getInstance().displayCustomDialog(activity,"Confirmation","You have successfully signed in to LinkedIn.");

                }catch (JSONException e){
                    e.printStackTrace();
                }
            }
        });

    }

    private void addRightButton() {


        if(messagesVo.getType().equals("studentsociety")){

            for (int i = 0; i < Constants.societyVoList.size(); i++){

                if(Constants.societyVoList.get(i).getId().equals(messagesVo.getAdder_id())){
                    imageView_Societies.setImageResource(R.drawable.removesoc);
                }else{
                    imageView_Societies.setImageResource(R.drawable.addedsoc);
                }
            }
        }else if(messagesVo.getType().equals("companybehalf")){

            for (int i = 0; i < userCompanyVoList.size(); i++){

                if(userCompanyVoList.get(i).getId().equals(messagesVo.getAdder_id())){

                    if(userCompanyVoList.get(i).getPortfolio_status().equals("1")){
                        imageView_Portfolio.setImageResource(R.drawable.addport);
                        imageView_msg.setImageResource(R.drawable.msgdetail);
                        added = "added";
                    }else{
                        imageView_Portfolio.setImageResource(R.drawable.removeport);
                        imageView_msg.setImageResource(R.drawable.msgstop);
                    }
                }else{
                    if(messagesVo.getAddedStatus().equals("1")){
                        imageView_Portfolio.setImageResource(R.drawable.addport);
                    }else{
                        imageView_Portfolio.setImageResource(R.drawable.removeport);
                    }
                }
            }

        }

        if(messagesVo.getType().equals("studentsociety")){

            if(Constants.societyVoList.size() > 0) {

                for (int i = 0; i < Constants.societyVoList.size(); i++) {

                    if (Constants.societyVoList.get(i).getId().equals(messagesVo.getAdder_id())) {

                        //SocietyVo societyVo = Constants.societyVoList.get(i);


                    } else {

                        if (messagesVo.getReport().equals("1")) {
                            imageView_msg.setImageResource(R.drawable.msgstop);
                        } else {
                            imageView_msg.setImageResource(R.drawable.msgdetail);
                        }

                    }
                }
            }else {

                if (messagesVo.getReport().equals("1")) {
                    imageView_msg.setImageResource(R.drawable.msgstop);
                } else {
                    imageView_msg.setImageResource(R.drawable.msgdetail);
                }

            }

        }else{

            for (int i = 0; i < userCompanyVoList.size(); i++){

                if(userCompanyVoList.get(i).getId().equals(messagesVo.getAdder_id())){

                    if(userCompanyVoList.get(i).getMessage_status().equals("1")){

                        imageView_msg.setImageResource(R.drawable.msgstop);

                    }else{

                        imageView_msg.setImageResource(R.drawable.msgdetail);
                    }
                }
            }

        }

    }

    private void addremovePort(String cid) {

        Map<String, Object> map = new HashMap<String, Object>();

        map.put("uid", SharedPrefrenceUtil.getPrefrence(activity, Constants.USER_ID,""));
        map.put("cid",cid);

        request.getResponse(activity, map,WebServiceAPI.API_addportfolio, true, new ICallBack() {
            @Override
            public void onCallBackResult(String response) {

                try {

                    JSONObject jsonObject = new JSONObject(response);

                    added = "added";

                }catch(JSONException k){
                    k.printStackTrace();
                }

            }
        });

    }

    public void updateLikeDislike(String status, String Message_id){

        Map<String, Object> map = new HashMap<String, Object>();

        map.put("user_id", SharedPrefrenceUtil.getPrefrence(activity, Constants.USER_ID,""));
        map.put("Message_id",Message_id);
        map.put("status",status);

        request.getResponse(activity, map,WebServiceAPI.API_MessageLikeDislike, true, new ICallBack() {
            @Override
            public void onCallBackResult(String response) {

                try {

                    JSONObject jsonObject = new JSONObject(response);



                }catch(JSONException k){
                    k.printStackTrace();
                }

            }
        });

    }

    public void updateMsg(String report,String api,String company_id,String tag){

        Map<String, Object> map = new HashMap<String, Object>();

        map.put("user_id", SharedPrefrenceUtil.getPrefrence(activity, Constants.USER_ID,""));
        map.put(tag,company_id);
        map.put("status",report);

       // WebServiceAPI.API_MessageStatus
        request.getResponse(activity, map,api, true, new ICallBack() {
            @Override
            public void onCallBackResult(String response) {

                try {

                    JSONObject jsonObject = new JSONObject(response);

                    String status = jsonObject.getString(WebServiceAPI.status);


                }catch(JSONException k){
                    k.printStackTrace();
                }

            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_right);
        finish();
    }
}
