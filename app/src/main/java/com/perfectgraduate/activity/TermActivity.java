package com.perfectgraduate.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.widget.TextViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;


import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.perfectgraduate.R;
import com.perfectgraduate.api.PerfectGraduateRequest;
import com.perfectgraduate.api.WebServiceAPI;
import com.perfectgraduate.app.PerfectGraduateApplication;
import com.perfectgraduate.database.DatabaseHelper;
import com.perfectgraduate.listener.ICallBack;
import com.perfectgraduate.utility.Constants;
import com.perfectgraduate.utility.SharedPrefrenceUtil;
import com.perfectgraduate.vo.DisciplineVo;
import com.perfectgraduate.vo.LocationVo;
import com.perfectgraduate.vo.SocietyVo;
import com.perfectgraduate.vo.UserCompanyVo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TermActivity extends BaseActivity implements View.OnClickListener {

    TextView ui_terms,textView_submit;
    ImageView ui_close, ui_check;

    boolean checked;
    private PerfectGraduateRequest request;
    private List<SocietyVo> societyVoList;
    private List<UserCompanyVo> userCompanyVoList;
    private List<LocationVo> locationVoList;
    private SharedPreferences preferences;
    private DatabaseHelper databaseHelper;
    StringBuilder result;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_term);

        request = new PerfectGraduateRequest(this);
        preferences = getSharedPreferences("prefrence", Context.MODE_PRIVATE);
        societyVoList = new ArrayList<>();
        userCompanyVoList = new ArrayList<>();
        result = new StringBuilder();
        databaseHelper = new DatabaseHelper(this);

        ui_terms = (TextView) findViewById(R.id.tv_terms);

        ui_close = (ImageView) findViewById(R.id.imv_back);
        textView_submit = (TextView) findViewById(R.id.textView_submit);
        ui_close.setOnClickListener(this);
        textView_submit.setOnClickListener(this);

        ui_check = (ImageView) findViewById(R.id.imv_check);
        ui_check.setOnClickListener(this);

        checked = false;

        setText();
    }

    public void setText() {

        String data = null;

        int resId = R.raw.terms;

        InputStream inputStream = getResources().openRawResource(resId);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        int i;
        try {
            i = inputStream.read();
            while (i != -1) {
                byteArrayOutputStream.write(i);
                i = inputStream.read();
            }

            data = new String(byteArrayOutputStream.toByteArray());
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        ui_terms.setText(data);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.imv_back) {
            onBackPressed();
        } else if (v.getId() == R.id.imv_check) {

            if (checked) {
                ui_check.setImageResource(R.drawable.agree_unchecked);
                checked = false;
                textView_submit.setTextColor(getResources().getColor(R.color.default_btn));
                textView_submit.setBackgroundResource(R.drawable.white_buttonrect);
            } else {
                ui_check.setImageResource(R.drawable.agree_checked);
                checked = true;
                textView_submit.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                textView_submit.setBackgroundResource(R.drawable.white_success_btn_rect);
            }
        }else if(v.getId() == R.id.textView_submit) {
            if (checked){
                if (!PerfectGraduateApplication.getInstance().isConnectInternet()) {
                        PerfectGraduateApplication.getInstance().setShowDialog(this, "", getString(R.string.no_internet_connection));
                } else {
                    if (!SharedPrefrenceUtil.getPrefrence(TermActivity.this, Constants.USER_ID, "").equals("")) {
                        UpStudent();
                    } else {
                        UpdateStudent();
                    }
                }
            }
        }
    }

    // for fb login
    private void UpStudent() {

        Map<String, Object> map = new HashMap<String, Object>();

        map.put("gender",SharedPrefrenceUtil.getPrefrence(this,Constants.Gender,""));
        map.put("id",SharedPrefrenceUtil.getPrefrence(this,Constants.USER_ID,""));
        map.put("cid",SharedPrefrenceUtil.getPrefrence(this,Constants.Country_id,""));
        map.put("sid",SharedPrefrenceUtil.getPrefrence(this,Constants.State_id,""));
        map.put("uid",SharedPrefrenceUtil.getPrefrence(this,Constants.University_id,""));
        map.put("gpid",SharedPrefrenceUtil.getPrefrence(this,Constants.Position_id,""));
        map.put("did",SharedPrefrenceUtil.getPrefrence(this,Constants.Discipline_id,""));
        map.put("sdid",SharedPrefrenceUtil.getPrefrence(this,Constants.SubDiscipline_id,""));
        map.put("did1",SharedPrefrenceUtil.getPrefrence(this,Constants.Discipline2_id,""));
        map.put("sdid1",SharedPrefrenceUtil.getPrefrence(this,Constants.SubDiscipline2_id,""));
        map.put("device", FirebaseInstanceId.getInstance().getToken());
        map.put("device type","android");


        request.getResponse(this, map, WebServiceAPI.API_UpStudent, true, new ICallBack() {
            @Override
            public void onCallBackResult(String response) {

                try {

                    JSONObject jsonObject = new JSONObject(response);

                    String status = jsonObject.getString(WebServiceAPI.status);

                    if(Boolean.parseBoolean(status)){


                        addAllSociety();

                    }else{
                        UpStudent();
                    }

                }catch(JSONException k){
                    k.printStackTrace();
                }

            }
        });
        /*Intent intent = new Intent(TermActivity.this, HomeActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);*/

    }

    //for tem login
    private void UpdateStudent() {

        Map<String, Object> map = new HashMap<String, Object>();

        map.put("gender",Constants.Gender);
        map.put("id",SharedPrefrenceUtil.getPrefrence(this,Constants.USER_ID,""));
        map.put("cid",SharedPrefrenceUtil.getPrefrence(this,Constants.Country_id,""));
        map.put("sid",SharedPrefrenceUtil.getPrefrence(this,Constants.State_id,""));
        map.put("uid",SharedPrefrenceUtil.getPrefrence(this,Constants.University_id,""));
        map.put("gpid",SharedPrefrenceUtil.getPrefrence(this,Constants.Position_id,""));
        map.put("did",SharedPrefrenceUtil.getPrefrence(this,Constants.Discipline_id,""));
        map.put("sdid",SharedPrefrenceUtil.getPrefrence(this,Constants.SubDiscipline_id,""));
        map.put("did1",SharedPrefrenceUtil.getPrefrence(this,Constants.Discipline2_id,""));
        map.put("sdid1",SharedPrefrenceUtil.getPrefrence(this,Constants.SubDiscipline2_id,""));
        map.put("device", FirebaseInstanceId.getInstance().getToken());
        map.put("device type","android");


        request.getResponse(this, map, WebServiceAPI.API_UpdateStudent, true, new ICallBack() {
            @Override
            public void onCallBackResult(String response) {

                try {

                    JSONObject jsonObject = new JSONObject(response);

                    String status = jsonObject.getString(WebServiceAPI.status);

                    if(Boolean.parseBoolean(status)){

                        SharedPrefrenceUtil.setPrefrence(TermActivity.this,Constants.USER_ID,jsonObject.getString("user_id"));
                        addAllSociety();

                    }else{
                        UpdateStudent();
                    }

                }catch(JSONException k){
                    k.printStackTrace();
                }

            }
        });

    }

    private void addAllSociety() {

        Map<String, Object> map = new HashMap<String, Object>();


        map.put("sid",Constants.Society_id);
        map.put("uid",SharedPrefrenceUtil.getPrefrence(this,Constants.USER_ID,""));


        request.getResponse(this, map, WebServiceAPI.API_addAllSociety, false, new ICallBack() {
            @Override
            public void onCallBackResult(String response) {

                try {

                    JSONObject jsonObject = new JSONObject(response);

                    String status = jsonObject.getString(WebServiceAPI.status);

                    if(Boolean.parseBoolean(status)){

                        JSONArray jsonArray = jsonObject.getJSONArray(WebServiceAPI.data);

                        if(jsonArray != null && jsonArray.length() > 0){

                            for (int i = 0; i < jsonArray.length(); i++){

                                JSONObject object = jsonArray.getJSONObject(i);

                                SocietyVo societyVo = new SocietyVo();

                                societyVo.setId(object.getString("id"));
                                societyVo.setSociety_id(object.getString("society_id"));
                                societyVo.setParent_id(object.getString("parent_id"));
                                societyVo.setStaff_id(object.getString("staff_id"));
                                societyVo.setCreator_name(object.getString("creator_name"));
                                societyVo.setWebsite(object.getString("website"));
                                societyVo.setSociety_email(object.getString("society_email"));
                                societyVo.setPassword(object.getString("password"));
                                societyVo.setFirst_login_status(object.getString("first_login_status"));
                                societyVo.setChange_pass(object.getString("change_pass"));
                                societyVo.setPass(object.getString("pass"));
                                societyVo.setUni_id(object.getString("uni_id"));
                                societyVo.setCountry_id(object.getString("country_id"));
                                societyVo.setState_id(object.getString("state_id"));
                                societyVo.setSocietylogo(object.getString("societylogo"));
                                societyVo.setCreator_mobile(object.getString("creator_mobile"));
                                societyVo.setSociety_name(object.getString("society_name"));
                                societyVo.setSociety_president(object.getString("society_president"));
                                societyVo.setPresident_email(object.getString("president_email"));
                                societyVo.setPresident_mobile(object.getString("president_mobile"));
                                societyVo.setTreasurer_name(object.getString("Treasurer_name"));
                                societyVo.setTreasurer_email(object.getString("Treasurer_email"));
                                societyVo.setTreasure_mobile(object.getString("Treasure_mobile"));
                                societyVo.setSecetery_name(object.getString("Secetery_name"));
                                societyVo.setSecetery_email(object.getString("Secetery_email"));
                                societyVo.setSecetery_mobile(object.getString("Secetery_mobile"));
                                societyVo.setFacebookemail(object.getString("facebookemail"));
                                societyVo.setUser_type(object.getString("user_type"));
                                societyVo.setStatus(object.getString("status"));
                                societyVo.setLocks(object.getString("locks"));
                                societyVo.setDate(object.getString("date"));
                                societyVo.setKey(object.getString("key"));
                                societyVo.setCreated_by(object.getString("created_by"));
                                societyVo.setModified_date(object.getString("modified_date"));
                                societyVo.setSociety_code(object.getString("society_code"));
                                societyVo.setLogo(object.getString("logo"));

                                societyVoList.add(societyVo);

                            }

                        }

                        societyDetail();

                    }else{

                    }

                }catch(JSONException k){
                    k.printStackTrace();
                }

            }
        });

    }

    private void societyDetail() {

        Map<String, Object> map = new HashMap<String, Object>();

        map.put("uid",SharedPrefrenceUtil.getPrefrence(this,Constants.USER_ID,""));


        request.getResponse(this, map, WebServiceAPI.API_societyDetail, false, new ICallBack() {
            @Override
            public void onCallBackResult(String response) {

                try {

                    JSONObject jsonObject = new JSONObject(response);

                    String status = jsonObject.getString(WebServiceAPI.status);

                    String society = jsonObject.getString("society");

                    if(Boolean.parseBoolean(status)){

                        JSONArray jsonArray = jsonObject.getJSONArray(WebServiceAPI.data);

                        if(jsonArray != null && jsonArray.length() > 0){

                            for (int i = 0; i < jsonArray.length(); i++){

                                JSONObject object = jsonArray.getJSONObject(i);

                                SocietyVo societyVo = new SocietyVo();

                                societyVo.setId(object.getString("id"));
                                societyVo.setSociety_id(object.getString("society_id"));
                                societyVo.setParent_id(object.getString("parent_id"));
                                societyVo.setStaff_id(object.getString("staff_id"));
                                societyVo.setCreator_name(object.getString("creator_name"));
                                societyVo.setWebsite(object.getString("website"));
                                societyVo.setSociety_email(object.getString("society_email"));
                                societyVo.setPassword(object.getString("password"));
                                societyVo.setFirst_login_status(object.getString("first_login_status"));
                                societyVo.setChange_pass(object.getString("change_pass"));
                                societyVo.setPass(object.getString("pass"));
                                societyVo.setUni_id(object.getString("uni_id"));
                                societyVo.setCountry_id(object.getString("country_id"));
                                societyVo.setState_id(object.getString("state_id"));
                                societyVo.setSocietylogo(object.getString("societylogo"));
                                societyVo.setCreator_mobile(object.getString("creator_mobile"));
                                societyVo.setSociety_name(object.getString("society_name"));
                                societyVo.setSociety_president(object.getString("society_president"));
                                societyVo.setPresident_email(object.getString("president_email"));
                                societyVo.setPresident_mobile(object.getString("president_mobile"));
                                societyVo.setTreasurer_name(object.getString("Treasurer_name"));
                                societyVo.setTreasurer_email(object.getString("Treasurer_email"));
                                societyVo.setTreasure_mobile(object.getString("Treasure_mobile"));
                                societyVo.setSecetery_name(object.getString("Secetery_name"));
                                societyVo.setSecetery_email(object.getString("Secetery_email"));
                                societyVo.setSecetery_mobile(object.getString("Secetery_mobile"));
                                societyVo.setFacebookemail(object.getString("facebookemail"));
                                societyVo.setUser_type(object.getString("user_type"));
                                societyVo.setStatus(object.getString("status"));
                                societyVo.setLocks(object.getString("locks"));
                                societyVo.setDate(object.getString("date"));
                                societyVo.setKey(object.getString("key"));
                                societyVo.setCreated_by(object.getString("created_by"));
                                societyVo.setModified_date(object.getString("modified_date"));
                                societyVo.setSociety_code(object.getString("society_code"));
                                societyVo.setLogo(object.getString("logo"));

                                societyVoList.add(societyVo);

                            }

                            SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                            SharedPreferences.Editor editor = sharedPrefs.edit();
                            Gson gson = new Gson();
                            String json = gson.toJson(societyVoList);
                            editor.putString("societyDetail", json);
                            editor.apply();
                        }
                        getAllcompany();
                        //user_company();
                    }else{

                    }

                }catch(JSONException k){
                    k.printStackTrace();
                }

            }
        });

    }

    private void getAllcompany() {

        Map<String, Object> map = new HashMap<String, Object>();

        map.put("id",SharedPrefrenceUtil.getPrefrence(TermActivity.this,Constants.USER_ID,""));


        request.getResponse(TermActivity.this, map, WebServiceAPI.API_getAllCompanies, false, new ICallBack() {
            @Override
            public void onCallBackResult(String response) {

                try {

                    JSONObject jsonObject = new JSONObject(response);

                    String status = jsonObject.getString(WebServiceAPI.status);

                    if(Boolean.parseBoolean(status)){

                        JSONArray jsonArray = jsonObject.getJSONArray(WebServiceAPI.data);

                        if(SharedPrefrenceUtil.getPrefrence(TermActivity.this,Constants.isAllCompany,"").equals("Database")){

                            SharedPreferences.Editor editor = preferences.edit();
                            editor.putBoolean("login", true);
                            editor.apply();

                            Intent stdDetailIntent = new Intent(TermActivity.this, HomeActivity.class);
                            stdDetailIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(stdDetailIntent);
                            overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_left);
                            finish();

                        }else{
                            getAllcompany();
                        }


                        if(jsonArray != null && jsonArray.length() > 0){

                            databaseHelper.insertAllCompanies(TermActivity.this, jsonArray);

                        }
                    }

                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putBoolean("login", true);
                    editor.apply();

                    Intent stdDetailIntent = new Intent(TermActivity.this, HomeActivity.class);
                    stdDetailIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(stdDetailIntent);
                    overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_left);
                    finish();

                }catch(JSONException k){
                    k.printStackTrace();
                }

            }
        });

    }

    private void user_company() {

        Map<String, Object> map = new HashMap<String, Object>();

        map.put("uid",SharedPrefrenceUtil.getPrefrence(this,Constants.USER_ID,""));


        request.getResponse(this, map, WebServiceAPI.API_user_company, false, new ICallBack() {
            @Override
            public void onCallBackResult(String response) {

                try {

                    JSONObject jsonObject = new JSONObject(response);

                    String status = jsonObject.getString(WebServiceAPI.status);

                    if(Boolean.parseBoolean(status)){

                        JSONArray jsonArray = jsonObject.getJSONArray(WebServiceAPI.data);

                        if(jsonArray != null && jsonArray.length() > 0){



                        }

                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putBoolean("login", true);
                        editor.apply();

                        Intent intent = new Intent(TermActivity.this,HomeActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_left);
                        finish();

                    }else{

                    }

                }catch(JSONException k){
                    k.printStackTrace();
                }

            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_right);
        finish();
    }
}
