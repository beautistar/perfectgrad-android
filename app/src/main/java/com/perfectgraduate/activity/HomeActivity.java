package com.perfectgraduate.activity;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.preference.PreferenceManager;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.linkedin.platform.APIHelper;
import com.linkedin.platform.LISessionManager;
import com.linkedin.platform.errors.LIApiError;
import com.linkedin.platform.errors.LIAuthError;
import com.linkedin.platform.listeners.ApiListener;
import com.linkedin.platform.listeners.ApiResponse;
import com.linkedin.platform.listeners.AuthListener;
import com.linkedin.platform.utils.Scope;
import com.perfectgraduate.R;
import com.perfectgraduate.adapter.NavDrawerListAdapter;
import com.perfectgraduate.api.PerfectGraduateRequest;
import com.perfectgraduate.api.WebServiceAPI;
import com.perfectgraduate.app.PerfectGraduateApplication;
import com.perfectgraduate.database.DatabaseHelper;
import com.perfectgraduate.fragment.AllCompanyFragment;
import com.perfectgraduate.fragment.CountryFilterFragment;
import com.perfectgraduate.fragment.DetailFragment;
import com.perfectgraduate.fragment.DisciplinesFragment;
import com.perfectgraduate.fragment.EventsDetailsFragment;
import com.perfectgraduate.fragment.HomeFragment;
import com.perfectgraduate.fragment.LocationFilterFragment;
import com.perfectgraduate.fragment.MessagesFragment;
import com.perfectgraduate.fragment.PortfolioFragment;
import com.perfectgraduate.fragment.SalaryGuideFragment;
import com.perfectgraduate.fragment.SearchCountryFilterFragment;
import com.perfectgraduate.fragment.SearchFragment;
import com.perfectgraduate.fragment.SearchLocationFilterFragment;
import com.perfectgraduate.fragment.SocietyFragment;
import com.perfectgraduate.fragment.StudentDetailsFragment;
import com.perfectgraduate.fragment.StudentSocietyFragment;
import com.perfectgraduate.listener.ICallBack;
import com.perfectgraduate.utility.Constants;
import com.perfectgraduate.utility.SharedPrefrenceUtil;
import com.perfectgraduate.vo.MessagesVo;
import com.perfectgraduate.vo.NavDrawerItem;
import com.perfectgraduate.vo.UserCompanyVo;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener {


    private PerfectGraduateRequest request;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
    private CharSequence mTitle;
    private CharSequence mDrawerTitle;
    private TextView textView_header;
    // slide menu items
    private String[] navMenuTitles;
    private TypedArray navMenuIcons;
    private FrameLayout frame_container;
    private ArrayList<NavDrawerItem> navDrawerItems;
    private NavDrawerListAdapter adapter;
    private LinearLayout drawer;

    public static SalaryGuideFragment salaryGuideFragment;
    public static DetailFragment detailFragment;
    private PortfolioFragment portfolioFragment;
    private SocietyFragment societyFragment;
    private HomeFragment homeFragment;

    //Header

    private ImageView imageView_edit,imageView_students,imageView_logo,imageView_filter,imageView_info,imageView_calender,imageView_searchfilter
            ,imageView_backsearch,imageView_logo_portfolio;
    private TextView textView_subheader,textView_cancel,textView_select,textView_candidate,textView_cancelsearch,textView_selectsearch;
    private ImageView imageView_back,txt_menu;

    //Bottom bar
    private LinearLayout linearLayout_red_ribbon,tabBar_layout;

    private ImageView imageView_Portfolio,imageView_Disciplines,imageView_Search,imageView_Messages,imageView_Salary,imageView_star;

    private TextView textView_Portfolio,textView_Disciplines,textView_Search,textView_Messages,textView_Salary,admin_badge,student_badge;


    private DatabaseHelper databaseHelper;
    CallbackManager mCallbackManager;
    //Linkedin

    private static final String host = "api.linkedin.com";
    private static final String topCardUrl = "https://" + host + "/v1/people/~:" +
            "(email-address,formatted-name,phone-numbers,public-profile-url,picture-url,picture-urls::(original))";

    SharedPreferences sharedPrefs;
    SharedPreferences.Editor editor;
    Gson gson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        getSupportActionBar().hide();

        request = new PerfectGraduateRequest(this);

        databaseHelper = new DatabaseHelper(this);

        FacebookSdk.sdkInitialize(HomeActivity.this.getApplicationContext());

        portfolioFragment = new PortfolioFragment();
        societyFragment = new SocietyFragment();
        homeFragment = new HomeFragment();
        detailFragment = new DetailFragment();

        mCallbackManager = CallbackManager.Factory.create();

        sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        editor = sharedPrefs.edit();
        gson = new Gson();

        LoginManager.getInstance().registerCallback(mCallbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        if (AccessToken.getCurrentAccessToken() != null) {
                            RequestData();
                        }
                    }

                    @Override
                    public void onCancel() {
                    }

                    @Override
                    public void onError(FacebookException exception) {
                    }
                });

        try {
            if (!databaseHelper.checkDataBase()) {
                databaseHelper.createDataBase(0, "");
                databaseHelper.close();
            }
        } catch (IOException e1) {
            e1.printStackTrace();
        }

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        frame_container = (FrameLayout) findViewById(R.id.frame_container);
        textView_header = (TextView) findViewById(R.id.textView_header);
        linearLayout_red_ribbon = (LinearLayout) findViewById(R.id.linearLayout_red_ribbon);
        tabBar_layout = (LinearLayout) findViewById(R.id.tabBar_layout);

        ((LinearLayout) findViewById(R.id.linearLayout_tab_Portfolio)).setOnClickListener(this);;
        ((LinearLayout) findViewById(R.id.linearLayout_tab_Disciplines)).setOnClickListener(this);
        ((LinearLayout) findViewById(R.id.linearLayout_tab_Search)).setOnClickListener(this);
        ((LinearLayout) findViewById(R.id.linearLayout_tab_Messages)).setOnClickListener(this);
        ((LinearLayout) findViewById(R.id.linearLayout_tab_Salary)).setOnClickListener(this);


        imageView_Portfolio = (ImageView) findViewById(R.id.imageView_Portfolio);
        imageView_Disciplines = (ImageView) findViewById(R.id.imageView_Disciplines);
        imageView_Search = (ImageView) findViewById(R.id.imageView_Search);
        imageView_Messages = (ImageView) findViewById(R.id.imageView_Messages);
        imageView_Salary = (ImageView) findViewById(R.id.imageView_Salary);
        imageView_star = (ImageView) findViewById(R.id.imageView_star);

        textView_Portfolio = (TextView) findViewById(R.id.textView_Portfolio);
        textView_Disciplines = (TextView) findViewById(R.id.textView_Disciplines);
        textView_Messages = (TextView) findViewById(R.id.textView_Messages);
        textView_Search = (TextView) findViewById(R.id.textView_Search);
        textView_Salary = (TextView) findViewById(R.id.textView_Salary);

        imageView_edit = (ImageView) findViewById(R.id.imageView_edit);
        imageView_students = (ImageView) findViewById(R.id.imageView_students);
        imageView_logo = (ImageView) findViewById(R.id.imageView_logo);
        imageView_filter = (ImageView) findViewById(R.id.imageView_filter);
        imageView_searchfilter = (ImageView) findViewById(R.id.imageView_searchfilter);
        imageView_info = (ImageView) findViewById(R.id.imageView_info);
        imageView_calender = (ImageView) findViewById(R.id.imageView_calender);
        imageView_back = (ImageView) findViewById(R.id.imageView_back);
        imageView_backsearch = (ImageView) findViewById(R.id.imageView_backsearch);
        imageView_logo_portfolio = (ImageView) findViewById(R.id.imageView_logo_portfolio);

        txt_menu = (ImageView) findViewById(R.id.txt_menu);
        textView_cancel = (TextView) findViewById(R.id.textView_cancel);
        textView_cancelsearch = (TextView) findViewById(R.id.textView_cancelsearch);
        textView_select = (TextView) findViewById(R.id.textView_select);
        textView_selectsearch = (TextView) findViewById(R.id.textView_selectsearch);
        admin_badge = (TextView) findViewById(R.id.admin_badge);
        student_badge = (TextView) findViewById(R.id.student_badge);

        textView_subheader = (TextView) findViewById(R.id.textView_subheader);;

        imageView_edit.setOnClickListener(this);
        imageView_students.setOnClickListener(this);
        imageView_logo.setOnClickListener(this);
        imageView_filter.setOnClickListener(this);
        imageView_info.setOnClickListener(this);
        imageView_back.setOnClickListener(this);
        textView_cancel.setOnClickListener(this);
        textView_select.setOnClickListener(this);
        imageView_calender.setOnClickListener(this);
        imageView_searchfilter.setOnClickListener(this);
        textView_cancelsearch.setOnClickListener(this);
        textView_selectsearch.setOnClickListener(this);
        imageView_backsearch.setOnClickListener(this);
        imageView_logo_portfolio.setOnClickListener(this);


        drawer = (LinearLayout) findViewById(R.id.drawer);
        // load slide menu items

        starVisible();

        textView_candidate = (TextView) findViewById(R.id.textView_candidate);

        textView_candidate.setShadowLayer(
                1f, // radius
                2.5f, // dx
                2.5f, // dy
                Color.parseColor("#000000"));

        drawerMenu();


        if (savedInstanceState == null) {
            // on first time display view for first nav item
            displayView(0);
        }



        txt_menu.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
                    mDrawerLayout.closeDrawers();
                } else {
                    mDrawerLayout.openDrawer(GravityCompat.START);
                }
            }
        });


        linearLayout_red_ribbon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeActivity.this,RedRibbonActivity.class);
                startActivity(intent);
            }
        });

    }

    public void starVisible() {

        if(SharedPrefrenceUtil.getPrefrence(HomeActivity.this,Constants.isMessages,"").equals("Database_messages"))
            imageView_star.setVisibility(View.INVISIBLE);
        else
            imageView_star.setVisibility(View.VISIBLE);

    }

    private void drawerMenu() {

        if(!(SharedPrefrenceUtil.getPrefrence(HomeActivity.this, Constants.FB_ID,"").equals("")) && !(SharedPrefrenceUtil.getPrefrence(HomeActivity.this,Constants.emailAddress,"").equals(""))){
            Log.e("fb_and_linkedin"," --- ");
            navMenuTitles = getResources().getStringArray(R.array.fb_and_linkedin);

            navMenuIcons = getResources()
                    .obtainTypedArray(R.array.fb_and_linkedin_icons);
        }else if(!SharedPrefrenceUtil.getPrefrence(HomeActivity.this, Constants.FB_ID,"").equals("")){
            Log.e("fb_only"," --- ");
            navMenuTitles = getResources().getStringArray(R.array.fb_only);

            navMenuIcons = getResources()
                    .obtainTypedArray(R.array.fb_only_icons);
        }else if(!SharedPrefrenceUtil.getPrefrence(HomeActivity.this, Constants.emailAddress,"").equals("")){
            Log.e("linkdin_only"," --- ");
            navMenuTitles = getResources().getStringArray(R.array.linkdin_only);

            navMenuIcons = getResources()
                    .obtainTypedArray(R.array.linkdin_only_icons);
        }else{
            Log.e("no_fb_no_linkedin"," --- ");
            navMenuTitles = getResources().getStringArray(R.array.no_fb_no_linkedin);

            navMenuIcons = getResources()
                    .obtainTypedArray(R.array.no_fb_no_linkedin_icons);
        }

        mTitle = mDrawerTitle = getTitle();

        mDrawerList = (ListView) findViewById(R.id.list_slidermenu);

        navDrawerItems = new ArrayList<NavDrawerItem>();

        // adding nav drawer items to array
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[0], navMenuIcons
                .getResourceId(0, -1)));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[1], navMenuIcons
                .getResourceId(1, -1)));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[2], navMenuIcons
                .getResourceId(2, -1)));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[3], navMenuIcons
                .getResourceId(3, -1)));

        if(!(SharedPrefrenceUtil.getPrefrence(HomeActivity.this, Constants.FB_ID,"").equals("")) && !(SharedPrefrenceUtil.getPrefrence(HomeActivity.this,Constants.emailAddress,"").equals(""))){

            navDrawerItems.add(new NavDrawerItem(navMenuTitles[4], navMenuIcons
                    .getResourceId(4, -1)));

        }else if(!SharedPrefrenceUtil.getPrefrence(HomeActivity.this, Constants.FB_ID,"").equals("")){

            navDrawerItems.add(new NavDrawerItem(navMenuTitles[4], navMenuIcons
                    .getResourceId(4, -1)));
            navDrawerItems.add(new NavDrawerItem(navMenuTitles[5], navMenuIcons
                    .getResourceId(5, -1)));

        }else if(!SharedPrefrenceUtil.getPrefrence(HomeActivity.this, Constants.emailAddress,"").equals("")){

            navDrawerItems.add(new NavDrawerItem(navMenuTitles[4], navMenuIcons
                    .getResourceId(4, -1)));
        }else{

            navDrawerItems.add(new NavDrawerItem(navMenuTitles[4], navMenuIcons
                    .getResourceId(4, -1)));
            navDrawerItems.add(new NavDrawerItem(navMenuTitles[5], navMenuIcons
                    .getResourceId(5, -1)));
            navDrawerItems.add(new NavDrawerItem(navMenuTitles[6], navMenuIcons
                    .getResourceId(6, -1)));
        }


        // setting the nav drawer list_group adapter
        adapter = new NavDrawerListAdapter(getApplicationContext(),
                navDrawerItems);
        mDrawerList.setAdapter(adapter);
        mDrawerList.setOnItemClickListener(new SlideMenuClickListener());


        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.drawable.ic_menu, //nav menu toggle icon
                R.string.app_name, // nav drawer open - description for accessibility
                R.string.app_name // nav drawer close - description for accessibility
        ) {
            public void onDrawerClosed(View view) {
                getSupportActionBar().setTitle(mTitle);
                // calling onPrepareOptionsMenu() to show action bar icons
                invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                getSupportActionBar().setTitle(mDrawerTitle);
                // calling onPrepareOptionsMenu() to hide action bar icons
                invalidateOptionsMenu();
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.linearLayout_tab_Portfolio:

                if(frame_container != null){
                    frame_container.removeAllViews();
                }

                imageView_Portfolio.setImageResource(R.drawable.portfolio_selected);
                imageView_Disciplines.setImageResource(R.drawable.discipline_unselected);
                imageView_Search.setImageResource(R.drawable.search_unselected);
                imageView_Messages.setImageResource(R.drawable.messages_unselected);
                imageView_Salary.setImageResource(R.drawable.sallary_unselected);

                textView_Portfolio.setTextColor(getResources().getColor(R.color.tab_txt_color));
                textView_Disciplines.setTextColor(getResources().getColor(R.color.grey));
                textView_Messages.setTextColor(getResources().getColor(R.color.grey));
                textView_Search.setTextColor(getResources().getColor(R.color.grey));
                textView_Salary.setTextColor(getResources().getColor(R.color.grey));

                textView_header.setText("Portfolio");
                admin_badge.setVisibility(View.INVISIBLE);
                student_badge.setVisibility(View.INVISIBLE);
                imageView_back.setVisibility(View.INVISIBLE);
                imageView_backsearch.setVisibility(View.INVISIBLE);
                textView_cancel.setVisibility(View.INVISIBLE);
                textView_select.setVisibility(View.INVISIBLE);
                textView_cancelsearch.setVisibility(View.INVISIBLE);
                textView_selectsearch.setVisibility(View.INVISIBLE);
                txt_menu.setVisibility(View.VISIBLE);
                imageView_filter.setVisibility(View.INVISIBLE);
                imageView_searchfilter.setVisibility(View.INVISIBLE);
                imageView_calender.setVisibility(View.INVISIBLE);
                imageView_info.setVisibility(View.INVISIBLE);
                textView_subheader.setVisibility(View.VISIBLE);
                imageView_edit.setVisibility(View.VISIBLE);
                imageView_students.setVisibility(View.VISIBLE);
                imageView_logo.setVisibility(View.VISIBLE);
                tabBar_layout.setVisibility(View.VISIBLE);
                linearLayout_red_ribbon.setVisibility(View.VISIBLE);

                starVisible();

                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.frame_container, new HomeFragment()).commit();

                break;
            case R.id.linearLayout_tab_Disciplines:
                imageView_Portfolio.setImageResource(R.drawable.portfolio_unselected);
                imageView_Disciplines.setImageResource(R.drawable.discipline_selected);
                imageView_Search.setImageResource(R.drawable.search_unselected);
                imageView_Messages.setImageResource(R.drawable.messages_unselected);
                imageView_Salary.setImageResource(R.drawable.sallary_unselected);

                textView_Portfolio.setTextColor(getResources().getColor(R.color.grey));
                textView_Disciplines.setTextColor(getResources().getColor(R.color.tab_txt_color));
                textView_Search.setTextColor(getResources().getColor(R.color.grey));
                textView_Messages.setTextColor(getResources().getColor(R.color.grey));
                textView_Salary.setTextColor(getResources().getColor(R.color.grey));

                textView_header.setText("Disciplines");
                imageView_back.setVisibility(View.INVISIBLE);
                imageView_backsearch.setVisibility(View.INVISIBLE);
                textView_cancel.setVisibility(View.INVISIBLE);
                textView_select.setVisibility(View.INVISIBLE);
                textView_cancelsearch.setVisibility(View.INVISIBLE);
                textView_selectsearch.setVisibility(View.INVISIBLE);
                txt_menu.setVisibility(View.VISIBLE);
                imageView_filter.setVisibility(View.VISIBLE);
                imageView_searchfilter.setVisibility(View.INVISIBLE);
                imageView_info.setVisibility(View.INVISIBLE);
                imageView_calender.setVisibility(View.INVISIBLE);
                textView_subheader.setVisibility(View.VISIBLE);
                imageView_edit.setVisibility(View.INVISIBLE);
                imageView_students.setVisibility(View.INVISIBLE);
                imageView_logo.setVisibility(View.INVISIBLE);
                tabBar_layout.setVisibility(View.VISIBLE);
                linearLayout_red_ribbon.setVisibility(View.GONE);
                hideBadges();
                starVisible();

                fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.frame_container, new DisciplinesFragment()).commit();
                break;
            case R.id.linearLayout_tab_Search:

                Constants.isSearch = "search";

                imageView_Portfolio.setImageResource(R.drawable.portfolio_unselected);
                imageView_Disciplines.setImageResource(R.drawable.discipline_unselected);
                imageView_Search.setImageResource(R.drawable.search_selected);
                imageView_Messages.setImageResource(R.drawable.messages_unselected);
                imageView_Salary.setImageResource(R.drawable.sallary_unselected);

                textView_Portfolio.setTextColor(getResources().getColor(R.color.grey));
                textView_Disciplines.setTextColor(getResources().getColor(R.color.grey));
                textView_Search.setTextColor(getResources().getColor(R.color.tab_txt_color));
                textView_Messages.setTextColor(getResources().getColor(R.color.grey));
                textView_Salary.setTextColor(getResources().getColor(R.color.grey));

                textView_header.setText("Company Search");
                textView_subheader.setText("All Locations");
                imageView_back.setVisibility(View.INVISIBLE);
                imageView_backsearch.setVisibility(View.INVISIBLE);
                textView_cancel.setVisibility(View.INVISIBLE);
                textView_select.setVisibility(View.INVISIBLE);
                textView_cancelsearch.setVisibility(View.INVISIBLE);
                textView_selectsearch.setVisibility(View.INVISIBLE);
                txt_menu.setVisibility(View.VISIBLE);
                imageView_filter.setVisibility(View.INVISIBLE);
                imageView_searchfilter.setVisibility(View.VISIBLE);
                imageView_info.setVisibility(View.INVISIBLE);
                imageView_calender.setVisibility(View.INVISIBLE);
                textView_subheader.setVisibility(View.VISIBLE);
                imageView_edit.setVisibility(View.INVISIBLE);
                imageView_students.setVisibility(View.INVISIBLE);
                imageView_logo.setVisibility(View.INVISIBLE);
                tabBar_layout.setVisibility(View.VISIBLE);
                linearLayout_red_ribbon.setVisibility(View.GONE);
                hideBadges();
                starVisible();

                fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.frame_container, new SearchFragment()).commit();

                break;
            case R.id.linearLayout_tab_Messages:
                imageView_Portfolio.setImageResource(R.drawable.portfolio_unselected);
                imageView_Disciplines.setImageResource(R.drawable.discipline_unselected);
                imageView_Search.setImageResource(R.drawable.search_unselected);
                imageView_Messages.setImageResource(R.drawable.message_selected);
                imageView_Salary.setImageResource(R.drawable.sallary_unselected);

                textView_Portfolio.setTextColor(getResources().getColor(R.color.grey));
                textView_Disciplines.setTextColor(getResources().getColor(R.color.grey));
                textView_Search.setTextColor(getResources().getColor(R.color.grey));
                textView_Messages.setTextColor(getResources().getColor(R.color.tab_txt_color));
                textView_Salary.setTextColor(getResources().getColor(R.color.grey));

                Constants.message = "messages tab";

                textView_header.setText("Companies");
                textView_subheader.setText(SharedPrefrenceUtil.getPrefrence(HomeActivity.this,Constants.subHeader,""));
                imageView_back.setVisibility(View.INVISIBLE);
                imageView_backsearch.setVisibility(View.INVISIBLE);
                textView_cancel.setVisibility(View.INVISIBLE);
                textView_select.setVisibility(View.INVISIBLE);
                textView_cancelsearch.setVisibility(View.INVISIBLE);
                textView_selectsearch.setVisibility(View.INVISIBLE);
                txt_menu.setVisibility(View.VISIBLE);
                imageView_filter.setVisibility(View.INVISIBLE);
                imageView_searchfilter.setVisibility(View.INVISIBLE);
                imageView_calender.setVisibility(View.VISIBLE);
                imageView_info.setVisibility(View.INVISIBLE);
                textView_subheader.setVisibility(View.VISIBLE);
                imageView_edit.setVisibility(View.INVISIBLE);
                imageView_students.setVisibility(View.INVISIBLE);
                imageView_logo.setVisibility(View.INVISIBLE);
                tabBar_layout.setVisibility(View.VISIBLE);
                linearLayout_red_ribbon.setVisibility(View.GONE);
                hideBadges();
                starVisible();

                fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.frame_container, new MessagesFragment()).commit();

                break;
            case R.id.linearLayout_tab_Salary:
                imageView_Portfolio.setImageResource(R.drawable.portfolio_unselected);
                imageView_Disciplines.setImageResource(R.drawable.discipline_unselected);
                imageView_Search.setImageResource(R.drawable.search_unselected);
                imageView_Messages.setImageResource(R.drawable.messages_unselected);
                imageView_Salary.setImageResource(R.drawable.salary_selected);

                textView_header.setText("Salary Guide");
                textView_subheader.setText("India 2016");
                imageView_back.setVisibility(View.GONE);
                imageView_backsearch.setVisibility(View.INVISIBLE);
                textView_cancel.setVisibility(View.INVISIBLE);
                textView_select.setVisibility(View.INVISIBLE);
                textView_cancelsearch.setVisibility(View.INVISIBLE);
                textView_selectsearch.setVisibility(View.INVISIBLE);
                txt_menu.setVisibility(View.VISIBLE);
                imageView_filter.setVisibility(View.INVISIBLE);
                imageView_searchfilter.setVisibility(View.INVISIBLE);
                imageView_calender.setVisibility(View.INVISIBLE);
                imageView_info.setVisibility(View.VISIBLE);
                textView_subheader.setVisibility(View.VISIBLE);
                imageView_edit.setVisibility(View.INVISIBLE);
                imageView_students.setVisibility(View.INVISIBLE);
                imageView_logo.setVisibility(View.INVISIBLE);
                tabBar_layout.setVisibility(View.VISIBLE);
                linearLayout_red_ribbon.setVisibility(View.GONE);

                textView_Portfolio.setTextColor(getResources().getColor(R.color.grey));
                textView_Disciplines.setTextColor(getResources().getColor(R.color.grey));
                textView_Search.setTextColor(getResources().getColor(R.color.grey));
                textView_Messages.setTextColor(getResources().getColor(R.color.grey));
                textView_Salary.setTextColor(getResources().getColor(R.color.tab_txt_color));


                hideBadges();
                starVisible();

                salaryGuideFragment = new SalaryGuideFragment();
                fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.frame_container, salaryGuideFragment).commit();


                break;
            case R.id.imageView_edit:
                if(Constants.isVisibleSociety) {
                    portfolioFragment.displayRemove();
                }else{
                    societyFragment.displayRemove();
                }
                break;
            case R.id.imageView_logo:

                imageView_Portfolio.setImageResource(R.drawable.portfolio_unselected);
                imageView_Disciplines.setImageResource(R.drawable.discipline_unselected);
                imageView_Search.setImageResource(R.drawable.search_unselected);
                imageView_Messages.setImageResource(R.drawable.message_selected);
                imageView_Salary.setImageResource(R.drawable.sallary_unselected);

                textView_Portfolio.setTextColor(getResources().getColor(R.color.grey));
                textView_Disciplines.setTextColor(getResources().getColor(R.color.grey));
                textView_Search.setTextColor(getResources().getColor(R.color.grey));
                textView_Messages.setTextColor(getResources().getColor(R.color.tab_txt_color));
                textView_Salary.setTextColor(getResources().getColor(R.color.grey));


                textView_header.setText(getString(R.string.app_name));
                textView_subheader.setText(SharedPrefrenceUtil.getPrefrence(HomeActivity.this,Constants.subHeader, ""));
                imageView_back.setVisibility(View.VISIBLE);
                imageView_backsearch.setVisibility(View.INVISIBLE);
                textView_cancel.setVisibility(View.INVISIBLE);
                textView_select.setVisibility(View.INVISIBLE);
                textView_cancelsearch.setVisibility(View.INVISIBLE);
                textView_selectsearch.setVisibility(View.INVISIBLE);
                txt_menu.setVisibility(View.INVISIBLE);
                imageView_filter.setVisibility(View.INVISIBLE);
                imageView_searchfilter.setVisibility(View.INVISIBLE);
                imageView_calender.setVisibility(View.VISIBLE);
                imageView_info.setVisibility(View.INVISIBLE);
                textView_subheader.setVisibility(View.VISIBLE);
                imageView_edit.setVisibility(View.INVISIBLE);
                imageView_students.setVisibility(View.INVISIBLE);
                imageView_logo.setVisibility(View.INVISIBLE);
                tabBar_layout.setVisibility(View.VISIBLE);
                linearLayout_red_ribbon.setVisibility(View.GONE);

                Constants.message = "admin messages";
                hideBadges();
                starVisible();

                fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.frame_container, new MessagesFragment()).commit();

                break;
            case R.id.imageView_calender:
                Constants.headerText = textView_header.getText().toString();
                Intent intent = new Intent(HomeActivity.this,MyEventsActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_left);
                break;
            case R.id.imageView_students:
                if(Constants.isVisibleSociety) {

                    Constants.isVisibleSociety = false;
                    homeFragment.changeItem(false);

                }else{

                    Constants.isVisibleSociety = true;
                    homeFragment.changeItem(true);

                }

                break;
            case R.id.imageView_filter:

                textView_header.setText("Location Filter");
                textView_cancel.setVisibility(View.INVISIBLE);
                textView_select.setVisibility(View.VISIBLE);
                textView_cancel.setVisibility(View.VISIBLE);
                textView_cancelsearch.setVisibility(View.INVISIBLE);
                textView_selectsearch.setVisibility(View.INVISIBLE);
                imageView_back.setVisibility(View.INVISIBLE);
                imageView_backsearch.setVisibility(View.INVISIBLE);
                txt_menu.setVisibility(View.INVISIBLE);
                imageView_filter.setVisibility(View.INVISIBLE);
                imageView_searchfilter.setVisibility(View.INVISIBLE);
                textView_subheader.setVisibility(View.INVISIBLE);
                imageView_calender.setVisibility(View.INVISIBLE);
                imageView_edit.setVisibility(View.INVISIBLE);
                imageView_students.setVisibility(View.INVISIBLE);
                imageView_logo.setVisibility(View.INVISIBLE);
                tabBar_layout.setVisibility(View.VISIBLE);
                imageView_info.setVisibility(View.INVISIBLE);
                linearLayout_red_ribbon.setVisibility(View.GONE);

                starVisible();
                hideBadges();

                fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.frame_container, new LocationFilterFragment()).commit();
                break;
            case R.id.imageView_searchfilter:

                fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.frame_container, new SearchLocationFilterFragment()).commit();

                starVisible();
                hideBadges();

                textView_header.setText("Location Filter");
                textView_cancel.setVisibility(View.INVISIBLE);
                textView_select.setVisibility(View.INVISIBLE);
                textView_cancel.setVisibility(View.INVISIBLE);
                textView_cancelsearch.setVisibility(View.VISIBLE);
                textView_selectsearch.setVisibility(View.VISIBLE);
                imageView_back.setVisibility(View.INVISIBLE);
                imageView_backsearch.setVisibility(View.INVISIBLE);
                txt_menu.setVisibility(View.INVISIBLE);
                imageView_filter.setVisibility(View.INVISIBLE);
                imageView_searchfilter.setVisibility(View.INVISIBLE);
                imageView_calender.setVisibility(View.INVISIBLE);
                imageView_edit.setVisibility(View.INVISIBLE);
                imageView_students.setVisibility(View.INVISIBLE);
                imageView_logo.setVisibility(View.INVISIBLE);
                tabBar_layout.setVisibility(View.VISIBLE);
                imageView_info.setVisibility(View.INVISIBLE);
                linearLayout_red_ribbon.setVisibility(View.GONE);


                break;
            case R.id.textView_cancelsearch:

                starVisible();

                if(Constants.checkedState != null && Constants.checkedState.size() > 0) {
                    Set<String> hStates = new HashSet<>();
                    hStates.addAll(Constants.checkedState);
                    Constants.checkedState.clear();
                    Constants.checkedState.addAll(hStates);
                }

                if(Constants.checkedCountry != null && Constants.checkedCountry.size() > 0) {
                    Set<String> hCountries = new HashSet<>();
                    hCountries.addAll(Constants.checkedCountry);
                    Constants.checkedCountry.clear();
                    Constants.checkedCountry.addAll(hCountries);
                }

                fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.frame_container, new SearchFragment()).commit();


                textView_header.setText("Company Search");
                textView_subheader.setText("All Locations");
                imageView_back.setVisibility(View.INVISIBLE);
                imageView_backsearch.setVisibility(View.INVISIBLE);
                textView_cancel.setVisibility(View.INVISIBLE);
                textView_select.setVisibility(View.INVISIBLE);
                textView_cancelsearch.setVisibility(View.INVISIBLE);
                textView_selectsearch.setVisibility(View.INVISIBLE);
                txt_menu.setVisibility(View.VISIBLE);
                imageView_filter.setVisibility(View.INVISIBLE);
                imageView_searchfilter.setVisibility(View.VISIBLE);
                imageView_info.setVisibility(View.INVISIBLE);
                imageView_calender.setVisibility(View.INVISIBLE);
                textView_subheader.setVisibility(View.VISIBLE);
                imageView_edit.setVisibility(View.INVISIBLE);
                imageView_students.setVisibility(View.INVISIBLE);
                imageView_logo.setVisibility(View.INVISIBLE);
                tabBar_layout.setVisibility(View.VISIBLE);
                linearLayout_red_ribbon.setVisibility(View.GONE);

                hideBadges();

                break;
            case R.id.textView_selectsearch:

                starVisible();

                if(Constants.isBack.equals("CountrySearch")){

                    Constants.isBack = "isBack";

                    if(Constants.checkedCountry != null && Constants.checkedCountry.size() > 0) {
                        Set<String> hCountries = new HashSet<>();
                        hCountries.addAll(Constants.checkedCountry);
                        Constants.checkedCountry.clear();
                        Constants.checkedCountry.addAll(hCountries);

                        String checkedCountry = gson.toJson(Constants.checkedCountry);
                        editor.putString("checkedCountry_search", checkedCountry);
                        editor.apply();

                        SharedPrefrenceUtil.setPrefrence(HomeActivity.this,Constants.locationFilter,"countryStates_search");
                    }else{
                        SharedPrefrenceUtil.setPrefrence(HomeActivity.this,Constants.locationFilter,"");
                        editor.putString("checkedCountry_search", "");
                        editor.apply();
                    }
                    fragmentManager = getSupportFragmentManager();
                    fragmentManager.beginTransaction()
                            .replace(R.id.frame_container, new SearchLocationFilterFragment()).commit();


                    textView_header.setText("Location Filter");
                    textView_select.setVisibility(View.INVISIBLE);
                    textView_cancel.setVisibility(View.INVISIBLE);
                    textView_cancelsearch.setVisibility(View.VISIBLE);
                    textView_selectsearch.setVisibility(View.VISIBLE);
                    textView_subheader.setText("");
                    imageView_back.setVisibility(View.INVISIBLE);
                    imageView_backsearch.setVisibility(View.INVISIBLE);
                    txt_menu.setVisibility(View.INVISIBLE);
                    imageView_filter.setVisibility(View.INVISIBLE);
                    imageView_searchfilter.setVisibility(View.INVISIBLE);
                    textView_subheader.setVisibility(View.INVISIBLE);
                    imageView_calender.setVisibility(View.INVISIBLE);
                    imageView_edit.setVisibility(View.INVISIBLE);
                    imageView_students.setVisibility(View.INVISIBLE);
                    imageView_logo.setVisibility(View.INVISIBLE);
                    tabBar_layout.setVisibility(View.VISIBLE);
                    imageView_info.setVisibility(View.INVISIBLE);
                    linearLayout_red_ribbon.setVisibility(View.GONE);

                    hideBadges();

                }else{

                    if((Constants.checkedState != null && Constants.checkedState.size() > 0) && (Constants.checkedCountry != null && Constants.checkedCountry.size() > 0)) {
                        Set<String> hCountries = new HashSet<>();
                        hCountries.addAll(Constants.checkedCountry);
                        Constants.checkedCountry.clear();
                        Constants.checkedCountry.addAll(hCountries);

                        String checkedCountry = gson.toJson(Constants.checkedCountry);
                        editor.putString("checkedCountry_search", checkedCountry);
                        editor.apply();

                        SharedPrefrenceUtil.setPrefrence(HomeActivity.this, Constants.locationFilter, "countryStates_search");

                        Set<String> hStates = new HashSet<>();
                        hStates.addAll(Constants.checkedState);
                        Constants.checkedState.clear();
                        Constants.checkedState.addAll(hStates);

                        String checkedState = gson.toJson(Constants.checkedState);
                        editor.putString("checkedState_search", checkedState);
                        editor.apply();
                    }else if(Constants.checkedState != null && Constants.checkedState.size() > 0) {
                            Set<String> hStates = new HashSet<>();
                            hStates.addAll(Constants.checkedState);
                            Constants.checkedState.clear();
                            Constants.checkedState.addAll(hStates);

                            String checkedState = gson.toJson(Constants.checkedState);
                            editor.putString("checkedState_search", checkedState);
                            editor.putString("checkedCountry_search", "");
                            editor.apply();

                            SharedPrefrenceUtil.setPrefrence(HomeActivity.this,Constants.locationFilter,"countryStates_search");
                        }else if(Constants.checkedCountry != null && Constants.checkedCountry.size() > 0) {
                            Set<String> hCountries = new HashSet<>();
                            hCountries.addAll(Constants.checkedCountry);
                            Constants.checkedCountry.clear();
                            Constants.checkedCountry.addAll(hCountries);

                            String checkedCountry = gson.toJson(Constants.checkedCountry);
                            editor.putString("checkedCountry_search", checkedCountry);
                            editor.putString("checkedState_search", "");
                            editor.apply();

                            SharedPrefrenceUtil.setPrefrence(HomeActivity.this,Constants.locationFilter,"countryStates_search");
                        }else{
                            SharedPrefrenceUtil.setPrefrence(HomeActivity.this,Constants.locationFilter,"");
                            editor.putString("checkedState_search", "");
                            editor.putString("checkedCountry_search", "");
                            editor.apply();
                        }

                    fragmentManager = getSupportFragmentManager();
                    fragmentManager.beginTransaction()
                            .replace(R.id.frame_container, new SearchFragment()).commit();


                    textView_header.setText("Company Search");
                    textView_subheader.setText("All Locations");
                    imageView_back.setVisibility(View.INVISIBLE);
                    imageView_backsearch.setVisibility(View.INVISIBLE);
                    textView_cancel.setVisibility(View.INVISIBLE);
                    textView_select.setVisibility(View.INVISIBLE);
                    textView_cancelsearch.setVisibility(View.INVISIBLE);
                    textView_selectsearch.setVisibility(View.INVISIBLE);
                    txt_menu.setVisibility(View.VISIBLE);
                    imageView_filter.setVisibility(View.INVISIBLE);
                    imageView_searchfilter.setVisibility(View.VISIBLE);
                    imageView_info.setVisibility(View.INVISIBLE);
                    imageView_calender.setVisibility(View.INVISIBLE);
                    textView_subheader.setVisibility(View.VISIBLE);
                    imageView_edit.setVisibility(View.INVISIBLE);
                    imageView_students.setVisibility(View.INVISIBLE);
                    imageView_logo.setVisibility(View.INVISIBLE);
                    tabBar_layout.setVisibility(View.VISIBLE);
                    linearLayout_red_ribbon.setVisibility(View.GONE);

                    hideBadges();

                }

                break;
            case R.id.imageView_backsearch:

                starVisible();

                fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.frame_container, new SearchLocationFilterFragment()).commit();

                hideBadges();


                textView_header.setText("Location Filter");
                textView_cancel.setVisibility(View.INVISIBLE);
                textView_select.setVisibility(View.INVISIBLE);
                textView_cancel.setVisibility(View.INVISIBLE);
                textView_cancelsearch.setVisibility(View.VISIBLE);
                textView_selectsearch.setVisibility(View.VISIBLE);
                textView_subheader.setText("");
                imageView_back.setVisibility(View.INVISIBLE);
                imageView_backsearch.setVisibility(View.INVISIBLE);
                txt_menu.setVisibility(View.INVISIBLE);
                imageView_filter.setVisibility(View.INVISIBLE);
                imageView_searchfilter.setVisibility(View.INVISIBLE);
                textView_subheader.setVisibility(View.INVISIBLE);
                imageView_calender.setVisibility(View.INVISIBLE);
                imageView_edit.setVisibility(View.INVISIBLE);
                imageView_students.setVisibility(View.INVISIBLE);
                imageView_logo.setVisibility(View.INVISIBLE);
                tabBar_layout.setVisibility(View.VISIBLE);
                imageView_info.setVisibility(View.INVISIBLE);
                linearLayout_red_ribbon.setVisibility(View.GONE);

                break;
            case R.id.imageView_info:
                hideBadges();
                starVisible();
                imageView_info.setVisibility(View.INVISIBLE);
                salaryGuideFragment.isDisclaimerVisible();
                break;
            case R.id.imageView_back:

                starVisible();

                switch (Constants.isBack) {
                    case "Country":

                        Constants.isBack = "isBack";

                        imageView_Portfolio.setImageResource(R.drawable.portfolio_unselected);
                        imageView_Disciplines.setImageResource(R.drawable.discipline_selected);
                        imageView_Search.setImageResource(R.drawable.search_unselected);
                        imageView_Messages.setImageResource(R.drawable.messages_unselected);
                        imageView_Salary.setImageResource(R.drawable.sallary_unselected);

                        textView_Portfolio.setTextColor(getResources().getColor(R.color.grey));
                        textView_Disciplines.setTextColor(getResources().getColor(R.color.tab_txt_color));
                        textView_Search.setTextColor(getResources().getColor(R.color.grey));
                        textView_Messages.setTextColor(getResources().getColor(R.color.grey));
                        textView_Salary.setTextColor(getResources().getColor(R.color.grey));

                        textView_header.setText("Location Filter");
                        textView_cancel.setVisibility(View.INVISIBLE);
                        textView_select.setVisibility(View.VISIBLE);
                        textView_cancel.setVisibility(View.VISIBLE);
                        textView_cancelsearch.setVisibility(View.INVISIBLE);
                        textView_selectsearch.setVisibility(View.INVISIBLE);
                        textView_subheader.setText("");
                        imageView_back.setVisibility(View.INVISIBLE);
                        imageView_backsearch.setVisibility(View.INVISIBLE);
                        txt_menu.setVisibility(View.INVISIBLE);
                        imageView_filter.setVisibility(View.INVISIBLE);
                        imageView_searchfilter.setVisibility(View.INVISIBLE);
                        textView_subheader.setVisibility(View.INVISIBLE);
                        imageView_calender.setVisibility(View.INVISIBLE);
                        imageView_edit.setVisibility(View.INVISIBLE);
                        imageView_students.setVisibility(View.INVISIBLE);
                        imageView_logo.setVisibility(View.INVISIBLE);
                        tabBar_layout.setVisibility(View.VISIBLE);
                        imageView_info.setVisibility(View.INVISIBLE);
                        linearLayout_red_ribbon.setVisibility(View.GONE);

                        hideBadges();

                        fragmentManager = getSupportFragmentManager();
                        fragmentManager.beginTransaction()
                                .replace(R.id.frame_container, new LocationFilterFragment()).commit();

                        break;
                    case "Event details":

                        imageView_Portfolio.setImageResource(R.drawable.portfolio_unselected);
                        imageView_Disciplines.setImageResource(R.drawable.discipline_unselected);
                        imageView_Search.setImageResource(R.drawable.search_unselected);
                        imageView_Messages.setImageResource(R.drawable.message_selected);
                        imageView_Salary.setImageResource(R.drawable.sallary_unselected);

                        textView_Portfolio.setTextColor(getResources().getColor(R.color.grey));
                        textView_Disciplines.setTextColor(getResources().getColor(R.color.grey));
                        textView_Search.setTextColor(getResources().getColor(R.color.grey));
                        textView_Messages.setTextColor(getResources().getColor(R.color.tab_txt_color));
                        textView_Salary.setTextColor(getResources().getColor(R.color.grey));

                        textView_header.setText("Companies");
                        textView_subheader.setText(SharedPrefrenceUtil.getPrefrence(HomeActivity.this, Constants.subHeader, ""));
                        imageView_back.setVisibility(View.INVISIBLE);
                        imageView_backsearch.setVisibility(View.INVISIBLE);
                        textView_cancel.setVisibility(View.INVISIBLE);
                        textView_select.setVisibility(View.INVISIBLE);
                        textView_cancelsearch.setVisibility(View.INVISIBLE);
                        textView_selectsearch.setVisibility(View.INVISIBLE);
                        txt_menu.setVisibility(View.VISIBLE);
                        imageView_filter.setVisibility(View.INVISIBLE);
                        imageView_searchfilter.setVisibility(View.INVISIBLE);
                        imageView_calender.setVisibility(View.VISIBLE);
                        imageView_info.setVisibility(View.INVISIBLE);
                        textView_subheader.setVisibility(View.VISIBLE);
                        imageView_edit.setVisibility(View.INVISIBLE);
                        imageView_students.setVisibility(View.INVISIBLE);
                        imageView_logo.setVisibility(View.INVISIBLE);
                        tabBar_layout.setVisibility(View.VISIBLE);
                        linearLayout_red_ribbon.setVisibility(View.GONE);

                        hideBadges();

                        fragmentManager = getSupportFragmentManager();
                        fragmentManager.beginTransaction()
                                .replace(R.id.frame_container, new MessagesFragment()).commit();

                        break;

                    case "discipline":

                        textView_header.setText("Disciplines");
                        textView_subheader.setText("All Locations");
                        imageView_back.setVisibility(View.INVISIBLE);
                        imageView_backsearch.setVisibility(View.INVISIBLE);
                        textView_cancel.setVisibility(View.INVISIBLE);
                        textView_select.setVisibility(View.INVISIBLE);
                        textView_cancelsearch.setVisibility(View.INVISIBLE);
                        textView_selectsearch.setVisibility(View.INVISIBLE);
                        txt_menu.setVisibility(View.VISIBLE);
                        imageView_filter.setVisibility(View.VISIBLE);
                        imageView_searchfilter.setVisibility(View.INVISIBLE);
                        imageView_info.setVisibility(View.INVISIBLE);
                        imageView_calender.setVisibility(View.INVISIBLE);
                        textView_subheader.setVisibility(View.VISIBLE);
                        imageView_edit.setVisibility(View.INVISIBLE);
                        imageView_students.setVisibility(View.INVISIBLE);
                        imageView_logo.setVisibility(View.INVISIBLE);
                        tabBar_layout.setVisibility(View.VISIBLE);
                        linearLayout_red_ribbon.setVisibility(View.GONE);

                        starVisible();
                        hideBadges();

                        fragmentManager = getSupportFragmentManager();
                        fragmentManager.beginTransaction()
                                .replace(R.id.frame_container, new DisciplinesFragment()).commit();

                        break;
                    default:
                        imageView_Portfolio.setImageResource(R.drawable.portfolio_selected);
                        imageView_Disciplines.setImageResource(R.drawable.discipline_unselected);
                        imageView_Search.setImageResource(R.drawable.search_unselected);
                        imageView_Messages.setImageResource(R.drawable.messages_unselected);
                        imageView_Salary.setImageResource(R.drawable.sallary_unselected);

                        textView_Portfolio.setTextColor(getResources().getColor(R.color.tab_txt_color));
                        textView_Disciplines.setTextColor(getResources().getColor(R.color.grey));
                        textView_Messages.setTextColor(getResources().getColor(R.color.grey));
                        textView_Search.setTextColor(getResources().getColor(R.color.grey));
                        textView_Salary.setTextColor(getResources().getColor(R.color.grey));

                        textView_header.setText(navMenuTitles[0]);
                        admin_badge.setVisibility(View.INVISIBLE);
                        student_badge.setVisibility(View.INVISIBLE);
                        textView_cancel.setVisibility(View.INVISIBLE);
                        textView_select.setVisibility(View.INVISIBLE);
                        textView_cancelsearch.setVisibility(View.INVISIBLE);
                        textView_selectsearch.setVisibility(View.INVISIBLE);
                        textView_subheader.setText(SharedPrefrenceUtil.getPrefrence(HomeActivity.this, Constants.subHeader, ""));
                        imageView_back.setVisibility(View.INVISIBLE);
                        imageView_backsearch.setVisibility(View.INVISIBLE);
                        txt_menu.setVisibility(View.VISIBLE);
                        imageView_filter.setVisibility(View.INVISIBLE);
                        imageView_searchfilter.setVisibility(View.INVISIBLE);
                        textView_subheader.setVisibility(View.VISIBLE);
                        imageView_calender.setVisibility(View.INVISIBLE);
                        imageView_edit.setVisibility(View.VISIBLE);
                        imageView_students.setVisibility(View.VISIBLE);
                        imageView_logo.setVisibility(View.VISIBLE);
                        tabBar_layout.setVisibility(View.VISIBLE);
                        imageView_info.setVisibility(View.INVISIBLE);
                        imageView_logo_portfolio.setVisibility(View.INVISIBLE);
                        linearLayout_red_ribbon.setVisibility(View.VISIBLE);

                        fragmentManager = getSupportFragmentManager();
                        fragmentManager.beginTransaction()
                                .replace(R.id.frame_container, new HomeFragment()).commit();

                        break;
                }
                break;
            case R.id.textView_select:

                starVisible();

                if(Constants.isBack.equals("Country")){

                    Constants.isBack = "isBack";

                    if(Constants.checkedCountry != null && Constants.checkedCountry.size() > 0) {
                        Set<String> hCountries = new HashSet<>();
                        hCountries.addAll(Constants.checkedCountry);
                        Constants.checkedCountry.clear();
                        Constants.checkedCountry.addAll(hCountries);

                        String checkedCountry = gson.toJson(Constants.checkedCountry);
                        editor.putString("checkedCountry", checkedCountry);
                        editor.apply();

                        SharedPrefrenceUtil.setPrefrence(HomeActivity.this,Constants.locationFilter,"countryStates");
                    }else{
                        SharedPrefrenceUtil.setPrefrence(HomeActivity.this,Constants.locationFilter,"");
                        editor.putString("checkedCountry", "");
                        editor.apply();
                    }


                    textView_header.setText("Location Filter");
                    textView_select.setVisibility(View.VISIBLE);
                    textView_cancel.setVisibility(View.VISIBLE);
                    textView_cancelsearch.setVisibility(View.INVISIBLE);
                    imageView_back.setVisibility(View.INVISIBLE);
                    imageView_backsearch.setVisibility(View.INVISIBLE);
                    txt_menu.setVisibility(View.INVISIBLE);
                    imageView_filter.setVisibility(View.INVISIBLE);
                    imageView_searchfilter.setVisibility(View.INVISIBLE);
                    textView_subheader.setVisibility(View.INVISIBLE);
                    imageView_calender.setVisibility(View.INVISIBLE);
                    imageView_edit.setVisibility(View.INVISIBLE);
                    imageView_students.setVisibility(View.INVISIBLE);
                    imageView_logo.setVisibility(View.INVISIBLE);
                    tabBar_layout.setVisibility(View.VISIBLE);
                    imageView_info.setVisibility(View.INVISIBLE);
                    linearLayout_red_ribbon.setVisibility(View.GONE);

                    hideBadges();

                    fragmentManager = getSupportFragmentManager();
                    fragmentManager.beginTransaction()
                            .replace(R.id.frame_container, new LocationFilterFragment()).commit();

                }else{

                    if((Constants.checkedState != null && Constants.checkedState.size() > 0) && (Constants.checkedCountry != null && Constants.checkedCountry.size() > 0)) {
                        Set<String> hCountries = new HashSet<>();
                        hCountries.addAll(Constants.checkedCountry);
                        Constants.checkedCountry.clear();
                        Constants.checkedCountry.addAll(hCountries);

                        String checkedCountry = gson.toJson(Constants.checkedCountry);
                        editor.putString("checkedCountry", checkedCountry);
                        editor.apply();

                        SharedPrefrenceUtil.setPrefrence(HomeActivity.this, Constants.locationFilter, "countryStates");

                        Set<String> hStates = new HashSet<>();
                        hStates.addAll(Constants.checkedState);
                        Constants.checkedState.clear();
                        Constants.checkedState.addAll(hStates);

                        String checkedState = gson.toJson(Constants.checkedState);
                        editor.putString("checkedState", checkedState);
                        editor.apply();
                    }else if(Constants.checkedState != null && Constants.checkedState.size() > 0) {
                        Set<String> hStates = new HashSet<>();
                        hStates.addAll(Constants.checkedState);
                        Constants.checkedState.clear();
                        Constants.checkedState.addAll(hStates);

                        String checkedState = gson.toJson(Constants.checkedState);
                        editor.putString("checkedState", checkedState);
                        editor.putString("checkedCountry", "");
                        editor.apply();

                        SharedPrefrenceUtil.setPrefrence(HomeActivity.this,Constants.locationFilter,"countryStates");
                    }else if(Constants.checkedCountry != null && Constants.checkedCountry.size() > 0) {
                        Set<String> hCountries = new HashSet<>();
                        hCountries.addAll(Constants.checkedCountry);
                        Constants.checkedCountry.clear();
                        Constants.checkedCountry.addAll(hCountries);

                        String checkedCountry = gson.toJson(Constants.checkedCountry);
                        editor.putString("checkedCountry", checkedCountry);
                        editor.putString("checkedState", "");
                        editor.apply();

                        SharedPrefrenceUtil.setPrefrence(HomeActivity.this,Constants.locationFilter,"countryStates");
                    }else{
                        SharedPrefrenceUtil.setPrefrence(HomeActivity.this,Constants.locationFilter,"");
                        editor.putString("checkedCountry", "");
                        editor.putString("checkedState", "");
                        editor.apply();
                    }


                    fragmentManager = getSupportFragmentManager();
                    fragmentManager.beginTransaction()
                            .replace(R.id.frame_container, new DisciplinesFragment()).commit();

                    textView_header.setText("Disciplines");
                    textView_subheader.setText("All Locations");
                    imageView_back.setVisibility(View.INVISIBLE);
                    imageView_backsearch.setVisibility(View.INVISIBLE);
                    textView_cancel.setVisibility(View.INVISIBLE);
                    textView_select.setVisibility(View.INVISIBLE);
                    textView_cancelsearch.setVisibility(View.INVISIBLE);
                    textView_selectsearch.setVisibility(View.INVISIBLE);
                    txt_menu.setVisibility(View.VISIBLE);
                    imageView_filter.setVisibility(View.VISIBLE);
                    imageView_searchfilter.setVisibility(View.INVISIBLE);
                    imageView_info.setVisibility(View.INVISIBLE);
                    imageView_calender.setVisibility(View.INVISIBLE);
                    textView_subheader.setVisibility(View.VISIBLE);
                    imageView_edit.setVisibility(View.INVISIBLE);
                    imageView_students.setVisibility(View.INVISIBLE);
                    imageView_logo.setVisibility(View.INVISIBLE);
                    tabBar_layout.setVisibility(View.VISIBLE);
                    linearLayout_red_ribbon.setVisibility(View.GONE);

                    hideBadges();

                }

                break;
            case R.id.textView_cancel:

                starVisible();

                if(Constants.checkedState != null && Constants.checkedState.size() > 0) {
                    Set<String> hStates = new HashSet<>();
                    hStates.addAll(Constants.checkedState);
                    Constants.checkedState.clear();
                    Constants.checkedState.addAll(hStates);
                }

                if(Constants.checkedCountry != null && Constants.checkedCountry.size() > 0) {
                    Set<String> hCountries = new HashSet<>();
                    hCountries.addAll(Constants.checkedCountry);
                    Constants.checkedCountry.clear();
                    Constants.checkedCountry.addAll(hCountries);
                }

                textView_header.setText("Disciplines");
                textView_subheader.setText("All Locations");
                imageView_back.setVisibility(View.INVISIBLE);
                imageView_backsearch.setVisibility(View.INVISIBLE);
                textView_cancel.setVisibility(View.INVISIBLE);
                textView_select.setVisibility(View.INVISIBLE);
                textView_cancelsearch.setVisibility(View.INVISIBLE);
                textView_selectsearch.setVisibility(View.INVISIBLE);
                txt_menu.setVisibility(View.VISIBLE);
                imageView_filter.setVisibility(View.VISIBLE);
                imageView_searchfilter.setVisibility(View.INVISIBLE);
                imageView_info.setVisibility(View.INVISIBLE);
                imageView_calender.setVisibility(View.INVISIBLE);
                textView_subheader.setVisibility(View.VISIBLE);
                imageView_edit.setVisibility(View.INVISIBLE);
                imageView_students.setVisibility(View.INVISIBLE);
                imageView_logo.setVisibility(View.INVISIBLE);
                tabBar_layout.setVisibility(View.VISIBLE);
                linearLayout_red_ribbon.setVisibility(View.GONE);

                hideBadges();

                fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.frame_container, new DisciplinesFragment()).commit();

                break;
            case R.id.imageView_logo_portfolio:
                if(Constants.port == 0){
                    detailFragment.addPort(Constants.cid,HomeActivity.this);
                }else{
                    detailFragment.removePort(Constants.cid,HomeActivity.this);
                }
                break;
            default:
                break;
        }
    }




    private class SlideMenuClickListener implements
            ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {
            // display view for selected nav drawer item
            displayView(position);
        }
    }

    public void displayView(int position) {
        Fragment fragment = null;
        switch (position) {
            case 0:
                fragment = new HomeFragment();
                imageView_back.setVisibility(View.INVISIBLE);
                txt_menu.setVisibility(View.VISIBLE);
                imageView_filter.setVisibility(View.INVISIBLE);
                imageView_searchfilter.setVisibility(View.INVISIBLE);
                textView_subheader.setVisibility(View.VISIBLE);
                imageView_calender.setVisibility(View.INVISIBLE);
                imageView_edit.setVisibility(View.VISIBLE);
                imageView_students.setVisibility(View.VISIBLE);
                imageView_logo.setVisibility(View.VISIBLE);
                tabBar_layout.setVisibility(View.VISIBLE);
                imageView_info.setVisibility(View.INVISIBLE);
                linearLayout_red_ribbon.setVisibility(View.VISIBLE);
                textView_cancel.setVisibility(View.INVISIBLE);
                textView_select.setVisibility(View.INVISIBLE);
                textView_cancelsearch.setVisibility(View.INVISIBLE);
                textView_selectsearch.setVisibility(View.INVISIBLE);
                imageView_backsearch.setVisibility(View.INVISIBLE);

                imageView_Portfolio.setImageResource(R.drawable.portfolio_selected);
                imageView_Disciplines.setImageResource(R.drawable.discipline_unselected);
                imageView_Search.setImageResource(R.drawable.search_unselected);
                imageView_Messages.setImageResource(R.drawable.messages_unselected);
                imageView_Salary.setImageResource(R.drawable.sallary_unselected);

                textView_Portfolio.setTextColor(getResources().getColor(R.color.tab_txt_color));
                textView_Disciplines.setTextColor(getResources().getColor(R.color.grey));
                textView_Messages.setTextColor(getResources().getColor(R.color.grey));
                textView_Search.setTextColor(getResources().getColor(R.color.grey));
                textView_Salary.setTextColor(getResources().getColor(R.color.grey));

                starVisible();

                break;
            case 1:
                fragment = new MessagesFragment();
                textView_subheader.setText("All requests and responses");
                imageView_back.setVisibility(View.INVISIBLE);
                txt_menu.setVisibility(View.VISIBLE);
                imageView_filter.setVisibility(View.INVISIBLE);
                imageView_searchfilter.setVisibility(View.INVISIBLE);
                textView_subheader.setVisibility(View.VISIBLE);
                imageView_calender.setVisibility(View.INVISIBLE);
                imageView_edit.setVisibility(View.INVISIBLE);
                imageView_students.setVisibility(View.INVISIBLE);
                imageView_info.setVisibility(View.INVISIBLE);
                imageView_logo.setVisibility(View.INVISIBLE);
                tabBar_layout.setVisibility(View.VISIBLE);
                linearLayout_red_ribbon.setVisibility(View.GONE);
                textView_cancel.setVisibility(View.INVISIBLE);
                textView_select.setVisibility(View.INVISIBLE);
                textView_cancelsearch.setVisibility(View.INVISIBLE);
                textView_selectsearch.setVisibility(View.INVISIBLE);
                imageView_backsearch.setVisibility(View.INVISIBLE);
                hideBadges();
                starVisible();

                Constants.message = "my events";

                imageView_Portfolio.setImageResource(R.drawable.portfolio_unselected);
                imageView_Disciplines.setImageResource(R.drawable.discipline_unselected);
                imageView_Search.setImageResource(R.drawable.search_unselected);
                imageView_Messages.setImageResource(R.drawable.message_selected);
                imageView_Salary.setImageResource(R.drawable.sallary_unselected);

                textView_Portfolio.setTextColor(getResources().getColor(R.color.grey));
                textView_Disciplines.setTextColor(getResources().getColor(R.color.grey));
                textView_Messages.setTextColor(getResources().getColor(R.color.tab_txt_color));
                textView_Search.setTextColor(getResources().getColor(R.color.grey));
                textView_Salary.setTextColor(getResources().getColor(R.color.grey));
                hideBadges();
                starVisible();

                break;
            case 2:
                fragment = new StudentDetailsFragment();
                imageView_back.setVisibility(View.INVISIBLE);
                txt_menu.setVisibility(View.VISIBLE);
                imageView_filter.setVisibility(View.INVISIBLE);
                imageView_searchfilter.setVisibility(View.INVISIBLE);
                textView_subheader.setVisibility(View.GONE);
                imageView_calender.setVisibility(View.INVISIBLE);
                imageView_edit.setVisibility(View.INVISIBLE);
                imageView_students.setVisibility(View.INVISIBLE);
                imageView_info.setVisibility(View.INVISIBLE);
                imageView_logo.setVisibility(View.INVISIBLE);
                tabBar_layout.setVisibility(View.GONE);
                linearLayout_red_ribbon.setVisibility(View.GONE);
                textView_cancel.setVisibility(View.INVISIBLE);
                textView_select.setVisibility(View.INVISIBLE);
                textView_cancelsearch.setVisibility(View.INVISIBLE);
                textView_selectsearch.setVisibility(View.INVISIBLE);
                imageView_backsearch.setVisibility(View.INVISIBLE);
                hideBadges();
                starVisible();
                break;
            case 3:
                fragment = new StudentSocietyFragment();
                textView_subheader.setText(SharedPrefrenceUtil.getPrefrence(HomeActivity.this,Constants.university,""));
                imageView_back.setVisibility(View.INVISIBLE);
                txt_menu.setVisibility(View.VISIBLE);
                imageView_filter.setVisibility(View.INVISIBLE);
                imageView_searchfilter.setVisibility(View.INVISIBLE);
                textView_subheader.setVisibility(View.VISIBLE);
                imageView_calender.setVisibility(View.INVISIBLE);
                imageView_edit.setVisibility(View.INVISIBLE);
                imageView_info.setVisibility(View.INVISIBLE);
                imageView_students.setVisibility(View.INVISIBLE);
                imageView_logo.setVisibility(View.INVISIBLE);
                tabBar_layout.setVisibility(View.GONE);
                linearLayout_red_ribbon.setVisibility(View.GONE);
                textView_cancel.setVisibility(View.INVISIBLE);
                textView_select.setVisibility(View.INVISIBLE);
                textView_cancelsearch.setVisibility(View.INVISIBLE);
                textView_selectsearch.setVisibility(View.INVISIBLE);
                imageView_backsearch.setVisibility(View.INVISIBLE);
                hideBadges();
                starVisible();
                break;
            case 4:

                if(!(SharedPrefrenceUtil.getPrefrence(HomeActivity.this, Constants.FB_ID,"").equals("")) && !(SharedPrefrenceUtil.getPrefrence(this,Constants.emailAddress,"").equals(""))){
                    logout();
                }else if(!SharedPrefrenceUtil.getPrefrence(HomeActivity.this, Constants.FB_ID,"").equals("")){
                    linkedInLogin();
                }else if(!SharedPrefrenceUtil.getPrefrence(HomeActivity.this, Constants.emailAddress,"").equals("")){
                    logout();
                }else{
                    linkedInLogin();
                }

                break;
            case 5:

                if(!SharedPrefrenceUtil.getPrefrence(HomeActivity.this, Constants.FB_ID,"").equals("")){

                    logout();
                }else if((SharedPrefrenceUtil.getPrefrence(HomeActivity.this, Constants.FB_ID,"").equals("")) && (SharedPrefrenceUtil.getPrefrence(this,Constants.emailAddress,"").equals(""))){
                    fbLogin();
                }
                break;
            case 6:

                logout();

                break;
            default:
                break;
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.frame_container, fragment).commit();

            // update selected item and title, then close the drawer
            mDrawerList.setItemChecked(position, true);
            mDrawerList.setSelection(position);
            if(position == 3){
                textView_header.setText("Student Society");
            }else{
                textView_header.setText(navMenuTitles[position]);
            }

            setTitle(navMenuTitles[position]);
            mDrawerLayout.closeDrawer(drawer);
        } else {
            // error in creating fragment
            Log.e("MainActivity", "Error in creating fragment");
        }

    }

    private void fbLogin() {

        PerfectGraduateApplication.getInstance().displayCustomDialogPostNeg(HomeActivity.this,
                "Save My Portfolio", "Please confirm if you would like to save your portfolio using your Facebook login?", new ICallBack() {
                    @Override
                    public void onCallBackResult(String response) {
                        LoginManager.getInstance().logInWithReadPermissions(HomeActivity.this, Arrays.asList("public_profile", "email"));
                    }
                },"Sign In",getString(R.string.no));


    }


    private void RequestData() {

        GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {

                        JSONObject json = response.getJSONObject();

                        Log.e("response"," -- "+json.toString());

                        try {
                            if (json != null) {

                                SharedPrefrenceUtil.setPrefrence(HomeActivity.this,Constants.FB_ID,json.getString("id"));
                                SharedPrefrenceUtil.setPrefrence(HomeActivity.this,Constants.FIRST_NAME,json.getString("first_name"));
                                SharedPrefrenceUtil.setPrefrence(HomeActivity.this,Constants.LAST_NAME,json.getString("last_name"));

                                if (json.has("email")) {
                                    SharedPrefrenceUtil.setPrefrence(HomeActivity.this,Constants.EMAIL_ID,json.getString("email"));
                                }
                                registerFb();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,first_name,last_name,email");
        request.setParameters(parameters);
        request.executeAsync();

    }

    private void registerFb() {

        Map<String, Object> map = new HashMap<String, Object>();

        map.put("fb_id",SharedPrefrenceUtil.getPrefrence(HomeActivity.this,Constants.FB_ID,""));
        map.put("email",SharedPrefrenceUtil.getPrefrence(HomeActivity.this,Constants.EMAIL_ID,""));
        map.put("Name",SharedPrefrenceUtil.getPrefrence(HomeActivity.this,Constants.FIRST_NAME,"")+" "+SharedPrefrenceUtil.getPrefrence(this,Constants.LAST_NAME,""));
        map.put("device", FirebaseInstanceId.getInstance().getToken());
        map.put("device type", "android");

        request.getResponse(HomeActivity.this, map, WebServiceAPI.API_register_fb, true, new ICallBack() {
            @Override
            public void onCallBackResult(String response) {
                try{

                    JSONObject jsonObject = new JSONObject(response);

                    String status = jsonObject.getString(WebServiceAPI.status);
                    mDrawerLayout.closeDrawer(drawer);
                    drawerMenu();
                }catch (JSONException e){
                    e.printStackTrace();
                }
            }
        });

    }

    private void linkedInLogin() {

        if(SharedPrefrenceUtil.getPrefrence(HomeActivity.this,Constants.FB_ID,"").equals("")){
            PerfectGraduateApplication.getInstance().displayDialogOKCallBack(HomeActivity.this,"","Please save your portfolio before continuing.", new ICallBack() {
                @Override
                public void onCallBackResult(String response) {
                    fbLogin();
                }
            });

        }else{
            PerfectGraduateApplication.getInstance().displayCustomDialogPostNeg(HomeActivity.this,
                    "LinkedIn Sign In", "Please Sign in to LinkedIn to request invitations to company events.", new ICallBack() {
                        @Override
                        public void onCallBackResult(String response) {
                            login_linkedin();

                        }
                    },"Sign In",getString(R.string.no));

        }
    }

    private void logout() {

        PerfectGraduateApplication.getInstance().displayCustomDialogPostNeg(HomeActivity.this,
                getString(R.string.app_name), "Are you sure you want to log out?", new ICallBack() {
                    @Override
                    public void onCallBackResult(String response) {

                        Map<String, Object> map = new HashMap<String, Object>();

                        map.put("uid",SharedPrefrenceUtil.getPrefrence(HomeActivity.this,Constants.USER_ID,""));

                        request.getResponse(HomeActivity.this, map, WebServiceAPI.API_logout, true, new ICallBack() {
                            @Override
                            public void onCallBackResult(String response) {

                                try {
                                    JSONObject jsonObject = new JSONObject(response);

                                    if(Boolean.parseBoolean(jsonObject.getString("status"))){

                                        SharedPrefrenceUtil.setPrefrence(HomeActivity.this, Constants.FB_ID,"");
                                        SharedPrefrenceUtil.setPrefrence(HomeActivity.this, Constants.FIRST_NAME,"");
                                        SharedPrefrenceUtil.setPrefrence(HomeActivity.this, Constants.LAST_NAME,"");
                                        SharedPrefrenceUtil.setPrefrence(HomeActivity.this, Constants.USER_ID,"");
                                        SharedPrefrenceUtil.setPrefrence(HomeActivity.this, Constants.isAllCompany,"");
                                        SharedPrefrenceUtil.setPrefrence(HomeActivity.this, Constants.isFirstTime,"");
                                        SharedPrefrenceUtil.setPrefrence(HomeActivity.this, Constants.isDiscipline,"");
                                        SharedPrefrenceUtil.setPrefrence(HomeActivity.this, Constants.isDisciplineFilter,"");
                                        SharedPrefrenceUtil.setPrefrence(HomeActivity.this, Constants.isMessages,"");
                                        SharedPrefrenceUtil.setPrefrence(HomeActivity.this, Constants.isCountries,"");
                                        SharedPrefrenceUtil.setPrefrence(HomeActivity.this, Constants.isStates,"");
                                        SharedPrefrenceUtil.setPrefrence(HomeActivity.this,Constants.emailAddress,"");

                                        SharedPrefrenceUtil.setPrefrence(HomeActivity.this,Constants.emailAddress,"");
                                        SharedPrefrenceUtil.setPrefrence(HomeActivity.this,Constants.formattedName,"");
                                        SharedPrefrenceUtil.setPrefrence(HomeActivity.this,Constants.pictureUrl,"");
                                        SharedPrefrenceUtil.setPrefrence(HomeActivity.this,Constants.publicProfileUrl,"");
                                        SharedPrefrenceUtil.setPrefrence(HomeActivity.this,Constants.locationFilter,"");
                                        SharedPrefrenceUtil.setPrefrence(HomeActivity.this,Constants.isSocietyDetail,"");
                                        SharedPrefrenceUtil.setPrefrence(HomeActivity.this,Constants.isSalary,"");
                                        SharedPrefrenceUtil.setPrefrence(HomeActivity.this,Constants.isCompanyFilter,"");
                                        SharedPrefrenceUtil.setPrefrence(HomeActivity.this,Constants.isSearch,"");
                                        SharedPrefrenceUtil.setPrefrence(HomeActivity.this,Constants.isBack,"");
                                        SharedPrefrenceUtil.setPrefrence(HomeActivity.this,Constants.score,"");
                                        SharedPrefrenceUtil.setPrefrence(HomeActivity.this,Constants.abr,"");
                                        SharedPrefrenceUtil.setPrefrence(HomeActivity.this,Constants.university,"");


                                        SharedPreferences settings = getSharedPreferences("prefrence", Context.MODE_PRIVATE);
                                        SharedPreferences.Editor editor = settings.edit();
                                        editor.putBoolean("login", false);
                                        editor.remove("login");
                                        editor.apply();

                                        databaseHelper.deleteTable();

                                        Intent intent = new Intent(HomeActivity.this,LoginActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(intent);
                                        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
                                        finish();
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                },"","");

    }


    public void login_linkedin(){
        LISessionManager.getInstance(getApplicationContext()).init(HomeActivity.this, buildScope(), new AuthListener() {
            @Override
            public void onAuthSuccess() {

                SharedPrefrenceUtil.setPrefrence(HomeActivity.this,Constants.Access_token,LISessionManager.getInstance(getApplicationContext()).getSession().getAccessToken().toString());

            }

            @Override
            public void onAuthError(LIAuthError error) {

               /* Toast.makeText(getApplicationContext(), "failed " + error.toString(),
                        Toast.LENGTH_LONG).show();*/

                Log.e("onAuthError"," -- "+error.toString());

            }
        }, true);
    }

    private static Scope buildScope() {
        return Scope.build(Scope.R_BASICPROFILE, Scope.R_EMAILADDRESS);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        LISessionManager.getInstance(getApplicationContext()).onActivityResult(this,
                requestCode, resultCode, data);
        if(mCallbackManager.onActivityResult(requestCode, resultCode, data)) {
            return;
        }else {
            getUserData();
        }
    }

    public void getUserData(){
        APIHelper apiHelper = APIHelper.getInstance(getApplicationContext());
        apiHelper.getRequest(HomeActivity.this, topCardUrl, new ApiListener() {
            @Override
            public void onApiSuccess(ApiResponse result) {
                try {

                    JSONObject jsonObject = new JSONObject(result.getResponseDataAsJson().toString());

                    SharedPrefrenceUtil.setPrefrence(HomeActivity.this,Constants.emailAddress,jsonObject.getString("emailAddress"));
                    SharedPrefrenceUtil.setPrefrence(HomeActivity.this,Constants.formattedName,jsonObject.getString("formattedName"));
                    SharedPrefrenceUtil.setPrefrence(HomeActivity.this,Constants.pictureUrl,jsonObject.getString("pictureUrl"));
                    SharedPrefrenceUtil.setPrefrence(HomeActivity.this,Constants.publicProfileUrl,jsonObject.getString("publicProfileUrl"));

                    studentLinkedIn();


                } catch (Exception e) {

                    e.printStackTrace();
                }

            }

            @Override
            public void onApiError(LIApiError error) {
            }
        });
    }

    private void studentLinkedIn() {

        Map<String, Object> map = new HashMap<String, Object>();

        map.put("uid",SharedPrefrenceUtil.getPrefrence(HomeActivity.this,Constants.USER_ID,""));
        map.put("image", SharedPrefrenceUtil.getPrefrence(HomeActivity.this,Constants.pictureUrl,""));
        map.put("url", SharedPrefrenceUtil.getPrefrence(HomeActivity.this,Constants.publicProfileUrl,""));

        request.getResponse(HomeActivity.this, map, WebServiceAPI.API_studentLinkedIn, true, new ICallBack() {
            @Override
            public void onCallBackResult(String response) {
                try{

                    JSONObject jsonObject = new JSONObject(response);
                    mDrawerLayout.closeDrawer(drawer);
                    drawerMenu();
                    PerfectGraduateApplication.getInstance().displayCustomDialog(HomeActivity.this,"Confirmation","You have successfully signed in to LinkedIn.");

                }catch (JSONException e){
                    e.printStackTrace();
                }
            }
        });

    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getSupportActionBar().setTitle(mTitle);
    }

    /**
     * When using the ActionBarDrawerToggle, you must call it during
     * onPostCreate() and onConfigurationChanged()...
     */

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_right);
        finish();
    }

    public void isInfoVisible(){

        if (imageView_info.getVisibility() != View.VISIBLE){
            imageView_info.setVisibility(View.VISIBLE);
        }

    }

    public void setScore(String Score,String subHeader){

        if(Integer.parseInt(Score) >= 0 && Integer.parseInt(Score) < 2000 ){
            linearLayout_red_ribbon.setBackground(getResources().getDrawable(R.drawable.red_ribbon));
        }else if(Integer.parseInt(Score) >= 2000 && Integer.parseInt(Score) < 5000){
            linearLayout_red_ribbon.setBackground(getResources().getDrawable(R.drawable.blue_ribbon));
        }else if(Integer.parseInt(Score) >= 5000 && Integer.parseInt(Score) < 8000){
            linearLayout_red_ribbon.setBackground(getResources().getDrawable(R.drawable.green_ribbon));
        }else if(Integer.parseInt(Score) > 8000){
            linearLayout_red_ribbon.setBackground(getResources().getDrawable(R.drawable.yellow_ribbon));
        }
        textView_candidate.setText("Candidate: "+Score+" Pts");
        textView_subheader.setText(subHeader);

        if(Integer.parseInt(SharedPrefrenceUtil.getPrefrence(HomeActivity.this,Constants.admin,"")) > 0) {
            admin_badge.setVisibility(View.VISIBLE);
            admin_badge.setText(SharedPrefrenceUtil.getPrefrence(HomeActivity.this, Constants.admin, ""));
        }else {
            admin_badge.setVisibility(View.INVISIBLE);
        }

    }


    public void particularMessage(String header,UserCompanyVo item,int pos){

        imageView_Portfolio.setImageResource(R.drawable.portfolio_unselected);
        imageView_Disciplines.setImageResource(R.drawable.discipline_unselected);
        imageView_Search.setImageResource(R.drawable.search_unselected);
        imageView_Messages.setImageResource(R.drawable.message_selected);
        imageView_Salary.setImageResource(R.drawable.sallary_unselected);

        textView_Portfolio.setTextColor(getResources().getColor(R.color.grey));
        textView_Disciplines.setTextColor(getResources().getColor(R.color.grey));
        textView_Search.setTextColor(getResources().getColor(R.color.grey));
        textView_Messages.setTextColor(getResources().getColor(R.color.tab_txt_color));
        textView_Salary.setTextColor(getResources().getColor(R.color.grey));


        textView_header.setText(header);
        textView_subheader.setText(SharedPrefrenceUtil.getPrefrence(HomeActivity.this,Constants.subHeader,""));
        imageView_back.setVisibility(View.VISIBLE);
        textView_cancel.setVisibility(View.INVISIBLE);
        textView_select.setVisibility(View.INVISIBLE);
        textView_cancelsearch.setVisibility(View.INVISIBLE);
        textView_selectsearch.setVisibility(View.INVISIBLE);
        txt_menu.setVisibility(View.INVISIBLE);
        imageView_filter.setVisibility(View.INVISIBLE);
        imageView_searchfilter.setVisibility(View.INVISIBLE);
        imageView_calender.setVisibility(View.VISIBLE);
        imageView_info.setVisibility(View.INVISIBLE);
        textView_subheader.setVisibility(View.VISIBLE);
        imageView_edit.setVisibility(View.INVISIBLE);
        imageView_students.setVisibility(View.INVISIBLE);
        imageView_logo.setVisibility(View.INVISIBLE);
        tabBar_layout.setVisibility(View.VISIBLE);
        linearLayout_red_ribbon.setVisibility(View.GONE);
        imageView_backsearch.setVisibility(View.INVISIBLE);

        starVisible();
        hideBadges();

        FragmentManager fragmentManager = getSupportFragmentManager();

        MessagesFragment fragment = new MessagesFragment();//pass value to be displayed in inflated view
        Bundle bundle = new Bundle();
        bundle.putSerializable(fragment.ARG_OBJECT,
                item);
        fragment.setArguments(bundle);

        fragmentManager.beginTransaction()
                .replace(R.id.frame_container, fragment).commit();
    }

    public void particularSocietyMessage(String society_name) {

        imageView_Portfolio.setImageResource(R.drawable.portfolio_unselected);
        imageView_Disciplines.setImageResource(R.drawable.discipline_unselected);
        imageView_Search.setImageResource(R.drawable.search_unselected);
        imageView_Messages.setImageResource(R.drawable.message_selected);
        imageView_Salary.setImageResource(R.drawable.sallary_unselected);

        textView_Portfolio.setTextColor(getResources().getColor(R.color.grey));
        textView_Disciplines.setTextColor(getResources().getColor(R.color.grey));
        textView_Search.setTextColor(getResources().getColor(R.color.grey));
        textView_Messages.setTextColor(getResources().getColor(R.color.tab_txt_color));
        textView_Salary.setTextColor(getResources().getColor(R.color.grey));

        imageView_back.setVisibility(View.VISIBLE);
        imageView_backsearch.setVisibility(View.INVISIBLE);
        txt_menu.setVisibility(View.INVISIBLE);
        textView_cancel.setVisibility(View.INVISIBLE);
        textView_select.setVisibility(View.INVISIBLE);
        textView_cancelsearch.setVisibility(View.INVISIBLE);
        textView_selectsearch.setVisibility(View.INVISIBLE);
        textView_header.setText(society_name);
        textView_subheader.setText(SharedPrefrenceUtil.getPrefrence(HomeActivity.this,Constants.university,""));
        imageView_filter.setVisibility(View.INVISIBLE);
        imageView_searchfilter.setVisibility(View.INVISIBLE);
        imageView_calender.setVisibility(View.VISIBLE);
        imageView_info.setVisibility(View.INVISIBLE);
        textView_subheader.setVisibility(View.VISIBLE);
        imageView_edit.setVisibility(View.INVISIBLE);
        imageView_students.setVisibility(View.INVISIBLE);
        imageView_logo.setVisibility(View.INVISIBLE);
        tabBar_layout.setVisibility(View.VISIBLE);
        linearLayout_red_ribbon.setVisibility(View.GONE);
        hideBadges();
        starVisible();

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.frame_container, new MessagesFragment()).commit();
    }

    public void CountryFilter(){

        Constants.isBack = "Country";
        if(Constants.checkedState != null && Constants.checkedState.size() > 0) {
            Set<String> hStates = new HashSet<>();
            hStates.addAll(Constants.checkedState);
            Constants.checkedState.clear();
            Constants.checkedState.addAll(hStates);
        }

        if(Constants.checkedCountry != null && Constants.checkedCountry.size() > 0) {
            Set<String> hCountries = new HashSet<>();
            hCountries.addAll(Constants.checkedCountry);
            Constants.checkedCountry.clear();
            Constants.checkedCountry.addAll(hCountries);
        }

        textView_header.setText("Select Country");
        textView_cancel.setVisibility(View.INVISIBLE);
        imageView_back.setVisibility(View.VISIBLE);
        txt_menu.setVisibility(View.INVISIBLE);
        textView_subheader.setVisibility(View.GONE);
        imageView_filter.setVisibility(View.INVISIBLE);
        imageView_searchfilter.setVisibility(View.INVISIBLE);
        imageView_calender.setVisibility(View.INVISIBLE);
        imageView_edit.setVisibility(View.INVISIBLE);
        imageView_students.setVisibility(View.INVISIBLE);
        imageView_logo.setVisibility(View.INVISIBLE);
        tabBar_layout.setVisibility(View.VISIBLE);
        imageView_info.setVisibility(View.INVISIBLE);
        linearLayout_red_ribbon.setVisibility(View.GONE);
        textView_select.setVisibility(View.VISIBLE);
        textView_cancelsearch.setVisibility(View.INVISIBLE);
        textView_selectsearch.setVisibility(View.INVISIBLE);
        imageView_backsearch.setVisibility(View.INVISIBLE);

        starVisible();
        hideBadges();

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.frame_container, new CountryFilterFragment()).commit();

    }

    public void allCompany(String strChild) {

        textView_header.setText(strChild);
        textView_cancel.setVisibility(View.INVISIBLE);
        textView_select.setVisibility(View.INVISIBLE);
        textView_cancelsearch.setVisibility(View.INVISIBLE);
        textView_selectsearch.setVisibility(View.INVISIBLE);
        imageView_back.setVisibility(View.VISIBLE);
        txt_menu.setVisibility(View.INVISIBLE);
        imageView_filter.setVisibility(View.VISIBLE);
        imageView_searchfilter.setVisibility(View.INVISIBLE);
        textView_subheader.setVisibility(View.VISIBLE);
        textView_subheader.setText("All Locations");
        imageView_calender.setVisibility(View.INVISIBLE);
        imageView_edit.setVisibility(View.INVISIBLE);
        imageView_students.setVisibility(View.INVISIBLE);
        imageView_logo.setVisibility(View.INVISIBLE);
        tabBar_layout.setVisibility(View.VISIBLE);
        imageView_info.setVisibility(View.INVISIBLE);
        linearLayout_red_ribbon.setVisibility(View.GONE);
        imageView_backsearch.setVisibility(View.INVISIBLE);
        hideBadges();
        starVisible();

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.frame_container, new AllCompanyFragment()).commit();
    }

    public void SearchCountryFilter(){

        if(Constants.checkedState != null && Constants.checkedState.size() > 0) {
            Set<String> hStates = new HashSet<>();
            hStates.addAll(Constants.checkedState);
            Constants.checkedState.clear();
            Constants.checkedState.addAll(hStates);
        }

        if(Constants.checkedCountry != null && Constants.checkedCountry.size() > 0) {
            Set<String> hCountries = new HashSet<>();
            hCountries.addAll(Constants.checkedCountry);
            Constants.checkedCountry.clear();
            Constants.checkedCountry.addAll(hCountries);
        }

        Constants.isBack = "CountrySearch";

        textView_header.setText("Select Country");
        imageView_back.setVisibility(View.INVISIBLE);
        textView_cancel.setVisibility(View.INVISIBLE);
        textView_select.setVisibility(View.INVISIBLE);
        textView_cancelsearch.setVisibility(View.INVISIBLE);
        textView_selectsearch.setVisibility(View.VISIBLE);
        txt_menu.setVisibility(View.INVISIBLE);
        imageView_filter.setVisibility(View.INVISIBLE);
        imageView_searchfilter.setVisibility(View.INVISIBLE);
        textView_subheader.setVisibility(View.GONE);
        imageView_calender.setVisibility(View.INVISIBLE);
        imageView_edit.setVisibility(View.INVISIBLE);
        imageView_students.setVisibility(View.INVISIBLE);
        imageView_logo.setVisibility(View.INVISIBLE);
        tabBar_layout.setVisibility(View.VISIBLE);
        imageView_info.setVisibility(View.INVISIBLE);
        linearLayout_red_ribbon.setVisibility(View.GONE);
        imageView_backsearch.setVisibility(View.VISIBLE);

        hideBadges();
        starVisible();

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.frame_container, new SearchCountryFilterFragment()).commit();

    }

    public void displayHeader(String header,String subHeader){

        textView_subheader.setText(subHeader);
        textView_header.setText(header);

        starVisible();

    }

    public void eventDetails(MessagesVo messagesVo) {

        textView_header.setText(getString(R.string.event_details));
        admin_badge.setVisibility(View.INVISIBLE);
        textView_cancel.setVisibility(View.INVISIBLE);
        textView_select.setVisibility(View.INVISIBLE);
        textView_cancel.setVisibility(View.INVISIBLE);
        imageView_back.setVisibility(View.VISIBLE);
        txt_menu.setVisibility(View.INVISIBLE);
        imageView_filter.setVisibility(View.INVISIBLE);
        imageView_searchfilter.setVisibility(View.INVISIBLE);
        textView_subheader.setVisibility(View.INVISIBLE);
        imageView_calender.setVisibility(View.INVISIBLE);
        imageView_edit.setVisibility(View.INVISIBLE);
        imageView_students.setVisibility(View.INVISIBLE);
        imageView_logo.setVisibility(View.INVISIBLE);
        tabBar_layout.setVisibility(View.VISIBLE);
        imageView_info.setVisibility(View.INVISIBLE);
        linearLayout_red_ribbon.setVisibility(View.GONE);

        starVisible();

        Constants.isBack = "Event details";
        FragmentManager fragmentManager = getSupportFragmentManager();

        EventsDetailsFragment fragment = new EventsDetailsFragment();//pass value to be displayed in inflated view
        Bundle bundle = new Bundle();
        bundle.putSerializable(fragment.ARG_OBJECT,
                messagesVo);
        fragment.setArguments(bundle);

        fragmentManager.beginTransaction()
                .replace(R.id.frame_container, fragment).commit();

    }

    public void setSubheaderAll(){
        textView_subheader.setVisibility(View.VISIBLE);
        textView_subheader.setText("All Locations");
    }

    public void setSubheader(ArrayList<String> abrList){
        if(abrList.size() > 0) {
            textView_subheader.setVisibility(View.VISIBLE);
            textView_subheader.setText(convertToString(abrList));
        }else{
            textView_subheader.setVisibility(View.INVISIBLE);
        }
    }
    static String convertToString(ArrayList<String> abrList) {
        StringBuilder builder = new StringBuilder();
        // Append all Integers in StringBuilder to the StringBuilder.
        for (String abr : abrList) {
            builder.append(abr);
            builder.append(",");
        }
        // Remove last delimiter with setLength.
        builder.setLength(builder.length() - 1);
        return builder.toString();
    }

    public void changeSocietyImage(boolean society){
        if(society) {
            imageView_students.setImageResource(R.drawable.eventlogo_new);
            if(Constants.society_count > 0) {
                student_badge.setVisibility(View.VISIBLE);
                student_badge.setText(""+Constants.society_count);
            }else{
                student_badge.setVisibility(View.INVISIBLE);
            }
        }else {
            imageView_students.setImageResource(R.drawable.portremoved);
            if(Constants.portfolio_count > 0) {
                student_badge.setVisibility(View.VISIBLE);
                student_badge.setText(""+Constants.portfolio_count);
            }else{
                student_badge.setVisibility(View.INVISIBLE);
            }
        }
    }
    public void hideBadges() {

        student_badge.setVisibility(View.GONE);
        admin_badge.setVisibility(View.GONE);

    }

    @Override
    protected void onResume() {
        super.onResume();
        hideBadges();
    }

    public void displayDetail(String title) {

        textView_header.setText(title);
        imageView_back.setVisibility(View.VISIBLE);
        textView_select.setVisibility(View.INVISIBLE);
        textView_cancelsearch.setVisibility(View.INVISIBLE);
        textView_selectsearch.setVisibility(View.INVISIBLE);
        txt_menu.setVisibility(View.INVISIBLE);
        imageView_filter.setVisibility(View.INVISIBLE);
        imageView_searchfilter.setVisibility(View.INVISIBLE);
        textView_subheader.setVisibility(View.GONE);
        imageView_calender.setVisibility(View.INVISIBLE);
        imageView_edit.setVisibility(View.INVISIBLE);
        imageView_students.setVisibility(View.INVISIBLE);
        imageView_logo.setVisibility(View.INVISIBLE);
        tabBar_layout.setVisibility(View.VISIBLE);
        imageView_info.setVisibility(View.INVISIBLE);
        linearLayout_red_ribbon.setVisibility(View.GONE);
        imageView_backsearch.setVisibility(View.INVISIBLE);
        imageView_logo_portfolio.setVisibility(View.VISIBLE);
        hideBadges();
        starVisible();

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.frame_container, new DetailFragment()).commit();

    }

    public void updatePort(){
        if(Constants.port == 0){
            imageView_logo_portfolio.setImageResource(R.drawable.portremoved);
            imageView_logo_portfolio.setBackground(getResources().getDrawable(R.drawable.white_border));
        }else{
            imageView_logo_portfolio.setImageResource(R.drawable.portadd);
            imageView_logo_portfolio.setBackground(getResources().getDrawable(R.drawable.port_add));
        }
    }

}
