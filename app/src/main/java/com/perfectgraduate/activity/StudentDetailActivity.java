package com.perfectgraduate.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.perfectgraduate.R;
import com.perfectgraduate.api.PerfectGraduateRequest;
import com.perfectgraduate.api.WebServiceAPI;
import com.perfectgraduate.app.PerfectGraduateApplication;
import com.perfectgraduate.listener.ICallBack;
import com.perfectgraduate.utility.Constants;
import com.perfectgraduate.utility.SharedPrefrenceUtil;
import com.perfectgraduate.vo.CountryVo;
import com.perfectgraduate.vo.DisciplineVo;
import com.perfectgraduate.vo.PositionVo;
import com.perfectgraduate.vo.StateVo;
import com.perfectgraduate.vo.SubDisciplineVo;
import com.perfectgraduate.vo.UniversityVo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class StudentDetailActivity extends BaseActivity implements View.OnClickListener {

    private StudentDetailActivity activity;
    private TextView textView_gender,textView_country,textView_location,textView_university,
            textView_position,textView_discipline,textView_subdiscipline,textView_discipline2,
            textView_subdiscipline2;
    private PerfectGraduateRequest request;

    private List<CountryVo> countryVoList;
    private List<StateVo> stateVoList;
    private List<UniversityVo> universityVoList;
    private List<DisciplineVo> disciplineVoList;
    private List<DisciplineVo> disciplineVoList2;
    private List<SubDisciplineVo> subDisciplineVoList;
    private List<SubDisciplineVo> subDisciplineVoList2;
    private List<PositionVo> positionVoList;
    private List<String> list_country;
    private List<String> list_state;
    private List<String> list_university;
    private List<String> list_position;
    private List<String> list_discipline;
    private List<String> list_subdiscipline;
    private List<String> list_discipline2;
    private List<String> list_subdiscipline2;

    private int genderSelect = 0;
    private int country = 0;
    private int state = 0;
    private int university = 0;
    private int position = 0;
    private int discipline = 0;
    private int subdiscipline = 0;
    private int discipline2 = 0;
    private int subdiscipline2 = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_detail);

        PerfectGraduateApplication.getInstance().displayCustomDialog(StudentDetailActivity.this,getString(R.string.alert_title), getString(R.string.alert_msg1));

        init();
    }

    private void init()
    {
        activity = this;

        request = new PerfectGraduateRequest(activity);

        countryVoList = new ArrayList<>();
        list_country = new ArrayList<>();

        stateVoList = new ArrayList<>();
        list_state = new ArrayList<>();

        universityVoList = new ArrayList<>();
        list_university = new ArrayList<>();

        disciplineVoList = new ArrayList<>();
        list_discipline = new ArrayList<>();

        subDisciplineVoList = new ArrayList<SubDisciplineVo>();
        list_subdiscipline = new ArrayList<>();

        positionVoList = new ArrayList<>();
        list_position = new ArrayList<>();

        disciplineVoList2 = new ArrayList<>();
        list_discipline2 = new ArrayList<>();

        subDisciplineVoList2 = new ArrayList<>();
        list_subdiscipline2 = new ArrayList<>();

        ((ImageView) findViewById(R.id.imv_back)).setOnClickListener(activity);

        ((TextView) findViewById(R.id.tv_next)).setOnClickListener(activity);

        ((LinearLayout) findViewById(R.id.linearLayout_gender)).setOnClickListener(activity);
        ((LinearLayout) findViewById(R.id.linearLayout_country)).setOnClickListener(activity);
        ((LinearLayout) findViewById(R.id.linearLayout_location)).setOnClickListener(activity);
        ((LinearLayout) findViewById(R.id.linearLayout_university)).setOnClickListener(activity);
        ((LinearLayout) findViewById(R.id.linearLayout_position)).setOnClickListener(activity);
        ((LinearLayout) findViewById(R.id.linearLayout_discipline)).setOnClickListener(activity);
        ((LinearLayout) findViewById(R.id.linearLayout_subdiscipline)).setOnClickListener(activity);
        ((LinearLayout) findViewById(R.id.linearLayout_discipline2)).setOnClickListener(activity);
        ((LinearLayout) findViewById(R.id.linearLayout_subdiscipline2)).setOnClickListener(activity);

        textView_gender = (TextView) findViewById(R.id.textView_gender);
        textView_country = (TextView) findViewById(R.id.textView_country);
        textView_location = (TextView) findViewById(R.id.textView_location);
        textView_university = (TextView) findViewById(R.id.textView_university);
        textView_position = (TextView) findViewById(R.id.textView_position);
        textView_discipline = (TextView) findViewById(R.id.textView_discipline);
        textView_subdiscipline = (TextView) findViewById(R.id.textView_subdiscipline);
        textView_discipline2 = (TextView) findViewById(R.id.textView_discipline2);
        textView_subdiscipline2 = (TextView) findViewById(R.id.textView_subdiscipline2);

        if (!PerfectGraduateApplication.getInstance().isConnectInternet()) {
            PerfectGraduateApplication.getInstance().setShowDialog(activity, "", getString(R.string.no_internet_connection));
        } else {
            getCountry();
        }
    }
    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.imv_back :
                onBackPressed();
                break;
            case R.id.tv_next:
                if(TextUtils.isEmpty(textView_gender.getText().toString())){
                    PerfectGraduateApplication.getInstance().setShowDialog(activity,"Alert","Please fill all required the Fields");
                }else if(TextUtils.isEmpty(textView_country.getText().toString())){
                    PerfectGraduateApplication.getInstance().setShowDialog(activity,"Alert","Please fill all required the Fields");
                }else if(TextUtils.isEmpty(textView_location.getText().toString())){
                    PerfectGraduateApplication.getInstance().setShowDialog(activity,"Alert","Please fill all required the Fields");
                }else if(TextUtils.isEmpty(textView_university.getText().toString())){
                    PerfectGraduateApplication.getInstance().setShowDialog(activity,"Alert","Please fill all required the Fields");
                }else if(TextUtils.isEmpty(textView_position.getText().toString())){
                    PerfectGraduateApplication.getInstance().setShowDialog(activity,"Alert","Please fill all required the Fields");
                }else if(TextUtils.isEmpty(textView_discipline.getText().toString())){
                    PerfectGraduateApplication.getInstance().setShowDialog(activity,"Alert","Please fill all required the Fields");
                }else{
                    Intent intent = new Intent(this, StudentSocietyActivity.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_left);
                }
                break;
            case R.id.linearLayout_gender:
                getGender();
                break;
            case R.id.linearLayout_country:
                displayCountry();
                break;
            case R.id.linearLayout_location:
                displayState();
                break;
            case R.id.linearLayout_university:
                displayUniversity();
                break;
            case R.id.linearLayout_position:
                displayPosition();
                break;
            case R.id.linearLayout_discipline:
                displayDiscipline();
                break;
            case R.id.linearLayout_subdiscipline:
                displaySubDiscipline();
                break;
            case R.id.linearLayout_discipline2:
                displayDiscipline2();
                break;
            case R.id.linearLayout_subdiscipline2:
                displaySubDiscipline2();
                break;
        }
    }

    private void displaySubDiscipline2() {
        if(list_subdiscipline2.size() > 0) {
            final CharSequence[] items;
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);

            int selectedItem;

            builder.setTitle(getString(R.string.select_subdiscipline));


            items = list_subdiscipline2.toArray(new CharSequence[list_subdiscipline2.size()]);

            if(textView_subdiscipline2.getText().toString().equals("")){
                selectedItem = -1;
            }else{
                selectedItem = subdiscipline2;
            }

            builder.setSingleChoiceItems(items,selectedItem, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item) {
                    // Do something with the selection
                    subdiscipline2 = item;
                    if(items[item].equals(getString(R.string.none))){
                        textView_subdiscipline2.setText("");
                        Constants.SubDiscipline2_id = "";
                        SharedPrefrenceUtil.setPrefrence(activity,Constants.SubDiscipline2_name,"");
                    }else {
                        textView_subdiscipline2.setText(items[item]);
                        String cid = "";
                        for (int i = 0; i < subDisciplineVoList2.size(); i++) {
                            if (subDisciplineVoList2.get(i).getName().equals(items[item])) {
                                cid = subDisciplineVoList2.get(i).getCountry_id();

                                SharedPrefrenceUtil.setPrefrence(activity,Constants.SubDiscipline2_id,subDisciplineVoList2.get(i).getId());
                               // Constants.SubDiscipline2_id = subDisciplineVoList2.get(i).getId();
                            }
                        }
                    }
                    dialog.dismiss();
                }
            });
            AlertDialog alert = builder.create();
            alert.show();
        }

    }

    private void displayDiscipline2() {
        if(list_discipline2.size() > 0) {
            final CharSequence[] items;
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);

            int selectedItem = 0;

            builder.setTitle(getString(R.string.select_discipline));

            items = list_discipline2.toArray(new CharSequence[list_discipline2.size()]);

            if(textView_discipline2.getText().toString().equals("")){
                selectedItem = -1;
            }else{
                selectedItem = discipline2;
            }

            builder.setSingleChoiceItems(items,selectedItem, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item) {
                    // Do something with the selection
                    discipline2 = item;
                    if(items[item].equals(getString(R.string.none))){
                        textView_discipline2.setText("");
                        Constants.Discipline2_id = "";
                        SharedPrefrenceUtil.setPrefrence(activity,Constants.Discipline2_name,"");
                    }else {
                        textView_discipline2.setText(items[item]);
                        String did = "";
                        for (int i = 0; i < disciplineVoList2.size(); i++) {
                            if (disciplineVoList2.get(i).getName().equals(items[item])) {
                                did = disciplineVoList2.get(i).getId();
                                //Constants.Discipline2_id = disciplineVoList2.get(i).getId();
                                SharedPrefrenceUtil.setPrefrence(activity,Constants.Discipline2_id,disciplineVoList2.get(i).getId());
                            }
                        }

                        getSubDiscipline2(did);
                        dialog.dismiss();
                    }
                }
            });
            AlertDialog alert = builder.create();
            alert.show();
        }

    }


    private void displaySubDiscipline() {

        if(list_subdiscipline.size() > 0) {
            final CharSequence[] items;
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);

            int selectedItem = 0;

            builder.setTitle(getString(R.string.select_subdiscipline));

            items = list_subdiscipline.toArray(new CharSequence[list_subdiscipline.size()]);

            if(textView_subdiscipline.getText().toString().equals("")){
                selectedItem = -1;
            }else{
                selectedItem = subdiscipline;
            }

            builder.setSingleChoiceItems(items,selectedItem, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item) {
                    // Do something with the selection
                    subdiscipline = item;
                    if(items[item].equals(getString(R.string.none))){
                        textView_subdiscipline.setText("");
                        Constants.SubDiscipline_id = "";
                        SharedPrefrenceUtil.setPrefrence(activity,Constants.SubDiscipline_name,"");
                    }else {
                        textView_subdiscipline.setText(items[item]);
                        String cid = "";
                        for (int i = 0; i < subDisciplineVoList.size(); i++) {
                            if (subDisciplineVoList.get(i).getName().equals(items[item])) {
                                cid = subDisciplineVoList.get(i).getCountry_id();
                                //Constants.SubDiscipline_id = subDisciplineVoList.get(i).getId();
                                SharedPrefrenceUtil.setPrefrence(activity,Constants.SubDiscipline_id,subDisciplineVoList.get(i).getId());
                            }
                        }
                        getDiscipline2("", cid);
                        dialog.dismiss();
                    }
                }
            });
            AlertDialog alert = builder.create();
            alert.show();
        }

    }

    private void displayDiscipline() {

        if(list_discipline.size() > 0) {
            final CharSequence[] items;
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            int selectedItem = 0;
            builder.setTitle(getString(R.string.select_discipline));

            items = list_discipline.toArray(new CharSequence[list_discipline.size()]);

            if(textView_discipline.getText().toString().equals("")){
                selectedItem = -1;
            }else{
                selectedItem = discipline;
            }

            builder.setSingleChoiceItems(items,selectedItem, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item) {
                    // Do something with the selection

                        textView_discipline.setText(items[item]);
                        discipline = item;
                        String did = "";
                        for (int i = 0; i < disciplineVoList.size(); i++) {
                            if (disciplineVoList.get(i).getName().equals(items[item])) {
                                did = disciplineVoList.get(i).getId();
                                //Constants.Discipline_id = disciplineVoList.get(i).getId();
                                SharedPrefrenceUtil.setPrefrence(activity,Constants.Discipline_id,disciplineVoList.get(i).getId());
                            }
                        }
                        ((TextView) findViewById(R.id.tv_next)).setEnabled(true);
                        ((TextView) findViewById(R.id.tv_next)).setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                        ((TextView) findViewById(R.id.tv_next)).setBackgroundDrawable(getResources().getDrawable(R.drawable.white_success_btn_rect));

                    getSubDiscipline(did);
                    dialog.dismiss();
                }
            });
            AlertDialog alert = builder.create();
            alert.show();
        }

    }

    private void displayPosition() {
        if(list_position.size() > 0) {
            final CharSequence[] items;
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);

            int selectedItem = 0;

            builder.setTitle(getString(R.string.select_position));

            items = list_position.toArray(new CharSequence[list_position.size()]);

            if(textView_position.getText().toString().equals("")){
                selectedItem = -1;
            }else{
                selectedItem = position;
            }

            builder.setSingleChoiceItems(items,selectedItem, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item) {
                    // Do something with the selection

                        textView_position.setText(items[item]);
                        position = item;
                        String cid = "";
                        for (int i = 0; i < positionVoList.size(); i++) {
                            if (positionVoList.get(i).getName().equals(items[item])) {
                                cid = positionVoList.get(i).getCountry_id();
                                //Constants.Position_id = positionVoList.get(i).getId();
                                SharedPrefrenceUtil.setPrefrence(activity,Constants.Position_id,positionVoList.get(i).getId());
                            }
                        }
                        getDiscipline("", cid);
                    dialog.dismiss();
                }
            });
            AlertDialog alert = builder.create();
            alert.show();
        }

    }

    private void displayUniversity() {
        textView_position.setText("");
        textView_discipline.setText("");
        textView_subdiscipline.setText("");
        textView_discipline2.setText("");
        textView_subdiscipline2.setText("");
        if(list_university.size() > 0) {
            final CharSequence[] items;

            int selectedItem = 0;

            AlertDialog.Builder builder = new AlertDialog.Builder(activity);

            builder.setTitle(getString(R.string.select_university));

            items = list_university.toArray(new CharSequence[list_university.size()]);

            if(textView_university.getText().toString().equals("")){
                selectedItem = -1;
            }else{
                selectedItem = university;
            }

            builder.setSingleChoiceItems(items,selectedItem, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item) {
                    // Do something with the selection

                        textView_university.setText(items[item]);
                        String cid = "";
                        university = item;
                        for (int i = 0; i < universityVoList.size(); i++) {
                            if (universityVoList.get(i).getName().equals(items[item])) {
                                cid = universityVoList.get(i).getCountry_id();
                               // Constants.University_id = universityVoList.get(i).getId();
                                SharedPrefrenceUtil.setPrefrence(activity,Constants.University_id,universityVoList.get(i).getId());
                            }
                        }
                        getPosition(cid);
                    dialog.dismiss();

                }
            });
            AlertDialog alert = builder.create();
            alert.show();
        }

    }

    private void displayState() {
        textView_university.setText("");
        textView_position.setText("");
        textView_discipline.setText("");
        textView_subdiscipline.setText("");
        textView_discipline2.setText("");
        textView_subdiscipline2.setText("");
        if(list_state.size() > 0) {
            final CharSequence[] items;

            int selectedItem = 0;

            AlertDialog.Builder builder = new AlertDialog.Builder(activity);

            builder.setTitle(getString(R.string.select_state));

            items = list_state.toArray(new CharSequence[list_state.size()]);

            if(textView_location.getText().toString().equals("")){
                selectedItem = -1;
            }else{
                selectedItem = state;
            }

            builder.setSingleChoiceItems(items,selectedItem, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item) {
                    // Do something with the selection
                        textView_location.setText(items[item]);
                        String sid = "";
                        state = item;
                        for (int i = 0; i < stateVoList.size(); i++) {
                            if (stateVoList.get(i).getName().equals(items[item])) {
                                sid = stateVoList.get(i).getId();
                                //Constants.State_id = stateVoList.get(i).getId();
                                SharedPrefrenceUtil.setPrefrence(activity,Constants.State_id,stateVoList.get(i).getId());
                            }
                        }
                    getUniversity(sid);
                    dialog.dismiss();

                }
            });
            AlertDialog alert = builder.create();
            alert.show();
        }

    }

    private void displayCountry() {
        textView_location.setText("");
        textView_university.setText("");
        textView_position.setText("");
        textView_discipline.setText("");
        textView_subdiscipline.setText("");
        textView_discipline2.setText("");
        textView_subdiscipline2.setText("");
        if(list_country.size() > 0) {
            final CharSequence[] items;
            int selectedItem = 0;
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);

            builder.setTitle(getString(R.string.select_country));

            items = list_country.toArray(new CharSequence[list_country.size()]);

            if(textView_country.getText().toString().equals("")){
                selectedItem = -1;
            }else{
                selectedItem = country;
            }

            builder.setSingleChoiceItems(items,selectedItem, new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialog, int which)
                {
                    textView_country.setText(items[which]);
                    String cid = "";
                    country = which;
                    for (int i = 0; i < countryVoList.size(); i++) {
                        if (countryVoList.get(i).getName().equals(items[which])) {
                            cid = countryVoList.get(i).getId();
                           // Constants.Country_id = countryVoList.get(i).getId();
                            SharedPrefrenceUtil.setPrefrence(activity,Constants.Country_id,countryVoList.get(i).getId());
                        }
                    }

                    getLocation(cid);
                    dialog.dismiss();
                }
            });

            AlertDialog alert = builder.create();
            alert.show();

        }

    }


    private void getGender() {
        final CharSequence[] gender = {getString(R.string.male),getString(R.string.female)};
        AlertDialog.Builder alert = new AlertDialog.Builder(activity);
        alert.setTitle(getString(R.string.select_gender));

        int selectedItem = 0;

        if(textView_gender.getText().toString().equals("")){
            selectedItem = -1;
        }else{
            selectedItem = genderSelect;
        }

        alert.setSingleChoiceItems(gender,selectedItem, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {

                genderSelect = which;

                if(gender[which]== getString(R.string.male)) {
                    textView_gender.setText(getString(R.string.male));
                    SharedPrefrenceUtil.setPrefrence(activity,Constants.Gender,getString(R.string.male));
                }
                else if (gender[which]== getString(R.string.female)) {
                    textView_gender.setText(getString(R.string.female));
                    SharedPrefrenceUtil.setPrefrence(activity,Constants.Gender,getString(R.string.female));
                }
                dialog.dismiss();
            }
        });
        alert.show();
    }

    private void getCountry() {

        Map<String, Object> map = new HashMap<String, Object>();

        if(list_country.size() > 0 && list_country != null){
            list_country.clear();
        }
        if(countryVoList.size() > 0 && countryVoList != null){
            countryVoList.clear();
        }

        request.getResponse(activity, map, WebServiceAPI.API_getAllCountries, false, new ICallBack() {
            @Override
            public void onCallBackResult(String response) {

                try {

                    JSONObject jsonObject = new JSONObject(response);

                    String status = jsonObject.getString(WebServiceAPI.status);

                    if(Boolean.parseBoolean(status)){

                        JSONArray jsonArray = jsonObject.getJSONArray(WebServiceAPI.data);

                        if(jsonArray != null && jsonArray.length() > 0){

                            for (int i = 0; i < jsonArray.length(); i++){

                                JSONObject object = jsonArray.getJSONObject(i);

                                CountryVo countryVo = new CountryVo();

                                countryVo.setId(object.getString("id"));
                                countryVo.setName(object.getString("name"));
                                countryVo.setAbr(object.getString("abr"));

                                countryVoList.add(countryVo);
                                list_country.add(object.getString("name"));
                            }

                        }

                    }else{
                        if(list_country.size() > 0 && list_country != null){
                            list_country.clear();
                        }
                        if(countryVoList.size() > 0 && countryVoList != null){
                            countryVoList.clear();
                        }
                        textView_country.setText("");
                    }

                }catch(JSONException k){
                    k.printStackTrace();
                }

            }
        });

    }

    private void getLocation(String cid) {

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("cid",cid);

        if(list_state.size() > 0 && list_state != null){
            list_state.clear();
        }
        if(stateVoList.size() > 0 && stateVoList != null){
            stateVoList.clear();
        }

        request.getResponse(activity, map, WebServiceAPI.API_getAllStates, false, new ICallBack() {
            @Override
            public void onCallBackResult(String response) {

                try {

                    JSONObject jsonObject = new JSONObject(response);

                    String status = jsonObject.getString(WebServiceAPI.status);

                    if(Boolean.parseBoolean(status)){

                        JSONArray jsonArray = jsonObject.getJSONArray(WebServiceAPI.data);

                        if(jsonArray != null && jsonArray.length() > 0){

                            for (int i = 0; i < jsonArray.length(); i++){

                                JSONObject object = jsonArray.getJSONObject(i);

                                StateVo stateVo = new StateVo();

                                stateVo.setId(object.getString("id"));
                                stateVo.setCountry_id(object.getString("country_id"));
                                stateVo.setName(object.getString("name"));
                                stateVo.setAbr(object.getString("abr"));
                                stateVo.setMap_reference(object.getString("map_reference"));
                                stateVo.setLatitude(object.getString("latitude"));
                                stateVo.setLongitude(object.getString("longitude"));
                                stateVo.setStatus(object.getString("status"));
                                stateVo.setDate(object.getString("date"));

                                stateVoList.add(stateVo);
                                list_state.add(object.getString("name"));
                            }

                        }

                    }else{
                        if(list_state.size() > 0 && list_state != null){
                            list_state.clear();
                        }
                        if(stateVoList.size() > 0 && stateVoList != null){
                            stateVoList.clear();
                        }
                        textView_location.setText("");
                    }

                }catch(JSONException k){
                    k.printStackTrace();
                }

            }
        });

    }

    private void getUniversity(String sid) {

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("sid",sid);

        if(list_university.size() > 0 && list_university != null){
            list_university.clear();
        }
        if(universityVoList.size() > 0 && universityVoList != null){
            universityVoList.clear();
        }

        request.getResponse(activity, map, WebServiceAPI.API_getAllUniversities, false, new ICallBack() {
            @Override
            public void onCallBackResult(String response) {

                try {

                    JSONObject jsonObject = new JSONObject(response);

                    String status = jsonObject.getString(WebServiceAPI.status);

                    if(Boolean.parseBoolean(status)){

                        JSONArray jsonArray = jsonObject.getJSONArray(WebServiceAPI.data);

                        if(jsonArray != null && jsonArray.length() > 0){

                            for (int i = 0; i < jsonArray.length(); i++){


                                JSONObject object = jsonArray.getJSONObject(i);

                                UniversityVo universityVo = new UniversityVo();

                                universityVo.setId(object.getString("id"));
                                universityVo.setCountry_id(object.getString("country_id"));
                                universityVo.setName(object.getString("name"));
                                universityVo.setStatus(object.getString("status"));
                                universityVo.setDate(object.getString("date"));

                                universityVoList.add(universityVo);
                                list_university.add(object.getString("name"));
                            }

                        }

                    }else{
                        if(list_university.size() > 0 && list_university != null){
                            list_university.clear();
                        }
                        if(universityVoList.size() > 0 && universityVoList != null){
                            universityVoList.clear();
                        }
                        textView_university.setText("");
                    }

                }catch(JSONException k){
                    k.printStackTrace();
                }

            }
        });

    }

    private void getPosition(String cid) {

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("cid",cid);

        if(list_position.size() > 0 && list_position != null){
            list_position.clear();
        }
        if(positionVoList.size() > 0 && positionVoList != null){
            positionVoList.clear();
        }

        request.getResponse(activity, map, WebServiceAPI.API_getposition, false, new ICallBack() {
            @Override
            public void onCallBackResult(String response) {

                try {

                    JSONObject jsonObject = new JSONObject(response);

                    String status = jsonObject.getString(WebServiceAPI.status);

                    if(Boolean.parseBoolean(status)){

                        JSONArray jsonArray = jsonObject.getJSONArray(WebServiceAPI.data);

                        if(jsonArray != null && jsonArray.length() > 0){

                            for (int i = 0; i < jsonArray.length(); i++){

                                JSONObject object = jsonArray.getJSONObject(i);

                                PositionVo positionVo = new PositionVo();

                                positionVo.setId(object.getString("id"));
                                positionVo.setAbr(object.getString("abr"));
                                positionVo.setCountry_id(object.getString("country_id"));
                                positionVo.setName(object.getString("name"));
                                positionVo.setStatus(object.getString("status"));
                                positionVo.setDate(object.getString("date"));

                                positionVoList.add(positionVo);
                                list_position.add(object.getString("name"));
                            }

                        }

                    }else{
                        if(list_position.size() > 0 && list_position != null){
                            list_position.clear();
                        }
                        if(positionVoList.size() > 0 && positionVoList != null){
                            positionVoList.clear();
                        }
                        textView_position.setText("");
                    }
                }catch(JSONException k){
                    k.printStackTrace();
                }

            }
        });

    }


    private void getDiscipline(String uid,String cid) {

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("uid","");
        map.put("cid",cid);

        if(list_discipline.size() > 0 && list_discipline != null){
            list_discipline.clear();
        }
        if(disciplineVoList.size() > 0 && disciplineVoList != null){
            disciplineVoList.clear();
        }

        request.getResponse(activity, map, WebServiceAPI.API_getAlldisciplines, false, new ICallBack() {
            @Override
            public void onCallBackResult(String response) {

                try {

                    JSONObject jsonObject = new JSONObject(response);

                    String status = jsonObject.getString(WebServiceAPI.status);

                    if(Boolean.parseBoolean(status)){

                        JSONArray jsonArray = jsonObject.getJSONArray(WebServiceAPI.data);

                        if(jsonArray != null && jsonArray.length() > 0){

                            for (int i = 0; i < jsonArray.length(); i++) {


                                JSONObject object = jsonArray.getJSONObject(i);

                                DisciplineVo disciplineVo = new DisciplineVo();

                                disciplineVo.setId(object.getString("id"));
                                disciplineVo.setName(object.getString("name"));

                                /*JSONArray array = object.getJSONArray("subdiscipline");

                                if (array != null && array.length() > 0) {

                                    subDisciplineVoList = new ArrayList<SubDisciplineVo>();

                                    for (int j = 0; j < array.length(); j++) {

                                        JSONObject objectSub = array.getJSONObject(j);

                                        SubDisciplineVo subDisciplineVo = new SubDisciplineVo();

                                        subDisciplineVo.setId(objectSub.getString("id"));
                                        subDisciplineVo.setName(objectSub.getString("name"));
                                        subDisciplineVo.setCountry_id(objectSub.getString("country_id"));
                                        subDisciplineVo.setDiscipline_id(objectSub.getString("discipline_id"));
                                        subDisciplineVo.setDate(objectSub.getString("date"));
                                        subDisciplineVo.setStatus(objectSub.getString("status"));

                                        subDisciplineVoList.add(subDisciplineVo);
                                    }
                                }

                                disciplineVo.setSubDisciplineVoList(subDisciplineVoList);*/

                                disciplineVoList.add(disciplineVo);

                                list_discipline.add(object.getString("name"));
                            }
                        }

                    }else{
                        if(list_discipline.size() > 0 && list_discipline != null){
                            list_discipline.clear();
                        }
                        if(disciplineVoList.size() > 0 && disciplineVoList != null){
                            disciplineVoList.clear();
                        }
                        textView_discipline.setText("");
                    }

                }catch(JSONException k){
                    k.printStackTrace();
                }

            }
        });

    }

    private void getSubDiscipline(String did) {

        Map<String, Object> map = new HashMap<String, Object>();

        map.put("did",did);

        if(list_subdiscipline.size() > 0 && list_subdiscipline != null){
            list_subdiscipline.clear();
        }
        if(subDisciplineVoList.size() > 0 && subDisciplineVoList != null){
            subDisciplineVoList.clear();
        }

        request.getResponse(activity, map, WebServiceAPI.API_getAllSubdisciplines, false, new ICallBack() {
            @Override
            public void onCallBackResult(String response) {

                try {

                    JSONObject jsonObject = new JSONObject(response);

                    String status = jsonObject.getString(WebServiceAPI.status);

                    if(Boolean.parseBoolean(status)){

                        JSONArray jsonArray = jsonObject.getJSONArray(WebServiceAPI.data);

                        if(jsonArray != null && jsonArray.length() > 0){

                            for (int j = 0; j < jsonArray.length(); j++) {

                                if(j == 0){
                                    list_subdiscipline.add(getString(R.string.none));
                                }

                                JSONObject objectSub = jsonArray.getJSONObject(j);

                                SubDisciplineVo subDisciplineVo = new SubDisciplineVo();

                                subDisciplineVo.setId(objectSub.getString("id"));
                                subDisciplineVo.setName(objectSub.getString("name"));
                                subDisciplineVo.setCountry_id(objectSub.getString("country_id"));
                                subDisciplineVo.setDiscipline_id(objectSub.getString("discipline_id"));
                                subDisciplineVo.setDate(objectSub.getString("date"));
                                subDisciplineVo.setStatus(objectSub.getString("status"));

                                subDisciplineVoList.add(subDisciplineVo);

                                list_subdiscipline.add(objectSub.getString("name"));
                            }
                        }

                    }else{
                        if(list_subdiscipline.size() > 0 && list_subdiscipline != null){
                            list_subdiscipline.clear();
                        }
                        if(subDisciplineVoList.size() > 0 && subDisciplineVoList != null){
                            subDisciplineVoList.clear();
                        }
                        textView_subdiscipline.setText("");
                    }

                }catch(JSONException k){
                    k.printStackTrace();
                }

            }
        });

    }


    private void getDiscipline2(String uid,String cid) {

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("uid","");
        map.put("cid",cid);

        if(list_discipline2.size() > 0 && list_discipline2 != null){
            list_discipline2.clear();
        }
        if(disciplineVoList2.size() > 0 && disciplineVoList2 != null){
            disciplineVoList2.clear();
        }

        request.getResponse(activity, map, WebServiceAPI.API_getAlldisciplines, false, new ICallBack() {
            @Override
            public void onCallBackResult(String response) {

                try {

                    JSONObject jsonObject = new JSONObject(response);

                    String status = jsonObject.getString(WebServiceAPI.status);

                    if(Boolean.parseBoolean(status)){

                        JSONArray jsonArray = jsonObject.getJSONArray(WebServiceAPI.data);

                        if(jsonArray != null && jsonArray.length() > 0){

                            for (int i = 0; i < jsonArray.length(); i++) {

                                if(i == 0){
                                    list_discipline2.add(getString(R.string.none));
                                }

                                JSONObject object = jsonArray.getJSONObject(i);

                                DisciplineVo disciplineVo = new DisciplineVo();

                                disciplineVo.setId(object.getString("id"));
                                disciplineVo.setName(object.getString("name"));

                                disciplineVoList2.add(disciplineVo);

                                list_discipline2.add(object.getString("name"));
                            }
                        }

                    }else{
                        if(list_discipline2.size() > 0 && list_discipline2 != null){
                            list_discipline2.clear();
                        }
                        if(disciplineVoList2.size() > 0 && disciplineVoList2 != null){
                            disciplineVoList2.clear();
                        }
                        textView_discipline2.setText("");
                    }

                }catch(JSONException k){
                    k.printStackTrace();
                }

            }
        });

    }

    private void getSubDiscipline2(String did) {

        Map<String, Object> map = new HashMap<String, Object>();

        map.put("did",did);

        if(list_subdiscipline2.size() > 0 && list_subdiscipline2 != null){
            list_subdiscipline2.clear();
        }
        if(subDisciplineVoList2.size() > 0 && subDisciplineVoList2 != null){
            subDisciplineVoList2.clear();
        }

        request.getResponse(activity, map, WebServiceAPI.API_getAllSubdisciplines, false, new ICallBack() {
            @Override
            public void onCallBackResult(String response) {

                try {

                    JSONObject jsonObject = new JSONObject(response);

                    String status = jsonObject.getString(WebServiceAPI.status);

                    if(Boolean.parseBoolean(status)){

                        JSONArray jsonArray = jsonObject.getJSONArray(WebServiceAPI.data);

                        if(jsonArray != null && jsonArray.length() > 0){

                            for (int j = 0; j < jsonArray.length(); j++) {

                                if(j == 0){
                                    list_subdiscipline2.add(getString(R.string.none));
                                }

                                JSONObject objectSub = jsonArray.getJSONObject(j);

                                SubDisciplineVo subDisciplineVo = new SubDisciplineVo();

                                subDisciplineVo.setId(objectSub.getString("id"));
                                subDisciplineVo.setName(objectSub.getString("name"));
                                subDisciplineVo.setCountry_id(objectSub.getString("country_id"));
                                subDisciplineVo.setDiscipline_id(objectSub.getString("discipline_id"));
                                subDisciplineVo.setDate(objectSub.getString("date"));
                                subDisciplineVo.setStatus(objectSub.getString("status"));

                                subDisciplineVoList2.add(subDisciplineVo);

                                list_subdiscipline2.add(objectSub.getString("name"));
                            }
                        }

                    }else{
                        if(list_subdiscipline2.size() > 0 && list_subdiscipline2 != null){
                            list_subdiscipline2.clear();
                        }
                        if(subDisciplineVoList2.size() > 0 && subDisciplineVoList2 != null){
                            subDisciplineVoList2.clear();
                        }
                        textView_subdiscipline2.setText("");
                    }

                }catch(JSONException k){
                    k.printStackTrace();
                }

            }
        });

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_right);
        finish();
    }

}
