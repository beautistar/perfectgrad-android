package com.perfectgraduate.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.firebase.iid.FirebaseInstanceId;
import com.perfectgraduate.R;
import com.perfectgraduate.api.PerfectGraduateRequest;
import com.perfectgraduate.api.WebServiceAPI;
import com.perfectgraduate.database.DatabaseHelper;
import com.perfectgraduate.listener.ICallBack;
import com.perfectgraduate.utility.Constants;
import com.perfectgraduate.utility.SharedPrefrenceUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;


public class LoginActivity extends BaseActivity implements View.OnClickListener {

    TextView ui_fbLogin, ui_tmpLogin;
    ImageView ui_i;
    SharedPreferences preferences;
    private CallbackManager mCallbackManager;
    private LoginActivity activity;
    private PerfectGraduateRequest request;
    private DatabaseHelper databaseHelper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activity = this;
        request = new PerfectGraduateRequest(activity);
        databaseHelper = new DatabaseHelper(activity);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow(); // in Activity's onCreate() for instance
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }
        preferences = getSharedPreferences("prefrence", Context.MODE_PRIVATE);
        if (preferences.getBoolean("login", false)) {
            Intent intent = new Intent(activity, HomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
            finish();
        } else {
            setContentView(R.layout.activity_login);
            loadLayout();
        }

    }

    private void loadLayout() {

        getSupportActionBar().hide();

        ui_fbLogin = (TextView) findViewById(R.id.tv_fb_login);

        ui_tmpLogin = (TextView) findViewById(R.id.tv_tmp_login);
        ui_tmpLogin.setOnClickListener(activity);
        ui_fbLogin.setOnClickListener(activity);

        ui_tmpLogin.setShadowLayer(
                1f, // radius
                2.5f, // dx
                2.5f, // dy
                Color.parseColor("#0a4daa"));

        ui_i = (ImageView) findViewById(R.id.imv_i);
        ui_i.setOnClickListener(activity);

        FacebookSdk.sdkInitialize(activity.getApplicationContext());

        mCallbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().registerCallback(mCallbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        if (AccessToken.getCurrentAccessToken() != null) {
                            RequestData();
                        }
                    }

                    @Override
                    public void onCancel() {
                    }

                    @Override
                    public void onError(FacebookException exception) {
                    }
                });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(mCallbackManager.onActivityResult(requestCode, resultCode, data)) {
            return;
        }
    }

    private void RequestData() {

        GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {

                        JSONObject json = response.getJSONObject();

                        Log.e("response"," -- "+json.toString());

                        try {
                            if (json != null) {

                                SharedPrefrenceUtil.setPrefrence(activity,Constants.FB_ID,json.getString("id"));
                                SharedPrefrenceUtil.setPrefrence(activity,Constants.FIRST_NAME,json.getString("first_name"));
                                SharedPrefrenceUtil.setPrefrence(activity,Constants.LAST_NAME,json.getString("last_name"));

                                if (json.has("email")) {
                                    SharedPrefrenceUtil.setPrefrence(activity,Constants.EMAIL_ID,json.getString("email"));
                                }
                                gotoTmpLoginStudentDetail();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,first_name,last_name,email");
        request.setParameters(parameters);
        request.executeAsync();

    }

    @Override
    public void onClick(View view) {

        if (view.getId() == R.id.tv_tmp_login) {
            Intent stdDetailIntent = new Intent(activity, StudentDetailActivity.class);
            startActivity(stdDetailIntent);
            overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_left);
        } else if(view.getId() == R.id.tv_fb_login){
            LoginManager.getInstance().logInWithReadPermissions(activity, Arrays.asList("public_profile", "email"));
        }else if(view.getId() == R.id.imv_i){
            // goto i
            gotoI();
        }
    }

    private void gotoI() {

        Intent iIntent = new Intent(activity, InformationActivity.class);
        startActivity(iIntent);
    }

    private void gotoTmpLoginStudentDetail() {

        Map<String, Object> map = new HashMap<String, Object>();

        map.put("fb_id",SharedPrefrenceUtil.getPrefrence(activity,Constants.FB_ID,""));
        map.put("email",SharedPrefrenceUtil.getPrefrence(activity,Constants.EMAIL_ID,""));
        map.put("Name",SharedPrefrenceUtil.getPrefrence(activity,Constants.FIRST_NAME,"")+" "+SharedPrefrenceUtil.getPrefrence(this,Constants.LAST_NAME,""));
        map.put("device", FirebaseInstanceId.getInstance().getToken());
        map.put("device type", "android");

        request.getResponse(activity, map, WebServiceAPI.API_register_fb, true, new ICallBack() {
            @Override
            public void onCallBackResult(String response) {
                try{

                    JSONObject jsonObject = new JSONObject(response);

                    String status = jsonObject.getString(WebServiceAPI.status);

                    SharedPrefrenceUtil.setPrefrence(activity,Constants.USER_ID,jsonObject.getString("user_id"));

                    if(jsonObject.getString("message").equals("succesfully register")){
                        Intent stdDetailIntent = new Intent(activity, StudentDetailActivity.class);
                        startActivity(stdDetailIntent);
                        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_left);
                    }else if(jsonObject.getString("message").equals("User Already Registered")){
                        if (jsonObject.getString("check").equals("no")){
                            Intent stdDetailIntent = new Intent(activity, StudentDetailActivity.class);
                            startActivity(stdDetailIntent);
                            overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_left);
                        }else{

                            if(SharedPrefrenceUtil.getPrefrence(activity,Constants.isAllCompany,"").equals("Database")){

                                SharedPreferences.Editor editor = preferences.edit();
                                editor.putBoolean("login", true);
                                editor.apply();

                                Intent stdDetailIntent = new Intent(activity, HomeActivity.class);
                                stdDetailIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(stdDetailIntent);
                                overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_left);
                                finish();

                            }else{
                                getAllcompany();
                            }
                        }
                        SharedPrefrenceUtil.setPrefrence(activity,Constants.USER_ID,jsonObject.getString("user_id"));
                    }

                }catch (JSONException e){
                    e.printStackTrace();
                }
            }
        });

    }

    private void getAllcompany() {

        Map<String, Object> map = new HashMap<String, Object>();

        map.put("id",SharedPrefrenceUtil.getPrefrence(activity,Constants.USER_ID,""));


        request.getResponse(activity, map, WebServiceAPI.API_getAllCompanies, false, new ICallBack() {
            @Override
            public void onCallBackResult(String response) {

                try {

                    JSONObject jsonObject = new JSONObject(response);

                    String status = jsonObject.getString(WebServiceAPI.status);

                    if(Boolean.parseBoolean(status)){

                        JSONArray jsonArray = jsonObject.getJSONArray(WebServiceAPI.data);


                        if(jsonArray != null && jsonArray.length() > 0){

                            databaseHelper.insertAllCompanies(activity, jsonArray);

                        }
                    }

                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putBoolean("login", true);
                    editor.apply();

                    Intent stdDetailIntent = new Intent(activity, HomeActivity.class);
                    stdDetailIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(stdDetailIntent);
                    overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_left);
                    finish();

                }catch(JSONException k){
                    k.printStackTrace();
                }

            }
        });

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_right);
        finish();
    }
}
