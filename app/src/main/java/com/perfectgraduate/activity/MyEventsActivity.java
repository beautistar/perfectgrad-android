package com.perfectgraduate.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.perfectgraduate.R;
import com.perfectgraduate.adapter.MessageAdapter;
import com.perfectgraduate.adapter.MyEventsAdapter;
import com.perfectgraduate.utility.Constants;
import com.perfectgraduate.utility.SharedPrefrenceUtil;

/**
 * Created by user on 10-Dec-16.
 */

public class MyEventsActivity extends AppCompatActivity{

    private ImageView imv_back;
    private ListView listView_calender;
    private LinearLayout linearLayout_companySociety;
    private TextView textView_calendersubheader,textView_studentsocieties;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_myevents);

        getSupportActionBar().hide();

        imv_back = (ImageView) findViewById(R.id.imv_back);
        listView_calender = (ListView) findViewById(R.id.listView_calender);
        linearLayout_companySociety = (LinearLayout) findViewById(R.id.linearLayout_companySociety);
        textView_calendersubheader = (TextView) findViewById(R.id.textView_calendersubheader);
        textView_studentsocieties = (TextView) findViewById(R.id.textView_studentsocieties);

        imv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        textView_calendersubheader.setText(Constants.headerText);

        if(Constants.message.equals("messages tab")){
            if(Constants.company_msg){
                MyEventsAdapter adapter = new MyEventsAdapter(MyEventsActivity.this, Constants.SocietyInvites);
                listView_calender.setAdapter(adapter);
            }else{
                MyEventsAdapter adapter = new MyEventsAdapter(MyEventsActivity.this, Constants.CompanyInvites);
                listView_calender.setAdapter(adapter);
            }

        }else{
            MyEventsAdapter adapter = new MyEventsAdapter(MyEventsActivity.this, Constants.CompanyInvites);
            listView_calender.setAdapter(adapter);
        }

        linearLayout_companySociety.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(textView_studentsocieties.getText().toString().equals(getString(R.string.student_societies))){
                    textView_studentsocieties.setText(getString(R.string.companies));
                    textView_calendersubheader.setText(getString(R.string.companies));

                    MyEventsAdapter adapter = new MyEventsAdapter(MyEventsActivity.this, Constants.CompanyInvites);
                    listView_calender.setAdapter(adapter);

                }else if(textView_studentsocieties.getText().toString().equals(getString(R.string.companies))){
                    textView_studentsocieties.setText(getString(R.string.student_societies));
                    textView_calendersubheader.setText(getString(R.string.student_societies));

                    MyEventsAdapter adapter = new MyEventsAdapter(MyEventsActivity.this, Constants.SocietyInvites);
                    listView_calender.setAdapter(adapter);

                }
            }
        });




    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_right);
        finish();
    }

}
