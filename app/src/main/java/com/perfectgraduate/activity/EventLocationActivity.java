package com.perfectgraduate.activity;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.perfectgraduate.R;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by user on 09-Jan-17.
 */

public class EventLocationActivity extends FragmentActivity implements OnMapReadyCallback {

    private EventLocationActivity activity;
    private GoogleMap mMap;
    private ImageView imageView_back;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eventlocation);
        init();
    }

    private void init() {

        activity = this;

        ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapFragment))
                .getMapAsync(activity);

        imageView_back = (ImageView) findViewById(R.id.imageView_back);

        imageView_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.clear();
        LatLng latLng = null;
        LatLngBounds.Builder bld = new LatLngBounds.Builder();
        double longitude = 0.0;
        double latitude = 0.0;

        Intent intent = getIntent();
        String location = intent.getStringExtra("location");

        Geocoder coder = new Geocoder(activity);
        try {
            ArrayList<Address> adresses = (ArrayList<Address>) coder.getFromLocationName(location, 50);
            for(Address add : adresses){

                longitude = add.getLongitude();
                latitude = add.getLatitude();

                Log.e("lat long = "," -- "+longitude+" "+latitude);

            }

            latLng = new LatLng(latitude, longitude);
            bld.include(latLng);
            mMap.addMarker(new MarkerOptions()
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_mappin))
                    .position(latLng).title(location).snippet(""));

           /* int width = getResources().getDisplayMetrics().widthPixels;
            int height = getResources().getDisplayMetrics().heightPixels;
            int padding = (int) (width * 0.10); // offset from edges of the map 12% of screen

            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, width, height, 10);
            mMap.moveCamera(cu);*/

            mMap.moveCamera(CameraUpdateFactory
                    .newLatLng(latLng));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(12),
                    1500, null);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_right);
        finish();
    }
}
