package com.perfectgraduate.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.callback.BitmapAjaxCallback;
import com.androidquery.callback.ImageOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.PlaceLikelihood;
import com.google.android.gms.location.places.PlaceLikelihoodBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.perfectgraduate.R;
import com.perfectgraduate.api.PerfectGraduateRequest;
import com.perfectgraduate.api.WebServiceAPI;
import com.perfectgraduate.app.PerfectGraduateApplication;
import com.perfectgraduate.database.DatabaseHelper;
import com.perfectgraduate.fragment.PortfolioFragment;
import com.perfectgraduate.fragment.SalaryGuideFragment;
import com.perfectgraduate.listener.ICallBack;
import com.perfectgraduate.utility.Constants;
import com.perfectgraduate.utility.SharedPrefrenceUtil;
import com.perfectgraduate.vo.AllAwardsVo;
import com.perfectgraduate.vo.ApplicationDueDateAllVo;
import com.perfectgraduate.vo.DisciplineVo;
import com.perfectgraduate.vo.DisciplinesVo;
import com.perfectgraduate.vo.ItemVo;
import com.perfectgraduate.vo.LinksVo;
import com.perfectgraduate.vo.LocationVo;
import com.perfectgraduate.vo.QuesAnsVo;
import com.perfectgraduate.vo.SalaryVo;
import com.perfectgraduate.vo.UserCompanyVo;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by user on 08-Dec-16.
 */

public class DetailActivity extends FragmentActivity implements OnMapReadyCallback{
       // GoogleApiClient.ConnectionCallbacks,
        //GoogleApiClient.OnConnectionFailedListener,
       // LocationListener {

    private static final String TAG = "Map Screen";
    private GoogleMap mMap;
    private CameraPosition mCameraPosition;

    // The entry point to Google Play services, used by the Places API and Fused Location Provider.
    private GoogleApiClient mGoogleApiClient;
    // A request object to store parameters for requests to the FusedLocationProviderApi.
    private LocationRequest mLocationRequest;
    // The desired interval for location updates. Inexact. Updates may be more or less frequent.
    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;
    // The fastest rate for active location updates. Exact. Updates will never be more frequent
    // than this value.
    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =
            UPDATE_INTERVAL_IN_MILLISECONDS / 2;

    // A default location (Sydney, Australia) and default zoom to use when location permission is
    // not granted.
    private final LatLng mDefaultLocation = new LatLng(-33.8523341, 151.2106085);
    private static final int DEFAULT_ZOOM = 15;
    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    private boolean mLocationPermissionGranted;

    // The geographical location where the device is currently located.
    private Location mCurrentLocation;

    // Keys for storing activity state.
    private static final String KEY_CAMERA_POSITION = "camera_position";
    private static final String KEY_LOCATION = "location";

    private ImageView imv_back,imageView_add_remove,imageView_profile,transparent_image_textView;
    private TextView textView_company_info,textView_fullname,textView_header,textView_area;
    private DatabaseHelper databaseHelper;
    private List<UserCompanyVo> userCompanyVoList;
    private List<String> local;
    private List<String> international;

    private LinearLayout linearLayout_awards,linearLayout_social,linearLayout_honour;
    private TextView textView_stop,textView_info,textView_questions,textView_target,textView_application;

    private List<ApplicationDueDateAllVo> dueDateAllVoList;
    private List<AllAwardsVo> allAwardsVoList;
    private List<DisciplinesVo> disciplinesVoList;
    private List<QuesAnsVo> quesAnsVoList;
    private List<LinksVo> linksVoList;
    private AQuery aQuery;
    private PerfectGraduateRequest request;

    private int msgStop = 0;
    private int port = 0;
    private String cid = "";

    private GridView grid;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        // Retrieve location and camera position from saved instance state.
        if (savedInstanceState != null) {
            mCurrentLocation = savedInstanceState.getParcelable(KEY_LOCATION);
            mCameraPosition = savedInstanceState.getParcelable(KEY_CAMERA_POSITION);
        }

        setContentView(R.layout.activity_detail);

        ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapFragment))
                .getMapAsync(this);

        request = new PerfectGraduateRequest(this);

        userCompanyVoList = new ArrayList<UserCompanyVo>();

        dueDateAllVoList = new ArrayList<ApplicationDueDateAllVo>();
        allAwardsVoList = new ArrayList<AllAwardsVo>();
        disciplinesVoList = new ArrayList<DisciplinesVo>();
        quesAnsVoList = new ArrayList<QuesAnsVo>();
        linksVoList = new ArrayList<LinksVo>();

        aQuery = new AQuery(this);

        databaseHelper = new DatabaseHelper(DetailActivity.this);

        local = new ArrayList<String>();
        international = new ArrayList<String>();

        textView_company_info = (TextView) findViewById(R.id.textView_company_info);
        textView_fullname = (TextView) findViewById(R.id.textView_fullname);
        textView_header = (TextView) findViewById(R.id.textView_header);
        textView_area = (TextView) findViewById(R.id.textView_area);
        textView_stop = (TextView) findViewById(R.id.textView_stop);
        textView_info = (TextView) findViewById(R.id.textView_info);
        textView_questions = (TextView) findViewById(R.id.textView_questions);
        textView_target = (TextView) findViewById(R.id.textView_target);
        textView_application = (TextView) findViewById(R.id.textView_application);
        linearLayout_awards = (LinearLayout) findViewById(R.id.linearLayout_awards);
        linearLayout_social = (LinearLayout) findViewById(R.id.linearLayout_social);
        linearLayout_honour = (LinearLayout) findViewById(R.id.linearLayout_honour);
        grid = (GridView) findViewById(R.id.grid);


        final ScrollView mainScrollView = (ScrollView) findViewById(R.id.main_scrollview);
        ImageView transparentImageView = (ImageView) findViewById(R.id.transparent_image);

        transparentImageView.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        mainScrollView.requestDisallowInterceptTouchEvent(true);
                        // Disable touch on transparent view
                        return false;

                    case MotionEvent.ACTION_UP:
                        // Allow ScrollView to intercept touch events.
                        mainScrollView.requestDisallowInterceptTouchEvent(false);
                        return true;

                    case MotionEvent.ACTION_MOVE:
                        mainScrollView.requestDisallowInterceptTouchEvent(true);
                        return false;

                    default:
                        return true;
                }
            }
        });

        imv_back = (ImageView) findViewById(R.id.imv_back);
        imageView_add_remove = (ImageView) findViewById(R.id.imageView_add_remove);
        imageView_profile = (ImageView) findViewById(R.id.imageView_profile);

        textView_company_info.setMovementMethod(new ScrollingMovementMethod());
        textView_company_info.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                textView_company_info.getParent().requestDisallowInterceptTouchEvent(true);

                return false;
            }
        });

        mainScrollView.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                textView_company_info.getParent().requestDisallowInterceptTouchEvent(false);

                return false;
            }
        });


        imageView_add_remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(port == 0){
                    addPort(cid);
                }else{
                    removePort(cid);
                }
            }
        });

        textView_stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(msgStop == 1){
                    PerfectGraduateApplication.getInstance().displayCustomDialogPostNeg(DetailActivity.this, "Continue receiving messages",
                            "Please confirm if you would like to continue \nreceiving messages from this \ncompany?", new ICallBack() {
                                @Override
                                public void onCallBackResult(String response) {
                                    stopReceiving(cid);
                                }
                            },"","");
                }else{
                    PerfectGraduateApplication.getInstance().displayCustomDialogPostNeg(DetailActivity.this, "Stop receiving messages",
                            "Please confirm if you would like to stop \nreceiving messages from this \ncompany?", new ICallBack() {
                                @Override
                                public void onCallBackResult(String response) {
                                    stopReceiving(cid);
                                }
                            },"","");

                }

                databaseHelper.updateMsgStatus(cid,String.valueOf(msgStop));
            }
        });

        textView_application.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Constants.dateAllVoList.addAll(dueDateAllVoList);
                Intent intent = new Intent(DetailActivity.this,ApplicationDueDateActivity.class);
                startActivity(intent);
            }
        });

        textView_target.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Constants.disciplinesVoList.addAll(disciplinesVoList);
                Intent intent = new Intent(DetailActivity.this,TargetDisciplinesActivity.class);
                startActivity(intent);
            }
        });

        linearLayout_awards.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Constants.allAwardsVoList.addAll(allAwardsVoList);
                Intent intent = new Intent(DetailActivity.this,AwardsActivity.class);
                startActivity(intent);
            }
        });

        textView_questions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Constants.quesAnsVoList.addAll(quesAnsVoList);
                Intent intent = new Intent(DetailActivity.this,FAQActivity.class);
                startActivity(intent);
            }
        });

        textView_area.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Dialog dialog = new Dialog(DetailActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
                dialog.setContentView(R.layout.dialog_area);
                dialog.setCancelable(false);
                Window window = dialog.getWindow();
                window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                window.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.CENTER);
                TextView dialogCancel = (TextView) dialog.findViewById(R.id.textView_close);
                LinearLayout linearLayout_local =  (LinearLayout) dialog.findViewById(R.id.linearLayout_local);
                LinearLayout linearLayout_local_detail =  (LinearLayout) dialog.findViewById(R.id.linearLayout_local_detail);
                LinearLayout linearLayout_international = (LinearLayout) dialog.findViewById(R.id.linearLayout_international);
                LinearLayout linearLayout_international_detail = (LinearLayout) dialog.findViewById(R.id.linearLayout_international_detail);

                if(local.size() == 0){

                    linearLayout_local_detail.setVisibility(View.GONE);

                }else{

                    linearLayout_local_detail.setVisibility(View.VISIBLE);

                    for(int i = 0; i < local.size(); i++) {

                        final TextView mTextView = new TextView(DetailActivity.this);

                        mTextView.setTextColor(getColor(R.color.black));
                        mTextView.setTextSize(14);

                        mTextView.setText(local.get(i));

                        linearLayout_local.addView(mTextView);
                    }
                }

                if(international.size() == 0){

                    linearLayout_international_detail.setVisibility(View.GONE);

                }else{

                    linearLayout_international_detail.setVisibility(View.VISIBLE);

                    for(int i = 0; i < international.size(); i++) {

                        final TextView mTextView = new TextView(DetailActivity.this);

                        mTextView.setTextColor(getColor(R.color.black));

                        mTextView.setTextSize(14);

                        mTextView.setText(international.get(i));

                        linearLayout_international.addView(mTextView);
                    }

                }

                dialogCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
        });

        imv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onMapReady(GoogleMap map) {
        mMap = map;
        mMap.clear();
        LatLng latLng = null;
        LatLngBounds.Builder bld = new LatLngBounds.Builder();

        String id = "";
        String status = "";
        String Message_status = "";
        String url = "";

        if(Constants.company_id.equals("")) {

            userCompanyVoList = databaseHelper.getAllCompanyDetail(PerfectGraduateApplication.getInstance().getUserCompanyVo().getId());

            for (int i = 0; i < userCompanyVoList.size(); i++){

                textView_header.setText(userCompanyVoList.get(i).getCompany_name());

                textView_fullname.setText(userCompanyVoList.get(i).getCompany_name());
                textView_company_info.setText(userCompanyVoList.get(i).getCompany_intro());
                id = userCompanyVoList.get(i).getId();

                local = Arrays.asList(userCompanyVoList.get(i).getRegstates().split("\\s*,\\s*"));
                international = Arrays.asList(userCompanyVoList.get(i).getInternational().split("\\s*,\\s*"));

                status = userCompanyVoList.get(i).getPortfolio_status();
                Message_status = userCompanyVoList.get(i).getMessage_status();

                cid = userCompanyVoList.get(i).getId();
                url = userCompanyVoList.get(i).getLogo();
            }

        }else{


            userCompanyVoList = databaseHelper.getAllCompanyDetail(Constants.company_id);

            for (int i = 0; i < userCompanyVoList.size(); i++){

                textView_header.setText(userCompanyVoList.get(i).getCompany_name());

                textView_fullname.setText(userCompanyVoList.get(i).getCompany_name());
                textView_company_info.setText(userCompanyVoList.get(i).getCompany_intro());
                id = userCompanyVoList.get(i).getId();

                local = Arrays.asList(userCompanyVoList.get(i).getRegstates().split("\\s*,\\s*"));
                international = Arrays.asList(userCompanyVoList.get(i).getInternational().split("\\s*,\\s*"));

                status = userCompanyVoList.get(i).getPortfolio_status();
                Message_status = userCompanyVoList.get(i).getMessage_status();

                cid = userCompanyVoList.get(i).getId();
                url = userCompanyVoList.get(i).getLogo();
            }

            Constants.company_id = "";

        }

        ImageOptions options = new ImageOptions();

        options.round = 10;

        options.memCache = true;

        options.fileCache = true;

        String imageUrl = "http://www.perfectgraduate.com/perfect/assets/uploads/"+url;

        if(!url.equals("")){
            aQuery.id(imageView_profile).image(imageUrl, options);
        }

        dueDateAllVoList = databaseHelper.getApplicationDueDates(id,false);

        disciplinesVoList = databaseHelper.getDisciplinesDetail(id,false);

        allAwardsVoList = databaseHelper.getAllAwards(id,false);

        quesAnsVoList = databaseHelper.getQuesAns(id,false);

        linksVoList = databaseHelper.getLinks(id,false);


        if(Message_status.equals("1")){
            msgStop = 1;
            textView_stop.setText("Messages are stopped");
            textView_stop.setTextColor(Color.RED);
        }else{
            msgStop = 0;
            textView_stop.setText("Stop receiving messages");
            textView_stop.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
        }

        Constants.headerText = textView_fullname.getText().toString();
        if(status.equals("1")){
            port = 1;
            imageView_add_remove.setImageResource(R.drawable.addport);
            imageView_add_remove.setBackground(getResources().getDrawable(R.drawable.port_add));
        }else{
            port = 0;
            imageView_add_remove.setImageResource(R.drawable.removeport);
            imageView_add_remove.setBackground(getResources().getDrawable(R.drawable.white_border));
        }

        if(dueDateAllVoList.size() == 0){
            textView_application.setVisibility(View.GONE);
        }else{
            textView_application.setVisibility(View.VISIBLE);
        }

        if(disciplinesVoList.size() == 0){
            textView_target.setVisibility(View.GONE);
        }else{
            textView_target.setVisibility(View.VISIBLE);
        }

        if(allAwardsVoList.size() == 0){
            linearLayout_awards.setVisibility(View.GONE);
        }else{
            linearLayout_awards.setVisibility(View.VISIBLE);
        }

        if(quesAnsVoList.size() == 0){
            textView_questions.setVisibility(View.GONE);
        }else{
            textView_questions.setVisibility(View.VISIBLE);
        }

        if(linksVoList.size() == 0){
            textView_info.setVisibility(View.GONE);
            linearLayout_social.setVisibility(View.GONE);
        }else{
            textView_info.setVisibility(View.VISIBLE);
            linearLayout_social.setVisibility(View.VISIBLE);
        }

        ArrayAdapter<LinksVo> arrayAdapter = new ArrayAdapter<LinksVo>(DetailActivity.this,R.layout.row_grid,
                linksVoList){

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                ViewHolder holder = null;
                if (convertView == null) {
                    convertView = getLayoutInflater().inflate(R.layout.row_grid, null);
                    holder = new ViewHolder();

                    holder.SingleView = (ImageView) convertView.findViewById(R.id.SingleView);

                    convertView.setTag(holder);
                } else {
                    holder = (ViewHolder) convertView.getTag();
                }
                final LinksVo linksVo = getItem(position);

                aQuery.id(holder.SingleView).image("http://www.perfectgraduate.com/perfect/assets/uploads/"+linksVo.getMedia_logo(), true, true, 0, 0, new BitmapAjaxCallback() {
                    @Override
                    protected void callback(String url, ImageView iv, Bitmap bm, AjaxStatus status) {

                        if (bm != null) {
                            iv.setImageBitmap(bm);
                        }
                    }
                });


                holder.SingleView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                       /* Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(linksVo.getMedia_link()));
                        startActivity(browserIntent);*/
                        Intent i = new Intent(Intent.ACTION_VIEW);

                        if(linksVo.getMedia_link().contains("http")){
                            i.setData(Uri.parse(linksVo.getMedia_link()));
                        }else{
                            i.setData(Uri.parse("http://"+linksVo.getMedia_link()));
                        }
                        startActivity(i);
                    }
                });

                return convertView;
            }
        };

        grid.setAdapter(arrayAdapter);

       /* if(linksVoList != null && linksVoList.size() > 0) {

            linearLayout_social.removeAllViews();

            for (int i = 0; i < linksVoList.size(); i++) {

                final ImageView imageView = new ImageView(DetailActivity.this);
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(200,200);
                if(i == 0) {
                    params.setMargins(0, 0, 0, 0);
                }else{
                    params.setMargins(8, 0, 0, 0);
                }
                imageView.setLayoutParams(params);
                imageView.setId(1000 + i);
                imageView.setTag(i);

                aQuery.id(imageView).image("http://www.perfectgraduate.com/perfect/assets/uploads/"+linksVoList.get(i).getMedia_logo(), true, true, 0, 0, new BitmapAjaxCallback() {
                    @Override
                    protected void callback(String url, ImageView iv, Bitmap bm, AjaxStatus status) {

                        if (bm != null) {
                            iv.setImageBitmap(bm);
                        }
                    }
                });

                linearLayout_social.addView(imageView);
            }
        }*/

        if(allAwardsVoList != null && allAwardsVoList.size() > 0) {

            linearLayout_honour.removeAllViews();

            for (int i = 0; i < allAwardsVoList.size(); i++) {

                final ImageView imageView = new ImageView(DetailActivity.this);
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(50,50);
                if(i == 0) {
                    params.setMargins(0, 0, 0, 0);
                }else{
                    params.setMargins(10, 0, 0, 0);
                }
                imageView.setLayoutParams(params);
                imageView.setId(1000 + i);
                imageView.setTag(i);

                if(allAwardsVoList.get(i).getAwards().equals("2")){
                    imageView.setImageResource(R.drawable.yellow_cup);
                }else if(allAwardsVoList.get(i).getAwards().equals("3")){
                    imageView.setImageResource(R.drawable.light_silver);
                }else if(allAwardsVoList.get(i).getAwards().equals("1")){
                    imageView.setImageResource(R.drawable.honour);
                }

                linearLayout_honour.addView(imageView);
            }
        }

        List<LocationVo> locationVoList = databaseHelper.getLocations(id);

        if (locationVoList != null && locationVoList.size() > 0) {

            for (int i = 0; i < locationVoList.size(); i++) {
                if (!(locationVoList.get(i).getLat().equalsIgnoreCase("") && locationVoList.get(i).getLongitude().equalsIgnoreCase(""))) {
                    latLng = new LatLng(Double.parseDouble(locationVoList.get(i).getLat()), Double.parseDouble(locationVoList.get(i).getLongitude()));
                    bld.include(latLng);
                    mMap.addMarker(new MarkerOptions()
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_mappin))
                            .position(latLng).title(locationVoList.get(i).getLocation()).snippet(""));
                }
            }

            mMap.moveCamera(CameraUpdateFactory
                    .newLatLng(latLng));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(8),
                    1500, null);
        }

    }

    static class ViewHolder{
        private ImageView SingleView;
    }

    private void addPort(final String cid) {

        Map<String, Object> map = new HashMap<String, Object>();

        databaseHelper.updatePortfolio(cid,"0");

        map.put("uid", SharedPrefrenceUtil.getPrefrence(this, Constants.USER_ID,""));
        map.put("cid",cid);

        request.getResponse(this, map, WebServiceAPI.API_addportfolio, true, new ICallBack() {
            @Override
            public void onCallBackResult(String response) {

                try {

                    JSONObject jsonObject = new JSONObject(response);

                    port = 1;
                        imageView_add_remove.setImageResource(R.drawable.portadd);
                        imageView_add_remove.setBackground(getResources().getDrawable(R.drawable.port_add));
                        PerfectGraduateApplication.getInstance().displayCustomDialog(DetailActivity.this,"Nice choice!","The company has been successfully added to your portfolio.");

                }catch(JSONException k){
                    k.printStackTrace();
                }

            }
        });

    }

    private void removePort(final String cid) {

        Map<String, Object> map = new HashMap<String, Object>();

        databaseHelper.updatePortfolio(cid,"1");

        map.put("uid", SharedPrefrenceUtil.getPrefrence(this, Constants.USER_ID,""));
        map.put("cid",cid);

        request.getResponse(this, map, WebServiceAPI.API_removeCompany, true, new ICallBack() {
            @Override
            public void onCallBackResult(String response) {

                try {

                    JSONObject jsonObject = new JSONObject(response);

                    port = 0;

                        imageView_add_remove.setImageResource(R.drawable.removeport);
                        imageView_add_remove.setBackground(getResources().getDrawable(R.drawable.white_border));
                        PerfectGraduateApplication.getInstance().displayCustomDialog(DetailActivity.this,"Let's remove it...","The company has now been removed from your portfolio.");

                }catch(JSONException k){
                    k.printStackTrace();
                }

            }
        });

    }

    private void stopReceiving(String cid) {

        Map<String, Object> map = new HashMap<String, Object>();

        map.put("user_id", SharedPrefrenceUtil.getPrefrence(DetailActivity.this, Constants.USER_ID,""));
        map.put("company_id",cid);

        request.getResponse(DetailActivity.this, map,WebServiceAPI.API_MessageStatus, true, new ICallBack() {
            @Override
            public void onCallBackResult(String response) {

                try {

                    JSONObject jsonObject = new JSONObject(response);

                    String data = jsonObject.getString("data");

                    if(msgStop == 0){
                        msgStop = 1;
                        textView_stop.setText("Messages are stopped");
                        textView_stop.setTextColor(Color.RED);
                    }else{
                        msgStop = 0;
                        textView_stop.setText("Stop receiving messages");
                        textView_stop.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    }


                }catch(JSONException k){
                    k.printStackTrace();
                }

            }
        });

    }

    private void createLocationRequest() {
        mLocationRequest = new LocationRequest();

        /*
         * Sets the desired interval for active location updates. This interval is
         * inexact. You may not receive updates at all if no location sources are available, or
         * you may receive them slower than requested. You may also receive updates faster than
         * requested if other applications are requesting location at a faster interval.
         */
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);

        /*
         * Sets the fastest rate for active location updates. This interval is exact, and your
         * application will never receive updates faster than this value.
         */
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);

        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    /**
     * Gets the current location of the device and starts the location update notifications.
     */
    private void getDeviceLocation() {
        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         * onRequestPermissionsResult.
         */
        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true;
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
        /*
         * Get the best and most recent location of the device, which may be null in rare
         * cases when a location is not available.
         * Also request regular updates about the device location.
         */
        /*if (mLocationPermissionGranted) {
            mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,
                    mLocationRequest, this);
        }*/
    }

    /**
     * Handles the result of the request for location permissions.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        mLocationPermissionGranted = false;
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mLocationPermissionGranted = true;
                }
            }
        }
       // updateLocationUI();
    }

}
