package com.perfectgraduate.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.perfectgraduate.R;
import com.perfectgraduate.api.PerfectGraduateRequest;
import com.perfectgraduate.api.WebServiceAPI;
import com.perfectgraduate.app.PerfectGraduateApplication;
import com.perfectgraduate.listener.ICallBack;
import com.perfectgraduate.utility.Constants;
import com.perfectgraduate.utility.SharedPrefrenceUtil;
import com.perfectgraduate.utility.customtextview.OpenSansBoldTextView;
import com.perfectgraduate.utility.customtextview.OpenSansRegularTextView;
import com.perfectgraduate.vo.DisciplineVo;
import com.perfectgraduate.vo.SocietyVo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class StudentSocietyActivity extends BaseActivity implements View.OnClickListener {
    int sequanceNumber=1;
    TextView ui_submit;
    ImageView ui_close;
    private PerfectGraduateRequest request;
    private StudentSocietyActivity activity;
    private List<SocietyVo> societyVoList;
    private List<String> list_society;
    private List<String> list_society_result=new ArrayList<>();
    String result="";
    private TextView student_university,textView_addMore;
    private LinearLayout linearLayout_addmore_society;
    private int pos =  5;
    private int society = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_society);

        loadLayout();

    }

    private void loadLayout() {

        activity = this;

        societyVoList = new ArrayList<>();
        list_society = new ArrayList<>();


        student_university = (TextView) findViewById(R.id.student_university);
        textView_addMore = (TextView) findViewById(R.id.textView_addMore);
        linearLayout_addmore_society = (LinearLayout) findViewById(R.id.linearLayout_addmore_society);

        request = new PerfectGraduateRequest(activity);


        ui_submit = (TextView) findViewById(R.id.tv_submit);
        ui_submit.setOnClickListener(activity);

        ui_close = (ImageView) findViewById(R.id.imv_back);
        ui_close.setOnClickListener(activity);

        //getLatLong();

        getAllStudentSocieties();

        student_university.setText(Constants.University_name);

        createNewTextView();
        createNewTextView();
        createNewTextView();
        createNewTextView();

        textView_addMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                createNewTextView();
                createNewTextView();
            }
        });

    }

    private void createNewTextView() {

        View viewToLoad = LayoutInflater.from(
                getApplicationContext()).inflate(
                R.layout.activity_student_society_include, null);

        LinearLayout society_include_lin_addmore_society= (LinearLayout) viewToLoad.findViewById(R.id.society_include_lin_addmore_society);
        final TextView society_include_txt_addmore_society= (TextView) viewToLoad.findViewById(R.id.society_include_txt_addmore_society);
        TextView society_include_txt_addmore_society_name= (TextView) viewToLoad.findViewById(R.id.society_include_txt_addmore_society_name);

        society_include_txt_addmore_society_name.setText(sequanceNumber+") Society");
        society_include_txt_addmore_society.setTag(sequanceNumber);

        society_include_lin_addmore_society.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displaySociety(society_include_txt_addmore_society);
            }
        });


        linearLayout_addmore_society.addView(viewToLoad);
        if(list_society.size()<sequanceNumber)
        {
            list_society_result.add("");
        }
        sequanceNumber++;
    }



    private void getAllStudentSocieties() {

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("uid", SharedPrefrenceUtil.getPrefrence(this,Constants.University_id,""));


        request.getResponse(activity, map, WebServiceAPI.API_getAllStudentSocieties, true, new ICallBack() {
            @Override
            public void onCallBackResult(String response) {

                try {

                    JSONObject jsonObject = new JSONObject(response);

                    String status = jsonObject.getString(WebServiceAPI.status);

                    if(Boolean.parseBoolean(status)){

                        JSONArray jsonArray = jsonObject.getJSONArray(WebServiceAPI.data);

                        if(jsonArray != null && jsonArray.length() > 0){

                            for (int i = 0; i < jsonArray.length(); i++) {

                                JSONObject object = jsonArray.getJSONObject(i);

                                SocietyVo societyVo = new SocietyVo();

                                societyVo.setId(object.getString("id"));
                                societyVo.setSociety_id(object.getString("society_id"));
                                societyVo.setParent_id(object.getString("parent_id"));
                                societyVo.setStaff_id(object.getString("staff_id"));
                                societyVo.setCreator_name(object.getString("creator_name"));
                                societyVo.setWebsite(object.getString("website"));
                                societyVo.setSociety_email(object.getString("society_email"));
                                societyVo.setPassword(object.getString("password"));
                                societyVo.setFirst_login_status(object.getString("first_login_status"));
                                societyVo.setChange_pass(object.getString("change_pass"));
                                societyVo.setPass(object.getString("pass"));
                                societyVo.setUni_id(object.getString("uni_id"));
                                societyVo.setCountry_id(object.getString("country_id"));
                                societyVo.setState_id(object.getString("state_id"));
                                societyVo.setSocietylogo(object.getString("societylogo"));
                                societyVo.setCreator_name(object.getString("creator_mobile"));
                                societyVo.setSociety_name(object.getString("society_name"));
                                societyVo.setSociety_president(object.getString("society_president"));
                                societyVo.setPresident_email(object.getString("president_email"));
                                societyVo.setPresident_mobile(object.getString("president_mobile"));
                                societyVo.setTreasurer_name(object.getString("Treasurer_name"));
                                societyVo.setTreasurer_email(object.getString("Treasurer_email"));
                                societyVo.setTreasure_mobile(object.getString("Treasure_mobile"));
                                societyVo.setSecetery_name(object.getString("Secetery_name"));
                                societyVo.setSecetery_email(object.getString("Secetery_email"));
                                societyVo.setSecetery_mobile(object.getString("Secetery_mobile"));
                                societyVo.setFacebookemail(object.getString("facebookemail"));
                                societyVo.setUser_type(object.getString("user_type"));
                                societyVo.setStatus(object.getString("status"));
                                societyVo.setLocks(object.getString("locks"));
                                societyVo.setDate(object.getString("date"));
                                societyVo.setKey(object.getString("key"));

                                societyVoList.add(societyVo);
                                list_society.add(object.getString("society_name"));
                                list_society_result.add("");
                            }
                        }

                    }else{

                    }

                    PerfectGraduateApplication.getInstance().displayCustomDialog(StudentSocietyActivity.this,getString(R.string.almost_there), getString(R.string.msg_2));

                }catch(JSONException k){
                    k.printStackTrace();
                }

            }
        });

    }

    private void displaySociety(final TextView textView) {

        if(list_society.size() > 0) {
            final CharSequence[] items;
            int selectedItem = 0;
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);

            builder.setTitle(getString(R.string.select_society));

            items = list_society.toArray(new CharSequence[list_society.size()]);

            if(textView.getText().toString().equals("")){
                selectedItem = -1;
            }else{
                selectedItem = society;
            }

            builder.setSingleChoiceItems(items,selectedItem, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item) {
                    // Do something with the selection
                    society = item;
                    int pos= (int) textView.getTag();
                    if(!list_society_result.contains(items[item]+"")) {
                        textView.setText(items[item]);
                        list_society_result.set(pos-1,items[item]+"");

                        for (int i = 0; i < societyVoList.size(); i++) {
                            if (societyVoList.get(i).getSociety_name().equals(items[item])) {
                                if(result.equalsIgnoreCase(""))
                                    result=societyVoList.get(i).getId()+"";
                                else
                                    result=result+","+societyVoList.get(i).getId()+"";
                            }
                        }

                        Log.e("result"," -- "+result);

                    }
                    else
                    {

                        Log.e("result","Please select another Society");
                    }

                    dialog.dismiss();
                }
            });
            AlertDialog alert = builder.create();
            alert.show();
        }

    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.tv_submit :
                gotoTerms();
                break;

            case R.id.imv_back :
                onBackPressed();
                break;

        }
    }

    private void gotoTerms() {

        Constants.Society_id = result.toString();

        /*for (int i = 0; i < list_society_result.size(); i++) {

            if(result.equalsIgnoreCase(""))
                result=list_society_result.get(i)+"";
            else
                result=result+","+list_society_result.get(i)+"";
        }
        SharedPrefrenceUtil.getPrefrence(activity,Constants.Society_id,result.toString());*/

        Intent intent = new Intent(this, TermActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_left);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_right);
        finish();
    }

}
