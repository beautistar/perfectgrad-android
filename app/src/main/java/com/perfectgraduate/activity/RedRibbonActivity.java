package com.perfectgraduate.activity;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.perfectgraduate.R;

/**
 * Created by user on 07-Dec-16.
 */

public class RedRibbonActivity extends AppCompatActivity{

    private ImageView imv_close,imageView_visible,imageView_visible2;
    private LinearLayout linearLayout_perfect,linearLayout_visible,linearLayout_how_to,linearLayout_visible2;
    private ScrollView scroll;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow(); // in Activity's onCreate() for instance
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }

        setContentView(R.layout.activity_info);

        getSupportActionBar().hide();

        scroll = (ScrollView) findViewById(R.id.scroll);

        imv_close = (ImageView) findViewById(R.id.imv_close);
        imageView_visible = (ImageView) findViewById(R.id.imageView_visible);
        imageView_visible2 = (ImageView) findViewById(R.id.imageView_visible2);

        linearLayout_perfect = (LinearLayout) findViewById(R.id.linearLayout_perfect);
        linearLayout_visible = (LinearLayout) findViewById(R.id.linearLayout_visible);
        linearLayout_how_to = (LinearLayout) findViewById(R.id.linearLayout_how_to);
        linearLayout_visible2 = (LinearLayout) findViewById(R.id.linearLayout_visible2);


        imv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        linearLayout_perfect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(linearLayout_visible.getVisibility() == View.VISIBLE){
                    linearLayout_visible.setVisibility(View.GONE);
                    imageView_visible.setImageDrawable(getResources().getDrawable(R.drawable.add_white));
                }else {
                    linearLayout_visible.setVisibility(View.VISIBLE);
                    imageView_visible.setImageDrawable(getResources().getDrawable(R.drawable.negative_white));
                }
            }
        });

        linearLayout_how_to.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(linearLayout_visible2.getVisibility() == View.VISIBLE){
                    linearLayout_visible2.setVisibility(View.GONE);
                    imageView_visible2.setImageDrawable(getResources().getDrawable(R.drawable.add_white));
                }else {
                    scroll.fullScroll(View.FOCUS_DOWN);
                    linearLayout_visible2.setVisibility(View.VISIBLE);
                    imageView_visible2.setImageDrawable(getResources().getDrawable(R.drawable.negative_white));
                }
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
