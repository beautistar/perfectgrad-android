package com.perfectgraduate.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.perfectgraduate.R;
import com.perfectgraduate.adapter.ApplicationExpandAdapter;
import com.perfectgraduate.adapter.TargetDisciplineAdapter;
import com.perfectgraduate.database.DatabaseHelper;
import com.perfectgraduate.utility.Constants;
import com.perfectgraduate.vo.DisciplineVo;
import com.perfectgraduate.vo.DisciplinesVo;
import com.perfectgraduate.vo.SubDisciplineVo;
import com.perfectgraduate.vo.Sub_disciplineVo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 17-Jan-17.
 */

public class TargetDisciplinesActivity extends AppCompatActivity{

    private TextView textView_header,textView_subHeader;
    private TargetDisciplinesActivity activity;
    private ImageView imv_back;
    private ExpandableListView expandableListView;
    private List<DisciplinesVo> disciplinesVoList;
    private DatabaseHelper databaseHelper;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_application);

        getSupportActionBar().hide();

        activity = this;
        textView_header = (TextView) findViewById(R.id.textView_header);
        textView_subHeader = (TextView) findViewById(R.id.textView_subHeader);
        imv_back = (ImageView) findViewById(R.id.imv_back);

        disciplinesVoList = new ArrayList<DisciplinesVo>();

        textView_subHeader.setText("Target Disciplines");

        databaseHelper = new DatabaseHelper(activity);

        textView_header.setText(Constants.headerText);

        expandableListView = (ExpandableListView) findViewById(R.id.expandableListView);

        expandableListView.setGroupIndicator(getResources().getDrawable(R.drawable.add_white));

        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {
                expandableListView.setGroupIndicator(getResources().getDrawable(R.drawable.negative_white));
            }
        });

        expandableListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {
                expandableListView.setGroupIndicator(getResources().getDrawable(R.drawable.add_white));
            }
        });



        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {

                return false;
            }
        });

        List<Sub_disciplineVo> VoList;

        for (int i = 0; i < Constants.disciplinesVoList.size(); i++){

            VoList = databaseHelper.getSubDisciplinesDetail(Constants.disciplinesVoList.get(i).getDiscipline_id());

            DisciplinesVo disciplineVo = new DisciplinesVo();

            disciplineVo.setCompany_id(Constants.disciplinesVoList.get(i).getCompany_id());
            disciplineVo.setDiscipline_id(Constants.disciplinesVoList.get(i).getDiscipline_id());
            disciplineVo.setName(Constants.disciplinesVoList.get(i).getName());
            disciplineVo.setSub_disciplineVoList(VoList);

            disciplinesVoList.add(disciplineVo);

        }

        TargetDisciplineAdapter expandableListAdapter = new TargetDisciplineAdapter(activity, disciplinesVoList);
        expandableListView.setAdapter(expandableListAdapter);
        expandableListView.setIndicatorBounds(0, 20);

        imv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Constants.disciplinesVoList.clear();
        finish();
    }

}
