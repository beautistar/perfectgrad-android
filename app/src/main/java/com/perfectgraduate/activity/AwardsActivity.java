package com.perfectgraduate.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.perfectgraduate.R;
import com.perfectgraduate.fragment.SalaryGuideFragment;
import com.perfectgraduate.utility.Constants;
import com.perfectgraduate.vo.AllAwardsVo;
import com.perfectgraduate.vo.SalaryVo;

/**
 * Created by user on 17-Jan-17.
 */

public class AwardsActivity extends AppCompatActivity{

    private TextView textView_header,textView_subHeader;
    private AwardsActivity activity;
    private ImageView imv_back;
    private ListView listView_awards;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_awards);

        getSupportActionBar().hide();

        activity = this;
        textView_header = (TextView) findViewById(R.id.textView_header);
        textView_subHeader = (TextView) findViewById(R.id.textView_subHeader);
        imv_back = (ImageView) findViewById(R.id.imv_back);
        listView_awards = (ListView) findViewById(R.id.listView_awards);

        textView_header.setText(Constants.headerText);

        ArrayAdapter<AllAwardsVo> arrayAdapter = new ArrayAdapter<AllAwardsVo>(activity,R.layout.row_awards,
                Constants.allAwardsVoList){

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                ViewHolder holder = null;
                if (convertView == null) {
                    convertView = getLayoutInflater().inflate(R.layout.row_awards, null);
                    holder = new ViewHolder();
                    holder.textView_award = (TextView) convertView.findViewById(R.id.textView_award);
                    holder.textView_year = (TextView) convertView.findViewById(R.id.textView_year);
                    holder.imageView_Award = (ImageView) convertView.findViewById(R.id.imageView_Award);
                    convertView.setTag(holder);
                } else {
                    holder = (ViewHolder) convertView.getTag();
                }
                AllAwardsVo allAwardsVo = getItem(position);

                switch (allAwardsVo.getAwards()) {
                    case "3":
                        holder.textView_award.setText("Silver Award");
                        holder.imageView_Award.setImageResource(R.drawable.light_silver);
                        break;
                    case "2":
                        holder.textView_award.setText("Gold Award");
                        holder.imageView_Award.setImageResource(R.drawable.yellow_cup);
                        break;
                    default:
                        holder.textView_award.setText("Honourable mention");
                        holder.imageView_Award.setImageResource(R.drawable.honour);
                        break;
                }

                holder.textView_year.setText(allAwardsVo.getDescription());

                return convertView;
            }
        };

        listView_awards.setAdapter(arrayAdapter);

        imv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Constants.allAwardsVoList.clear();
        finish();
    }

    static class ViewHolder{
        private TextView textView_award,textView_year;
        private ImageView imageView_Award;
    }

}
