package com.perfectgraduate.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.perfectgraduate.R;
import com.perfectgraduate.adapter.FAQAdapter;
import com.perfectgraduate.utility.Constants;
import com.perfectgraduate.vo.FAQVo;
import com.perfectgraduate.vo.QuesAnsVo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 17-Jan-17.
 */

public class FAQActivity extends AppCompatActivity{

    private TextView textView_header,textView_subHeader;
    private FAQActivity activity;
    private ImageView imv_back;
    ExpandableListView expandableListView;

    private List<FAQVo> faqVoList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_application);

        init();

    }

    private void init() {

        getSupportActionBar().hide();

        activity = this;
        textView_header = (TextView) findViewById(R.id.textView_header);
        textView_subHeader = (TextView) findViewById(R.id.textView_subHeader);
        imv_back = (ImageView) findViewById(R.id.imv_back);

        textView_header.setText(Constants.headerText);
        textView_subHeader.setText("Frequently Asked Questions");
        expandableListView = (ExpandableListView) findViewById(R.id.expandableListView);

        expandableListView.setGroupIndicator(getResources().getDrawable(R.drawable.add_white));

        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {
                expandableListView.setGroupIndicator(getResources().getDrawable(R.drawable.negative_white));
            }
        });

        expandableListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {
                expandableListView.setGroupIndicator(getResources().getDrawable(R.drawable.add_white));
            }
        });



        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {

                return false;
            }
        });

        faqVoList = new ArrayList<>();

        List<QuesAnsVo> quesAnsVoList;

        for (int i = 0; i < Constants.quesAnsVoList.size(); i++){

            FAQVo faqVo = new FAQVo();

            faqVo.setQuestion(Constants.quesAnsVoList.get(i).getQuestion());

            quesAnsVoList = new ArrayList<QuesAnsVo>();

            QuesAnsVo quesAnsVo = new QuesAnsVo();

            quesAnsVo.setId(Constants.quesAnsVoList.get(i).getId());
            quesAnsVo.setCompany_id(Constants.quesAnsVoList.get(i).getCompany_id());
            quesAnsVo.setAnswer(Constants.quesAnsVoList.get(i).getAnswer());

            quesAnsVoList.add(quesAnsVo);

            faqVo.setQuesAnsVoList(quesAnsVoList);

            faqVoList.add(faqVo);
        }
        FAQAdapter  faqAdapter = new FAQAdapter(activity,faqVoList);
        expandableListView.setAdapter(faqAdapter);
        expandableListView.setIndicatorBounds(0, 20);

        imv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Constants.quesAnsVoList.clear();
        finish();
    }
}
