package com.perfectgraduate.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import com.perfectgraduate.R;
import com.perfectgraduate.database.DatabaseHelper;

import java.io.IOException;


public class SplashActivity extends AppCompatActivity {

    private DatabaseHelper databaseHelper;
    private SplashActivity activity;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow(); // in Activity's onCreate() for instance
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }

        setContentView(R.layout.activity_splash);

        getSupportActionBar().hide();

        activity = this;

        databaseHelper = new DatabaseHelper(activity);

        try {
            if (!databaseHelper.checkDataBase()) {
                databaseHelper.createDataBase(0, "");
                databaseHelper.close();
            }
        } catch (IOException e1) {
            e1.printStackTrace();
        }


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                gotoLogin();
            }
        }, 2000);
    }

    private void gotoLogin () {

        Intent intent = new Intent(activity, LoginActivity.class);
        //Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_left);
        finish();
    }
}
