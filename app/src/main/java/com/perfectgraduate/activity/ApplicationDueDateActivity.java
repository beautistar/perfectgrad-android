package com.perfectgraduate.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.perfectgraduate.R;
import com.perfectgraduate.adapter.ApplicationExpandAdapter;
import com.perfectgraduate.adapter.ExpandableAdapter;
import com.perfectgraduate.utility.Constants;
import com.perfectgraduate.vo.ApplicationDueDateAllVo;
import com.perfectgraduate.vo.ApplicationVo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by user on 17-Jan-17.
 */

public class ApplicationDueDateActivity extends AppCompatActivity{

    private TextView textView_header;
    private ApplicationDueDateActivity activity;
    private ImageView imv_back;
    ExpandableListView expandableListView;

    private List<ApplicationVo> applicationVoList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_application);

        init();

    }

    private void init() {

        getSupportActionBar().hide();

        activity = this;
        textView_header = (TextView) findViewById(R.id.textView_header);
        imv_back = (ImageView) findViewById(R.id.imv_back);

        textView_header.setText(Constants.headerText);

        expandableListView = (ExpandableListView) findViewById(R.id.expandableListView);


        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {

            }
        });

        expandableListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {
            }
        });



        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {

                return false;
            }
        });

        applicationVoList =  new ArrayList<>();

        imv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

            for (int i = 0; i < Constants.dateAllVoList.size(); i++) {

                ApplicationVo applicationVo = new ApplicationVo();

                applicationVo.setProgram(Constants.dateAllVoList.get(i).getProgram());

                List<ApplicationDueDateAllVo> allVos = new ArrayList<>();

                ApplicationDueDateAllVo dueDateAllVo = new ApplicationDueDateAllVo();

                dueDateAllVo.setId(Constants.dateAllVoList.get(i).getId());
                dueDateAllVo.setCompany_id(Constants.dateAllVoList.get(i).getCompany_id());
                dueDateAllVo.setState_id(Constants.dateAllVoList.get(i).getState_id());
                dueDateAllVo.setProgram_id(Constants.dateAllVoList.get(i).getProgram_id());
                dueDateAllVo.setStart_date(Constants.dateAllVoList.get(i).getStart_date());
                dueDateAllVo.setClose_date(Constants.dateAllVoList.get(i).getClose_date());
                dueDateAllVo.setCreated(Constants.dateAllVoList.get(i).getCreated());
                dueDateAllVo.setState(Constants.dateAllVoList.get(i).getState());

                allVos.add(dueDateAllVo);

                applicationVo.setDateAllVoList(allVos);

                applicationVoList.add(applicationVo);
            }


        ApplicationExpandAdapter expandableListAdapter = new ApplicationExpandAdapter(activity, applicationVoList);
        expandableListView.setAdapter(expandableListAdapter);
        expandableListView.setIndicatorBounds(0, 20);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Constants.dateAllVoList.clear();
        finish();
    }



}
