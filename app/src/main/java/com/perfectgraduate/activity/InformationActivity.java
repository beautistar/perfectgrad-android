package com.perfectgraduate.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.support.v4.widget.TextViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.firebase.iid.FirebaseInstanceId;
import com.perfectgraduate.R;
import com.perfectgraduate.api.PerfectGraduateRequest;
import com.perfectgraduate.api.WebServiceAPI;
import com.perfectgraduate.database.DatabaseHelper;
import com.perfectgraduate.listener.ICallBack;
import com.perfectgraduate.utility.Constants;
import com.perfectgraduate.utility.SharedPrefrenceUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class InformationActivity extends BaseActivity implements View.OnClickListener {

    TextView ui_year, ui_tmp_login;
    ImageView ui_close;
    private CallbackManager mCallbackManager;
    SharedPreferences preferences;
    private PerfectGraduateRequest request;
    private DatabaseHelper databaseHelper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow(); // in Activity's onCreate() for instance
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }

        setContentView(R.layout.activity_information);

        loadLayout();

    }

    private void loadLayout() {

        getSupportActionBar().hide();

        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        preferences = getSharedPreferences("prefrence", Context.MODE_PRIVATE);

        request = new PerfectGraduateRequest(InformationActivity.this);
        databaseHelper = new DatabaseHelper(InformationActivity.this);

        if(year != 0) {

            ui_year = (TextView) findViewById(R.id.tv_year);
            ui_year.setText(getString(R.string.year) + " " + String.valueOf(year));
        }

        ((TextView) findViewById(R.id.tv_fb_login)).setOnClickListener(this);

        ui_tmp_login = (TextView) findViewById(R.id.tv_tmp_login);
        ui_tmp_login.setOnClickListener(this);

        ui_close = (ImageView) findViewById(R.id.imv_close);
        ui_close.setOnClickListener(this);

        FacebookSdk.sdkInitialize(this.getApplicationContext());

        mCallbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().registerCallback(mCallbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        if (AccessToken.getCurrentAccessToken() != null) {
                            RequestData();
                        }
                    }

                    @Override
                    public void onCancel() {
                    }

                    @Override
                    public void onError(FacebookException exception) {
                    }
                });

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.imv_close:
                onBackPressed();
                break;

            case R.id.tv_tmp_login :
                gotoTmpLogin();
                break;
            case R.id.tv_fb_login:
                LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email"));
                break;
        }
    }


    private void RequestData() {

        GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {

                        JSONObject json = response.getJSONObject();

                        try {
                            if (json != null) {

                                SharedPrefrenceUtil.setPrefrence(InformationActivity.this, Constants.USER_ID,json.getString("id"));
                                SharedPrefrenceUtil.setPrefrence(InformationActivity.this,Constants.FIRST_NAME,json.getString("first_name"));
                                SharedPrefrenceUtil.setPrefrence(InformationActivity.this,Constants.LAST_NAME,json.getString("last_name"));

                                if (json.has("email")) {
                                    SharedPrefrenceUtil.setPrefrence(InformationActivity.this,Constants.EMAIL_ID,json.getString("email"));
                                }
                                gotoTmpLoginStudentDetail();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,first_name,last_name,email");
        request.setParameters(parameters);
        request.executeAsync();

    }

    private void gotoTmpLoginStudentDetail() {

        PerfectGraduateRequest request = new PerfectGraduateRequest(this);

        Map<String, Object> map = new HashMap<String, Object>();

        map.put("fb_id",SharedPrefrenceUtil.getPrefrence(this,Constants.FB_ID,""));
        map.put("email",SharedPrefrenceUtil.getPrefrence(this,Constants.EMAIL_ID,""));
        map.put("Name",SharedPrefrenceUtil.getPrefrence(this,Constants.FIRST_NAME,"")+" "+SharedPrefrenceUtil.getPrefrence(this,Constants.LAST_NAME,""));
        map.put("device", FirebaseInstanceId.getInstance().getToken());
        map.put("device type", "android");

        request.getResponse(this, map, WebServiceAPI.API_register_fb, true, new ICallBack() {
            @Override
            public void onCallBackResult(String response) {
                try{

                    JSONObject jsonObject = new JSONObject(response);

                    String status = jsonObject.getString(WebServiceAPI.status);

                    SharedPrefrenceUtil.setPrefrence(InformationActivity.this,Constants.USER_ID,jsonObject.getString("user_id"));

                    if(jsonObject.getString("message").equals("succesfully register")){

                        gotoTmpLogin();
                    }else if(jsonObject.getString("message").equals("User Already Registered")){
                        if (jsonObject.getString("check").equals("no")){
                            gotoTmpLogin();
                        }else{
                            if(SharedPrefrenceUtil.getPrefrence(InformationActivity.this,Constants.isAllCompany,"").equals("Database")){

                                SharedPreferences.Editor editor = preferences.edit();
                                editor.putBoolean("login", true);
                                editor.apply();

                                Intent stdDetailIntent = new Intent(InformationActivity.this, HomeActivity.class);
                                stdDetailIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(stdDetailIntent);
                                overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_left);
                                finish();

                            }else{
                                getAllcompany();
                            }
                        }
                    }

                }catch (JSONException e){
                    e.printStackTrace();
                }
            }
        });


    }

    private void getAllcompany() {

        Map<String, Object> map = new HashMap<String, Object>();

        map.put("id",SharedPrefrenceUtil.getPrefrence(InformationActivity.this,Constants.USER_ID,""));


        request.getResponse(InformationActivity.this, map, WebServiceAPI.API_getAllCompanies, false, new ICallBack() {
            @Override
            public void onCallBackResult(String response) {

                try {

                    JSONObject jsonObject = new JSONObject(response);

                    String status = jsonObject.getString(WebServiceAPI.status);

                    if(Boolean.parseBoolean(status)){

                        JSONArray jsonArray = jsonObject.getJSONArray(WebServiceAPI.data);


                        if(jsonArray != null && jsonArray.length() > 0){

                            databaseHelper.insertAllCompanies(InformationActivity.this, jsonArray);

                        }
                    }

                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putBoolean("login", true);
                    editor.apply();

                    Intent stdDetailIntent = new Intent(InformationActivity.this, HomeActivity.class);
                    stdDetailIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(stdDetailIntent);
                    overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_left);
                    finish();

                }catch(JSONException k){
                    k.printStackTrace();
                }

            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(mCallbackManager.onActivityResult(requestCode, resultCode, data)) {
            return;
        }
    }

    private void gotoTmpLogin() {

        Intent loginIntent = new Intent(this, StudentDetailActivity.class);
        startActivity(loginIntent);
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_left);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
